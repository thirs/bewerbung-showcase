package hr.eonideas.ecemetery.commons.busevents;

/**
 * Created by Teo on 30.1.2016..
 */
public class CRUEvent {
    public enum EditableAction{
        EDIT,
        SAVE,
        REJECT
    }
    public final EditableAction action;

    public CRUEvent(EditableAction action) {
        this.action = action;
    }
}
