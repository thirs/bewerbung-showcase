package hr.eonideas.ecemetery.commons.busevents;

/**
 * Created by Teo on 30.1.2016..
 */
public class DrawerOpenEvent {
    public final boolean message;

    public DrawerOpenEvent(boolean message) {
        this.message = message;
    }
}
