package hr.eonideas.ecemetery.commons.interfaces;

import android.content.SharedPreferences;

import com.nostra13.universalimageloader.core.ImageLoader;

import hr.eonideas.ecemetery.parkovi.rest.model.Account;

/**
 * Created by Teo on 6.1.2016..
 */
public interface IApplicationProxy {
    public static String DEFAULT_SHARED_PREFERENCES_FNAME = "defaultsharedpref";

    public Account getUserAccount();
    public boolean isLoggedIn();
    public void setLoggedIn(Account account);
    public boolean isRegistered();
    public SharedPreferences getSharedPreferences();
    public void distroyAccountPreferences();
    public void clearImageLoaderCache();
    public void clearImageLoaderCache(String Uri);
    public void setAppLanguage(String languageLocal);
    public String getAppLanguage();
}
