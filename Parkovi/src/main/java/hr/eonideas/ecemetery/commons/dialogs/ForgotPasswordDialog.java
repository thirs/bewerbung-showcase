package hr.eonideas.ecemetery.commons.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.EditText;

import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by thirs on 2.4.2016..
 */
public class ForgotPasswordDialog extends DialogFragment {
    public static String TAG = "ForgotPasswordDialog";
    private IOnEmailSubmition mListener;
    private Context context;
    private EditText email;

    public static ForgotPasswordDialog newInstance() {
        Bundle args = new Bundle();
        ForgotPasswordDialog notificationDialog = new ForgotPasswordDialog();
        notificationDialog.setArguments(args);
        return notificationDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getTargetFragment() instanceof IOnEmailSubmition){
            context = getTargetFragment().getContext();
            mListener = (IOnEmailSubmition) getTargetFragment();
        }else if(getParentFragment() instanceof IOnEmailSubmition){
            context = getParentFragment().getContext();
            mListener = (IOnEmailSubmition) getParentFragment();
        }else if(getActivity() instanceof IOnEmailSubmition){
            context = getActivity();
            mListener = (IOnEmailSubmition) getActivity();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState){
        // Get the layout inflater
        final LayoutInflater inflater = getActivity().getLayoutInflater();
        final View v = inflater.inflate(R.layout.forgot_password_dialog, null);
        email = (EditText) v.findViewById(R.id.email);
        return new AlertDialog.Builder(getActivity())
                .setView(v)
                .setTitle(context.getResources().getString(R.string.change_password_dialog_title))
                .setPositiveButton(context.getResources().getString(R.string.change_password_dialog_submit), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if(mListener != null){
                            mListener.onEmailSubmition(email.getText().toString());
                        }
                    }
                })
                .create();
    }

    public interface IOnEmailSubmition {
        public void onEmailSubmition(String email);
    }
}
