package hr.eonideas.ecemetery.commons.busevents;

import hr.eonideas.ecemetery.parkovi.rest.model.Account;

/**
 * Created by Teo on 30.1.2016..
 */
public class AccountEvent {

    public final Account account;

    public AccountEvent(Account account) {
        this.account = account;
    }
}
