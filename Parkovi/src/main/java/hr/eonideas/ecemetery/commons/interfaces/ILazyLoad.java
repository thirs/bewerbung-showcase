package hr.eonideas.ecemetery.commons.interfaces;

/**
 * Created by thirs on 30.3.2016..
 */
public interface ILazyLoad{
    public void updateLazyList(Integer offset);
}
