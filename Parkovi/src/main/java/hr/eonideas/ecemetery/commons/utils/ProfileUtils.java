package hr.eonideas.ecemetery.commons.utils;

import android.content.Context;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IMemory;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;

/**
 * Created by thirs on 25.11.2015..
 */
public class ProfileUtils {

    public static String getProfileName(IProfile profile){
        String profileName =  profile.getFirstName() + " " + profile.getLastName();
        return profileName;
    }

    public static String getLifePeriod(IProfile profile, Context context ) {
        StringBuilder lifePeriod = new StringBuilder();
        lifePeriod.append(profile.getBirth() != null ? profile.getBirth() : context.getResources().getString(R.string.date_unknown));
        lifePeriod.append(" " +context.getResources().getString(R.string.until) + " ");
        if(profile.getDeath() != null){
            lifePeriod.append(profile.getDeath());
        }else{
            lifePeriod.append(profile.getBurial() != null ? profile.getBurial() : context.getResources().getString(R.string.date_unknown));
        }

        return lifePeriod.toString();
    }

    public static List<IProfile> getFavoriteProfilesFromAccount(Account account){
        List<IProfile> iProfileList = new ArrayList<IProfile>();
        List<Profile> profileList = account.getFavoriteProfiles();
        for(Profile p : profileList){
            iProfileList.add(p);
        }
        return iProfileList;
    }

    public static boolean isProfileInAccountFavorites(Account account,IProfile profile){
        boolean isFavorite = false;
        List<Profile> profileList = account.getFavoriteProfiles();
        for(IProfile p : profileList){
            if(p.getId() == profile.getId())
                isFavorite = true;
        }
        return isFavorite;
    }

    public static boolean isAccountProfilesOwner(Account account, IProfile profile){
        boolean isOwner = false;
        List<Profile> profileList = account.getOwnedProfiles();
        for(IProfile p : profileList){
            if(p.getId() == profile.getId())
                isOwner = true;
        }
        return isOwner;
    }

    public static int getProfileListPosition(IProfile profile, List<Profile> profileList){
        int position = -1;
        if(profileList != null && profile != null){
            for(int i = 0; i < profileList.size(); i++){
                if(profileList.get(i).getId() == profile.getId()){
                    position = i;
                    break;
                }
            }
        }
        return position;
    }



    public static int getProfileListPositionFromMemoryList(IProfile profile, List<IMemory> memoryList){
        int position = -1;
        if(memoryList != null && profile != null){
            for(int i = 0; i < memoryList.size(); i++){
                if(memoryList.get(i).getProfile().getId() == profile.getId()){
                    position = i;
                    break;
                }
            }
        }
        return position;
    }

    public static ArrayList<Image> getImageListFromProfile(Profile profile){
        ArrayList<Image> albumImages = new ArrayList<Image>();
        if(profile != null){
            Iterator iterator = profile.getProfileGallery().iterator();
            while(iterator.hasNext()){
                GalleryItem galleryItem = (GalleryItem)iterator.next();
                albumImages.add(galleryItem.getImage());
            }
        }
        return albumImages;
    }
}
