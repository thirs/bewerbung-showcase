package hr.eonideas.ecemetery.commons.utils;

import android.app.Activity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.interfaces.IAccount;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;

/**
 * Created by thirs on 2.12.2015..
 */
public class Utils {

    public static String getTime(String timeStamp){
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormat = new SimpleDateFormat("HH:mm");
        return outputFormat.format(date);
    }

    public static String getDate(String timeStamp){
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormat = new SimpleDateFormat("d MMM yyyy");
        return outputFormat.format(date);
    }

    public static Date getCurrentDate() {
        Calendar calendar = Calendar.getInstance();
        return calendar.getTime();
    }

    public static long getHoursBetweenDates(Date higherDate, Date lowerDate){
        long difference = higherDate.getTime() - lowerDate.getTime();
        long hoursBetweenDates = difference / (1000 * 60 * 60);
        return hoursBetweenDates;
    }

    public static Long getCleanedTimeString(String timeStamp){
        Long cleanedTimeString;
        DateFormat inputFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        Date date = null;
        try {
            date = inputFormat.parse(timeStamp);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        DateFormat outputFormat = new SimpleDateFormat("yyyyMMddHHmm");
        String s = outputFormat.format(date);
        try {
            cleanedTimeString = Long.parseLong(s);
        }catch (NumberFormatException e){
            cleanedTimeString = -1l;
        }
        return cleanedTimeString;
    }

    public static String getAccountName(IAccount account) {
        String accountName =  account.getFirstName() + " " + account.getLastName();
        return accountName;
    }

    public static CharSequence[] arrayListToArray(ArrayList<String> al){
        CharSequence[] cs = null;
        if(al != null && al.size() > 0){
            cs = new CharSequence[al.size()];
            for(int i = 0; i < al.size(); i++){
                cs[i] = al.get(i);
            }
        }
        return cs;
    }

    public static void hideKeyboard(Activity activity) {
        InputMethodManager imm = (InputMethodManager) activity.getSystemService(Activity.INPUT_METHOD_SERVICE);
        //Find the currently focused view, so we can grab the correct window token from it.
        View view = activity.getCurrentFocus();
        //If no view currently has focus, create a new one, just so we can grab a window token from it
        if (view == null) {
            view = new View(activity);
        }
        imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
    }

    public static List<Image> filterOutImages(List<GalleryItem> galleryItemList){
        List<Image> galleryImages = new ArrayList<Image>();
        for(GalleryItem galleryItem : galleryItemList){
            galleryImages.add(galleryItem.getImage());
        }
        return  galleryImages;
    }
}
