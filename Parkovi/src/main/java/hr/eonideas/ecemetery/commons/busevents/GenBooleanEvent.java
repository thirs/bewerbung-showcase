package hr.eonideas.ecemetery.commons.busevents;

/**
 * Created by Teo on 30.1.2016..
 */
public class GenBooleanEvent {
    public final boolean message;

    public GenBooleanEvent(boolean message) {
        this.message = message;
    }
}
