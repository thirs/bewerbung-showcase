package hr.eonideas.ecemetery.commons.utils;

import android.content.Context;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import hr.eonideas.ecemetery.parkovi.rest.model.ProfileLocation;

/**
 * Created by thirs on 8.11.2015..
 */
public class MapUtils {

    public static boolean checkGooglePlayServices(Context context){
        switch (GooglePlayServicesUtil.isGooglePlayServicesAvailable(context)) {
            case ConnectionResult.SUCCESS:
                return true;
            case ConnectionResult.SERVICE_MISSING:
                Toast.makeText(context, "SERVICE MISSING", Toast.LENGTH_SHORT).show();
                return false;
            case ConnectionResult.SERVICE_VERSION_UPDATE_REQUIRED:
                Toast.makeText(context, "UPDATE REQUIRED", Toast.LENGTH_SHORT).show();
                return false;
            default:
                return true;
                //Toast.makeText(getActivity(),GooglePlayServicesUtil.isGooglePlayServicesAvailable(getActivity()),Toast.LENGTH_SHORT).show();
        }
    }

    public static List<Marker> selectLowDistanceMarkers(List<Marker> markers, int maxDistanceMeters) {

        List<Marker> acceptedMarkers = new ArrayList<Marker>();

        if (markers == null) return acceptedMarkers;

        Map<Marker, Float> longestDist = new HashMap<Marker, Float>();

        for (Marker marker1 : markers) {

            // in this for loop we remember the max distance for each marker
            // think of a map with a flight company's routes from an airport
            // these lines is drawn for each airport
            // marker1 being the airport and marker2 destinations

            for (Marker marker2 : markers) {
                if (!marker1.equals(marker2)) {
                    float distance = distBetween(marker1.getPosition(), marker2.getPosition());
                    if (longestDist.containsKey(marker1)) {
                        // possible we have a longer distance
                        if (distance > longestDist.get(marker1))
                            longestDist.put(marker1, distance);
                    } else {
                        // first distance
                        longestDist.put(marker1, distance);
                    }
                }
            }
        }


        // examine the distances collected
        for (Marker marker: longestDist.keySet()) {
            if (longestDist.get(marker) <= maxDistanceMeters) acceptedMarkers.add(marker);
        }

        return acceptedMarkers;
    }


    public static float distBetween(LatLng pos1, LatLng pos2) {
        return distBetween(pos1.latitude, pos1.longitude, pos2.latitude, pos2.longitude);
    }

    public  static float distBetween(ProfileLocation loc1, ProfileLocation loc2){
        return distBetween(loc1.getLat().doubleValue(), loc1.getLon().doubleValue(), loc2.getLat().doubleValue(), loc2.getLon().doubleValue());
    }

    /** distance in meters **/
    public static float distBetween(double lat1, double lng1, double lat2, double lng2) {
        double earthRadius = 3958.75;
        double dLat = Math.toRadians(lat2 - lat1);
        double dLng = Math.toRadians(lng2 - lng1);
        double a = Math.sin(dLat / 2) * Math.sin(dLat / 2)
                + Math.cos(Math.toRadians(lat1))
                * Math.cos(Math.toRadians(lat2)) * Math.sin(dLng / 2)
                * Math.sin(dLng / 2);
        double c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
        double dist = earthRadius * c;

        int meterConversion = 1609;

        return (float) (dist * meterConversion);
    }

    public static float distanceBetweenInKm(double lat1, double lng1, double lat2, double lng2){
        float distanceInMeters = distBetween(lat1, lng1, lat2, lng2)/1000;
        BigDecimal bd = new BigDecimal(Float.toString(distanceInMeters));
        bd = bd.setScale(2, BigDecimal.ROUND_HALF_UP);
        return bd.floatValue();

    }



}
