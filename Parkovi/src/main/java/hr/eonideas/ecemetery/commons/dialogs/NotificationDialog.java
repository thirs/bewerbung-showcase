package hr.eonideas.ecemetery.commons.dialogs;

import android.app.Dialog;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;

import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by thirs on 2.4.2016..
 */
public class NotificationDialog extends DialogFragment {
    protected static final String TITLE = "title";
    protected static final String BUTTON_LABEL = "buttonlabel";
    protected static final String BUTTON_MESSAGE = "buttonmessage";
    private String dialogTitle;
    private String dialogButtonLabel;
    private String dialogMessage;
    private IOnNotificationConfirmed mListener;

    public static NotificationDialog newInstance(String title, String buttonLabel, String buttonMessage) {
        Bundle args = new Bundle();
        NotificationDialog notificationDialog = new NotificationDialog();
        if(title != null){
            args.putString(TITLE, title);
        }
        if(buttonLabel != null){
            args.putString(BUTTON_LABEL, buttonLabel);
        }
        if(buttonMessage != null){
            args.putString(BUTTON_MESSAGE, buttonMessage);
        }
        notificationDialog.setArguments(args);
        return notificationDialog;
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(getTargetFragment() instanceof IOnNotificationConfirmed){
            mListener = (IOnNotificationConfirmed) getTargetFragment();
        }
        if(getActivity() instanceof IOnNotificationConfirmed){
            mListener = (IOnNotificationConfirmed) getActivity();
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        this.dialogTitle = getArguments().getString(TITLE, getResources().getString(R.string.warning));
        this.dialogButtonLabel = getArguments().getString(BUTTON_LABEL, getResources().getString(R.string.ok));
        this.dialogMessage = getArguments().getString(BUTTON_MESSAGE, getDialogMessage());
        return new AlertDialog.Builder(getActivity())
                .setTitle(dialogTitle)
                .setMessage(dialogMessage)
                .setPositiveButton(dialogButtonLabel, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        if (mListener != null) {
                            mListener.notificationConfirmed();
                        }
                    }
                })
                .create();
    }

    protected String getDialogMessage() {
        String message = "";
        return message;
    }

    public interface IOnNotificationConfirmed {
        public void notificationConfirmed();
    }
}
