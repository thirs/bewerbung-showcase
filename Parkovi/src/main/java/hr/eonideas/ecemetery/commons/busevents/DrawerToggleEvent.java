package hr.eonideas.ecemetery.commons.busevents;

/**
 * Created by Teo on 30.1.2016..
 */
public class DrawerToggleEvent {
    public final boolean message;

    public DrawerToggleEvent(boolean message) {
        this.message = message;
    }
}
