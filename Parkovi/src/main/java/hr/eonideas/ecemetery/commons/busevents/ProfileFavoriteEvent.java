package hr.eonideas.ecemetery.commons.busevents;

import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;

/**
 * Created by Teo on 30.1.2016..
 */
public class ProfileFavoriteEvent {
    public final boolean message;
    public final IProfile profile;

    public ProfileFavoriteEvent(boolean message, IProfile profile) {
        this.message = message;
        this.profile = profile;
    }
}
