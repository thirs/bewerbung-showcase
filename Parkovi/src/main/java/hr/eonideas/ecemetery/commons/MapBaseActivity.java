package hr.eonideas.ecemetery.commons;

import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.util.Date;
import java.util.HashMap;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.utils.MapUtils;
import hr.eonideas.ecemetery.parkovi.rest.model.Poi;

/**
 * Created by thirs on 25.10.2015..
 */
public abstract class MapBaseActivity extends BaseActivity implements OnMapReadyCallback, GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, LocationListener {

    private static final String TAG = "MapBaseActivity";
    private static final int FASTEST_LOCATION_REQUEST_INTERVAL = 5000;
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 3;


    protected Location currentLocation;
    protected GoogleMap mMap;
    private GoogleApiClient mGoogleApiClient;
    private String mLastUpdateTime;
    private boolean mRequestingLocationUpdates;
    private LocationRequest mLocationRequest;
    private boolean currentLocationPointed = false;
    protected HashMap<Marker, Poi> poiMarkerMap;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        mGoogleApiClient = new GoogleApiClient.Builder(this)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .addApi(LocationServices.API)
                .build();
        createLocationRequest();
    }

    @Override
    public void onStart(){
        super.onStart();
        mGoogleApiClient.connect();
    }

    @Override
    public void onResume() {
        super.onResume();
        requireLocationUpdates();
    }

    abstract protected int enableLocationUpdates();

    protected void initializeMapSection(SupportMapFragment mapFragment){
        if (mapFragment != null) {
            mapFragment.getMapAsync(this);
        }
    }

    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ConnectionResult.API_UNAVAILABLE == MapsInitializer.initialize(this)) {
            Log.e("Address Map", "Could not initialize google play");
            mapLoadingError();
            return;
        }
        if (!MapUtils.checkGooglePlayServices(this)) {
            Log.e("Address Map", "Could not initialize google play");
            mapLoadingError();
            return;
        }
        setUpMap();
        mapLoadingSuccess();
    }

    abstract protected void mapLoadingError();

    abstract protected void mapLoadingSuccess();


    protected void createLocationRequest() {
        int locationRequestInterval = (enableLocationUpdates() > 0 && enableLocationUpdates() < FASTEST_LOCATION_REQUEST_INTERVAL) ? FASTEST_LOCATION_REQUEST_INTERVAL : enableLocationUpdates();
        mLocationRequest = new LocationRequest();
        mLocationRequest.setInterval(locationRequestInterval);
        mLocationRequest.setFastestInterval(FASTEST_LOCATION_REQUEST_INTERVAL);
        mLocationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }


    protected boolean requireLocationUpdates() {
        if(Filter.isDebuggingMode)
            Log.d(TAG,"Starting location updates");
        //Location updates is possible to start only when GoogleApiClient exists and LocationUpdates are enabled by integrator.
        //There is additional check which checks is LocationUpdates are already requested - in that case updates wont be requested
        if (mGoogleApiClient.isConnected() && (enableLocationUpdates()>0) && !mRequestingLocationUpdates) {
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION);
            if (permissionCheck == PackageManager.PERMISSION_GRANTED) {
                //Permissions for location updates granted
                startLocationUpdates();
            }else{
                if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Permissions for location updates already rejected
                    Toast.makeText(this,"Reinstall you app. Additional description inside dedicated dialog should be shown",Toast.LENGTH_LONG).show();
                } else {
                    //Asking for the location updates permissions for the first time
                    ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                }
            }
        }
        return mRequestingLocationUpdates;
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case MY_PERMISSIONS_REQUEST_FINE_LOCATION: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    startLocationUpdates();
                } else {
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            // other 'case' lines to check for other
            // permissions this app might request
        }
    }

    protected void startLocationUpdates(){
        mRequestingLocationUpdates = true;
        LocationServices.FusedLocationApi.requestLocationUpdates(mGoogleApiClient, mLocationRequest, this);
        mMap.setMyLocationEnabled(true);
    }

    @Override
    public void onConnected(Bundle bundle) {
        //GoogleApiClient connected
        currentLocation = LocationServices.FusedLocationApi.getLastLocation(mGoogleApiClient);
        requireLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {}

    @Override
    public void onConnectionFailed(ConnectionResult connectionResult) {}


    @Override
    public void onLocationChanged(Location location) {
        //Location updates callbacks callbacks
        currentLocation = location;
        mLastUpdateTime = DateFormat.getTimeInstance().format(new Date());
        if(currentLocation != null && !currentLocationPointed){
            animateCameraToPosition(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
            currentLocationPointed = true;
        }

    }

    @Override
    protected void onPause() {
        super.onPause();
        if(mRequestingLocationUpdates)
            stopLocationUpdates();
    }

    protected void stopLocationUpdates() {
        if(Filter.isDebuggingMode)
            Log.d(TAG,"Stopping location updates");
        mMap.setMyLocationEnabled(false);
        if (mGoogleApiClient.isConnected()) {
            LocationServices.FusedLocationApi.removeLocationUpdates(mGoogleApiClient, this);
            mRequestingLocationUpdates = false;
            mGoogleApiClient.disconnect();
        }
    }

    protected void showHomeLocation(){
        LatLng homeCoordinates = new LatLng(Double.valueOf(Filter.map_default_latitude), Double.valueOf(Filter.map_default_longitude));
        animateCameraToPosition(homeCoordinates);
    }

    protected boolean isGPSEnabled(){
        LocationManager locationManager = (LocationManager) getSystemService(Context.LOCATION_SERVICE);
        if (locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)){
            return true;
        }else{
            return false;
        }
    }

    //metode za manipulaciju mapom
    protected void setUpMap() {
        mMap.setMyLocationEnabled(false);
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(false);
        mMap.getUiSettings().setRotateGesturesEnabled(true);
        mMap.getUiSettings().setTiltGesturesEnabled(true);
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setZoomGesturesEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    protected void animateCameraToPosition(LatLng latLng) {
        CameraPosition position = new CameraPosition.Builder().target(latLng)
                .zoom(15.5f)
                .bearing(0)
                .tilt(25)
                .build();

        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    protected void showPois(List<Poi> pois2Show, String snippet, int pinResourceId) {
        showPois(pois2Show, snippet, pinResourceId, true);
    }

    protected void updatePois(List<Poi> pois2Show, String snippet, int pinResourceId) {
        showPois(pois2Show, snippet, pinResourceId, false);
    }

    protected void showPois(List<Poi> pois2Show, String snippet, int pinResourceId, boolean clearMarkers) {
        if(clearMarkers){
            poiMarkerMap = new HashMap<Marker, Poi>();
            mMap.clear();
        }

        for (Poi poi : pois2Show) {
            if(poi.getLat() == null || poi.getLon() ==  null){
                continue;
            }
            Marker m = mMap.addMarker(new MarkerOptions()
                    .position(new LatLng(poi.getLat().doubleValue(), poi.getLon().doubleValue()))
                    .title(poi.getProfile().getFirstName() + " " + poi.getProfile().getLastName())
                    .snippet(snippet)
                    .icon(BitmapDescriptorFactory.fromResource(pinResourceId)));
            if(poiMarkerMap != null)
                poiMarkerMap.put(m,poi);
        }
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        stopLocationUpdates();
    }
}
