package hr.eonideas.ecemetery.commons;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import java.util.Locale;

import hr.eonideas.ecemetery.commons.interfaces.IApplicationProxy;

/**
 * Created by thirs on 25.10.2015..
 */
public class ChangeLangActivity extends AppCompatActivity {
    private static String TAG = "ChangeLangActivity";

    protected IApplicationProxy app;
    private Configuration config;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        app = (IApplicationProxy)getApplication();
        config = new Configuration(getResources().getConfiguration());
        if(!config.locale.getLanguage().equals(app.getAppLanguage())){
            config.locale = new Locale(app.getAppLanguage());
            getResources().updateConfiguration(config, getResources().getDisplayMetrics());
            Log.i(TAG,"Localization is updated with value: " + config.locale.toString());
        }
        Log.i(TAG,"Localization is: " + config.locale.toString());
    }
}
