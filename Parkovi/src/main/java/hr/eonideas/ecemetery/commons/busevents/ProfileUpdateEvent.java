package hr.eonideas.ecemetery.commons.busevents;


import hr.eonideas.ecemetery.parkovi.rest.model.Profile;

/**
 * Created by Teo on 30.1.2016..
 */
public class ProfileUpdateEvent {
    public final Profile profile;

    public ProfileUpdateEvent(Profile profile) {
        this.profile = profile;
    }
}
