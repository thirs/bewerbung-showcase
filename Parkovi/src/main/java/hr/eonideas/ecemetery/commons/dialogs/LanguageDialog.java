package hr.eonideas.ecemetery.commons.dialogs;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.app.Dialog;
import android.app.DialogFragment;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by Teo on 3.3.2016..
 */



public class LanguageDialog extends DialogFragment {

    private String recentLanguageLocal;
    private Context context;
    private String[] languagesList;
    private String[] languageLocalsList;

    private LanguageChangeListener listener;

    public LanguageDialog() {
    }
    @SuppressLint("ValidFragment")
    public LanguageDialog(Activity activity, String recentLanguageLocal) {
        this.context = activity;
        this.recentLanguageLocal = recentLanguageLocal;
        this.languagesList = context.getResources().getStringArray(R.array.language_items);
        this.languageLocalsList = context.getResources().getStringArray(R.array.language_items_locals);
        if(!(getActivity() instanceof LanguageChangeListener)){
            this.listener = (LanguageChangeListener) activity;
        }
    }

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
        if(listener == null){
            dismiss();
        }
        AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
        builder.setTitle(R.string.select_language_dialog_title)
                .setItems(R.array.language_items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String selectedLanguageLocal = languageLocalsList[which];
                        if(recentLanguageLocal.equals(selectedLanguageLocal)){
                            listener.languageUnChanged(selectedLanguageLocal);
                        }else {
                            listener.languageChanged(selectedLanguageLocal);
                        }
                    }
                });
        return builder.create();
    }

    public interface LanguageChangeListener{
        public void languageChanged(String languageLocal);
        public void languageUnChanged(String languageLocal);
    }
}
