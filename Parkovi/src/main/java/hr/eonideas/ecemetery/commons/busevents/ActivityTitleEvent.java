package hr.eonideas.ecemetery.commons.busevents;

import hr.eonideas.ecemetery.parkovi.rest.model.Account;

/**
 * Created by Teo on 30.1.2016..
 */
public class ActivityTitleEvent {

    public final String title;

    public ActivityTitleEvent(String title) {
        this.title = title;
    }
}
