package hr.eonideas.ecemetery.commons.busevents;

/**
 * Created by Teo on 9.1.2016..
 */
public class ViewPagerFragmentSwitchEvent {
    private int fragmentIndex;

    public ViewPagerFragmentSwitchEvent(int fragmentIndex) {
        this.fragmentIndex = fragmentIndex;
    }

    public int getFragmentIndex() {
        return fragmentIndex;
    }
}
