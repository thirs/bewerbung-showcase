package hr.eonideas.ecemetery.commons.busevents;

/**
 * Created by Teo on 30.1.2016..
 */
public class LoggedInEvent {
    public final boolean message;

    public LoggedInEvent(boolean message) {
        this.message = message;
    }
}
