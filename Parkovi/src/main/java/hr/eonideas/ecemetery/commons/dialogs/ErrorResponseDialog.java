package hr.eonideas.ecemetery.commons.dialogs;

import android.app.Dialog;
import android.content.Context;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.v4.app.DialogFragment;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;

import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by thirs on 2.4.2016..
 */
public class ErrorResponseDialog extends NotificationDialog {

    public static ErrorResponseDialog newInstance(String title, String buttonLabel, ResponseBody errorResponseBody) {
        Bundle args = new Bundle();
        ErrorResponseDialog errorResponseDialog = new ErrorResponseDialog();
        if(title != null){
            args.putString(TITLE, title);
        }
        if(buttonLabel != null){
            args.putString(BUTTON_LABEL, buttonLabel);
        }
        if(errorResponseBody != null){
            args.putString(BUTTON_MESSAGE, parseErrorResponseMessage(errorResponseBody));
        }
        errorResponseDialog.setArguments(args);
        return errorResponseDialog;
    }

    @Override
    protected String getDialogMessage() {
        String message = getArguments().getString(BUTTON_MESSAGE, getResources().getString(R.string.error));
        return message;
    }

    private static String parseErrorResponseMessage(ResponseBody errorResponseBody){
        String errorMessage = null;
        JSONObject jsonObject = null;
        try {
            jsonObject = new JSONObject(errorResponseBody.string());
            errorMessage = jsonObject.get("message").toString();
        } catch (JSONException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return errorMessage;
    }

}
