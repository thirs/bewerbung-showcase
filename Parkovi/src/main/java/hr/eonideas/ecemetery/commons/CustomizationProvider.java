package hr.eonideas.ecemetery.commons;

import hr.eonideas.ecemetery.parkovi.ClassResolver;

public interface CustomizationProvider {
	
	public void addClassMapping(ClassResolver classResolver);

}
