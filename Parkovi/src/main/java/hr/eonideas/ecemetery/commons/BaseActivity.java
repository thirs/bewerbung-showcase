package hr.eonideas.ecemetery.commons;

import android.content.res.Configuration;
import android.os.Bundle;
import android.support.v4.app.FragmentManager;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;

import java.util.Locale;

import hr.eonideas.ecemetery.commons.interfaces.IApplicationProxy;
import hr.eonideas.ecemetery.parkovi.Parkovi;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IToolbarManager;

/**
 * Created by thirs on 25.10.2015..
 */
public class BaseActivity extends ChangeLangActivity implements IToolbarManager{
    private static String TAG = "BaseActivity";

    protected Toolbar toolbar;


    protected void initActionBar(CharSequence title) {
        toolbar = (Toolbar) findViewById(R.id.drawer_toolbar);
        setSupportActionBar(toolbar);
        if(getSupportActionBar() != null){
            //Actionbar things
            getSupportActionBar().setDisplayHomeAsUpEnabled(true);
            getSupportActionBar().setHomeButtonEnabled(true);
            if (title != null){
                getSupportActionBar().setTitle(title);
            }
        }
    }

    public Toolbar getToolbar() {
        return toolbar;
    }


    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                return true;
        }

        return super.onOptionsItemSelected(item);
    }

    public IApplicationProxy getApp(){
        return app;
    }
}
