package hr.eonideas.ecemetery.parkovi.advertisers;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.AdvertisersListAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.IAdAdapterItem;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;
import hr.eonideas.ecemetery.parkovi.rest.model.AdvertiserCategory;
import retrofit.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by thirs on 25.12.2015..
 */
public class AdvertisersListFragment extends BasicFragment implements IOnGetResponseListener, AdapterView.OnItemClickListener {
    public static final String TAG = Filter.PACKAGE + ".AdvertisersListFragment";
    public static final String AD_CATEGORY = "adcategory";

    private static final int ADVERTISER_REQUEST     = 33;
    private static final int TOP_ADVERTISER_REQUEST = 34;

    private ListView listView;
    private StickyListHeadersListView stickyList;

    private RestControler lc;
    private List<IAdAdapterItem> advertisersList;
    AdvertiserCategory selectedCategory;
    private AdvertisersListAdapter adapter;
    private List<IAdAdapterItem> topAdvertisersList;
    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(selectedCategory == null){
//            (th) todo do something since this should happen
        }
        setActionBar(selectedCategory.getTitle(), false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.advertisers_menu, null);

        selectedCategory = getArguments().getParcelable(AD_CATEGORY);
        stickyList = (StickyListHeadersListView) view.findViewById(R.id.list);
        stickyList.setOnItemClickListener(this);
        if(advertisersList == null || advertisersList.isEmpty()){
            if(lc == null){
                lc = new RestControler(getActivity());
            }
            lc.setOnGetResponseListener(this);
            lc.getAdvertiserTop(TOP_ADVERTISER_REQUEST);
        }
        return view;
    }

    private void updateListView() {
        if(advertisersList != null && !advertisersList.isEmpty()){
            adapter = new AdvertisersListAdapter(getActivity(), advertisersList);
            stickyList.setAdapter(adapter);
        }
        if(topAdvertisersList != null && !topAdvertisersList.isEmpty()){
            if(adapter == null){
                adapter = new AdvertisersListAdapter(getActivity());
            }
            adapter.setTopAdvertisersList(topAdvertisersList);
            stickyList.setAdapter(adapter);
            adapter.setHeaderTitles(getResources().getString(R.string.advertisers_header_topadvertisers), null);
        }

    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response != null && ((PagedResult)response.body()).getItems() instanceof List && !((List)((PagedResult)response.body()).getItems() == null)){
            if(requestId == TOP_ADVERTISER_REQUEST){
                if(lc == null){
                    lc = new RestControler(getActivity());
                }
                lc.setOnGetResponseListener(this);
                lc.getAdvertiser(selectedCategory, ADVERTISER_REQUEST);
                topAdvertisersList = (List<IAdAdapterItem>) ((PagedResult)response.body()).getItems();

            }
            else if (requestId == ADVERTISER_REQUEST){
                advertisersList = (List<IAdAdapterItem>) ((PagedResult)response.body()).getItems();
                updateListView();
            }
        }
    }

    @Override
    protected void backButtonHit() {
        mListener.onFragmentInteraction(-1, null, null);
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        Advertiser selectedAdvertiser;
        if(advertisersList.size() > position){
            selectedAdvertiser = (Advertiser) advertisersList.get(position);
        }else{
            selectedAdvertiser = (Advertiser)topAdvertisersList.get(position - advertisersList.size());
        }
        if(selectedAdvertiser != null){
            Bundle args = new Bundle();
            args.putParcelable(AdvertiserDetailsFragment.ADVERTISER, (Advertiser)selectedAdvertiser);
            mListener.onFragmentInteraction(-1, AdvertiserDetailsFragment.class, args);
        }
    }
}
