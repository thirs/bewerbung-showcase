package hr.eonideas.ecemetery.parkovi.rest.intefaces;


import com.squareup.okhttp.Response;

public interface IOnPostMPResponseListener {
	public void onPostMPResponse(Response response, int requestId);
}
