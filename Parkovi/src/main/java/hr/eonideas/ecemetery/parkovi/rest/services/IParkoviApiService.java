package hr.eonideas.ecemetery.parkovi.rest.services;

import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.data.AccountFBLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountLoginToken;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordReset;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordUpdate;
import hr.eonideas.ecemetery.parkovi.data.AccountRegister;
import hr.eonideas.ecemetery.parkovi.data.AccountUpdate;
import hr.eonideas.ecemetery.parkovi.data.GenericResponse;
import hr.eonideas.ecemetery.parkovi.data.MemoryParameter;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.data.ProfileParameter;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;
import hr.eonideas.ecemetery.parkovi.rest.model.AdvertiserCategory;
import hr.eonideas.ecemetery.parkovi.rest.model.Burial;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryHistory;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.Config;
import hr.eonideas.ecemetery.parkovi.rest.model.Gallery;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfoResponse;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.model.TourGuide;
import retrofit.Call;
import retrofit.http.Body;
import retrofit.http.DELETE;
import retrofit.http.GET;
import retrofit.http.Headers;
import retrofit.http.Multipart;
import retrofit.http.POST;
import retrofit.http.PUT;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;

public interface IParkoviApiService {
	public static final int STATUS_CODE_GENERAL_OK                         	 = 200;
	public static final int STATUS_CODE_POST_OK                              = 201;
	public static final int STATUS_CODE_REMOVED_OK                           = 204;
	public static final int STATUS_CODE_INVALID_INPUT              			 = 400;

	public static final int STATUS_CODE_UNAUTHORIZED_GET_ACCOUNT             = 401;
	public static final int STATUS_CODE_ACCOUNT_NOT_FOUND              		 = 404;

	//REST METHODS
//	Advertiser
	public static final String method_advertiser    				= "/advertiser";
	public static final String method_advertiserCategory    		= "/advertiser/category";
	public static final String method_advertiserTop    				= "/advertiser/top";
//	Cemetery
	public static final String method_cemeteryGallery      			= "/cemetery/gallery";
	public static final String method_cemeteryHistory      			= "/cemetery/history";
	public static final String method_cemeteryInfo   	   			= "/cemetery/info";
//	Configuration
	public static final String method_configuration 	   			= "/configuration";
//	Memory
	public static final String method_memory        	   			= "/memory";
//	Profile
	public static final String method_profile       				= "/profile";
	public static final String method_new_profile_avatar			= "/upload/avatar";
	public static final String method_new_profile_gallery_image		= "/upload/gallery";
//	TourGuide
	public static final String method_tourguide     				= "/tour";
//  Burial
	public static final String method_burial     					= "/burial";
//  Personalize
	public static final String method_register     					= "/account/register";
	public static final String method_login     					= "/account/login";
	public static final String method_fblogin     					= "/account/facebook/login";
	public static final String method_account						= "/account";
	public static final String method_account_password				= "/account/password";
	public static final String method_account_password_reset		= "/account/password/reset";
	public static final String method_uploadAccountAvatar			= "/account/upload/avatar";



		//	ADVERTISEMENT
		@GET("/cemetery/api/" + Filter.resApi_Version + method_advertiser)
		Call<PagedResult<Advertiser>> getAdvertiser(@Query("categoryId") Integer categoryId);

		@GET("/cemetery/api/" + Filter.resApi_Version + method_advertiserCategory)
		Call<PagedResult<AdvertiserCategory>> getAdvertiserCategory();

		@GET("/cemetery/api/" + Filter.resApi_Version + method_advertiserTop)
		Call<PagedResult<Advertiser>> getAdvertiserTop();


		//	CEMETERY
		@GET("/cemetery/api/" + Filter.resApi_Version + method_cemeteryGallery)
		Call<PagedResult<Gallery>> getCemeteryGallery();

		@GET("/cemetery/api/" + Filter.resApi_Version + method_cemeteryHistory)
//		Call<List<CemeteryHistory>> getCemeteryHistory();
		Call<PagedResult<CemeteryHistory>> getCemeteryHistory();

		@GET("/cemetery/api/" + Filter.resApi_Version + method_cemeteryInfo)
		Call<List<GroupedCemeteryInfo>> getCemeteryInfo();


		//	CONFIGURATION
		@GET("/cemetery/api/" + Filter.resApi_Version + method_configuration)
		Call<Config> getConfiguration();

		//	MEMORY
		@GET("/cemetery/api/" + Filter.resApi_Version + method_memory)
		Call<PagedResult<Memory>> getMemory(@Query("profileId") Integer profileId,@Query("offset") Integer offset,@Query("pageSize") Integer pageSize);

		@POST("/cemetery/api/" + Filter.resApi_Version + method_memory)
		Call<Memory> setMemory(@Body MemoryParameter memory);

		@PUT("/cemetery/api/" + Filter.resApi_Version + method_memory + "/{id}")
		Call<Memory> updateMemory(@Path("id") Integer id, @Body MemoryParameter memory);

		//	PROFILE
		@GET("/cemetery/api/" + Filter.resApi_Version + method_profile)
		Call<PagedResult<Profile>> getProfile(@Query("query") String query,@Query("offset") Integer offset,@Query("pageSize") Integer pageSize);

		@GET("/cemetery/api/" + Filter.resApi_Version + method_profile + "/{id}")
		Call<Profile> getSingleProfile(@Path("id") Integer id);

		@POST("/cemetery/api/" + Filter.resApi_Version + method_profile + "/{id}/favorite")
		Call<ResponseBody> setAsFavorite(@Path("id") Integer id);

		@DELETE("/cemetery/api/" + Filter.resApi_Version + method_profile + "/{id}/favorite")
		Call<ResponseBody> removeFromFavorites(@Path("id") Integer id);

		@POST("/cemetery/api/" + Filter.resApi_Version + method_profile + "/{id}/request_ownership")
		Call<ResponseBody> requestOwnership(@Path("id") Integer id);

		@PUT("/cemetery/api/" + Filter.resApi_Version + method_profile + "/{id}")
		Call<Profile> updateBiography(@Path("id") Integer id, @Body ProfileParameter biography);

		@DELETE("/cemetery/api/" + Filter.resApi_Version + method_profile + "/{id}"+method_new_profile_gallery_image+"/{itemId}")
		Call<ResponseBody> removeProfileGalleryImage(@Path("id") Integer profileId, @Path("itemId") Integer imageId);

		@POST("/cemetery/api/" + Filter.resApi_Version + method_profile + "/{id}"+method_new_profile_gallery_image)
		Call<ResponseBody> removeProfileGalleryImages(@Path("id") Integer id, @Body List<Integer> galleryImageIds);


		//	TOURGUIDE
		@GET("/cemetery/api/" + Filter.resApi_Version + method_tourguide)
		Call<PagedResult<TourGuide>> getTourGuide();


		//  BURIAL
		@GET("/cemetery/api/" + Filter.resApi_Version + method_burial)
		Call<PagedResult<Burial>> getBurialList();

		//  PERSONALIZATION
		@POST("/cemetery/api/" + Filter.resApi_Version + method_register)
		Call<Account> register(@Body AccountRegister accountRegister);

		@POST("/cemetery/api/" + Filter.resApi_Version + method_login)
		Call<Account> login(@Body AccountLogin accountRegister);

		@POST("/cemetery/api/" + Filter.resApi_Version + method_fblogin)
		Call<Account> fblogin(@Body AccountFBLogin accessToken);

		@GET("/cemetery/api/" + Filter.resApi_Version + method_account+"/{id}")
		Call<Account> recoverAccount(@Path("id") Integer id);

	    @PUT("/cemetery/api/" + Filter.resApi_Version + method_account)
		Call<Account> updateAccount(@Body AccountUpdate accountUpdate);

		@PUT("/cemetery/api/" + Filter.resApi_Version + method_account_password)
		Call<Account> updateAccountPassword(@Body AccountPasswordUpdate accountPasswordUpdate);

		@POST("/cemetery/api/" + Filter.resApi_Version + method_account_password_reset)
		Call<ResponseBody> resetAccountPassword(@Body AccountPasswordReset accountPasswordReset);
}