package hr.eonideas.ecemetery.parkovi.profile.map;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.util.TypedValue;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.SimpleGalleryRecyclerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.ImageAlbumActivity;
import hr.eonideas.ecemetery.parkovi.commons.ImageViewPager;
import hr.eonideas.ecemetery.parkovi.commons.MapPagerFragment;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.profile.ProfileFragment;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;

/**
 * Created by thirs on 6.12.2015..
 */
public class ProfileMapFragment extends MapPagerFragment implements SimpleGalleryRecyclerAdapter.IOnImageClickListener {

    private static final String TAG = Filter.PACKAGE + ".ProfileMapFragment";
    private IProfile profile;
    private MapView map;
    private View locationGallery;
    private TextView locationGalleryHeader;
    private View locationGalleryFullLink;
    private RecyclerView locationGalleryPreviewRecycler;

    @Override
    public String getDefinedTAG() {
        return null;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_full_fragment, container, false);
        profile = (IProfile) getArguments().getParcelable(ProfileFragment.PROFILE_LIGHT);
        initLayout(view);
        //then move map to 'location'
        if (map != null) {
            map.onCreate(null);
            map.onResume();
            map.getMapAsync(this);
        }

        setGalleryPreview();
        return view;
    }

    private void initLayout(View view) {
        map = (MapView) view.findViewById(R.id.mapImageView);
        locationGallery = view.findViewById(R.id.gallery_section);
        locationGalleryHeader = (TextView)view.findViewById(R.id.gallery_section_title);
        locationGalleryFullLink = view.findViewById(R.id.full_gallery_link);
        locationGalleryPreviewRecycler = (RecyclerView)view.findViewById(R.id.profile_gallery_recycler);
    }

    private void setGalleryPreview() {
        if(showGalleryPreview()){
            locationGallery.setVisibility(View.VISIBLE);
            locationGalleryHeader.setVisibility(View.VISIBLE);
            locationGalleryFullLink.setVisibility(View.VISIBLE);
            locationGalleryPreviewRecycler.setVisibility(View.GONE);

            locationGalleryHeader.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    locationGalleryPreviewRecycler.setVisibility(locationGalleryPreviewRecycler.getVisibility()==View.VISIBLE ? View.GONE : View.VISIBLE);
                }
            });
            locationGalleryFullLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAlbum();
                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            locationGalleryPreviewRecycler.setLayoutManager(layoutManager);
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            SimpleGalleryRecyclerAdapter galleryAdapter = new SimpleGalleryRecyclerAdapter(Utils.filterOutImages(profile.getProfileGallery()), R.layout.image_view);
            // Set up the ViewPager with the sections adapter.
            galleryAdapter.setOnImageClickListener(this);
            locationGalleryPreviewRecycler.setAdapter(galleryAdapter);
            galleryAdapter.notifyDataSetChanged();
        }else{
            locationGallery.setVisibility(View.GONE);
        }
    }

    private void openAlbum() {
        Intent intent = new Intent(getActivity(), ImageAlbumActivity.class);
        intent.putExtra(ImageAlbumActivity.ALBUM_GALLERY_ITEMS, new ArrayList(profile.getProfileGallery()));
        intent.putExtra(ImageAlbumActivity.PROFILE, (Profile) profile);
        intent.putExtra(ImageAlbumActivity.ALBUM_TITLE, ProfileUtils.getProfileName(profile));
        intent.putExtra(ImageAlbumActivity.IMAGE_GRID_MODE, ImageAlbumActivity.ImageGridMode.GALLERY);
        getParentFragment().startActivity(intent);
    }

    private void openFullSizeImage(Image imageItem) {
        Intent intent = new Intent(getActivity(), ImageViewPager.class);
        ArrayList<Image> singleItemList = new ArrayList<Image>(1);
        singleItemList.add(imageItem);
        intent.putExtra(ImageViewPager.IMAGE_INDEX, 0);
        intent.putExtra(ImageAlbumActivity.ALBUM_IMAGES, singleItemList);
        startActivity(intent);
    }

    private boolean showGalleryPreview() {
        boolean show = true;
        if(profile == null){
            show = false;
        }else if (!(profile.getProfileGallery() != null && profile.getProfileGallery().size() > 0)){
            show = false;
        }else{
            show = true;
        }
        return show;
    }



    @Override
    protected int enableLocationUpdates() {
        return 5000;
    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        super.onLocationChanged(location);
        Log.i(TAG,"Lokacija je updatana");

        if (currentLocation != null && !currentLocationPointed) {
            currentLocationPointed = true;
            List<LatLng> latLngs = new ArrayList<>();
            latLngs.add(new LatLng(profile.getLocation().getLat().doubleValue(),profile.getLocation().getLon().doubleValue()));
            smartLatLngAnimateCamera(location,latLngs);
            setMapPadding();
        }
    }

    @Override
    protected void mapLoadingError() {
        //todo ??
    }

    @Override
    protected void mapLoadingSuccess() {
        if(profile.getLocation() != null && profile.getLocation().getLat() != null && profile.getLocation().getLon() != null){
            LatLng profileLanLng = new LatLng(profile.getLocation().getLat().doubleValue(),profile.getLocation().getLon().doubleValue());
            showPois(profileLanLng,ProfileUtils.getProfileName(profile),null,R.drawable.pin_green_shadow_strong, false);
            List<LatLng> latLngs = new ArrayList<LatLng>();
            latLngs.add(profileLanLng);
            smartLatLngAnimateCamera(currentLocation,latLngs);
        }else{
            List<LatLng> latLngs = new ArrayList<LatLng>();
            latLngs.add(new LatLng(Double.valueOf(Filter.map_default_latitude), Double.valueOf(Filter.map_default_longitude)));
            smartLatLngAnimateCamera(currentLocation,latLngs);
        }
        setMapPadding();

    }

    //th ovaj dio radim zbog floating buttona koji se nalazi točno tamo gdje bi trebali biti zoom buttoni
    @Override
    protected void setUpMap() {
        super.setUpMap();
        setMapPadding();
    }

    private void setMapPadding() {
        TypedValue typedValue = new TypedValue();
        getResources().getValue(R.dimen.map_height_percentage_bottom_padding, typedValue, true);
        int padding = map.getMeasuredHeight() - (int)(map.getMeasuredHeight()*typedValue.getFloat());
        mMap.setPadding(0,0,0,padding);
    }


    @Override
    public void onImageClicked(Image image) {
        openFullSizeImage(image);
    }
}
