package hr.eonideas.ecemetery.parkovi.about.cemetery;

import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.ms.square.android.expandabletextview.ExpandableTextView;

import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by thirs on 18.10.2015..
 */
public class AboutCemeteryViewHolder extends ParentViewHolder {
    public ImageView headerImage;
    public TextView headerTitle;
    public TextView headerSubTitle;
    public ExpandableTextView headerContent;

    public AboutCemeteryViewHolder(View itemView) {
        super(itemView);
        headerImage = (ImageView) itemView.findViewById(R.id.header_img);
        headerTitle = (TextView) itemView.findViewById(R.id.header_title);
        headerSubTitle = (TextView) itemView.findViewById(R.id.header_subtitle);
        headerContent = (ExpandableTextView) itemView.findViewById(R.id.about_parent_content);

    }
}
