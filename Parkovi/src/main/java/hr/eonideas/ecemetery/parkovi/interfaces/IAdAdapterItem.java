package hr.eonideas.ecemetery.parkovi.interfaces;

import hr.eonideas.ecemetery.parkovi.rest.model.Image;

/**
 * Created by Teo on 14.1.2016..
 */
public interface IAdAdapterItem {
    public Integer getId();

    public void setId(Integer id);

    public String getTitle();

    public void setTitle(String title);

    public String getDescription();

    public void setDescription(String description);

    public Image getImage();

    public void setImage(Image image);
}
