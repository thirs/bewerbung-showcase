package hr.eonideas.ecemetery.parkovi.interfaces;

import java.math.BigDecimal;

/**
 * Created by Teo on 26.6.2016..
 */
public interface IMapLocation {
    public BigDecimal getLat();

    public void setLat(BigDecimal lat);

    public BigDecimal getLon();

    public void setLon(BigDecimal lon);

    public String getLocationTitle();

    public void setLocationTitle(String locationTitle);

    public String getLocationDescription();

    public void setLocationDescription(String locationDescription);
}
