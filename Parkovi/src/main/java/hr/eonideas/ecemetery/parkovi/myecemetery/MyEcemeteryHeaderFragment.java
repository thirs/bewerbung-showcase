package hr.eonideas.ecemetery.parkovi.myecemetery;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.soundcloud.android.crop.Crop;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.AccountEvent;
import hr.eonideas.ecemetery.commons.utils.FileUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.Manifest;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostMPResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by Teo on 24.4.2016..
 */
public class MyEcemeteryHeaderFragment extends BasicFragment implements View.OnClickListener, IOnPostMPResponseListener {
    public static final String TAG = "MyEcemeteryHeaderFragment";
    private static final int GALLERY_PERMISSIONS        = 31;

    private TextView accountOwnerName;
    private FragmentActivity activity;
    private Account account;
    private ImageLoader imageLoader;
    private RestControler lc;
    private Uri newAvatarImageUri;
    private CircleImageView userAvatarImage;

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return "";
    }

    public static MyEcemeteryHeaderFragment newInstance(){
        MyEcemeteryHeaderFragment fragment = new MyEcemeteryHeaderFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        activity = getActivity();
        account = app.getUserAccount();
        imageLoader = ImageLoader.getInstance();
        // Initialize views and adapter
        View view = inflater.inflate(R.layout.my_ecemetery_header_layout, container, false);

        initLayoutElements(view);

        //Present profile data header
        presentLayoutElements();

        return view;
    }

    private void initLayoutElements(View view) {
        accountOwnerName = (TextView) view.findViewById(R.id.account_owner_name);
        userAvatarImage = (CircleImageView) view.findViewById(R.id.profile_image);
    }

    private void presentLayoutElements() {
        //Account owners properties
        accountOwnerName.setText(Utils.getAccountName(account));
        if(account.getAvatar() != null){
            imageLoader.displayImage(account.getAvatar().getThumbUrl(), userAvatarImage);
        }
        userAvatarImage.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
        if(permissionCheck != PackageManager.PERMISSION_GRANTED){
            requireExternalStoragePermissions();
            return;
        }
        EasyImage.openChooserWithGallery(this, activity.getResources().getString(R.string.select_images_from_device), 0);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent result) {
        if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {

            MultipartBuilder mb = null;
            String addedImagePath = FileUtils.getPath(getActivity(), newAvatarImageUri);
            if(mb == null){
                mb = new MultipartBuilder().type(MultipartBuilder.FORM)
                        .addFormDataPart("file", "test-image.jpg", RequestBody.create(MediaType.parse("image/jpeg"), new File(addedImagePath)));
            }
            RequestBody requestBody = mb.build();
            if(lc == null){
                lc = new RestControler(getActivity());
            }
            lc.setOnPostMPResponseListener(MyEcemeteryHeaderFragment.this);
            lc.uploadAccountAvatarImage((Account)account,requestBody, -1);

        }else{
            super.onActivityResult(requestCode, resultCode, result);
            EasyImage.handleActivityResult(requestCode, resultCode, result, activity, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    //Some error handling
                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    //Handle the image
                    Uri selectedImageUri = Uri.fromFile(imageFile);
                    newAvatarImageUri = Uri.fromFile(new File(activity.getCacheDir(), "cropped"));
                    Crop.of(selectedImageUri, newAvatarImageUri).asSquare().start(activity.getApplicationContext(),MyEcemeteryHeaderFragment.this);
                }
            });
        }
    }

    @Override
    public void onPostMPResponse(Response response, int requestId) {
        if(response != null && response.code() == HttpURLConnection.HTTP_OK) {
            Image image = null;
            Gson gson = new Gson();
            try {
                image = gson.fromJson(response.body().charStream(), Image.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(image != null){
                app.clearImageLoaderCache(image.getThumbUrl());
                account.setAvatar(image);
                activity.runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        EventBus.getDefault().post(new AccountEvent((Account) account));
                    }
                });
            }
        }
    }
        //*******************PERMISSIONS*****************************
    private void requireExternalStoragePermissions() {
// Should we show an explanation?
        if (shouldShowRequestPermissionRationale(Manifest.permission.READ_EXTERNAL_STORAGE)) {

            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {
            // No explanation needed, we can request the permission.
            requestPermissions(new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSIONS);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case GALLERY_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EasyImage.openChooserWithGallery(this, activity.getResources().getString(R.string.select_images_from_device), 0);
                } else {
                    Log.i(TAG, "Permissions other that granted. GranResult length is "+ grantResults.length);
                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

}
