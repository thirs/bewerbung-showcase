package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 7.2.2016..
 */
public class ProfileParameter implements Parcelable {

    @SerializedName("biography")
    private String biography = null;

    public ProfileParameter(String biography) {
        this.biography = biography;
    }

    /**
     * Memory's main text.
     **/
    public String getBiography() {
        return biography;
    }

    public void setBiography(String biography) {
        this.biography = biography;
    }


    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProfileParameter {\n");

        sb.append("  biography: ").append(biography).append("\n");
        return sb.toString();
    }

    protected ProfileParameter(Parcel in) {
        biography = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(biography);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ProfileParameter> CREATOR = new Parcelable.Creator<ProfileParameter>() {
        @Override
        public ProfileParameter createFromParcel(Parcel in) {
            return new ProfileParameter(in);
        }

        @Override
        public ProfileParameter[] newArray(int size) {
            return new ProfileParameter[size];
        }
    };
}