package hr.eonideas.ecemetery.parkovi.personalization.register;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.FacebookSdk;
import com.squareup.okhttp.ResponseBody;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.commons.busevents.ViewPagerFragmentSwitchEvent;
import hr.eonideas.ecemetery.commons.dialogs.ErrorResponseDialog;
import hr.eonideas.ecemetery.commons.dialogs.NotificationDialog;
import hr.eonideas.ecemetery.commons.utils.FormUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordUpdate;
import hr.eonideas.ecemetery.parkovi.data.AccountRegister;
import hr.eonideas.ecemetery.parkovi.interfaces.IAccount;
import hr.eonideas.ecemetery.parkovi.personalization.PersonalizeFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import retrofit.Response;

/**
 * Created by Teo on 7.1.2016..
 */
public class PersonalizeRegisterFragment extends BasicPagerFragment implements IOnPostResponseListener {
    public static final String REGISTRATION_PERFORMED = "registrationperformed";
    private static final java.lang.String IS_UPDATE_MODE = "IS_UPDATE_MODE";
    private static final java.lang.String IS_CHANGE_PASS = "IS_CHANGE_PASS";
    public static String TAG = "PersonalizeRegisterFragment";

    private ViewGroup registrationForm;
    private LinearLayout formFooter;
    private LinearLayout oldPasswordBlock;
    private LinearLayout newPasswordBlock;
    private LinearLayout accountUpdateBlock;

    private EditText firstName;
    private EditText lastName;
    private EditText email;
    private EditText oldPassword;
    private EditText password1;
    private EditText password2;

    private ImageView submitButton;

    private RestControler lc;
    private boolean isUpdateMode;

    public static PersonalizeRegisterFragment newInstance(int page, boolean isUpdateMode) {
        Bundle args = new Bundle();
        args.putBoolean(IS_UPDATE_MODE, isUpdateMode);
        PersonalizeRegisterFragment fragment = new PersonalizeRegisterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        FacebookSdk.sdkInitialize(getContext());
        isUpdateMode = getArguments().getBoolean(IS_UPDATE_MODE, false);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personalize_register, container, false);
        initLayoutElements(view);

        password2.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND && !isUpdateMode) {
                    Utils.hideKeyboard(getActivity());
                    if (registrationFormOK()) {
                        Utils.hideKeyboard(getActivity());
                        submitRegistration();
                    }
                }
                return handled;
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (registrationFormOK()) {
                    Utils.hideKeyboard(getActivity());
                    submitRegistration();
                }
            }
        });
        return view;
    }

    private void submitRegistration() {
        if (lc == null) {
            lc = new RestControler(getActivity());
        }
        lc.setOnPostResponseListener(this);
        lc.register(new AccountRegister(firstName.getText().toString(), lastName.getText().toString(), email.getText().toString(), password1.getText().toString()));
    }

    private void initLayoutElements(View view) {
        registrationForm = (ViewGroup) view.findViewById(R.id.register_form);
        formFooter = (LinearLayout) view.findViewById(R.id.form_footer);
        oldPasswordBlock = (LinearLayout) view.findViewById(R.id.old_password_block);
        newPasswordBlock = (LinearLayout) view.findViewById(R.id.new_password_block);
        accountUpdateBlock = (LinearLayout) view.findViewById(R.id.account_update_block);

        firstName = (EditText) view.findViewById(R.id.firstName);
        lastName = (EditText) view.findViewById(R.id.lastName);
        email = (EditText) view.findViewById(R.id.email);
        oldPassword = (EditText) view.findViewById(R.id.old_password);
        password1 = (EditText) view.findViewById(R.id.password1);
        password2 = (EditText) view.findViewById(R.id.password2);
        submitButton = (ImageView) view.findViewById(R.id.submit_form);
        if(isUpdateMode){
            email.setEnabled(false);
            if(app.getUserAccount().inMode() == IAccount.AccountMode.PASS_UPDATE){
                accountUpdateBlock.setVisibility(View.GONE);
                formFooter.setVisibility(View.GONE);

                oldPasswordBlock.setVisibility(View.VISIBLE);
                newPasswordBlock.setVisibility(View.VISIBLE);
            }else{
                oldPasswordBlock.setVisibility(View.GONE);
                newPasswordBlock.setVisibility(View.GONE);
                formFooter.setVisibility(View.GONE);

                accountUpdateBlock.setVisibility(View.VISIBLE);
                firstName.setVisibility(View.VISIBLE);
                lastName.setVisibility(View.VISIBLE);
                email.setVisibility(View.VISIBLE);

                firstName.setText(app.getUserAccount().getFirstName());
                lastName.setText(app.getUserAccount().getLastName());
                email.setText(app.getUserAccount().getEmail());
            }
        }else{
            accountUpdateBlock.setVisibility(View.VISIBLE);
            newPasswordBlock.setVisibility(View.VISIBLE);
            oldPasswordBlock.setVisibility(View.GONE);
            formFooter.setVisibility(View.VISIBLE);
        }
    }

    public Account getUpdateFormData(){
        Account a = Account.getInstance(firstName.getText().toString(),lastName.getText().toString());
        return a;
    }


    public AccountPasswordUpdate getPassUpdateFormData() {
        AccountPasswordUpdate accountPasswordUpdate = null;
        if(changePassFormOK()){
            accountPasswordUpdate = new AccountPasswordUpdate(oldPassword.getText().toString(), password2.getText().toString());
        }
        return accountPasswordUpdate;
    }


    private boolean registrationFormOK(){
        boolean formOK = false;
        if(FormUtils.isEditTextEmpty(firstName)){
            Toast.makeText(getActivity(),"Missing First Name", Toast.LENGTH_LONG).show();
            return formOK;
        }
        if(FormUtils.isEditTextEmpty(lastName)){
            Toast.makeText(getActivity(),"Missing Last Name", Toast.LENGTH_LONG).show();
            return formOK;
        }
        formOK = true;
        return formOK;
    }

    private boolean changePassFormOK(){
        boolean formOK = false;
        if(FormUtils.isEditTextEmpty(oldPassword)){
            Toast.makeText(getActivity(),"Old Password missing", Toast.LENGTH_LONG).show();
            return formOK;
        }
        if(FormUtils.isEditTextEmpty(password1)){
            Toast.makeText(getActivity(),"New Password missing", Toast.LENGTH_LONG).show();
            return formOK;
        }
        if(FormUtils.isEditTextEmpty(password2)){
            Toast.makeText(getActivity(),"Please reenter new Password", Toast.LENGTH_LONG).show();
            return formOK;
        }
        if(!password1.getText().toString().equals(password2.getText().toString())){
            Toast.makeText(getActivity(),"Both passwords must be the same", Toast.LENGTH_LONG).show();
            return formOK;
        }
        formOK = true;
        return formOK;
    }

    @Override
    public void onPostResponse(Response response, int requestId) {
        int responseCode = response.code();
        if(responseCode == IParkoviApiService.STATUS_CODE_POST_OK){
            Toast.makeText(getActivity(), "Registration for email '" + email.getText() + "' was successful! Enter your login data.", Toast.LENGTH_LONG).show();
            app.getSharedPreferences().edit().putBoolean(REGISTRATION_PERFORMED, true).commit();
            EventBus.getDefault().post(new ViewPagerFragmentSwitchEvent(PersonalizeFragment.FRAGMENT_SWITCH_INDEX_LOGIN));
            FormUtils.clearForm(registrationForm);
        }else{
            ErrorResponseDialog dialog = ErrorResponseDialog.newInstance(null, null, response.errorBody());
            dialog.show(getFragmentManager(),"TEST");
        }
    }

}
