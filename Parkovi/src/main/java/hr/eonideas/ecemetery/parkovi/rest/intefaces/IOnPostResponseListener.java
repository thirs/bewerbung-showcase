package hr.eonideas.ecemetery.parkovi.rest.intefaces;

import retrofit.Response;

public interface IOnPostResponseListener {
	public void onPostResponse(Response response, int requestId);
}
