package hr.eonideas.ecemetery.parkovi.interfaces;

import android.support.v7.widget.Toolbar;

public interface IToolbarManager {
	public Toolbar getToolbar();
}
