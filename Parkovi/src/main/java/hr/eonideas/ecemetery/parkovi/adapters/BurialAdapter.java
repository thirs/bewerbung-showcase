package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;

import java.util.List;

import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IBurial;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by thirs on 1.12.2015..
 */
public class BurialAdapter extends BaseAdapter implements StickyListHeadersAdapter {

    private LayoutInflater inflater;
    private List<IBurial> burialList;
    private Context context;

    public BurialAdapter(Context context, List<IBurial> burialList) {
        this.context = context;
        this.inflater = LayoutInflater.from(context);
        this.burialList = burialList;
    }

    @Override
    public int getCount() {
        return burialList.size();
    }

    @Override
    public Object getItem(int position) {
        return burialList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder holder;

        if (convertView == null) {
            holder = new ViewHolder();
            convertView = inflater.inflate(R.layout.burials_list_item_layout, parent, false);
            holder.time = (TextView) convertView.findViewById(R.id.burial_item_time);
            holder.name = (TextView) convertView.findViewById(R.id.ancestor_data);
            holder.location = (TextView) convertView.findViewById(R.id.location_data);
            convertView.setTag(holder);
        } else {
            holder = (ViewHolder) convertView.getTag();
        }
        holder.time.setText(Utils.getTime(burialList.get(position).getTime().toString()));
        holder.name.setText(String.format(context.getResources().getString(R.string.burials_ancestor_data), burialList.get(position).getName(), burialList.get(position).getAge()));
        holder.location.setText(burialList.get(position).getLocation());
        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.burials_list_header_layout, parent, false);
            holder.date = (TextView) convertView.findViewById(R.id.items_header);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText = "" + Utils.getDate(burialList.get(position).getTime());
        holder.date.setText(headerText);
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        return Utils.getCleanedTimeString(burialList.get(position).getTime());
    }

    class HeaderViewHolder {
        TextView date;
    }

    class ViewHolder {
        TextView time;
        TextView name;
        TextView location;
    }
}
