package hr.eonideas.ecemetery.parkovi.profile.details;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.EditText;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.busevents.ProfileUpdateEvent;
import hr.eonideas.ecemetery.commons.dialogs.NotificationDialog;
import hr.eonideas.ecemetery.commons.utils.FormUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.data.ProfileParameter;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import retrofit.Response;

/**
 * Created by Teo on 19.1.2016..
 */
public class UpdateBiographyActivity extends BaseActivity implements View.OnClickListener, IOnPostResponseListener {

    public static final String PROFILE = Filter.PACKAGE + ".BIOGRAPHY";

    private IProfile profile;
    private ProfileParameter biography;

    private EditText profileBiography;
    private View updateBiography;
    private RestControler lc;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.update_biography);
        initActionBar("Profile Biography");
        init();
    }

    private void init() {
        profile = getIntent().getParcelableExtra(PROFILE);
        if(profile == null){
            //todo - nešt ne valja!!!!
        }
        biography = new ProfileParameter(profile.getAboutContent());
        profileBiography = (EditText) findViewById(R.id.profile_biography_text);
        updateBiography = findViewById(R.id.update_profile_biography);
        updateBiography.setOnClickListener(this);
        if(biography.getBiography() != null){
            profileBiography.setText(biography.getBiography());
        }
    }

    @Override
    public void onClick(View v) {
         if(filledMandatoryFields()){
             profile.setAboutContent(profileBiography.getText().toString());
             updateBiography();
         }else{
             NotificationDialog dialog = NotificationDialog.newInstance("Enter all data", "OK", null);
             dialog.show(getSupportFragmentManager(), "Diag");
         }
    }

    private void updateBiography(){
        //poziv metode za kreiranje memory-a
        if (lc == null) {
            lc = new RestControler(this);
        }
        lc.setOnPostResponseListener(this);
        lc.updateBiography(profile.getId(), new ProfileParameter(profile.getAboutContent()), app.getUserAccount());
    }

    private boolean filledMandatoryFields() {
        boolean filledMandatoryFields = true;
        if(FormUtils.isEditTextEmpty(profileBiography))
            filledMandatoryFields = false;
        return filledMandatoryFields;
    }


    @Override
    public void onPostResponse(Response response, int requestId) {
        if (response != null) {
            if (response.body() != null && response.body() instanceof Profile) {
                Intent resultIntent = new Intent();
                profile = (Profile) response.body();
                resultIntent.putExtra(ProfileDetailsFragment.UPDATED_PROFILE, (Profile)profile);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().post(new ProfileUpdateEvent((Profile) profile));
    }
}
