package hr.eonideas.ecemetery.parkovi.data;


import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryHistory;

/**
 * Created by thirs on 18.10.2015..
 */
public class AboutCemeteryParentObject extends CemeteryHistory implements ParentListItem {
    private List<AboutCemeteryChildObject> mChildList;

    public AboutCemeteryParentObject(CemeteryHistory cemeteryHistory) {
        super(cemeteryHistory);
        mChildList = new ArrayList<AboutCemeteryChildObject>();
        mChildList.add(new AboutCemeteryChildObject(cemeteryHistory.getFullDescription()));
    }

    @Override
    public List<AboutCemeteryChildObject> getChildItemList() {
        return mChildList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }
}
