package hr.eonideas.ecemetery.parkovi;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.v7.graphics.Palette;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;

import de.hdodenhof.circleimageview.CircleImageView;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;

/**
 * Created by thirs on 22.12.2015..
 */
public class NavigationHeaderFragment extends BasicFragment implements View.OnClickListener {
    public static final String TAG = Filter.PACKAGE + ".NavigationFooterFragment";
    private static final String LOGGED_IN = "loggedin";

    private View loginItem;
    private View logoutItem;
    private ImageView companyImage;
    private TextView headerTitle;
    private TextView headerSubtitle;
    private CircleImageView accountAvatar;
    private ImageLoader il;
    private View header;
    private boolean reloadAvatarImage; //to remove avatar image from cache or not

    public static NavigationHeaderFragment newInstance(boolean isLoggedIn) {
        NavigationHeaderFragment fragment = new NavigationHeaderFragment();
        Bundle args = new Bundle();
        args.putBoolean(LOGGED_IN, isLoggedIn);
        fragment.setArguments(args);
        return fragment;
    }



    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.navdrawer_header_items, null);
        il = ImageLoader.getInstance();
        initLayoutElements(v);

        updateMenuItems(getArguments().getBoolean(LOGGED_IN, false), false);
        return v;
    }

    private void initLayoutElements(View v) {
        header = v.findViewById(R.id.header_container);
        loginItem = v.findViewById(R.id.navmenuitem_login);
        loginItem.setOnClickListener(this);

        logoutItem = v.findViewById(R.id.navmenuitem_logout);
        logoutItem.setOnClickListener(this);

        companyImage = (ImageView)v.findViewById(R.id.drawer_header_company_image);
        accountAvatar = (CircleImageView)v.findViewById(R.id.drawer_header_account_avatar);
        headerTitle = (TextView)v.findViewById(R.id.company_name);
        headerSubtitle = (TextView)v.findViewById(R.id.company_adress);
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }

    @Override
    public void onClick(View v) {
         if (v.getId() == R.id.navmenuitem_login) {
            mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_LOGIN, null, null);
        } else if (v.getId() == R.id.navmenuitem_logout) {
            mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_LOGOUT, null, null);
        } else if(v.getId() == R.id.drawer_header_account_avatar){
            mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_MYECEMETERY, null, null);
        }
    }

    public void updateMenuItems(boolean isLoggedIn, boolean reloadAvatarImage) {
        this.reloadAvatarImage = reloadAvatarImage;
        if(isLoggedIn){
            Account a = Account.getInstance(null);
            logoutItem.setVisibility(View.VISIBLE);
            loginItem.setVisibility(View.GONE);
            companyImage.setVisibility(View.INVISIBLE);
            accountAvatar.setVisibility(View.VISIBLE);
            accountAvatar.setOnClickListener(this);
            setAccountProperties(a);
        }else{
            logoutItem.setVisibility(View.GONE);
            loginItem.setVisibility(View.VISIBLE);
            companyImage.setVisibility(View.VISIBLE);
            accountAvatar.setVisibility(View.INVISIBLE);
            accountAvatar.setOnClickListener(null);

            headerTitle.setText(R.string.nd_header_company_name);
            headerSubtitle.setText(R.string.nd_header_company_address);
            setHeaderBackgroundColor(false,null,true);
        }

    }

    private void setAccountProperties(final Account a) {
            if(reloadAvatarImage){
                if(a.getAvatar() != null)app.clearImageLoaderCache(a.getAvatar().getThumbUrl());
                reloadAvatarImage = false;
            }
            getActivity().runOnUiThread(new Runnable() {
                public void run() {
                    if(a != null && a.getAvatar() != null){
                        il.loadImage(a.getAvatar().getThumbUrl(), new ImageLoadingListener() {
                            @Override
                            public void onLoadingStarted(String imageUri, View view) {

                            }

                            @Override
                            public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                            }

                            @Override
                            public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                                setHeaderBackgroundColor(true,loadedImage,false);
                            }

                            @Override
                            public void onLoadingCancelled(String imageUri, View view) {

                            }
                        });
                    }else{
                        setHeaderBackgroundColor(true, BitmapFactory.decodeResource(getResources(),R.drawable.avatar_placeholder),true);
                    }
                    headerTitle.setText(a.getFirstName() + " " + a.getLastName());
                    headerSubtitle.setText(a.getEmail());
                }
            });
    }

    private void setHeaderBackgroundColor(boolean isLoggedIn, Bitmap loadedImage, boolean setDefaultColor){
        if(isLoggedIn){
            if(!setDefaultColor){
                Palette.from( loadedImage ).generate( new Palette.PaletteAsyncListener() {
                    @Override
                    public void onGenerated( Palette palette ) {
                        Palette.Swatch swatch = palette.getMutedSwatch();
                        if(swatch != null){
                            header.setBackgroundColor(swatch.getRgb());
                        }else{
                            header.setBackgroundColor(getResources().getColor(R.color.navigationDrawerHeader));
                        }
                    }
                });
            }else{
                header.setBackgroundColor(getResources().getColor(R.color.navigationDrawerHeader));
            }
            accountAvatar.setImageBitmap(loadedImage);
        }else{
            header.setBackgroundColor(getResources().getColor(R.color.navigationDrawerHeader));
        }
    }
}
