package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.rest.model.Gallery;

/**
 * Created by thirs on 21.10.2015..
 */
public class GalleryGridAdapter extends BaseAdapter {
    private List<Gallery> aboutGalleryList;
    private Context context;
    private ImageLoader imageLoader;

    public GalleryGridAdapter(Context context, List<Gallery> aboutGalleryList) {
        this.aboutGalleryList = aboutGalleryList;
        this.context = context;
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public int getCount() {
        return aboutGalleryList.size();
    }

    @Override
    public Gallery getItem(int position) {
        return aboutGalleryList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        public ImageView albumImageView;
        public TextView albumTitle;
        public TextView albumDesc;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.about_gallery_item, null);

            view.albumDesc = (TextView) convertView.findViewById(R.id.about_gallery_album_desc);
            view.albumTitle = (TextView) convertView.findViewById(R.id.about_gallery_album_title);
            view.albumImageView = (ImageView) convertView.findViewById(R.id.about_gallery_album_img);

            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }
        view.albumTitle.setText(aboutGalleryList.get(position).getTitle());
        view.albumDesc.setText(aboutGalleryList.get(position).getDescription());
        imageLoader.displayImage(aboutGalleryList.get(position).getImage().getFullUrl(), view.albumImageView);
        return convertView;
    }
}