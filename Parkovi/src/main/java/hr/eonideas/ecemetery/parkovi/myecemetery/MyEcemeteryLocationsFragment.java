package hr.eonideas.ecemetery.parkovi.myecemetery;

import android.location.Location;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.LocationsRecyclerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicLocationFragment;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.commons.MapPagerFragment;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;

/**
 * Created by Teo on 27.5.2016..
 */
public class MyEcemeteryLocationsFragment extends BasicLocationFragment {
    private static final String TAG = "MyEcemeteryLocationsFragment";

    public static final String LOCATION_LIST = Filter.PACKAGE + ".LOCATION_LIST";

    private RecyclerView locationsRecyclerView;
    private TextView emptyListText;
    private LocationsRecyclerAdapter adapter;


    @Override
    protected int enableLocationUpdates() {
        return 1;
    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        if(adapter != null){
            adapter.setCurrentLocation(location);
            adapter.notifyDataSetChanged();
        }
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_recycler_fragment, null);
        initLayoutElements(view);
        setLayoutElements();

        return view;
    }

    private void setLayoutElements() {
        Account a = Account.getInstance(null);
        if(a.getOwnedLocations() != null && a.getOwnedLocations().size() > 0){
            adapter = new LocationsRecyclerAdapter(a.getOwnedLocations());
            emptyListText.setVisibility(View.GONE);
            locationsRecyclerView.setVisibility(View.VISIBLE);
            locationsRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            locationsRecyclerView.setAdapter(adapter);
        }
    }

    private void initLayoutElements(View view) {
        locationsRecyclerView = (RecyclerView) view.findViewById(R.id.locationList);
        emptyListText = (TextView) view.findViewById(R.id.noContentMessage);
    }
}
