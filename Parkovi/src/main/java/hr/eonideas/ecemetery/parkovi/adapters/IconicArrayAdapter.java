package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.data.AboutInformationType;
import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;

/**
 * Created by thirs on 26.10.2015..
 */
public class IconicArrayAdapter extends RecyclerView.Adapter<IconicArrayAdapter.IconicViewHolder> {
    private List<IAboutInformationItem> cemetryInfo;
    private Context context;
    private ImageLoader imageLoader;
    private View.OnClickListener listener;

    public IconicArrayAdapter(Context context, List<IAboutInformationItem> informations, View.OnClickListener listener){
        this.context = context;
        this.cemetryInfo = informations;
        this.imageLoader = ImageLoader.getInstance();
        this.listener = listener;
    }

    @Override
    public IconicViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.contact_information_item, null);
        view.setOnClickListener(listener);
        IconicViewHolder viewHolder = new IconicViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(IconicViewHolder holder, int position) {
        IAboutInformationItem item = cemetryInfo.get(position);
        holder.about_information_label.setText(item.getInfoLabel());
        holder.about_information_value.setText(item.getInfoValue());
        if(item.getInfoType().equals(AboutInformationType.TYPE_ADDRESS)){
            holder.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_home));
        }else if(item.getInfoType().equals(AboutInformationType.TYPE_EMAIL)){
            holder.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_mail));
        }else if(item.getInfoType().equals(AboutInformationType.TYPE_MOBILE_PHONE)){
            holder.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_mobile));
        }else if(item.getInfoType().equals(AboutInformationType.TYPE_PHONE)){
            holder.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_phone));
        }else if(item.getInfoType().equals(AboutInformationType.TYPE_WEB)){
            holder.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_www));
        }else if(item.getInfoType().equals(AboutInformationType.TYPE_WORKING_TIME)){
            holder.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_workinghours));
        }
    }


    @Override
    public int getItemCount() {
        return cemetryInfo.size();
    }

    static class IconicViewHolder extends RecyclerView.ViewHolder {
        protected TextView about_information_label;
        protected TextView about_information_value;
        protected ImageView about_information_icon;

        public IconicViewHolder(View itemView) {
            super(itemView);
            about_information_label = (TextView) itemView.findViewById(R.id.about_information_label);
            about_information_value = (TextView) itemView.findViewById(R.id.about_information_value);
            about_information_icon = (ImageView) itemView.findViewById(R.id.about_information_icon);
        }
    }
}
