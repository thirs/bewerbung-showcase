package hr.eonideas.ecemetery.parkovi.personalization.login;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.DialogFragment;
import android.util.Log;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.facebook.AccessToken;
import com.facebook.CallbackManager;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.login.LoginResult;
import com.facebook.login.widget.LoginButton;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.util.Arrays;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.GenBooleanEvent;
import hr.eonideas.ecemetery.commons.busevents.ViewPagerFragmentSwitchEvent;
import hr.eonideas.ecemetery.commons.dialogs.ErrorResponseDialog;
import hr.eonideas.ecemetery.commons.dialogs.ForgotPasswordDialog;
import hr.eonideas.ecemetery.commons.dialogs.NotificationDialog;
import hr.eonideas.ecemetery.commons.utils.FormUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.data.AccountFBLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordReset;
import hr.eonideas.ecemetery.parkovi.interfaces.IAccount;
import hr.eonideas.ecemetery.parkovi.personalization.PersonalizeFragment;
import hr.eonideas.ecemetery.parkovi.personalization.register.PersonalizeRegisterFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnFailedResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import retrofit.Response;

/**
 * Created by Teo on 7.1.2016..
 */
public class PersonalizeLoginFragment extends BasicPagerFragment implements IOnPostResponseListener,ForgotPasswordDialog.IOnEmailSubmition {
    private static final int UP_LOGIN = 33;
    private static final int FB_LOGIN = 34;
    private static final int RESET_PASSWORD = 35;
    public static String TAG = "PersonalizeLoginFragment";
    private static final String LAST_LOGIN_USERNAME = "loginusername";

    private ViewGroup loginForm;
    private EditText email;
    private EditText password;
    private TextView forgotPassword;

    private ImageView submitButton;
    private LoginButton fbLoginButton;

    private RestControler lc;
    private CallbackManager callbackManager;

    public static PersonalizeLoginFragment newInstance(int page) {
        Bundle args = new Bundle();
        PersonalizeLoginFragment fragment = new PersonalizeLoginFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        callbackManager = CallbackManager.Factory.create();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.personalize_login, container, false);

        initLayoutElements(view);
        if(app.getSharedPreferences().contains(LAST_LOGIN_USERNAME)){
            email.setText(app.getSharedPreferences().getString(LAST_LOGIN_USERNAME,""));
        }
        password.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    Utils.hideKeyboard(getActivity());
                    app.getSharedPreferences().edit().putString(LAST_LOGIN_USERNAME, email.getText().toString()).commit();
                    submitLogin();
                }
                return handled;
            }
        });
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (loginFormOK()) {
                    Utils.hideKeyboard(getActivity());
                    app.getSharedPreferences().edit().putString(LAST_LOGIN_USERNAME, email.getText().toString()).commit();
                    submitLogin();
                }
            }
        });
        forgotPassword.setVisibility(View.VISIBLE);
        forgotPassword.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                DialogFragment df = ForgotPasswordDialog.newInstance();
                df.show(getChildFragmentManager(),ForgotPasswordDialog.TAG);
            }
        });

        fbLoginButton.setFragment(this);
        fbLoginButton.setReadPermissions(Arrays.asList("public_profile, email, user_birthday"));
        fbLoginButton.registerCallback(callbackManager,
                new FacebookCallback<LoginResult>() {
                    @Override
                    public void onSuccess(LoginResult loginResult) {
                        LoginResult res = loginResult;
                        // App code
                        GraphRequest request = GraphRequest.newMeRequest(
                                loginResult.getAccessToken(),
                                new GraphRequest.GraphJSONObjectCallback() {
                                    @Override
                                    public void onCompleted(JSONObject object, GraphResponse response) {
                                        Log.v("LoginActivity", response.toString());
                                        // AccessToken
                                        AccessToken.setCurrentAccessToken(response.getRequest().getAccessToken());
                                        String accessToken = response.getRequest().getAccessToken().getToken();
                                        if(accessToken != null && accessToken.length() > 0){
                                            submitFBLogin(accessToken);
                                        }
                                    }
                                });
                        Bundle parameters = new Bundle();
                        parameters.putString("fields", "id,name,email,gender,birthday,publish_actions");
                        request.setParameters(parameters);
                        request.executeAsync();

                    }

                    @Override
                    public void onCancel() {
                        Log.d(TAG, "Cancel");
                    }

                    @Override
                    public void onError(FacebookException exception) {
                        Log.d(TAG, "Exception");
                        exception.printStackTrace();
                    }
                });
        return view;
    }

    private void submitLogin() {
        if (lc == null) {
            lc = new RestControler(getActivity());
        }
        lc.setOnPostResponseListener(this);
        lc.login(new AccountLogin(email.getText().toString(), password.getText().toString()), UP_LOGIN);
    }

    private void submitFBLogin(String accessToken) {
        if (lc == null) {
            lc = new RestControler(getActivity());
        }
        lc.setOnPostResponseListener(this);
        lc.fblogin(new AccountFBLogin(accessToken), FB_LOGIN);
    }

    @Override
    public void onEmailSubmition(String email) {
        if(Filter.isDebuggingMode)
            Toast.makeText(getContext(),"Password would be reset for: " + email, Toast.LENGTH_LONG).show();
        if (lc == null) {
            lc = new RestControler(getActivity());
        }
        lc.setOnPostResponseListener(this);
        lc.resetAccountPassword(new AccountPasswordReset(email), RESET_PASSWORD);
    }

    private void initLayoutElements(View view) {
        loginForm = (ViewGroup) view.findViewById(R.id.login_form);
        email = (EditText) view.findViewById(R.id.email);
        password = (EditText) view.findViewById(R.id.password);
        forgotPassword = (TextView) view.findViewById(R.id.forgot_pass_link);
        submitButton = (ImageView) view.findViewById(R.id.submit_form);
        fbLoginButton = (LoginButton) view.findViewById(R.id.login_button);
    }

    private boolean loginFormOK() {
        boolean formOK = false;
        if (!FormUtils.isValidEmailAddress(email.getText().toString())) {
            Toast.makeText(getActivity(), "Invalid or empty Email", Toast.LENGTH_LONG).show();
            return formOK;
        }
        if (FormUtils.isEditTextEmpty(password)) {
            Toast.makeText(getActivity(), "Missing Password", Toast.LENGTH_LONG).show();
            return formOK;
        }
        formOK = true;
        return formOK;
    }

    @Override
    public void onPostResponse(Response response, int requestId) {
        int responseCode = response.code();
        if (responseCode == IParkoviApiService.STATUS_CODE_GENERAL_OK) {
            if(requestId == UP_LOGIN){
                Account account = ((Account) response.body());
                if(account != null){
                    account.setLoginType(IAccount.LoginType.UP);
                }
                updateAccountAndNotify(account);
            }else if(requestId == FB_LOGIN){
                Account account = ((Account) response.body());
                if(account != null){
                    account.setLoginType(IAccount.LoginType.FB);
                }
                updateAccountAndNotify(account);
            }else if(requestId == RESET_PASSWORD){
                Toast.makeText(getContext(),"Password reset",Toast.LENGTH_LONG).show();
            }
        }else{
            ErrorResponseDialog dialog = ErrorResponseDialog.newInstance(null, null, response.errorBody());
            dialog.show(getFragmentManager(),"TEST");
        }


    }

    private void updateAccountAndNotify(Account account) {
        Account.getInstance(account);
        app.setLoggedIn(account);
        EventBus.getDefault().post(new ViewPagerFragmentSwitchEvent(PersonalizeFragment.FRAGMENT_SWITCH_LOGIN_SUCCESS));
        EventBus.getDefault().post(new GenBooleanEvent(true));
        FormUtils.clearForm(loginForm);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        callbackManager.onActivityResult(requestCode, resultCode, data);
    }


}
