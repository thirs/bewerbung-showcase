package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Teo on 9.1.2016..
 */
public class AccountLogin implements Parcelable {
    String username;
    String password;

    public AccountLogin(String username, String password) {
        this.username = username;
        this.password = password;
    }

    public String getEmail() {
        return username;
    }

    public void setEmail(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    protected AccountLogin(Parcel in) {
        username = in.readString();
        password = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(username);
        dest.writeString(password);
    }

    @SuppressWarnings("unused")
    public static final Creator<AccountLogin> CREATOR = new Creator<AccountLogin>() {
        @Override
        public AccountLogin createFromParcel(Parcel in) {
            return new AccountLogin(in);
        }

        @Override
        public AccountLogin[] newArray(int size) {
            return new AccountLogin[size];
        }
    };
}