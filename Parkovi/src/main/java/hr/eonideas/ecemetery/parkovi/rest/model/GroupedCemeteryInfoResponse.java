package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teo on 8.6.2016..
 */
public class GroupedCemeteryInfoResponse implements Parcelable {

    List<GroupedCemeteryInfo> groupedCemeteryInfos;

    public List<GroupedCemeteryInfo> getGroupedCemeteryInfos() {
        return groupedCemeteryInfos;
    }

    public void setGroupedCemeteryInfos(List<GroupedCemeteryInfo> groupedCemeteryInfos) {
        this.groupedCemeteryInfos = groupedCemeteryInfos;
    }

    protected GroupedCemeteryInfoResponse(Parcel in) {
        if (in.readByte() == 0x01) {
            groupedCemeteryInfos = new ArrayList<GroupedCemeteryInfo>();
            in.readList(groupedCemeteryInfos, GroupedCemeteryInfo.class.getClassLoader());
        } else {
            groupedCemeteryInfos = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (groupedCemeteryInfos == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(groupedCemeteryInfos);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GroupedCemeteryInfoResponse> CREATOR = new Parcelable.Creator<GroupedCemeteryInfoResponse>() {
        @Override
        public GroupedCemeteryInfoResponse createFromParcel(Parcel in) {
            return new GroupedCemeteryInfoResponse(in);
        }

        @Override
        public GroupedCemeteryInfoResponse[] newArray(int size) {
            return new GroupedCemeteryInfoResponse[size];
        }
    };
}
