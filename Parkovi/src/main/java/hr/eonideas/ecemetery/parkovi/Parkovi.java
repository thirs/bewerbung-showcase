package hr.eonideas.ecemetery.parkovi;

import android.content.Context;
import android.content.SharedPreferences;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.content.pm.Signature;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.graphics.Bitmap;
import android.support.multidex.MultiDexApplication;
import android.util.Base64;
import android.util.DisplayMetrics;
import android.util.Log;

import com.crashlytics.android.Crashlytics;
import com.facebook.FacebookSdk;
import com.facebook.appevents.AppEventsLogger;
import com.nostra13.universalimageloader.core.DisplayImageOptions;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;
import com.nostra13.universalimageloader.utils.DiskCacheUtils;
import com.nostra13.universalimageloader.utils.MemoryCacheUtils;
import com.squareup.leakcanary.LeakCanary;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.util.Date;
import java.util.Locale;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.interfaces.IApplicationProxy;
import hr.eonideas.ecemetery.commons.mediahelper.SecureImageDownloader;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.personalization.register.PersonalizeRegisterFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import io.fabric.sdk.android.Fabric;

/**
 * Created by thirs on 17.10.2015..
 */
//public class Parkovi extends Application implements IApplicationProxy {
public class Parkovi extends MultiDexApplication implements IApplicationProxy {
    private static final String TAG = "ApplicationParkovi";

    private static final String APP_LANGUAGE = "APP_LANGUAGE";
    public static final String LOGGED_IN_WITH_TOKEN = "logintoken";
    public static final String LOGGED_IN_WITH_ID = "loginid";
    private static final String CACHE_CREATION_TIME = "cachecreationtime";
    private SharedPreferences sharedPref;
    private RestControler lc;



    @Override
    public void onCreate() {
        super.onCreate();
        if(Filter.isDebuggingMode){
            Fabric.with(this, new Crashlytics());
            LeakCanary.install(this);
        }

        ClassResolver.getInstance(getApplicationContext());
        initImageLoader();
        initSharedPreferences();
        updateLocale();
        if(Filter.isDebuggingMode){
            printKeyHash(getApplicationContext());
        }
        facebookInit();
    }

    private void facebookInit() {
        // Initialize the SDK before executing any other operations,
        FacebookSdk.sdkInitialize(getApplicationContext());
        AppEventsLogger.activateApp(this);
    }


    public static String printKeyHash(Context context) {
        PackageInfo packageInfo;
        String key = null;
        try {
            //getting application package name, as defined in manifest
            String packageName = context.getPackageName();

            //Retriving package info
            packageInfo = context.getPackageManager().getPackageInfo(packageName,
                    PackageManager.GET_SIGNATURES);

            Log.e("Package Name=", context.getPackageName());

            for (Signature signature : packageInfo.signatures) {
                MessageDigest md = MessageDigest.getInstance("SHA");
                md.update(signature.toByteArray());
                key = new String(Base64.encode(md.digest(), 0));

                // String key = new String(Base64.encodeBytes(md.digest()));
                Log.e("Key Hash=", key);
            }
        } catch (PackageManager.NameNotFoundException e1) {
            Log.e("Name not found", e1.toString());
        }
        catch (NoSuchAlgorithmException e) {
            Log.e("No such an algorithm", e.toString());
        } catch (Exception e) {
            Log.e("Exception", e.toString());
        }

        return key;
    }

    private void initSharedPreferences() {
        Context context = getApplicationContext();
        sharedPref = context.getSharedPreferences(DEFAULT_SHARED_PREFERENCES_FNAME, Context.MODE_PRIVATE);
    }

    @Override
    public boolean isLoggedIn(){
        String token = getSharedPreferences().getString(LOGGED_IN_WITH_TOKEN, null);
        return token != null;
    }

    @Override
    public void setLoggedIn(Account account) {
        getSharedPreferences().edit().putString(LOGGED_IN_WITH_TOKEN, account.getAuthToken()).commit();
        getSharedPreferences().edit().putInt(LOGGED_IN_WITH_ID, account.getId()).commit();
        if(!isRegistered())
            getSharedPreferences().edit().putBoolean(PersonalizeRegisterFragment.REGISTRATION_PERFORMED, true).commit();
    }

    @Override
    public boolean isRegistered() {
        boolean isRegistered = getSharedPreferences().getBoolean(PersonalizeRegisterFragment.REGISTRATION_PERFORMED, false);
        return isRegistered;
    }

    @Override
    public SharedPreferences getSharedPreferences() {
        if(sharedPref == null)
            initSharedPreferences();
        return sharedPref;
    }

    @Override
    public Account getUserAccount(){
        Account account = Account.getInstance(null);
        return account;
    }

    public void distroyAccountPreferences(){
        getSharedPreferences().edit().remove(LOGGED_IN_WITH_TOKEN).commit();
        getSharedPreferences().edit().remove(LOGGED_IN_WITH_ID).commit();
    }

    private void initImageLoader(){
        //todo - optimizirati cashe ... potencijalni "out of memory"
        DisplayImageOptions defaultOptions = new DisplayImageOptions.Builder()
                .cacheInMemory(true)
                .cacheOnDisc(true)
                .bitmapConfig(Bitmap.Config.RGB_565)
                .build();

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(getApplicationContext())
                .defaultDisplayImageOptions(defaultOptions)
                .imageDownloader(new SecureImageDownloader(getApplicationContext(),Filter.CONNECT_TIMEOUT * 1000, Filter.READ_TIMEOUT * 1000))
                .build();
        ImageLoader il = ImageLoader.getInstance();
        il.init(config);
        maintainImageLoaderCache();
    }

    private void maintainImageLoaderCache() {
        Date currentTime = Utils.getCurrentDate();
        if(getSharedPreferences().contains(CACHE_CREATION_TIME)){
            Date cacheCreationTime = new Date(getSharedPreferences().getLong(CACHE_CREATION_TIME, -1));
            if(Utils.getHoursBetweenDates(currentTime, cacheCreationTime) > Filter.CACHE_VALIDITY_IN_HOURS){
                clearImageLoaderCache();
                Log.i(TAG, "Cache validty expired and was cleared. Cache cleared after "+ Utils.getHoursBetweenDates(currentTime, cacheCreationTime) +" hours");
                getSharedPreferences().edit().putLong(CACHE_CREATION_TIME,currentTime.getTime()).commit();
            }else{
                Log.i(TAG, "Cache still valid." + Utils.getHoursBetweenDates(currentTime, cacheCreationTime) + " hours passed after cache was create");
            }
        }else{
            Log.i(TAG, "Cache first time initialized");
            getSharedPreferences().edit().putLong(CACHE_CREATION_TIME,currentTime.getTime()).commit();
        }
    }

    public void clearImageLoaderCache(){
        ImageLoader il = ImageLoader.getInstance();
        il.clearDiscCache();
        il.clearMemoryCache();
    }

    @Override
    public void clearImageLoaderCache(String uri) {
        ImageLoader il = ImageLoader.getInstance();
        MemoryCacheUtils.removeFromCache(uri,il.getMemoryCache());
        DiskCacheUtils.removeFromCache(uri, il.getDiskCache());

    }

    public void updateLocale() {
        Resources res = getResources();
        DisplayMetrics dm = res.getDisplayMetrics();
        Configuration conf = res.getConfiguration();
        conf.locale = new Locale(getAppLanguage());
        res.updateConfiguration(conf, dm);
    }

    @Override
    public void setAppLanguage(String languageLocal) {
        getSharedPreferences().edit().putString(APP_LANGUAGE,languageLocal).commit();
    }

    @Override
    public String getAppLanguage() {
        return getSharedPreferences().getString(APP_LANGUAGE, Filter.default_locale.getLanguage());
    }
}
