package hr.eonideas.ecemetery.parkovi.commons;

import android.content.Context;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.busevents.ActivityTitleEvent;
import hr.eonideas.ecemetery.commons.busevents.DrawerOpenEvent;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.GalleryViewPagerAdapter;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;

public class ImageViewPager extends BaseActivity {

    public static final String IMAGE_INDEX = Filter.PACKAGE + ".IMAGE_INDEX";
    /**
     * The {@link android.support.v4.view.PagerAdapter} that will provide
     * fragments for each of the sections. We use a
     * {@link FragmentPagerAdapter} derivative, which will keep every
     * loaded fragment in memory. If this becomes too memory intensive, it
     * may be best to switch to a
     * {@link android.support.v4.app.FragmentStatePagerAdapter}.
     */
    private GalleryViewPagerAdapter mSectionsPagerAdapter;
    private ArrayList<Image> imageGalleryList;
    private int imageIndex;
    private ViewPager mViewPager;

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.about_gallery_full_screen_image_activity);
        imageGalleryList = getIntent().getParcelableArrayListExtra(ImageAlbumActivity.ALBUM_IMAGES);
        imageIndex = getIntent().getIntExtra(IMAGE_INDEX, 0);

        // Create the adapter that will return a fragment for each of the three
        // primary sections of the activity.
        mSectionsPagerAdapter = new GalleryViewPagerAdapter(getSupportFragmentManager(), imageGalleryList);

        // Set up the ViewPager with the sections adapter.
        mViewPager = (ViewPager) findViewById(R.id.container);
        mViewPager.setAdapter(mSectionsPagerAdapter);
        mViewPager.setCurrentItem(imageIndex);
    }

    public void onEvent(ActivityTitleEvent event){
        setActivityTitle(mViewPager.getCurrentItem()+1 + "/" + imageGalleryList.size());
    }

    private void setActivityTitle(String title) {
        initActionBar(title);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class FullScreenFragment extends BasicPagerFragment {
        /**
         * The fragment argument representing the section number for this
         * fragment.
         */
        private static final String ARG_POSITION_NUMBER = Filter.PACKAGE + ".ARG_SECTION_NUMBER";
        private static final String ARG_IMAGES_LIST = Filter.PACKAGE + ".ARG_IMAGES_LIST";
        private static final String ARG_IMAGE_URI_LIST = Filter.PACKAGE + ".ARG_IMAGE_URI_LIST";
        private ImageLoader imageLoader;
        private List<Image> imageGalleryList;
        private List<Uri> imageUriList;
        private int sectionNumber;
        private ViewPagerClickListener mListener;

        /**
         * Returns a new instance of this fragment for the given section
         * number.
         */
        public static FullScreenFragment newInstance(int position, List<Image> images, List<Uri>imageUris) {
            FullScreenFragment fragment = new FullScreenFragment();
            Bundle args = new Bundle();
            args.putInt(ARG_POSITION_NUMBER, position);
            args.putParcelableArrayList(ARG_IMAGES_LIST, new ArrayList<Image>(images));
            if(imageUris != null)
                args.putParcelableArrayList(ARG_IMAGE_URI_LIST, new ArrayList<Uri>(imageUris));
            fragment.setArguments(args);
            return fragment;
        }

        public FullScreenFragment() {
        }

        public void setViewPagerListener(ViewPagerClickListener mListener) {
            this.mListener = mListener;
        }

        @Override
        protected void isVisibleToUser() {
            super.isVisibleToUser();
            EventBus.getDefault().post(new ActivityTitleEvent(""));
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_image_full_screen_activity, container, false);
            imageGalleryList = getArguments().getParcelableArrayList(ARG_IMAGES_LIST);
            imageUriList = getArguments().getParcelableArrayList(ARG_IMAGE_URI_LIST);
            sectionNumber = getArguments().getInt(ARG_POSITION_NUMBER);
            imageLoader = ImageLoader.getInstance();
            hr.eonideas.ecemetery.commons.widgets.ZoomableImageView imageView = (hr.eonideas.ecemetery.commons.widgets.ZoomableImageView) rootView.findViewById(R.id.fullSizeImage);
            if(imageUriList != null && imageUriList.size()>0){
                if(sectionNumber < imageUriList.size()){//prikazati dodane slike iz galerije
                    imageLoader.displayImage(imageUriList.get(sectionNumber).getPath(), imageView);
                }else{//prikazati postojeće slike sa servera
                    imageLoader.displayImage(imageGalleryList.get(sectionNumber - imageUriList.size()).getFullUrl(), imageView);
                }
            }else{
                imageLoader.displayImage(imageGalleryList.get(sectionNumber).getFullUrl(), imageView);
            }
            imageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (mListener != null) {
                        mListener.onItemClick(sectionNumber);
                    }
                }
            });
            return rootView;
        }
    }

    public interface ViewPagerClickListener{
        public void onItemClick(int position);
    }
}
