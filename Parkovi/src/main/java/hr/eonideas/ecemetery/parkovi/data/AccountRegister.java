package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Teo on 9.1.2016..
 */
public class AccountRegister implements Parcelable {
    String firstName;
    String lastName;
    String email;
    String password;

    public AccountRegister(String firstName, String lastName, String email, String password) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.password = password;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    protected AccountRegister(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        password = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeString(password);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AccountRegister> CREATOR = new Parcelable.Creator<AccountRegister>() {
        @Override
        public AccountRegister createFromParcel(Parcel in) {
            return new AccountRegister(in);
        }

        @Override
        public AccountRegister[] newArray(int size) {
            return new AccountRegister[size];
        }
    };
}