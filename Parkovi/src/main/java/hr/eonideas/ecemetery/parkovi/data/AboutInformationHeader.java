package hr.eonideas.ecemetery.parkovi.data;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryInfo;

/**
 * Created by Teo on 5.6.2016..
 */
public class AboutInformationHeader implements ParentListItem {

    private List<CemeteryInfo> mChildrenList;
    private String title;

    public AboutInformationHeader(List<CemeteryInfo> mChildrenList) {
        this.mChildrenList = mChildrenList;
    }

    public AboutInformationHeader(List<CemeteryInfo> mChildrenList, String headerTitle) {
        this(mChildrenList);
        this.title = headerTitle;
    }

    public String getTitle() {
        return title;
    }

    @Override
    public List<CemeteryInfo> getChildItemList() {
        return mChildrenList;
    }

    @Override
    public boolean isInitiallyExpanded() {
        return false;
    }

}
