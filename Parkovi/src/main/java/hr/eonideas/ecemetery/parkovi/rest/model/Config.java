package hr.eonideas.ecemetery.parkovi.rest.model;


import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import android.os.Parcelable;


public class Config implements Parcelable {
  
  @SerializedName("infoSectionType")
  private String infoSectionType = null;
  @SerializedName("infoSectionTitle")
  private String infoSectionTitle = null;
  @SerializedName("mainMenuDefaultSearchTag")
  private String mainMenuDefaultSearchTag = null;

  
  /**
   * Type of info section to display (calendar, tagged_profiles, top_advertisers, greetings).
   **/
  public String getInfoSectionType() {
    return infoSectionType;
  }
  public void setInfoSectionType(String infoSectionType) {
    this.infoSectionType = infoSectionType;
  }

  
  /**
   * Title of info section.
   **/
  public String getInfoSectionTitle() {
    return infoSectionTitle;
  }
  public void setInfoSectionTitle(String infoSectionTitle) {
    this.infoSectionTitle = infoSectionTitle;
  }

  
  /**
   * Default tag used for searching profiles.
   **/
  public String getMainMenuDefaultSearchTag() {
    return mainMenuDefaultSearchTag;
  }
  public void setMainMenuDefaultSearchTag(String mainMenuDefaultSearchTag) {
    this.mainMenuDefaultSearchTag = mainMenuDefaultSearchTag;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Config {\n");
    
    sb.append("  infoSectionType: ").append(infoSectionType).append("\n");
    sb.append("  infoSectionTitle: ").append(infoSectionTitle).append("\n");
    sb.append("  mainMenuDefaultSearchTag: ").append(mainMenuDefaultSearchTag).append("\n");
    sb.append("}\n");
    return sb.toString();
  }

    protected Config(Parcel in) {
        infoSectionType = in.readString();
        infoSectionTitle = in.readString();
        mainMenuDefaultSearchTag = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(infoSectionType);
        dest.writeString(infoSectionTitle);
        dest.writeString(mainMenuDefaultSearchTag);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Config> CREATOR = new Parcelable.Creator<Config>() {
        @Override
        public Config createFromParcel(Parcel in) {
            return new Config(in);
        }

        @Override
        public Config[] newArray(int size) {
            return new Config[size];
        }
    };
}