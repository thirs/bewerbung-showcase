package hr.eonideas.ecemetery.parkovi.rest.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import hr.eonideas.ecemetery.parkovi.interfaces.IBurial;

public class Burial implements Parcelable, IBurial {

    @SerializedName("id")
    private Integer id = null;
    @SerializedName("fullName")
    private String fullName = null;
    @SerializedName("maidenName")
    private String maidenName = null;
    @SerializedName("age")
    private Integer age = null;
    @SerializedName("cemeteryName")
    private String cemeteryName = null;
    @SerializedName("burialDate")
    private String burialDate = null;

    public Burial(Integer id, String fullName, String maidenName, Integer age, String cemeteryName, String burialDate) {
        this.id = id;
        this.fullName = fullName;
        this.maidenName = maidenName;
        this.age = age;
        this.cemeteryName = cemeteryName;
        this.burialDate = burialDate;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return fullName;
    }

    public void setFullName(String fullName) {
        this.fullName = fullName;
    }

    public String getMaidenName() {
        return maidenName;
    }

    public void setMaidenName(String maidenName) {
        this.maidenName = maidenName;
    }

    public Integer getAge() {
        return age;
    }

    public void setAge(Integer age) {
        this.age = age;
    }

    public String getLocation() {
        return cemeteryName;
    }

    public void setCemeteryName(String cemeteryName) {
        this.cemeteryName = cemeteryName;
    }

    public String getTime() {
        return burialDate;
    }

    public void setBurialDate(String burialDate) {
        this.burialDate = burialDate;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Burial {\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("  fullName: ").append(fullName).append("\n");
        sb.append("  maidenName: ").append(maidenName).append("\n");
        sb.append("  age: ").append(age).append("\n");
        sb.append("  cemeteryName: ").append(cemeteryName).append("\n");
        sb.append("  burialDate: ").append(burialDate).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected Burial(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        fullName = in.readString();
        maidenName = in.readString();
        age = in.readByte() == 0x00 ? null : in.readInt();
        cemeteryName = in.readString();
        burialDate = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(fullName);
        dest.writeString(maidenName);
        if (age == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(age);
        }
        dest.writeString(cemeteryName);
        dest.writeString(burialDate);
    }

    @SuppressWarnings("unused")
    public static final Creator<Burial> CREATOR = new Creator<Burial>() {
        @Override
        public Burial createFromParcel(Parcel in) {
            return new Burial(in);
        }

        @Override
        public Burial[] newArray(int size) {
            return new Burial[size];
        }
    };
}