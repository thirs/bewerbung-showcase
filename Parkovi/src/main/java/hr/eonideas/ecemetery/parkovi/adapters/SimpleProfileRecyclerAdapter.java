package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collection;
import java.util.Collections;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;

/**
 * Created by Teo on 10.2.2016..
 */
public class SimpleProfileRecyclerAdapter extends RecyclerView.Adapter<SimpleProfileRecyclerAdapter.ProfileViewHolder> {

    private static final String TAG = "SimpleProfileRecyclerAdapter";
    private ImageLoader imageLoader;
    private List<IProfile> profiles;
    private Context context;
    private IOnProfileClickListener mListener;

    public SimpleProfileRecyclerAdapter(List<IProfile> profiles) {
        this.profiles = profiles;
        this.imageLoader = ImageLoader.getInstance();
    }

    public void setOnImageClickListener(IOnProfileClickListener listener){
        this.mListener = listener;
    }

    public void orderProfileList(){
        if(profiles.size() > 1){
            Collections.sort(profiles);
        }else if(profiles.size() == 1){
            profiles.get(0).compareTo(profiles.get(0));
        }
        if(Filter.isDebuggingMode){
            //Logg our new Profile order
            for(IProfile profile : profiles){
                Log.d(TAG, ProfileUtils.getProfileName(profile) + ". Born "+ profile.getBirth() +"; Died "+ profile.getDeath() +";");
            }
        }
    }

    @Override
    public ProfileViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new ProfileViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.profile_anniversary_view, parent, false));
    }

    @Override
    public void onBindViewHolder(ProfileViewHolder holder, int position) {
        IProfile profileItem = null;
        holder.profileImage.setImageDrawable(null);
        profileItem = profiles.get(position);
        if (profileItem != null){
            if(profileItem.getImage() !=null){
                imageLoader.displayImage(profileItem.getImage().getThumbUrl(), holder.profileImage);
            }else{
                holder.profileImage.setImageResource(R.drawable.placeholder_image);
            }
            holder.profileName.setText(ProfileUtils.getProfileName(profileItem));
            if(profileItem.getProfileAnniversary().isHasAnniversary()){
                holder.anniversaryTypeIcon.setVisibility(View.VISIBLE);
                holder.anniversaryDate.setVisibility(View.VISIBLE);
                holder.anniversaryTypeIcon.setImageResource(profileItem.getProfileAnniversary().isHasBirthAnniversary() ? R.drawable.cradle_green : R.drawable.icon_cross_green);
                holder.anniversaryDate.setText(profileItem.getProfileAnniversary().isHasBirthAnniversary() ? profileItem.getBirth() : profileItem.getDeath());
            }else{
                holder.anniversaryTypeIcon.setVisibility(View.GONE);
                holder.anniversaryDate.setVisibility(View.GONE);
            }
        }
        holder.profileAnniversary.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IProfile p = (IProfile) v.getTag();
                if (mListener != null) {
                    mListener.onProfileClicked(p);
                }

            }
        });
        holder.profileAnniversary.setTag(profileItem);

    }

    @Override
    public int getItemCount() {
        int count = profiles.size();
        return count;
    }


    public static class ProfileViewHolder extends RecyclerView.ViewHolder {
        public View profileAnniversary;
        public ImageView profileImage;
        public TextView profileName;
        public View anniversary_section;
        public ImageView anniversaryTypeIcon;
        public TextView anniversaryDate;

        public ProfileViewHolder(View itemView) {
            super(itemView);
            profileAnniversary = itemView.findViewById(R.id.profile_anniversary);
            profileImage = (ImageView) itemView.findViewById(R.id.profile_avatar);
            profileName = (TextView) itemView.findViewById(R.id.profile_name);
            anniversary_section = itemView.findViewById(R.id.anniversary_section);
            anniversaryTypeIcon = (ImageView) itemView.findViewById(R.id.anniversary_type_symbol);
            anniversaryDate = (TextView) itemView.findViewById(R.id.anniversary_date);
        }
    }

    public interface IOnProfileClickListener{
        public void onProfileClicked(IProfile profile);
    }
}
