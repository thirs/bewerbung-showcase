package hr.eonideas.ecemetery.parkovi.rest.model;


import com.google.gson.annotations.SerializedName;
import android.os.Parcel;
import android.os.Parcelable;


public class Image implements Parcelable {
  
  @SerializedName("thumbUrl")
  private String thumbUrl = null;
  @SerializedName("fullUrl")
  private String fullUrl = null;

  
  /**
   * Thumbnail image url.
   **/
  public String getThumbUrl() {
    return thumbUrl;
  }
  public void setThumbUrl(String thumbUrl) {
    this.thumbUrl = thumbUrl;
  }

  
  /**
   * Full image url.
   **/
  public String getFullUrl() {
    return fullUrl;
  }
  public void setFullUrl(String fullUrl) {
    this.fullUrl = fullUrl;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Image {\n");
    
    sb.append("  thumbUrl: ").append(thumbUrl).append("\n");
    sb.append("  fullUrl: ").append(fullUrl).append("\n");
    sb.append("}\n");
    return sb.toString();
  }

    public Image(String fullUrl, String thumbUrl) {
        this.fullUrl = fullUrl;
        this.thumbUrl = thumbUrl;
    }

    protected Image(Parcel in) {
        thumbUrl = in.readString();
        fullUrl = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(thumbUrl);
        dest.writeString(fullUrl);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Image> CREATOR = new Parcelable.Creator<Image>() {
        @Override
        public Image createFromParcel(Parcel in) {
            return new Image(in);
        }

        @Override
        public Image[] newArray(int size) {
            return new Image[size];
        }
    };
}