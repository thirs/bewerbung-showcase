package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 2.1.2016..
 */
public class AccountLight implements Parcelable {

    @SerializedName("id")
    private Integer id = null;

    @SerializedName("firstName")
    private String firstName = null;

    @SerializedName("lastName")
    private String lastName = null;

    @SerializedName("email")
    private String email = null;

    @SerializedName("image")
    private Image image = null;

    public AccountLight(Integer id, String firstName, String lastName, String email, Image image) {
        this.id = id;
        this.firstName = firstName;
        this.lastName = lastName;
        this.email = email;
        this.image = image;
    }

    /**
     * Unique identifier of memory.
     **/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Account's first name.
     **/
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    /**
     * Account's last name.
     **/
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    /**
     * Account's email.
     **/
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    /**
     **/
    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public String getFullName() {
        return this.firstName + " " + this.lastName;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AccountResource {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  firstName: ").append(firstName).append("\n");
        sb.append("  lastName: ").append(lastName).append("\n");
        sb.append("  image: ").append(image).append("\n");
        sb.append("  email: ").append(email).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected AccountLight(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        image = (Image) in.readValue(Image.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeValue(image);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AccountLight> CREATOR = new Parcelable.Creator<AccountLight>() {
        @Override
        public AccountLight createFromParcel(Parcel in) {
            return new AccountLight(in);
        }

        @Override
        public AccountLight[] newArray(int size) {
            return new AccountLight[size];
        }
    };
}