package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Adapter.ExpandableRecyclerAdapter;
import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;
import com.bignerdranch.expandablerecyclerview.Model.ParentWrapper;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ChildViewHolder;
import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.about.information.AboutMapFooterFragment;
import hr.eonideas.ecemetery.parkovi.data.AboutInformationHeader;
import hr.eonideas.ecemetery.parkovi.data.AboutInformationType;
import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;
import hr.eonideas.ecemetery.parkovi.interfaces.IGroupedAboutInformationItem;

/**
 * Created by Teo on 17.1.2016..
 */
public class IconicAboutAdapter extends ExpandableRecyclerAdapter<IconicAboutAdapter.VHItemHeader, ChildViewHolder> implements ExpandableRecyclerAdapter.ExpandCollapseListener, View.OnClickListener {
    private static final int TYPE_ITEM_MAP   = 3;

    private Context context;
    private List<ParentListItem> listParentItems;
    private View.OnClickListener listener;
    private FragmentManager fm;
    private int expandedHeader = -1;

    public IconicAboutAdapter(Context context, List<ParentListItem> listParentItems, View.OnClickListener listener) {
        super(listParentItems);
        this.listParentItems = listParentItems;
        this.context = context;
        this.listener = listener;
    }

    public IconicAboutAdapter(Context context, List<ParentListItem> listParentItems, View.OnClickListener listener, FragmentManager fm) {
        this(context, listParentItems, listener);
        setExpandCollapseListener(this);
        this.fm = fm;
    }

    @Override
    public VHItemHeader onCreateParentViewHolder(ViewGroup parentViewGroup) {
        View view = LayoutInflater.from(parentViewGroup.getContext()).inflate(R.layout.contact_information_item_header, parentViewGroup, false);
        return new VHItemHeader(view);
    }

    @Override
    public VHItem onCreateChildViewHolder(ViewGroup childViewGroup) {
        View v = LayoutInflater.from(childViewGroup.getContext()).inflate(R.layout.contact_information_item, childViewGroup, false);
        if(listener != null){
            v.setOnClickListener(listener);
        }else{
            v.setOnClickListener(this);
        }
        return new VHItem(v);
    }

    @Override
    public void onBindParentViewHolder(VHItemHeader parentViewHolder, int position, ParentListItem parentListItem) {
        if(parentListItem instanceof AboutInformationHeader){
            AboutInformationHeader informationHeader = (AboutInformationHeader) parentListItem;
            parentViewHolder.about_information_item_header.setText(informationHeader.getTitle());
        }else{
            parentViewHolder.about_information_item_header.setText("");
        }
    }

    @Override
    public void onBindChildViewHolder(ChildViewHolder childViewHolder, int position, Object childListItem) {
            //todo ---> castanje Object u IAboutInformationItem!!!
            IAboutInformationItem currentItem = (IAboutInformationItem) childListItem;
            if(currentItem.getInfoType() != AboutInformationType.TYPE_MAP && currentItem.getInfoType() != null){
                VHItem VHitem = (VHItem) childViewHolder;
                VHitem.about_information_container.setTag(position - expandedHeader - 1); //to je nacin na koji taj expandable adapter salje position
                VHitem.about_information_label.setText(currentItem.getInfoLabel());
                VHitem.about_information_value.setText(currentItem.getInfoValue());
                if (currentItem.getInfoType().equals(AboutInformationType.TYPE_ADDRESS)) {
                    VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_home));
                } else if (currentItem.getInfoType().equals(AboutInformationType.TYPE_EMAIL)) {
                    VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_mail));
                } else if (currentItem.getInfoType().equals(AboutInformationType.TYPE_MOBILE_PHONE)) {
                    VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_mobile));
                } else if (currentItem.getInfoType().equals(AboutInformationType.TYPE_PHONE)) {
                    VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_phone));
                } else if (currentItem.getInfoType().equals(AboutInformationType.TYPE_WEB)) {
                    VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_www));
                } else if (currentItem.getInfoType().equals(AboutInformationType.TYPE_WORKING_TIME)) {
                    VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_workinghours));
                }
            }
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        Object listItem = getListItem(position);
        //ako nije headeritem i ako je trenutno expandani MAP header item koji je zadnji
        if (!(listItem instanceof ParentWrapper) && (expandedHeader == (listParentItems.size()-1))) {
            return TYPE_ITEM_MAP;
        }else{
            return super.getItemViewType(position);
        }
    }

    public int getExpandedHeader(){
        return expandedHeader;
    }

    @Override
    public void expandParent(int parentIndex) {
        super.expandParent(parentIndex);
        this.expandedHeader = parentIndex;
    }

    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup viewGroup, int viewType) {
        if (viewType == TYPE_ITEM_MAP) {
            View view = LayoutInflater.from(viewGroup.getContext()).inflate(R.layout.about_information_fragment, viewGroup, false);
            return new VHFooter(view);
        } else {
            return super.onCreateViewHolder(viewGroup,viewType);
        }
    }


    public class VHFooter extends ChildViewHolder {
        public VHFooter(View view) {
            super(view);
            Fragment fragment = (Fragment) fm.findFragmentByTag(AboutMapFooterFragment.TAG);
            if (fragment == null) {
                fragment = AboutMapFooterFragment.newInstance();
            }
            fm.beginTransaction().add(R.id.mapFragmentContainer, fragment, AboutMapFooterFragment.TAG).addToBackStack(AboutMapFooterFragment.TAG).commit();
        }
    }

     class VHItem extends ChildViewHolder {
         protected TextView about_information_label;
         protected TextView about_information_value;
         protected ImageView about_information_icon;
         protected View about_information_container;

        public VHItem(View itemView) {
            super(itemView);
            about_information_label = (TextView) itemView.findViewById(R.id.about_information_label);
            about_information_value = (TextView) itemView.findViewById(R.id.about_information_value);
            about_information_icon = (ImageView) itemView.findViewById(R.id.about_information_icon);
            about_information_container = itemView.findViewById(R.id.contact_info_container);
        }
    }

    class VHItemHeader extends ParentViewHolder {
        protected TextView about_information_item_header;

        public VHItemHeader(View itemView) {
            super(itemView);
            about_information_item_header = (TextView) itemView.findViewById(R.id.information_header);
        }
    }

    @Override
    public void onListItemExpanded(int position) {
        expandedHeader = position;
        int numberOfHeaders = listParentItems.size();
        if(numberOfHeaders > 1){
            for (int i = 0; i < numberOfHeaders; i++){
                if(position == i){
                    continue;
                }else{
                    collapseParent(i);
                }
            }
        }
    }

    @Override
    public void onListItemCollapsed(int position) {

    }


    // TODO: 8.6.2016. prebaciti onClick listener u adapter tako da se to maintaina samo na jendom mjestu  
    @Override
    public void onClick(View v) {
        int expandedHeader = getExpandedHeader();
        if(v.getTag() == null || expandedHeader == -1 || context == null ){
            return;
        }
        int itemPosition = (int) v.getTag();
        if(Filter.isDebuggingMode){
            Toast.makeText(context,"Postion is:" + itemPosition,Toast.LENGTH_SHORT).show();
        }
        IAboutInformationItem item = ((IGroupedAboutInformationItem)listParentItems.get(expandedHeader)).getItems().get(itemPosition);
        if(item.getInfoType().equals(AboutInformationType.TYPE_ADDRESS)){
            Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + item.getInfoValue());
            Intent intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            context.startActivity(intent);
        }
        else if(item.getInfoType().equals(AboutInformationType.TYPE_EMAIL)){
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto",item.getInfoValue(), null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
            context.startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
        else if(item.getInfoType().equals(AboutInformationType.TYPE_MOBILE_PHONE) || item.getInfoType().equals(AboutInformationType.TYPE_PHONE)){
            Intent intent = new Intent();
            intent.setAction("android.intent.action.DIAL");
            intent.setData(Uri.parse("tel:"+item.getInfoValue()));
            context.startActivity(intent);
        }
        else if(item.getInfoType().equals(AboutInformationType.TYPE_WEB)){
            String url = item.getInfoValue().startsWith("http://") && !item.getInfoValue().startsWith("https://"    ) ? item.getInfoValue() : "http://" + item.getInfoValue();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            context.startActivity(intent);
        }
    }


}
