package hr.eonideas.ecemetery.parkovi.rest.model;

import java.util.*;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;


public class Gallery implements Parcelable {
  
  @SerializedName("image")
  private Image image = null;
  @SerializedName("title")
  private String title = null;
  @SerializedName("description")
  private String description = null;
  @SerializedName("items")
  private List<GalleryItem> items = null;

  
  /**
   **/
  public Image getImage() {
    return image;
  }
  public void setImage(Image image) {
    this.image = image;
  }

  
  /**
   * Gallery title.
   **/
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  
  /**
   * Gallery title.
   **/
  public String getDescription() {
    return description;
  }
  public void setDescription(String description) {
    this.description = description;
  }

  
  /**
   **/
  public List<GalleryItem> getItems() {
    return items;
  }
  public void setItems(List<GalleryItem> items) {
    this.items = items;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Gallery {\n");
    
    sb.append("  image: ").append(image).append("\n");
    sb.append("  title: ").append(title).append("\n");
    sb.append("  description: ").append(description).append("\n");
    sb.append("  items: ").append(items).append("\n");
    sb.append("}\n");
    return sb.toString();
  }

    protected Gallery(Parcel in) {
        image = (Image) in.readValue(Image.class.getClassLoader());
        title = in.readString();
        description = in.readString();
        if (in.readByte() == 0x01) {
            items = new ArrayList<GalleryItem>();
            in.readList(items, GalleryItem.class.getClassLoader());
        } else {
            items = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeValue(image);
        dest.writeString(title);
        dest.writeString(description);
        if (items == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(items);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Gallery> CREATOR = new Parcelable.Creator<Gallery>() {
        @Override
        public Gallery createFromParcel(Parcel in) {
            return new Gallery(in);
        }

        @Override
        public Gallery[] newArray(int size) {
            return new Gallery[size];
        }
    };
}