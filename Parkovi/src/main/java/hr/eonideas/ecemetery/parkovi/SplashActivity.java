package hr.eonideas.ecemetery.parkovi;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Resources;
import android.os.Bundle;
import android.view.MotionEvent;

import hr.eonideas.ecemetery.Filter;

/**
 * Created by thirs on 1.11.2015..
 */
public class SplashActivity extends Activity {

    public static final String FROM_SPLASH = Filter.PACKAGE + "FROM_SPLASH";

    /**
     * The thread to process splash screen events
     */
    private Thread mSplashThread;

    /** Called when the activity is first created. */
    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        final SplashActivity sPlashScreen = this;

        // Splash screen view
        setContentView(R.layout.splash);


        Resources res = getResources();
        final int splashDuration = res.getInteger(R.integer.splash_duration);

        // The thread to wait for splash screen events
        mSplashThread =  new Thread(){
            @Override
            public void run(){
                try {
                    synchronized(this){
                        // Wait given period of time or exit on touch
                        wait(splashDuration);
                    }
                }
                catch(InterruptedException ex){
                }

                // Run next activity
                Intent intent = new Intent();
                intent.setClass(sPlashScreen, HostActivity.class);
                intent.putExtra(FROM_SPLASH, true);
                intent.setFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
                startActivity(intent);
                finish();
            }
        };
        mSplashThread.start();

    }

    /**
     * Processes splash screen touch events
     */
    @Override
    public boolean onTouchEvent(MotionEvent evt)
    {
        if(evt.getAction() == MotionEvent.ACTION_DOWN)
        {
            synchronized(mSplashThread){
                mSplashThread.notifyAll();
            }
        }
        return true;
    }
}
