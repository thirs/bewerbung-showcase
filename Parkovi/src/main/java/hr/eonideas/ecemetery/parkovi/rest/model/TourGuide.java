package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.interfaces.ITourGuide;


public class TourGuide implements Parcelable, ITourGuide {

    @SerializedName("title")
    private String title = null;
    @SerializedName("subTitle")
    private String subTitle = null;
    @SerializedName("shortText")
    private String shortText = null;
    @SerializedName("fullText")
    private String fullText = null;
    @SerializedName("duration")
    private Integer duration = null;
    @SerializedName("distance")
    private BigDecimal distance = null;
    @SerializedName("image")
    private Image image = null;
    @SerializedName("poi")
    private List<Poi> POI = null;
    @SerializedName("galleryItems")
    private List<GalleryItem> galleryItems = null;


    /**
     * Tour guide title.
     **/
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSubTitle() {
        return subTitle;
    }

    public void setSubTitle(String subTitle) {
        subTitle = subTitle;
    }

    /**
     * Tour guide description.
     **/
    public String getShortText() {
        return shortText;
    }

    public void setShortText(String shortText) {
        this.shortText = shortText;
    }

    public BigDecimal getDistance() {
        return distance;
    }

    public void setDistance(BigDecimal distance) {
        this.distance = distance;
    }

    public Integer getDuration() {
        return duration;
    }

    public void setDuration(Integer duration) {
        this.duration = duration;
    }

    public String getFullText() {
        return fullText;
    }

    public void setFullText(String fullText) {
        this.fullText = fullText;
    }

    /**
     **/
    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }


    /**
     **/
    public List<Poi> getPOI() {
        return POI;
    }

    public void setPOI(List<Poi> POI) {
        this.POI = POI;
    }

    public List<GalleryItem> getGalleryItems() {
        return galleryItems;
    }

    public void setGalleryItems(List<GalleryItem> galleryItems) {
        this.galleryItems = galleryItems;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class TourGuide {\n");
        sb.append("  title: ").append(title).append("\n");
        sb.append("  subTitle: ").append(subTitle).append("\n");
        sb.append("  shortText: ").append(shortText).append("\n");
        sb.append("  fullText: ").append(fullText).append("\n");
        sb.append("  duration: ").append(duration).append("\n");
        sb.append("  distance: ").append(distance).append("\n");
        sb.append("  image: ").append(image).append("\n");
        sb.append("  POI: ").append(POI).append("\n");
        sb.append("  galleryItems: ").append(galleryItems).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected TourGuide(Parcel in) {
        title = in.readString();
        subTitle = in.readString();
        shortText = in.readString();
        fullText = in.readString();
        duration = in.readByte() == 0x00 ? null : in.readInt();
        distance = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        image = (Image) in.readValue(Image.class.getClassLoader());
        if (in.readByte() == 0x01) {
            POI = new ArrayList<Poi>();
            in.readList(POI, Poi.class.getClassLoader());
        } else {
            POI = null;
        }
        if (in.readByte() == 0x01) {
            galleryItems = new ArrayList<GalleryItem>();
            in.readList(galleryItems, GalleryItem.class.getClassLoader());
        } else {
            galleryItems = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(subTitle);
        dest.writeString(shortText);
        dest.writeString(fullText);
        if (duration == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(duration);
        }
        dest.writeValue(distance);
        dest.writeValue(image);
        if (POI == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(POI);
        }
        if (galleryItems == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(galleryItems);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<TourGuide> CREATOR = new Parcelable.Creator<TourGuide>() {
        @Override
        public TourGuide createFromParcel(Parcel in) {
            return new TourGuide(in);
        }

        @Override
        public TourGuide[] newArray(int size) {
            return new TourGuide[size];
        }
    };
}