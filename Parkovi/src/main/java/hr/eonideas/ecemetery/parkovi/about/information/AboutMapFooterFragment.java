package hr.eonideas.ecemetery.parkovi.about.information;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.google.android.gms.maps.MapView;
import com.google.android.gms.maps.model.LatLng;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.BasicMapFragment;

/**
 * Created by Teo on 25.5.2016..
 */
public class AboutMapFooterFragment extends BasicMapFragment {
    public static final String TAG = "BasicMapFragment";
    private MapView map;
    private View locationGallery;


    public static AboutMapFooterFragment newInstance() {
        Bundle args = new Bundle();
        AboutMapFooterFragment fragment = new AboutMapFooterFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.map_full_fragment, container, false);
        initLayout(view);
        locationGallery.setVisibility(View.GONE);
        //then move map to 'location'
        if (map != null) {
            map.onCreate(null);
            map.onResume();
            map.getMapAsync(this);
        }
        return view;
    }

    private void initLayout(View view) {
        map = (MapView) view.findViewById(R.id.mapImageView);
        locationGallery = view.findViewById(R.id.gallery_section);
    }

    @Override
    protected int enableLocationUpdates() {
        return 1;
    }

    @Override
    protected void mapLoadingError() {

    }

    @Override
    public void onLocationChanged(android.location.Location location) {
        super.onLocationChanged(location);
        Log.i(TAG,"Lokacija je updatana");
        if (currentLocation != null && !currentLocationPointed) {
            currentLocationPointed = true;
            List<LatLng> latLngs = new ArrayList<>();
            latLngs.add(new LatLng(Double.parseDouble(Filter.map_default_latitude), Double.parseDouble(Filter.map_default_longitude)));
            smartLatLngAnimateCamera(location,latLngs);
        }
    }

    @Override
    protected void setUpMap() {
        super.setUpMap();
        mMap.getUiSettings().setZoomControlsEnabled(false);
        mMap.getUiSettings().setMapToolbarEnabled(true);
        mMap.getUiSettings().setScrollGesturesEnabled(false);
    }

    @Override
    protected void mapLoadingSuccess() {
        LatLng ll = new LatLng(Double.parseDouble(Filter.map_default_latitude), Double.parseDouble(Filter.map_default_longitude));
        showPois(ll,"Groblje","Varaždinsko gradsko Groblje",R.drawable.pin_green_shadow_strong, false);
        animateCameraToPosition(ll);
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }


}
