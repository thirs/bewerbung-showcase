package hr.eonideas.ecemetery.parkovi.main;

import android.content.res.Resources;
import android.os.Bundle;
import android.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.AccountEvent;
import hr.eonideas.ecemetery.commons.busevents.GenBooleanEvent;
import hr.eonideas.ecemetery.commons.busevents.LoggedInEvent;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.HostActivity;
import hr.eonideas.ecemetery.parkovi.Parkovi;
import hr.eonideas.ecemetery.parkovi.adapters.SimpleProfileRecyclerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.personalization.login.PersonalizeLoginFragment;
import hr.eonideas.ecemetery.parkovi.profile.ProfileFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnFailedResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import retrofit.Response;

/**
 * A simple {@link Fragment} subclass.
 * Activities that contain this fragment must implement the
 * {@link MainMenuFragment.OnFragmentInteractionListener} interface
 * to handle interaction events.
 * Use the {@link MainMenuFragment#newInstance} factory method to
 * create an instance of this fragment.
 */
public class MainMenuFragment extends BasicFragment implements View.OnClickListener, IOnGetResponseListener, IOnFailedResponseListener, SimpleProfileRecyclerAdapter.IOnProfileClickListener {

    public static final String TAG = "MAIN_MENU";

    private boolean isLogged = false;
    private RestControler lc;
    private SimpleProfileRecyclerAdapter profileAnniversaryAdapter;

    private FrameLayout section_frame_4; //Memories & Favorites
    private TextView infoSectionTitle; //Memories & Favorites section title


    public static MainMenuFragment newInstance() {
        MainMenuFragment fragment = new MainMenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public MainMenuFragment() {
        // Required empty public constructor
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return getResources().getString(R.string.main_menu_title);
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(app != null && app.isLoggedIn()){
            if(Account.getInstance(null).getId() == -1){
                if(lc == null){
                    lc = new RestControler(getActivity());
                }
                lc.setOnGetResponseListener(this);
                app.getUserAccount().setAuthToken(app.getSharedPreferences().getString(Parkovi.LOGGED_IN_WITH_TOKEN, null));
                lc.recoverAccount(app.getUserAccount(), app.getSharedPreferences().getInt(Parkovi.LOGGED_IN_WITH_ID,-1));
            }else{
                isLogged = true;
            }
        }
    }

    @Override
    public void onResume() {
        super.onResume();
        int numberOfFavorites = ProfileUtils.getFavoriteProfilesFromAccount(Account.getInstance(null)).size();
        switchToPostLogin(app != null && app.isLoggedIn() && numberOfFavorites > 0);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.main_menu, container, false);

        prepareLayout(v, inflater);
        return v;
    }

    private void prepareLayout(View v, LayoutInflater inflater) {
        // ABOUT SECTION CEMETERY
        Resources resources = getActivity().getResources();
        TextView aboutSectionTitle = (TextView) v.findViewById(R.id.section_title_1);
        aboutSectionTitle.setText(resources.getString(R.string.section_title_about_cemetery));
        LinearLayout aboutSectionBlockLayout = (LinearLayout) v.findViewById(R.id.about_section_block_layout);
        if (aboutSectionBlockLayout == null) {
            // Inflate the Hidden Layout Information View
            FrameLayout section_frame_1 = (FrameLayout) v.findViewById(R.id.section_frame_1);
            View aboutSectionBlock = inflater.inflate(R.layout.mainmenu_buttons_cemetery, section_frame_1, false);
            section_frame_1.addView(aboutSectionBlock);

            section_frame_1.findViewById(R.id.about_cemetery_btn).setOnClickListener(this);
            section_frame_1.findViewById(R.id.tour_guides_btn).setOnClickListener(this);
        }

        // eSERVICES SECTION - foreseean for eServices tutorial or links to most used services
        TextView servicesSectionTitle = (TextView) v.findViewById(R.id.section_title_2);
        servicesSectionTitle.setText(resources.getString(R.string.section_title_mobile_services));
        LinearLayout eServicesSectionBlockLayout = (LinearLayout) v.findViewById(R.id.e_services_section_block_layout);
        if (eServicesSectionBlockLayout == null) {
            // Inflate the Hidden Layout Information View
            FrameLayout section_frame_2 = (FrameLayout) v.findViewById(R.id.section_frame_2);
            View eServicesSectionBlock = inflater.inflate(R.layout.mainmenu_buttons_services, section_frame_2, false);
            section_frame_2.addView(eServicesSectionBlock);

            section_frame_2.findViewById(R.id.e_services_section_block_layout).setOnClickListener(this);
//            section_frame_2.setVisibility(View.GONE);
        }
//        servicesSectionTitle.setVisibility(View.GONE);


        // ASSIST SECTION
        TextView assistSectionTitle = (TextView) v.findViewById(R.id.section_title_3);
        assistSectionTitle.setText(resources.getString(R.string.section_title_assist));
        LinearLayout assistSectionBlockLayout = (LinearLayout) v.findViewById(R.id.assist_section_block_layout);
        if (assistSectionBlockLayout == null) {
            // Inflate the Hidden Layout Information View
            FrameLayout section_frame_3 = (FrameLayout) v.findViewById(R.id.section_frame_3);
            View assistSectionBlock = inflater.inflate(R.layout.mainmenu_buttons_assist, section_frame_3, false);
            section_frame_3.addView(assistSectionBlock);

            section_frame_3.findViewById(R.id.burial_btn).setOnClickListener(this);
            section_frame_3.findViewById(R.id.emergency_btn).setOnClickListener(this);
            section_frame_3.findViewById(R.id.find_btn).setOnClickListener(this);
        }

        // INFO SECTION - foreseean for memories section (pre-login) or anniversary of favorites
        infoSectionTitle = (TextView) v.findViewById(R.id.section_title_4);
        LinearLayout memoriesSectionBlockLayout = (LinearLayout) v.findViewById(R.id.memories_section_block_layout);
        if (memoriesSectionBlockLayout == null) {
            // Inflate the Hidden Layout Information View
            section_frame_4 = (FrameLayout) v.findViewById(R.id.section_frame_4);
            View memoriesSectionBlock = inflater.inflate(R.layout.mainmenu_buttons_memories, section_frame_4, false);
            section_frame_4.addView(memoriesSectionBlock);
            int numberOfFavorites = ProfileUtils.getFavoriteProfilesFromAccount(Account.getInstance(null)).size();
            switchToPostLogin(app != null && app.isLoggedIn() && numberOfFavorites > 0);
        }
    }

    private void switchToPostLogin(boolean b) {
        if(b){
            infoSectionTitle.setText(getActivity().getResources().getString(R.string.section_title_info_logged));
            section_frame_4.findViewById(R.id.recyclerView).setVisibility(View.VISIBLE);
            section_frame_4.findViewById(R.id.memories_cemetery_btn).setVisibility(View.GONE);
            RecyclerView profileRecyclerView = (RecyclerView) section_frame_4.findViewById(R.id.recyclerView);
            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            profileRecyclerView.setLayoutManager(layoutManager);
            profileRecyclerView.setVisibility(View.VISIBLE);
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            profileAnniversaryAdapter = new SimpleProfileRecyclerAdapter(ProfileUtils.getFavoriteProfilesFromAccount(Account.getInstance(null)));
            // Set up the ViewPager with the sections adapter.
            profileAnniversaryAdapter.orderProfileList();
            profileAnniversaryAdapter.setOnImageClickListener(this);
            profileRecyclerView.setAdapter(profileAnniversaryAdapter);
        }else{
            infoSectionTitle.setText(getActivity().getResources().getString(R.string.section_title_info));
            section_frame_4.findViewById(R.id.recyclerView).setVisibility(View.GONE);
            section_frame_4.findViewById(R.id.memories_cemetery_btn).setVisibility(View.VISIBLE);
            section_frame_4.findViewById(R.id.memories_section_block_layout).setOnClickListener(this);
        }
    }

    @Override
    public void onProfileClicked(IProfile profile) {
        Bundle args = new Bundle();
        args.putParcelable(ProfileFragment.PROFILE_LIGHT, (Profile)profile);
        mListener.onFragmentInteraction(-1, ProfileFragment.class, args);
    }

    public void onEvent(LoggedInEvent loginEvent){
        switchToPostLogin(loginEvent.message);
    }


    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.about_cemetery_btn:
                mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_ABOUT, null, null);
                break;
            case R.id.tour_guides_btn:
                mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_TOURS, null, null);
                break;
            case R.id.burial_btn:
                mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_BURIALS, null, null);
                break;
            case R.id.emergency_btn:
                unavailableToast();
                break;
            case R.id.find_btn:
                mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_PROFILES, null, null);
                break;
            case R.id.e_services_section_block_layout:
                unavailableToast();
                break;
            case R.id.memories_section_block_layout:
                mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_MEMORIES, null, null);
                break;
        }

    }

    private void unavailableToast() {
        Toast.makeText(getActivity(),"The service is not available!",Toast.LENGTH_LONG).show();
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        int responseCode = response.code();
        if (responseCode == IParkoviApiService.STATUS_CODE_GENERAL_OK) {
            if(response != null && (response.body() instanceof Account)){
                Account account = ((Account) response.body());
                Account.getInstance(account);
                if(Filter.isDebuggingMode){
                    Log.i(TAG, "Account recovered for username " + account.getEmail());
                }
                isLogged = true;
                app.setLoggedIn(account);
                switchToPostLogin(true);
                EventBus.getDefault().post(new AccountEvent(account));
            }
        }else if (responseCode == IParkoviApiService.STATUS_CODE_ACCOUNT_NOT_FOUND){
            app.distroyAccountPreferences();
            Account.getInstance(null).resetAccount();
            ((HostActivity) getActivity()).updateNavigationDrawerItems(INavDrawerItem.MENU_ITEM_HOME);
        }
    }

    @Override
    public void onFailedResponse(Throwable throwable, int requestId) {
        app.distroyAccountPreferences();
        Account.getInstance(null).resetAccount();
        ((HostActivity) getActivity()).updateNavigationDrawerItems(INavDrawerItem.MENU_ITEM_HOME);
    }
}
