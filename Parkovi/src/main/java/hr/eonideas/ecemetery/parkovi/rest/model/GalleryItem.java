package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;


public class GalleryItem implements Parcelable {

    @SerializedName("id")
    private Integer id = null;
  @SerializedName("image")
  private Image image = null;
  @SerializedName("title")
  private String title = null;
  @SerializedName("index")
  private Integer index = null;

    public GalleryItem(Integer id, Image image, String title, Integer index) {
        this.id = id;
        this.image = image;
        this.title = title;
        this.index = index;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
   **/
  public Image getImage() {
    return image;
  }
  public void setImage(Image image) {
    this.image = image;
  }

  
  /**
   * Image title.
   **/
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  
  /**
   * Image position inside gallery.
   **/
  public Integer getIndex() {
    return index;
  }
  public void setIndex(Integer index) {
    this.index = index;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class GalleryItem {\n");

      sb.append("  id: ").append(id).append("\n");
    sb.append("  image: ").append(image).append("\n");
    sb.append("  title: ").append(title).append("\n");
    sb.append("  index: ").append(index).append("\n");
    sb.append("}\n");
    return sb.toString();
  }

    protected GalleryItem(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        image = (Image) in.readValue(Image.class.getClassLoader());
        title = in.readString();
        index = in.readByte() == 0x00 ? null : in.readInt();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeValue(image);
        dest.writeString(title);
        if (index == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(index);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GalleryItem> CREATOR = new Parcelable.Creator<GalleryItem>() {
        @Override
        public GalleryItem createFromParcel(Parcel in) {
            return new GalleryItem(in);
        }

        @Override
        public GalleryItem[] newArray(int size) {
            return new GalleryItem[size];
        }
    };
}