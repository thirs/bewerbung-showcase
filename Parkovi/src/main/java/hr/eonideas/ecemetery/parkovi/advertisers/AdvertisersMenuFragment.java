package hr.eonideas.ecemetery.parkovi.advertisers;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.AdvertisersCategoryAdapter;
import hr.eonideas.ecemetery.parkovi.adapters.AdvertisersListAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.IAdAdapterItem;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;
import hr.eonideas.ecemetery.parkovi.rest.model.AdvertiserCategory;
import retrofit.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by thirs on 25.12.2015..
 */
public class AdvertisersMenuFragment extends BasicFragment implements IOnGetResponseListener, AdapterView.OnItemClickListener {
    public static final String TAG = Filter.PACKAGE + ".AdvertisersMenuFragment";

    private static final int TOP_ADVERTISER_REQUEST         = 33;
    private static final int ADVERTISERS_CATEGORY_REQUEST   = 34;

    private StickyListHeadersListView stickyList;
    private RestControler lc;
    private List<IAdAdapterItem> advertiserCategoryList;
    private List<IAdAdapterItem> topAdvertisersList;
    private AdvertisersCategoryAdapter adapter;

    public static AdvertisersMenuFragment newInstance() {
        AdvertisersMenuFragment fragment = new AdvertisersMenuFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return getResources().getString(R.string.advertisers_menu_title);
    }

    @Override
    public void onResume() {
        super.onResume();
        updateListView();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.advertisers_menu, null);
        setActionBar(getScreenTitle(), true);

        stickyList = (StickyListHeadersListView) view.findViewById(R.id.list);
        stickyList.setOnItemClickListener(this);
        if(advertiserCategoryList == null || advertiserCategoryList.isEmpty()){
            if(lc == null){
                lc = new RestControler(getActivity());
            }
            lc.setOnGetResponseListener(this);
            lc.getAdvertiserTop(TOP_ADVERTISER_REQUEST);
        }
        return view;
    }

    private void updateListView() {
        if(advertiserCategoryList != null && !advertiserCategoryList.isEmpty()){
            adapter = new AdvertisersCategoryAdapter(getActivity(), advertiserCategoryList);
            stickyList.setAdapter(adapter);
        }
        if(topAdvertisersList != null && !topAdvertisersList.isEmpty() && adapter != null){
            adapter.setTopAdvertisersList(topAdvertisersList);
            adapter.setHeaderTitles(null, getResources().getString(R.string.advertisers_header_topadvertisers));
        }
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response != null && ((PagedResult)response.body()).getItems() instanceof List && !((List)((PagedResult)response.body()).getItems()).isEmpty()){
            if(requestId == TOP_ADVERTISER_REQUEST){
                if(lc == null){
                    lc = new RestControler(getActivity());
                }
                lc.setOnGetResponseListener(this);
                lc.getAdvertiserCategory(ADVERTISERS_CATEGORY_REQUEST);
                topAdvertisersList = (List<IAdAdapterItem>) ((PagedResult)response.body()).getItems();

            }
            else if (requestId == ADVERTISERS_CATEGORY_REQUEST){
                advertiserCategoryList = (List<IAdAdapterItem>) ((PagedResult)response.body()).getItems();
                updateListView();
            }
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        if(advertiserCategoryList.size() >  position){
            AdvertiserCategory selectedCategory = (AdvertiserCategory) parent.getItemAtPosition(position);
            if(selectedCategory != null){
                Intent intent = new Intent(getActivity(), AdvertiserActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(AdvertisersListFragment.AD_CATEGORY, selectedCategory);
                startActivity(intent);
            }
        }else if (topAdvertisersList != null && topAdvertisersList.size() > (position - advertiserCategoryList.size())){
            Advertiser selectedAdvertiser = (Advertiser)topAdvertisersList.get(position - advertiserCategoryList.size());
            if(selectedAdvertiser != null){
                Intent intent = new Intent(getActivity(), AdvertiserActivity.class);
                intent.addFlags(Intent.FLAG_ACTIVITY_NO_ANIMATION);
                intent.putExtra(AdvertiserDetailsFragment.ADVERTISER, selectedAdvertiser);
                startActivity(intent);
            }
        }
    }
}
