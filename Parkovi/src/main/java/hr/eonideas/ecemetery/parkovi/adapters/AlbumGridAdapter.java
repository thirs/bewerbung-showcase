package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;

/**
 * Created by thirs on 21.10.2015..
 */
public class AlbumGridAdapter extends BaseAdapter{
    private List<Image> albumImageList;
    private Set<Integer> markedImagesIndexList;
    private Context context;
    private ImageLoader imageLoader;
    private boolean isEditable;

    public AlbumGridAdapter(Context context, List<Image> images) {
        this(context, images, true);
    }

    public void updateImagesList(Image image){
        if(albumImageList == null){
            albumImageList = new ArrayList<Image>();
            albumImageList.add(image);
        }else{
            albumImageList.add(image);
        }
    }

    public void updateImagesList(ArrayList<GalleryItem> galleryItems){
        if(albumImageList == null){
            albumImageList = new ArrayList<Image>();
        }
        List<Image> temporaryImageList = new ArrayList<Image>();
        for(GalleryItem item : galleryItems){
            temporaryImageList.add(item.getImage());
        }
        albumImageList.addAll(temporaryImageList);
    }

    public AlbumGridAdapter(Context context, List<Image> images, boolean isEditable) {
        this.albumImageList = images;
        this.context = context;
        imageLoader = ImageLoader.getInstance();
        this.isEditable = isEditable;
        if(isEditable){
            markedImagesIndexList = new HashSet<Integer>();
        }
    }

    @Override
    public int getCount() {
        return albumImageList.size();
    }

    @Override
    public Image getItem(int position) {
        return albumImageList.get(position);
    }

    @Override
    public long getItemId(int position) {
        return 0;
    }

    public static class ViewHolder {
        public ImageView albumImageView;
        public ImageView marker;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        ViewHolder view;
        LayoutInflater inflator = (LayoutInflater) context.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        if (convertView == null) {
            view = new ViewHolder();
            convertView = inflator.inflate(R.layout.image_album_item, null);
            view.albumImageView = (ImageView) convertView.findViewById(R.id.album_img);
            view.marker = (ImageView) convertView.findViewById(R.id.marker);
            convertView.setTag(view);
        } else {
            view = (ViewHolder) convertView.getTag();
        }
        view.albumImageView.setTag(position);
        imageLoader.displayImage(albumImageList.get(position).getThumbUrl(), view.albumImageView);
        view.marker.setVisibility(markedImagesIndexList.contains(position) ? View.VISIBLE : View.GONE);
        return convertView;
    }

    public void markImage(int position){
        if(position == -1){
            markedImagesIndexList.clear();
        }else{
            if(markedImagesIndexList.contains(position)){
                markedImagesIndexList.remove(position);
            }else{
                markedImagesIndexList.add(position);
            }
        }
    }

    public Set<Integer> getMarkedImagesIndexList(){
        TreeSet orderedIdexes = new TreeSet();
        orderedIdexes.addAll(markedImagesIndexList);
        return orderedIdexes.descendingSet();
    }
}