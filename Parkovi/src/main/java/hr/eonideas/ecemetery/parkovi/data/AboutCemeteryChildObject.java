package hr.eonideas.ecemetery.parkovi.data;

import android.widget.TextView;

/**
 * Created by thirs on 18.10.2015..
 */
public class AboutCemeteryChildObject {
    private String mChildContent;

    public AboutCemeteryChildObject(String content){
        mChildContent = content;
    }

    public String getmChildContent() {
        return mChildContent;
    }

    public void setmChildContent(String mChildContent) {
        this.mChildContent = mChildContent;
    }
}
