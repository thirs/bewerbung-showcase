package hr.eonideas.ecemetery.parkovi;

import android.app.Activity;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.util.Log;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.BaseAdapter;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.Toast;

import com.achep.header2actionbar.FadingActionBarHelper;
import com.facebook.AccessToken;
import com.facebook.FacebookSdk;
import com.facebook.GraphRequest;
import com.facebook.GraphResponse;
import com.facebook.HttpMethod;
import com.facebook.login.LoginManager;

import java.util.ArrayList;
import java.util.Locale;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.busevents.AccountEvent;
import hr.eonideas.ecemetery.commons.busevents.DrawerOpenEvent;
import hr.eonideas.ecemetery.commons.busevents.GenBooleanEvent;
import hr.eonideas.ecemetery.commons.busevents.LoggedInEvent;
import hr.eonideas.ecemetery.parkovi.about.AboutFragment;
import hr.eonideas.ecemetery.parkovi.adapters.NavigationDrawerAdapter;
import hr.eonideas.ecemetery.parkovi.advertisers.AdvertiserActivity;
import hr.eonideas.ecemetery.parkovi.advertisers.AdvertisersMenuFragment;
import hr.eonideas.ecemetery.parkovi.burials.BurialsFragment;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.commons.NavDrawerConfiguration;
import hr.eonideas.ecemetery.parkovi.commons.NavMenuItem;
import hr.eonideas.ecemetery.parkovi.data.AccountFBLogin;
import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;
import hr.eonideas.ecemetery.parkovi.main.MainMenuFragment;
import hr.eonideas.ecemetery.parkovi.memories.MemoriesFragment;
import hr.eonideas.ecemetery.parkovi.myecemetery.MyEcemeteryFragment;
import hr.eonideas.ecemetery.parkovi.personalization.PersonalizeFragment;
import hr.eonideas.ecemetery.parkovi.personalization.login.PersonalizeLoginFragment;
import hr.eonideas.ecemetery.parkovi.profile.FindDeceasedFragment;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.settings.SettingsActivty;
import hr.eonideas.ecemetery.parkovi.tours.ToursFragment;

public class NavigationActivity extends BaseActivity implements BasicFragment.OnFragmentInteractionListener {

    private static final String TAG = Filter.PACKAGE + ".NavigationActivity";
    private static final int SETTINGS = 33;
    private static final String ROOT_FRAGMENT_TAG = MainMenuFragment.TAG;

    protected NavDrawerConfiguration navConf;
    protected boolean enableDrawer;
    protected DrawerLayout mDrawerLayoutId;
    protected ActionBarDrawerToggle mDrawerToggle;
    protected ListView mDrawerList;
    protected LinearLayout mDrawerMenu;
    private int activeMenuItem;
    private BaseAdapter navAdapter;
    private INavDrawerItem[] menu;

    private FadingActionBarHelper mFadingActionBarHelper;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        onCreate(savedInstanceState, true);
    }


    protected void onCreate(Bundle savedInstanceState, boolean enableDrawer) {
        super.onCreate(savedInstanceState);
        this.enableDrawer = enableDrawer;
        FacebookSdk.sdkInitialize(getApplicationContext());
        init();
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void onResume() {
        super.onResume();
        createNavDrawerMenu();
        updateNavigationDrawerItems(activeMenuItem,true);
    }

    protected void init() {
        navConf = getNavDrawerConfiguration();
        setContentView(navConf.getDrawerMenuLayout());

        //initialize footer buttons
        navConf.setDrawerMenuHeaderId(R.id.header_container);
        final FragmentManager fm = getSupportFragmentManager();
        fm.addOnBackStackChangedListener(new FragmentManager.OnBackStackChangedListener() {
            public void onBackStackChanged() {
                Fragment f = fm.findFragmentById(getNavDrawerConfiguration().getFragmentContainerId());
                if (f instanceof BasicFragment)
                    // do something with f
                    ((BasicFragment)f).onResume();
            }
        });
        //(th) ubacivanje header u menu
        FragmentTransaction fragmentTransaction = fm.beginTransaction();
        fragmentTransaction.replace(navConf.getDrawerMenuHeaderId(), NavigationHeaderFragment.newInstance(app.isLoggedIn() && app.getUserAccount().getId() > -1), NavigationHeaderFragment.TAG);
        fragmentTransaction.commit();

        mDrawerLayoutId = (DrawerLayout) findViewById(navConf.getDrawerLayoutId());
        mDrawerList = (ListView) findViewById(navConf.getDrawerMenuListId());
        mDrawerMenu = (LinearLayout) findViewById(navConf.getDrawerMenuLayoutId());
        navAdapter = navConf.getDrawerMenuAdapter();
        mDrawerList.setAdapter(navAdapter);
        mDrawerList.setOnItemClickListener(new DrawerItemClickListener());
        setupDrawer(enableDrawer);
        initActionBar(getResources().getString(R.string.main_menu_title));
    }

    private void setupDrawer(boolean enableDrawer) {
        if (enableDrawer) {
            mDrawerLayoutId.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
            mDrawerToggle = new ActionBarDrawerToggle(this, mDrawerLayoutId, navConf.getDrawerOpenDesc(), navConf.getDrawerClosedDesc()) {
                public void onDrawerClosed(View view) {
                    supportInvalidateOptionsMenu();
                    syncState();
                }

                public void onDrawerOpened(View drawerView) {
                    supportInvalidateOptionsMenu();
                    syncState();
                }
            };
            mDrawerLayoutId.setDrawerListener(mDrawerToggle);
        } else {
            mDrawerLayoutId.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }

        if (mDrawerToggle != null) {
            mDrawerToggle.syncState();
        }
    }

    protected void manageFragment(Fragment fragment, String TAG, boolean rootCategory){
        /*th -> add fragment if backstack is empty;
        if backstack not empty and existing fragment, than pop everything above this fragment from backstackt
        if backstack not empty and new fragment, than respect replaceFragment flag*/

        FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() == 0){
            addFragment(fragment,TAG);
        }else{
            boolean alreadyExistingFragment = false;
            for(int i = 0; i < fm.getBackStackEntryCount();i++){
                if(fm.getBackStackEntryAt(i).getName().equals(TAG)){
                    alreadyExistingFragment = true;
                    fm.popBackStack(fm.getBackStackEntryAt(i).getId(),0);
                }
            }
            if(!alreadyExistingFragment){
                if(rootCategory){
                    fm.popBackStack(fm.getBackStackEntryAt(0).getId(),0);
                }
                addFragment(fragment, TAG);
            }
        }
    }

    protected void addFragment(Fragment fragment, String TAG) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(getNavDrawerConfiguration().getFragmentContainerId(), fragment, TAG).addToBackStack(TAG).commit();
    }


    protected NavDrawerConfiguration getNavDrawerConfiguration() {
        if (navConf == null)
            return initiateNavDrawerConfiguration();
        else
            return navConf;
    }

    protected NavDrawerConfiguration initiateNavDrawerConfiguration() {
        createNavDrawerMenu();
        navConf = new NavDrawerConfiguration();
        navConf.setDrawerMenuHeaderId(R.id.header_container);
        navConf.setDrawerMenuLayout(R.layout.activity_root);
        navConf.setFragmentContainerId(R.id.fragment_container);
        navConf.setDrawerLayoutId(R.id.drawer_layout);
        navConf.setDrawerMenuListId(R.id.drawer_menu_list);
        navConf.setDrawerMenuLayoutId(R.id.drawer_slider);
        navConf.setNavigationDrawerItems(menu);
        navConf.setDrawerMenuAdapter(new NavigationDrawerAdapter(this, R.layout.navdrawer_item, menu));
        return navConf;

    }

    protected void createNavDrawerMenu() {
        Log.d(TAG, "Active menu item is " + activeMenuItem);
        ArrayList<INavDrawerItem> menuList = new ArrayList<INavDrawerItem>();
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_HOME, R.string.navbar_menu_item_home, R.drawable.ic_home, false, true, activeMenuItem == INavDrawerItem.MENU_ITEM_HOME));
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_ABOUT, R.string.navbar_menu_item_about, R.drawable.ic_about, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_ABOUT));
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_TOURS, R.string.navbar_menu_item_tours, R.drawable.ic_tours, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_TOURS));
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_BURIALS, R.string.navbar_menu_item_burials, R.drawable.ic_burials, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_BURIALS));
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_PROFILES, R.string.navbar_menu_item_profiles, R.drawable.ic_find, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_PROFILES));
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_MEMORIES, R.string.navbar_menu_item_memories, R.drawable.ic_memories, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_MEMORIES));
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_ADVERTISERS, R.string.navbar_menu_item_advertisers, R.drawable.ic_adverts, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_ADVERTISERS));
        if(app.isLoggedIn() && app.getUserAccount().getId() > -1){
//            menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_MYECEMETERY, R.string.navbar_menu_item_myecemetery, R.drawable.icon_mycemetery, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_MYECEMETERY,true));

        }
        menuList.add(new NavMenuItem(INavDrawerItem.MENU_ITEM_SETTINGS, R.string.navbar_menu_item_settings, R.drawable.icon_settings_gray, true, true, activeMenuItem == INavDrawerItem.MENU_ITEM_SETTINGS, true));
        menu = new INavDrawerItem[menuList.size()];
        menuList.toArray(menu);
    }


    @Override
    protected void onPostCreate(Bundle savedInstanceState) {
        super.onPostCreate(savedInstanceState);
        if (mDrawerToggle != null) {
            mDrawerToggle.syncState();
        }
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
        if (mDrawerToggle != null) {
            mDrawerToggle.onConfigurationChanged(newConfig);
        }
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        InputMethodManager inputMethodManager = (InputMethodManager) getSystemService(Activity.INPUT_METHOD_SERVICE);
        inputMethodManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), 0);
        if (mDrawerToggle != null) {
            if (mDrawerToggle.onOptionsItemSelected(item)) {
                EventBus.getDefault().post(new DrawerOpenEvent(mDrawerLayoutId.isDrawerOpen(this.mDrawerMenu)));
                return true;
            } else {
                return false;
            }
        } else
            return super.onOptionsItemSelected(item);

    }

    @Override
    public void onBackPressed() {
        FragmentManager fm = getSupportFragmentManager();
        if(fm.getBackStackEntryCount() > 1){
            super.onBackPressed();
        }

    }

    @Override
    public boolean onKeyDown(int keyCode, KeyEvent event) {
        if (keyCode == KeyEvent.KEYCODE_MENU) {
            if (mDrawerToggle != null) {
                if (mDrawerLayoutId.isDrawerOpen(this.mDrawerMenu)) {
                    this.mDrawerLayoutId.closeDrawer(this.mDrawerMenu);
                } else {
                    this.mDrawerLayoutId.openDrawer(this.mDrawerMenu);
                }
            }
            return true;
        }else if (keyCode == KeyEvent.KEYCODE_BACK) {
            FragmentManager fm = getSupportFragmentManager();
            if(fm.getBackStackEntryCount() <= 1){
                if(Filter.isDebuggingMode)Log.d(TAG,"Size of backstack "+fm.getBackStackEntryCount());
                return true;
            }
        }
        return super.onKeyDown(keyCode, event);
    }

    //Drawer Menu pickers
    protected void onNavItemSelected(int position) {
        onNavItemSelected(position,null);
    }

    //Drawer Menu pickers
    protected void onNavItemSelected(int position, Bundle args) {
        if (position > 0) {
            if (Filter.isDebuggingMode) {
                if (getNavDrawerConfiguration().getNavigationDrawerItems().length > position)
                    Toast.makeText(this, "You Clicked at " + getNavDrawerConfiguration().getNavigationDrawerItems()[position], Toast.LENGTH_SHORT).show();
            }
        }

        //close navigation drawer after selection is made
        if (mDrawerLayoutId.isDrawerOpen(mDrawerMenu)) {
            mDrawerLayoutId.closeDrawer(mDrawerMenu);
        }

        Fragment fragment;
        Intent intent;
        switch (position) {
            case INavDrawerItem.MENU_ITEM_LOGOUT:
                manageLogout();
                EventBus.getDefault().post(new LoggedInEvent(false));
                if(activeMenuItem != INavDrawerItem.MENU_ITEM_MYECEMETERY){
                    break;
                }
            case INavDrawerItem.MENU_ITEM_HOME:
                activeMenuItem = INavDrawerItem.MENU_ITEM_HOME;
                fragment = getSupportFragmentManager().findFragmentByTag(MainMenuFragment.TAG);
                if (fragment == null) {
                    fragment = MainMenuFragment.newInstance();
                }
                manageFragment(fragment, MainMenuFragment.TAG, true);
                break;
            case INavDrawerItem.MENU_ITEM_ABOUT:
                activeMenuItem = INavDrawerItem.MENU_ITEM_ABOUT;
                fragment = getSupportFragmentManager().findFragmentByTag(AboutFragment.TAG);
                if (fragment == null) {
                    fragment = AboutFragment.newInstance();
                }
                manageFragment(fragment, AboutFragment.TAG, true);
                break;
            case INavDrawerItem.MENU_ITEM_TOURS:
                activeMenuItem = INavDrawerItem.MENU_ITEM_TOURS;
                fragment = getSupportFragmentManager().findFragmentByTag(ToursFragment.TAG);
                if (fragment == null) {
                    fragment = ToursFragment.newInstance();
                }
                manageFragment(fragment, ToursFragment.TAG, true);
                break;
            case INavDrawerItem.MENU_ITEM_MEMORIES:
                activeMenuItem = INavDrawerItem.MENU_ITEM_MEMORIES;
                fragment = getSupportFragmentManager().findFragmentByTag(MemoriesFragment.TAG);
                if (fragment == null) {
                    fragment = MemoriesFragment.newInstance();
                }
                manageFragment(fragment, MemoriesFragment.TAG, true);
                break;
            case INavDrawerItem.MENU_ITEM_PROFILES:
                activeMenuItem = INavDrawerItem.MENU_ITEM_PROFILES;
                fragment = getSupportFragmentManager().findFragmentByTag(FindDeceasedFragment.TAG);
                if (fragment == null) {
                    fragment = FindDeceasedFragment.newInstance();
                }
                manageFragment(fragment, FindDeceasedFragment.TAG, true);
                break;
            case INavDrawerItem.MENU_ITEM_MYECEMETERY:
                boolean reatachFragment = activeMenuItem == INavDrawerItem.MENU_ITEM_MYECEMETERY;
                activeMenuItem = INavDrawerItem.MENU_ITEM_MYECEMETERY;
                fragment = getSupportFragmentManager().findFragmentByTag(MyEcemeteryFragment.TAG);
                if (fragment == null) {
                    fragment = MyEcemeteryFragment.newInstance();
                }
                manageFragment(fragment, MyEcemeteryFragment.TAG, true);
                if(reatachFragment){
                    reattachAllFragments();
                }
                break;
            case INavDrawerItem.MENU_ITEM_ADVERTISERS:
                activeMenuItem = INavDrawerItem.MENU_ITEM_ADVERTISERS;
                fragment = getSupportFragmentManager().findFragmentByTag(AdvertisersMenuFragment.TAG);
                if (fragment == null) {
                    fragment = AdvertisersMenuFragment.newInstance();
                }
                manageFragment(fragment, AdvertisersMenuFragment.TAG, true);
//                intent = new Intent(this, AdvertiserActivity.class);
//                startActivityForResult(intent,SETTINGS);
                break;
            case INavDrawerItem.MENU_ITEM_BURIALS:
                activeMenuItem = INavDrawerItem.MENU_ITEM_BURIALS;
                fragment = getSupportFragmentManager().findFragmentByTag(BurialsFragment.TAG);
                if (fragment == null) {
                    fragment = BurialsFragment.newInstance();
                }
                manageFragment(fragment, BurialsFragment.TAG, true);
                break;
            case INavDrawerItem.MENU_ITEM_SETTINGS:
                activeMenuItem = INavDrawerItem.MENU_ITEM_SETTINGS;
                intent = new Intent(this, SettingsActivty.class);
                startActivityForResult(intent,SETTINGS);
                break;
            case INavDrawerItem.MENU_ITEM_LOGIN:
                activeMenuItem = INavDrawerItem.MENU_ITEM_LOGIN;
                fragment = getSupportFragmentManager().findFragmentByTag(PersonalizeFragment.TAG);
                if (fragment == null) {
                    fragment = PersonalizeFragment.newInstance();
                }
                manageFragment(fragment, PersonalizeFragment.TAG, true);
                break;
        }
        updateNavigationDrawerItems(position);
    }

    private void manageLogout() {
        app.getSharedPreferences().edit().remove(Parkovi.LOGGED_IN_WITH_TOKEN).commit();
        app.getSharedPreferences().edit().remove(Parkovi.LOGGED_IN_WITH_ID).commit();
        Account.getInstance(null).resetAccount();

        if (AccessToken.getCurrentAccessToken() == null) {
            Log.d(TAG, "AccessToken is null. Finishing logout");
            return; // already logged out
        }

        LoginManager.getInstance().logOut();

    }

    public void updateNavigationDrawerItems(int position){
        updateNavigationDrawerItems(position, false);
    }

    public void updateNavigationDrawerItems(int position, boolean refreshAvatar){
        //indikatori footer menu items se moraju setirati u zasebnom fragmentu
        //todo - izmisliti nešt pametnije!!!!!! plan 2020g
        NavigationHeaderFragment navigationFragment = (NavigationHeaderFragment) getSupportFragmentManager().findFragmentByTag(NavigationHeaderFragment.TAG);
        if (navigationFragment != null) {
            if(Filter.isDebuggingMode){
                Log.i(TAG, "Updating NavigationHeaderFragment");
                Log.i(TAG, "Login data available: " + app.isLoggedIn() + ". UserID is " + (app.isLoggedIn() ? app.getUserAccount().getId() : -1));
            }
            navigationFragment.updateMenuItems(app.isLoggedIn() && app.getUserAccount().getId() > -1, refreshAvatar);
        }

//        getSupportActionBar().setTitle(mTitle);
        createNavDrawerMenu();
        navConf.setNavigationDrawerItems(menu);
        ((NavigationDrawerAdapter) navAdapter).updateItems(menu);
    }


    @Override
    public void onFragmentInteraction(int menuItemId, Class fragmentClass, Bundle args) {
        //if predefined menu category
        if (menuItemId >= 0)
            onNavItemSelected(menuItemId);
            //if directly defined class
        else if (fragmentClass != null && args != null) {
            Fragment fragment;
            fragment = ClassResolver.getInstance(this).getNewFragment(fragmentClass, args);
            if (fragment instanceof BasicFragment)
                manageFragment(fragment, ((BasicFragment) fragment).getDefinedTAG(), false);
            else
                manageFragment(fragment, "UNKNOWN", false);
        }
    }

    private class DrawerItemClickListener implements ListView.OnItemClickListener {
        @Override
        public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
            INavDrawerItem selectedItem = navConf.getNavigationDrawerItems()[position];

            if (selectedItem.updateActionBarTitle()) {
                setTitle(selectedItem.getLabelResourceId());
            }
            onNavItemSelected(selectedItem.getId());
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == SETTINGS  && data != null){
            boolean languageChanged = data.getBooleanExtra(SettingsActivty.LANGUAGE_CHANGED,false);
            if(languageChanged){
                reattachAllFragments();
            }
        }else{
            super.onActivityResult(requestCode, resultCode, data);
            Fragment fragment = (Fragment) getSupportFragmentManager().findFragmentByTag(PersonalizeFragment.TAG);
            if (fragment != null) {
                fragment.onActivityResult(requestCode, resultCode, data);
            }
        }
    }

    private void reattachAllFragments() {
        FragmentManager fm = getSupportFragmentManager();
        FragmentTransaction fragTransaction = fm.beginTransaction();
        for(int i = 0; i < fm.getBackStackEntryCount();i++){
            Fragment f = fm.findFragmentByTag(fm.getBackStackEntryAt(i).getName());
            fragTransaction.detach(f);
            fragTransaction.attach(f);
        }
        fragTransaction.commitAllowingStateLoss();
    }



    public FadingActionBarHelper getFadingActionBarHelper() {
        return mFadingActionBarHelper;
    }

    public void onEvent(AccountEvent event){
        updateNavigationDrawerItems(activeMenuItem,true);
        reattachAllFragments();
    }


    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

}
