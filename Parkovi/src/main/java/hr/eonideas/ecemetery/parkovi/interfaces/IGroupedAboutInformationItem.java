package hr.eonideas.ecemetery.parkovi.interfaces;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryInfo;

/**
 * Created by thirs on 27.10.2015..
 */
public interface IGroupedAboutInformationItem {
    public List<CemeteryInfo> getItems();
    public String getSectionTitle();
}
