package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import hr.eonideas.ecemetery.parkovi.interfaces.IAdAdapterItem;


public class AdvertiserCategory implements Parcelable, IAdAdapterItem {
    @SerializedName("id")
    private Integer id = null;
    @SerializedName("title")
    private String title = null;
    @SerializedName("description")
    private String description = null;
    @SerializedName("image")
    private Image image = null;

    public AdvertiserCategory(Integer id, String title, String description, Image image) {
        this.id = id;
        this.title = title;
        this.description = description;
        this.image = image;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * Category title.
     **/
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Category description.
     **/
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     **/
    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class AdvertiserCategory {\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("  title: ").append(title).append("\n");
        sb.append("  description: ").append(description).append("\n");
        sb.append("  image: ").append(image).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected AdvertiserCategory(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        title = in.readString();
        description = in.readString();
        image = (Image) in.readValue(Image.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
            dest.writeString(title);
            dest.writeString(description);
            dest.writeValue(image);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AdvertiserCategory> CREATOR = new Parcelable.Creator<AdvertiserCategory>() {
        @Override
        public AdvertiserCategory createFromParcel(Parcel in) {
            return new AdvertiserCategory(in);
        }

        @Override
        public AdvertiserCategory[] newArray(int size) {
            return new AdvertiserCategory[size];
        }
    };
}