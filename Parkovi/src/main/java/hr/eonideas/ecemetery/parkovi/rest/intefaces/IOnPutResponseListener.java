package hr.eonideas.ecemetery.parkovi.rest.intefaces;

import retrofit.Response;

public interface IOnPutResponseListener {
	public void onPutResponse(Response response, int requestId);
}
