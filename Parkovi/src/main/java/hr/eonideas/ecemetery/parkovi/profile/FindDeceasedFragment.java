package hr.eonideas.ecemetery.parkovi.profile;

import android.app.Activity;
import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.view.KeyEvent;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.EditorInfo;
import android.view.inputmethod.InputMethodManager;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Set;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.ProfileFavoriteEvent;
import hr.eonideas.ecemetery.commons.busevents.ProfileUpdateEvent;
import hr.eonideas.ecemetery.commons.interfaces.ILazyLoad;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.ProfileListAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import retrofit.Response;

/**
 * Created by thirs on 25.12.2015..
 */
public class FindDeceasedFragment extends BasicFragment implements IOnGetResponseListener, AdapterView.OnItemClickListener, ILazyLoad {


    public static final String TAG = Filter.PACKAGE + ".FindDeceasedFragment";
    public static final String PROFILE_LIST = Filter.PACKAGE + ".PROFILE_LIST";
    private static final String SEARCH_HISTORY = Filter.PACKAGE + ".SEARCH_HISTORY";


    private EditText inputSearch;
    private ListView listView;
    private RestControler lc;
    private List<Profile> profileLightList;
    private ProfileListAdapter adapter;

    private IProfile modifiedProfile;
    private List<Profile> predefinedProfileList;

    private ArrayList<String> searchHistory;
    private int offset;
    private Integer totalCount;
    private TextView historyLink;

    public static FindDeceasedFragment newInstance() {
        FindDeceasedFragment fragment = new FindDeceasedFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return getResources().getString(R.string.navbar_menu_item_profiles);
    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        fetchSearchHistory();
    }

    //Eventbus is used to sinkronize navigationdrawer with floating menu
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    public void onResume() {
        super.onResume();
        if(adapter !=null) {
            if (modifiedProfile != null) {
                int profilePosition = ProfileUtils.getProfileListPosition(modifiedProfile, profileLightList);
                if (profilePosition > -1) {
                    profileLightList.set(profilePosition, (Profile) modifiedProfile);
                }
            }
            adapter.notifyDataSetChanged();
        }
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profiles_find, null);
        predefinedProfileList = getArguments().getParcelableArrayList(PROFILE_LIST);
        initViewElements(view);
        if(predefinedProfileList != null){
            inputSearch.setVisibility(View.GONE);
            historyLink.setVisibility(View.GONE);
            updateListView(predefinedProfileList,-1);
        }
        return view;
    }

    private void initViewElements(View view) {
        inputSearch = (EditText) view.findViewById(R.id.input_search);
        listView = (ListView) view.findViewById(R.id.listview);
        inputSearch.setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                boolean handled = false;
                if (actionId == EditorInfo.IME_ACTION_SEND) {
                    adapter = null;
                    offset = 0;
                    fetchProfiles(inputSearch.getText().toString());
                    handled = true;
                    updateSearchIntoHistory(inputSearch.getText().toString());
                }
                return handled;
            }
        });
        listView.setOnItemClickListener(this);
        historyLink = (TextView) view.findViewById(R.id.search_history);
        historyLink.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (searchHistory != null && searchHistory.size() > 0) {
                    AlertDialog.Builder b = new AlertDialog.Builder(getActivity());
                    b.setTitle(getActivity().getResources().getString(R.string.search_history));
                    b.setItems(Utils.arrayListToArray(searchHistory), new DialogInterface.OnClickListener() {
                        @Override
                        public void onClick(DialogInterface dialog, int which) {
                            offset = 0;
                            inputSearch.setText(Utils.arrayListToArray(searchHistory)[which]);
                            fetchProfiles(Utils.arrayListToArray(searchHistory)[which].toString());
                        }
                    });
                    b.show();
                }

            }
        });
    }

    private void fetchProfiles(String s) {
        if (lc == null) {
            lc = new RestControler(getActivity());
        }
        lc.setOnGetResponseListener(FindDeceasedFragment.this);
        lc.getProfile(s, offset,Filter.PAGE_SIZE);
    }

    private void updateListView(List<Profile> list2show, int totalCount) {
        if(list2show != null && !list2show.isEmpty()){
            if(adapter == null || totalCount < 0){
                //(th) totalcount < 0 only when FindDeceasedFragment is initalized
                adapter = new ProfileListAdapter(getActivity(), list2show);
                adapter.setLazyLoadListener(this);
                adapter.setTotalCount(totalCount);
                listView.setAdapter(adapter);
            }else{
                adapter.updateList(list2show);
                adapter.notifyDataSetChanged();
                listView.requestFocus();
            }
        }else{
            adapter = new ProfileListAdapter(getActivity(), list2show);
            adapter.setTotalCount(totalCount);
            listView.setAdapter(adapter);
        }
        Utils.hideKeyboard(getActivity());
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response != null && response.body() != null){
            if(((PagedResult) response.body()).getItems() instanceof List && !((List<Profile>)((PagedResult) response.body()).getItems()).isEmpty()){
                totalCount = ((PagedResult) response.body()).getTotal();
                profileLightList = (List<Profile>)((PagedResult) response.body()).getItems();
                updateListView(profileLightList,((PagedResult) response.body()).getTotal());
            }else if(((PagedResult) response.body()).getItems() instanceof List && ((List<Profile>)((PagedResult) response.body()).getItems()).isEmpty()){
                profileLightList = (List<Profile>)((PagedResult) response.body()).getItems();
                updateListView(profileLightList, -1);
            }
        }
    }

    public void onEvent(ProfileUpdateEvent event){
        modifiedProfile = event.profile;
        int predefinedProfilePosition = ProfileUtils.getProfileListPosition(modifiedProfile, predefinedProfileList);
        if(predefinedProfilePosition > -1){
            predefinedProfileList.set(predefinedProfilePosition, (Profile) modifiedProfile);
        }
        int accountsFavoritesPosition = ProfileUtils.getProfileListPosition(modifiedProfile, Account.getInstance(null).getFavoriteProfiles());
        if(accountsFavoritesPosition > -1){
            Account.getInstance(null).getFavoriteProfiles().set(accountsFavoritesPosition, (Profile) modifiedProfile);
        }
    }

    public void onEvent(ProfileFavoriteEvent event){
        IProfile profile = event.profile;
        if(profile != null && adapter != null){
            adapter.remove((Profile)profile);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        IProfile selectedProfile = (IProfile) parent.getItemAtPosition(position);
        if(selectedProfile != null){
            Bundle args = new Bundle();
            args.putParcelable(ProfileFragment.PROFILE_LIGHT, (Profile)selectedProfile);
            mListener.onFragmentInteraction(-1, ProfileFragment.class, args);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
        storeSearchHistory();
    }

    private void fetchSearchHistory() {
        Set<String> ss = app.getSharedPreferences().getStringSet(SEARCH_HISTORY, null);
        if(ss != null && ss.size() > 0){
            searchHistory = new ArrayList<String>(ss);
        }else{
            searchHistory = new ArrayList<String>(Filter.SEARCH_HISTORY_SIZE);
        }
    }

    private void updateSearchIntoHistory(String s) {
        if(searchHistory == null){
            searchHistory = new ArrayList<String>(Filter.SEARCH_HISTORY_SIZE);
            searchHistory.add(s);
        }else if (searchHistory.size() == Filter.SEARCH_HISTORY_SIZE){
            searchHistory.remove(searchHistory.size() -1);
            searchHistory.add(0,s);
        }else{
            searchHistory.add(0,s);
        }
    }

    private void storeSearchHistory() {
        if(searchHistory != null && searchHistory.size() <= Filter.SEARCH_HISTORY_SIZE){
            app.getSharedPreferences().edit().putStringSet(SEARCH_HISTORY, new LinkedHashSet<String>(searchHistory)).commit();
        }else if(searchHistory != null && searchHistory.size() > Filter.SEARCH_HISTORY_SIZE){
            app.getSharedPreferences().edit().putStringSet(SEARCH_HISTORY, new LinkedHashSet<String>(new ArrayList(searchHistory.subList(0, Filter.SEARCH_HISTORY_SIZE)))).commit();
        }
    }

    @Override
    public void updateLazyList(Integer offset) {
        if(predefinedProfileList == null || predefinedProfileList.size() == 0){
            this.offset = offset;
            fetchProfiles(inputSearch.getText().toString());
        }
    }


}
