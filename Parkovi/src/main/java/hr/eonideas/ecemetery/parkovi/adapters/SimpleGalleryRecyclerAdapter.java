package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.ImageAlbumActivity;
import hr.eonideas.ecemetery.parkovi.commons.ImageViewPager;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;

/**
 * Created by Teo on 10.2.2016..
 */
public class SimpleGalleryRecyclerAdapter extends RecyclerView.Adapter<SimpleGalleryRecyclerAdapter.GalleryViewHolder> {

    private ImageLoader imageLoader;
    private List<Image> images;
    private int itemLayout;
    private Context context;
    private IOnImageClickListener mListener;

    public SimpleGalleryRecyclerAdapter(List<Image> images, int itemLayout) {
        this.images = images;
        this.itemLayout = itemLayout;
        this.imageLoader = ImageLoader.getInstance();
    }

    public void setOnImageClickListener(IOnImageClickListener listener){
        this.mListener = listener;
    }

    public void updateImagesList(List<GalleryItem> galleryItems){
        images = new ArrayList<Image>();
        List<Image> temporaryImageList = new ArrayList<Image>();
        for(GalleryItem item : galleryItems){
            temporaryImageList.add(item.getImage());
        }
        images.addAll(temporaryImageList);
    }

    @Override
    public GalleryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new GalleryViewHolder(LayoutInflater.from(parent.getContext()).inflate(itemLayout, parent, false));
    }

    @Override
    public void onBindViewHolder(GalleryViewHolder holder, int position) {
        Image imageItem = null;
        holder.image.setImageDrawable(null);
        imageItem = images.get(position);
        if (imageItem != null){
            imageLoader.displayImage(imageItem.getThumbUrl(), holder.image);
        }
        holder.image.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Image i = (Image) v.getTag();
                if(mListener != null){
                    mListener.onImageClicked(i);
                }

            }
        });
        holder.image.setTag(imageItem);

    }

    @Override
    public int getItemCount() {
        int count = images.size();
        return count;
    }


    public static class GalleryViewHolder extends RecyclerView.ViewHolder {
        public ImageView image;

        public GalleryViewHolder(View itemView) {
            super(itemView);
            image = (ImageView) itemView.findViewById(R.id.image_item);
        }
    }

    public interface IOnImageClickListener{
        public void onImageClicked(Image image);
    }
}
