package hr.eonideas.ecemetery.parkovi.commons;

/**
 * Created by thirs on 22.10.2015..
 */

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.GridView;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;
import java.util.Set;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.busevents.ProfileUpdateEvent;
import hr.eonideas.ecemetery.commons.dialogs.NotificationDialog;
import hr.eonideas.ecemetery.commons.utils.FileUtils;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.Manifest;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.AlbumGridAdapter;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.profile.details.ProfileDetailsFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnDeleteResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostMPResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit.Response;

public class ImageAlbumActivity extends BaseActivity implements View.OnClickListener, IOnDeleteResponseListener, IOnPostMPResponseListener, NotificationDialog.IOnNotificationConfirmed {
    private static final int GALLERY_PERMISSIONS        = 31;

    public static final String ALBUM_IMAGES = Filter.PACKAGE + ".ALBUM_IMAGES";
    public static final String ALBUM_TITLE = Filter.PACKAGE + ".ALBUM_TITLE";
    public static final String PROFILE = Filter.PACKAGE + ".PROFILE";
    public static final String ALBUM_GALLERY_ITEMS = Filter.PACKAGE + ".ALBUM_GALLERY_ITEMS";
    public static final String IMAGE_GRID_MODE = Filter.PACKAGE + ".IMAGE_GRID_MODE";;
    public static final String PICKED_IMAGE_POSITION = Filter.PACKAGE + ".PICKED_IMAGE_POSITION";

    private GridView gridView;
    private ArrayList<Image> imageAlbumList;
    private List<GalleryItem> imageAlbumGalleryItems;
    private AlbumGridAdapter albumGridAdapter;
    private IProfile profile;
    private RestControler lc;
    private ImageGridMode previousGridMode = ImageGridMode.GALLERY;
    private ImageGridMode newGridMode = ImageGridMode.GALLERY;
    private ImageView actionDelete;
    private ImageView actionShare;
    private View actionTab;
    private String imageAlbumTitle;
    private Uri newImageUri;


    public enum ImageGridMode{
        GALLERY,
        EDIT,
        PICKER
    }

    //todo - prebaciti u mainActivity i onda iz childa prema potrebi paliti
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        MenuInflater inflater = getMenuInflater();
        if(profile != null && profile.isAdmin()){
            inflater.inflate(R.menu.album_menu, menu);
            if(newGridMode == ImageGridMode.EDIT && albumGridAdapter != null && albumGridAdapter.getMarkedImagesIndexList().size() > 0){
                menu.findItem(R.id.revert_menu).setVisible(true);
                actionTab.setVisibility(View.VISIBLE);
            }else{
                actionTab.setVisibility(View.GONE);
                getSupportActionBar().setTitle(imageAlbumTitle);
                menu.findItem(R.id.revert_menu).setVisible(false);
            }
        }
        return super.onCreateOptionsMenu(menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add_menu){
            int permissionCheck = ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE);
            if(permissionCheck != PackageManager.PERMISSION_GRANTED){
                requireExternalStoragePermissions();
                return true;
            }
            EasyImage.openChooserWithGallery(this, getResources().getString(R.string.select_images_from_device), 0);
            return true;
        }else if (item.getItemId() == R.id.revert_menu){
            newGridMode = previousGridMode;
            albumGridAdapter.markImage(-1);
            albumGridAdapter.notifyDataSetChanged();
            invalidateOptionsMenu();
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.image_album_activity);
        init();
    }

    private void init() {
        imageAlbumGalleryItems = getIntent().getParcelableArrayListExtra(ALBUM_GALLERY_ITEMS);
        imageAlbumList = new ArrayList<Image>();
        Iterator iterator = imageAlbumGalleryItems.iterator();
        while(iterator.hasNext()){
            GalleryItem galleryItem = (GalleryItem)iterator.next();
            imageAlbumList.add(galleryItem.getImage());
        }
        profile = getIntent().getParcelableExtra(PROFILE);
        if(app.isLoggedIn() && profile != null){
            Account account = Account.getInstance(null);
            profile.setIsFavorite(ProfileUtils.isProfileInAccountFavorites(account, profile));
            profile.setIsAdmin(ProfileUtils.isAccountProfilesOwner(account, profile));
        }

        newGridMode = previousGridMode = (ImageGridMode) getIntent().getSerializableExtra(IMAGE_GRID_MODE);
        imageAlbumTitle = getIntent().getStringExtra(ALBUM_TITLE);
        initActionBar(imageAlbumTitle);
        initLayoutElements();
        if (imageAlbumList != null && !imageAlbumList.isEmpty()) {
            initializeGalleryGrid();
        }
    }

    private void initLayoutElements() {
        gridView = (GridView) findViewById(R.id.gridview);
        actionTab = findViewById(R.id.action_tab);
        actionTab.setVisibility(View.GONE);
        actionDelete = (ImageView)findViewById(R.id.action_delete);
        actionDelete.setOnClickListener(this);
        actionShare = (ImageView)findViewById(R.id.action_share);
        actionShare.setOnClickListener(this);
    }


    private void initializeGalleryGrid() {
        albumGridAdapter = new AlbumGridAdapter(this, imageAlbumList);
        gridView.setAdapter(albumGridAdapter);
        if(profile != null &&   profile.isAdmin()){
            gridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener() {
                @Override
                public boolean onItemLongClick(AdapterView<?> parent, View view, int position, long id) {
                    newGridMode = ImageGridMode.EDIT;
                    actionTab.setVisibility(View.VISIBLE);
                    albumGridAdapter.markImage(position);
                    albumGridAdapter.notifyDataSetChanged();
                    invalidateOptionsMenu();
                    initActionBar(albumGridAdapter.getMarkedImagesIndexList().size() + " " + getResources().getString(R.string.label_seleted));
                    return true;
                }
            });
        }
        gridView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                switch (newGridMode) {
                    case GALLERY:
                        openFullSizeImage(position);
                        break;
                    case EDIT:
                        albumGridAdapter.markImage(position);
                        albumGridAdapter.notifyDataSetChanged();
                        initActionBar(albumGridAdapter.getMarkedImagesIndexList().size() + " " + getResources().getString(R.string.label_seleted));
                        invalidateOptionsMenu();
                        break;
                    case PICKER:
                        Intent resultIntent = new Intent();
                        resultIntent.putExtra(PICKED_IMAGE_POSITION, position);
                        setResult(Activity.RESULT_OK, resultIntent);
                        finish();
                        break;
                }

            }
        });
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.action_delete){
            NotificationDialog dialog = NotificationDialog.newInstance(getResources().getString(R.string.warning),null,"Your are about to delete " + albumGridAdapter.getMarkedImagesIndexList().size() + " images. Are you sure about that?");
            dialog.show(getSupportFragmentManager(),"TEST");
        }else if (v.getId() == R.id.action_share){

        }
    }

    @Override
    public void notificationConfirmed() {
        removeGalleryItems();
    }

    private void openFullSizeImage(int position) {
        Intent intent = new Intent(this, ImageViewPager.class);
        intent.putExtra(ImageViewPager.IMAGE_INDEX, position);
        intent.putExtra(ImageAlbumActivity.ALBUM_IMAGES, imageAlbumList);
        startActivity(intent);
    }

    private void updateProfileAndBroadcast() {
        profile.setProfileGallery(imageAlbumGalleryItems);
        EventBus.getDefault().post(new ProfileUpdateEvent((Profile) profile));
    }

    @Override
    public void onBackPressed(){
        finish();
    }

    private void updateProfileGallery() {
        if (lc == null) {
            lc = new RestControler(this);
        }
        MultipartBuilder mb = null;
        String addedImagePath = FileUtils.getPath(this, newImageUri);
        if(mb == null){
            mb = new MultipartBuilder().type(MultipartBuilder.FORM)
                    .addFormDataPart("file", "gallery_image.jpg", RequestBody.create(MediaType.parse("image/jpeg"), new File(addedImagePath)));
        }else{
            mb.addFormDataPart("file", "gallery_image.jpg", RequestBody.create(MediaType.parse("image/jpeg"), new File(addedImagePath)));
        }
        RequestBody requestBody = mb.build();
        lc.setOnPostMPResponseListener(this);
        lc.uploadProfileImage(profile.getId(), app.getUserAccount(), requestBody, -1, false);
    }

    private void removeGalleryItems(){
        //todo ubaciti dialog za double confirmation
        newGridMode = previousGridMode;
        invalidateOptionsMenu();

        List<Integer> galleryItemIndex = new ArrayList<>();
        for(Integer index : albumGridAdapter.getMarkedImagesIndexList()){
            galleryItemIndex.add(imageAlbumGalleryItems.get(index).getId());
        }
        if(lc == null){
            lc = new RestControler(this);
        }
        lc.setOnDeleteResponseListener(this);
        lc.removeProfileGalleryImages(profile.getId(), galleryItemIndex, app.getUserAccount(), -1);

        actionTab.setVisibility(View.GONE);
        initActionBar(imageAlbumTitle);
    }



    @Override
    public void onDeleteResponse(Response response, int requestId) {
        if(response != null && response.code() == IParkoviApiService.STATUS_CODE_REMOVED_OK){
            removeDeletedImages();
            albumGridAdapter.markImage(-1);
            albumGridAdapter.notifyDataSetChanged();
            updateProfileAndBroadcast();
        }else{
            Toast.makeText(this,"Something went wrong - the images might be used in memories",Toast.LENGTH_LONG).show();
            albumGridAdapter.markImage(-1);
            albumGridAdapter.notifyDataSetChanged();
        }
        actionTab.setVisibility(View.GONE);
        albumGridAdapter.getMarkedImagesIndexList().clear();
    }

    private void removeDeletedImages() {
        Set<Integer> images2DeleteIndexes = albumGridAdapter.getMarkedImagesIndexList();
        for(int index2Delete : images2DeleteIndexes){
            imageAlbumList.remove(index2Delete);
            imageAlbumGalleryItems.remove(index2Delete);
        }
    }


    @Override
    protected void onStop() {
        super.onStop();
        if(profile != null){
            EventBus.getDefault().post(new ProfileUpdateEvent((Profile) profile));
        }

    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        EasyImage.handleActivityResult(requestCode, resultCode, data, this, new DefaultCallback() {
            @Override
            public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                //Some error handling
            }

            @Override
            public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                //Handle the image
                newImageUri = Uri.fromFile(imageFile);
                if (newImageUri != null) {
                    updateProfileGallery();
                }
            }
        });
    }



    @Override
    public void onPostMPResponse(com.squareup.okhttp.Response response, int requestId) {
        if(response != null && response.code() == HttpURLConnection.HTTP_OK) {
            ResponseBody rb = response.body();
            ArrayList<GalleryItem> profileGallery = null;
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<GalleryItem>>(){}.getType();
            try {
                profileGallery = gson.fromJson(rb.charStream(), listType);
            } catch (IOException e) {
                e.printStackTrace();
            }
            profile.getProfileGallery().addAll(profileGallery);
            EventBus.getDefault().post(new ProfileUpdateEvent((Profile) profile));
            albumGridAdapter.updateImagesList(profileGallery);
            runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    albumGridAdapter.notifyDataSetChanged();
                }
            });
        }

    }

    //*******************PERMISSIONS*****************************
    private void requireExternalStoragePermissions() {
// Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {

            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, GALLERY_PERMISSIONS);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case GALLERY_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    EasyImage.openChooserWithGallery(this, getResources().getString(R.string.select_images_from_device), 0);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
        }
    }

}
