package hr.eonideas.ecemetery.parkovi.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.commons.interfaces.ILazyLoad;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IMemory;
import hr.eonideas.ecemetery.parkovi.memories.MemoryDetailViewHolder;
import hr.eonideas.ecemetery.parkovi.memories.MemoryListViewHolder;

/**
 * Created by thirs on 7.11.2015..
 */

public class MemoriesListAdapter extends ArrayAdapter<IMemory> {
    private boolean isDetailItem;
    private Context context;
    private List<IMemory> memories;
    private ImageLoader imageLoader;
    private int layoutResourceId;
    private Integer totalCount;
    private ILazyLoad lazyLoadListener;

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public void setLazyLoadListener(ILazyLoad lazyLoadListener) {
        this.lazyLoadListener = lazyLoadListener;
    }


    private void wakeUpLoading(Integer offset){
        lazyLoadListener.updateLazyList(offset);
    }


    public void updateList(List<IMemory> profiles){
        if(profiles != null){
            this.memories.addAll(profiles);
        }else{
            this.memories = null;
        }

    }


    public MemoriesListAdapter(Activity activity, List<IMemory> memories) {
        super(activity, -1, memories);
        this.context = activity;
        this.memories = memories;
        this.imageLoader = ImageLoader.getInstance();
        this.layoutResourceId = R.layout.list_item_memory;
    }

    public MemoriesListAdapter(Activity activity, List<IMemory> memories, boolean isDetailItem) {
        super(activity, -1, memories);
        this.context = activity;
        this.memories = memories;
        this.imageLoader = ImageLoader.getInstance();
        this.isDetailItem = isDetailItem;
        this.layoutResourceId = isDetailItem ? R.layout.detail_item_memory : R.layout.list_item_memory;
    }


    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(position == memories.size()-1 && totalCount > memories.size() && lazyLoadListener != null) {
            wakeUpLoading(position+1);
        }
        Object tempHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(layoutResourceId, null);
            if(!isDetailItem)
                tempHolder = new MemoryListViewHolder(convertView);
            else
                tempHolder = new MemoryDetailViewHolder(convertView,null);
            convertView.setTag(tempHolder);
        } else {
            tempHolder = convertView.getTag();
        }
        IMemory memory = memories.get(position);

        if(isDetailItem){
            MemoryDetailViewHolder holder = (MemoryDetailViewHolder)tempHolder;
            if(memory.getAccountId() <= 0 && memory.getAuthor() == null){
                holder.authorSection.setVisibility(View.GONE);
            }else if(memory.getAccountId() > 0){
                holder.authorSection.setVisibility(View.VISIBLE);
//            imageLoader.displayImage(memory.getProfileLight().getImage().getThumbUrl(), holder.authorAvatar);
//            holder.autorName.setText(User.username);
            }else{
                holder.authorSection.setVisibility(View.VISIBLE);
                holder.authorAvatar.setVisibility(View.INVISIBLE);
                holder.authorName.setText(memory.getAuthor());
            }
            if(memory.getImage() != null){
                imageLoader.displayImage(memory.getImage().getThumbUrl(), holder.memoryImage);
            }
            holder.memoryText.setText(memory.getMemoryText());
        }else{
            MemoryListViewHolder holder = (MemoryListViewHolder)tempHolder;
            if(memory.getProfile().getImage() != null){
                imageLoader.displayImage(memory.getImage().getThumbUrl(), holder.memoryAvatar);
            }else{
                holder.memoryAvatar.setImageResource(R.drawable.placeholder_image);
            }
            holder.profileName.setText(ProfileUtils.getProfileName(memory.getProfile()));
            holder.profilePeriod.setText(ProfileUtils.getLifePeriod(memory.getProfile(),context));
            if(memory.getAccountId() <= 0 && memory.getAuthor() == null){
                holder.authorSection.setVisibility(View.GONE);
            }else if(memory.getAccountId() > 0){
                holder.authorSection.setVisibility(View.VISIBLE);
//            imageLoader.displayImage(memory.getProfileLight().getImage().getThumbUrl(), holder.authorAvatar);
//            holder.autorName.setText(User.username);
            }else{
                holder.authorSection.setVisibility(View.VISIBLE);
                holder.authorAvatar.setVisibility(View.INVISIBLE);
                holder.authorName.setText(memory.getAuthor());
            }
        }


        return convertView;
    }
}