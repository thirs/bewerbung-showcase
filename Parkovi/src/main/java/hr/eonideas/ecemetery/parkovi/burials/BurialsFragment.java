package hr.eonideas.ecemetery.parkovi.burials;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.BurialAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.IBurial;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import retrofit.Response;
import se.emilsjolander.stickylistheaders.StickyListHeadersListView;

/**
 * Created by thirs on 24.10.2015..
 */
public class BurialsFragment extends BasicFragment implements IOnGetResponseListener {

    public static final String TAG = "BURIALS_FRAGMENT";
    private RestControler lc;
    private StickyListHeadersListView stickyList;
    private BurialAdapter adapter;
    private List<IBurial> burialList;


    public static BurialsFragment newInstance() {
        BurialsFragment fragment = new BurialsFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public BurialsFragment() {
        // Required empty public constructor
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return getResources().getString(R.string.navbar_menu_item_burials);
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(lc == null){
            lc = new RestControler(getActivity());
        }
        lc.setOnGetResponseListener(this);
        lc.getBurialList();
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.burials_fragment, container, false);
        ((ImageView)view.findViewById(R.id.header_img)).setImageResource(R.drawable.schedule_image);
        ((TextView)view.findViewById(R.id.header_title)).setText(R.string.navbar_menu_item_burials);
        TextView headerSubTitle = ((TextView)view.findViewById(R.id.header_subtitle));
        headerSubTitle.setText(R.string.burials_header_subtitle);
        headerSubTitle.setVisibility(View.VISIBLE);
        stickyList = (StickyListHeadersListView) view.findViewById(R.id.list);

        if(burialList != null && !burialList.isEmpty()){
            updateListView();
        }
        return view;
    }


    private void updateListView() {
        if(!burialList.isEmpty() && burialList != null){
            adapter = new BurialAdapter(getActivity(), burialList);
            stickyList.setAdapter(adapter);
        }
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response != null){
            if(((PagedResult)response.body()).getItems() instanceof List && !((List)((PagedResult)response.body()).getItems()).isEmpty()){
                burialList = (List<IBurial>) ((PagedResult)response.body()).getItems();
                updateListView();
            }
        }
    }
}
