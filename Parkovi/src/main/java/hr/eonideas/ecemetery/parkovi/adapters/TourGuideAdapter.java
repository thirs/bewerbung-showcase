package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.test.suitebuilder.TestMethod;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.facebook.TestUserManager;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.commons.utils.FormUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.ITourGuide;

/**
 * Created by thirs on 26.10.2015..
 */
public class TourGuideAdapter extends RecyclerView.Adapter<TourGuideAdapter.ToursViewHolder> {
    private List<ITourGuide> tourGuides;
    private Context context;
    private ImageLoader imageLoader;
    OnItemClickListener mItemClickListener;

    public TourGuideAdapter(Context context, List<ITourGuide> tourGuides){
        this.context = context;
        this.tourGuides = hideWithoutPOIs(tourGuides);
//        this.tourGuides = tourGuides;
        this.imageLoader = ImageLoader.getInstance();
    }

    public TourGuideAdapter(Context context, List<ITourGuide> tourGuides, OnItemClickListener listener){
        this(context, tourGuides);
        this.mItemClickListener = listener;
    }

    private List<ITourGuide> hideWithoutPOIs(List<ITourGuide> sourceList) {
        List<ITourGuide> cleanded = new ArrayList<ITourGuide>();
        for(ITourGuide tourGuide : sourceList){
            if(tourGuide.getPOI().size()>0){
                cleanded.add(tourGuide);
            }
        }
        return cleanded;
    }

    @Override
    public ToursViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.tours_item, null);
        ToursViewHolder viewHolder = new ToursViewHolder(view);
        return viewHolder;
    }

    @Override
    public void onBindViewHolder(final ToursViewHolder holder, int position) {
        ITourGuide item = tourGuides.get(position);
        imageLoader.displayImage(item.getImage().getFullUrl(), holder.tour_img);
        holder.tour_title.setText(item.getTitle());
        if(!TextUtils.isEmpty(item.getSubTitle())){
            holder.tour_subtitle.setText(item.getSubTitle());
        }else{
            holder.tour_subtitle.setVisibility(View.GONE);
        }

        holder.tour_short_description.setText(item.getShortText());
        holder.tour_poi_number.setText(String.valueOf(item.getPOI().size()));
        holder.tour_length.setText(String.valueOf(item.getDistance()));
        holder.tour_duration.setText(String.valueOf(item.getDuration()));
    }


    @Override
    public int getItemCount() {
        return tourGuides.size();
    }

    public class ToursViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
        protected LinearLayout tour_container;
        protected ImageView tour_img;
        protected TextView tour_title;
        protected TextView tour_subtitle;
        protected TextView tour_short_description;
        protected TextView tour_poi_number;
        protected TextView tour_length;
        protected TextView tour_duration;

        public ToursViewHolder(View itemView) {
            super(itemView);
            tour_container = (LinearLayout) itemView.findViewById(R.id.tour_container);
            tour_img = (ImageView) itemView.findViewById(R.id.header_img);
            tour_title = (TextView) itemView.findViewById(R.id.header_title);
            tour_subtitle = (TextView) itemView.findViewById(R.id.header_subtitle);
            tour_subtitle.setVisibility(View.VISIBLE);
            tour_short_description = (TextView) itemView.findViewById(R.id.tour_short_description);
            tour_poi_number = (TextView) itemView.findViewById(R.id.tour_poi_number);
            tour_length = (TextView) itemView.findViewById(R.id.tour_length);
            tour_duration = (TextView) itemView.findViewById(R.id.tour_duration);

            tour_container.setOnClickListener(this);
        }

        @Override
        public void onClick(View v) {
            if(mItemClickListener != null){
                mItemClickListener.onItemClick(v, getAdapterPosition());
            }
        }
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void SetOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}
