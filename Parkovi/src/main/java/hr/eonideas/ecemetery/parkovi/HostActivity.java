package hr.eonideas.ecemetery.parkovi;

import android.content.Intent;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.main.MainMenuFragment;

/**
 * Created by thirs on 17.10.2015..
 */
public class HostActivity extends NavigationActivity {


    @Override
    protected void init() {
        super.init();
        FragmentManager fragmentManager = getSupportFragmentManager();
        Fragment fragment = (Fragment) fragmentManager.findFragmentByTag(MainMenuFragment.TAG);
        if (fragment == null) {
            fragment = MainMenuFragment.newInstance();
        }
        manageFragment(fragment, MainMenuFragment.TAG, false);
    }
    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        /**child v4.fragments aren't receiving this due to bug. So forward to child fragments manually
         * https://code.google.com/p/android/issues/detail?id=189121
         */
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        List<Fragment> fragments = getSupportFragmentManager().getFragments();
        if (fragments != null) {
            for (Fragment fragment : fragments) {
                if(fragment != null){
                    fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                }
            }
        }
    }
}
