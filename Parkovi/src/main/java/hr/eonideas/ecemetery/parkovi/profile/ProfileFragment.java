package hr.eonideas.ecemetery.parkovi.profile;

import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.facebook.AccessToken;
import com.facebook.FacebookCallback;
import com.facebook.FacebookException;
import com.facebook.share.ShareApi;
import com.facebook.share.Sharer;
import com.facebook.share.model.SharePhoto;
import com.facebook.share.model.SharePhotoContent;
import com.github.clans.fab.FloatingActionButton;
import com.github.clans.fab.FloatingActionMenu;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.assist.FailReason;
import com.nostra13.universalimageloader.core.listener.ImageLoadingListener;
import com.soundcloud.android.crop.Crop;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.List;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.DrawerOpenEvent;
import hr.eonideas.ecemetery.commons.busevents.ProfileFavoriteEvent;
import hr.eonideas.ecemetery.commons.busevents.ProfileUpdateEvent;
import hr.eonideas.ecemetery.commons.utils.FileUtils;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.commons.widgets.CustomViewPager;
import hr.eonideas.ecemetery.parkovi.Manifest;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.SmartFragmentPagerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.interfaces.IMemory;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.profile.details.ProfileDetailsFragment;
import hr.eonideas.ecemetery.parkovi.profile.map.ProfileMapFragment;
import hr.eonideas.ecemetery.parkovi.profile.memories.CreateMemoryActivity;
import hr.eonideas.ecemetery.parkovi.profile.memories.ProfileMemoriesFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnDeleteResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostMPResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit.Response;

/**
 * Created by thirs on 24.10.2015..
 */
public class ProfileFragment extends BasicFragment implements View.OnClickListener, IOnPostResponseListener, IOnDeleteResponseListener, IOnPostMPResponseListener{
    public static final String TAG = "PROFILE_HOST";
    public static final String MEMORY = Filter.PACKAGE + ".memory";
    public static final String CREATED_MEMORY = Filter.PACKAGE + ".createdmemory";
    public static final String PROFILE_LIGHT = Filter.PACKAGE + ".profilelight";
    public static final String UPDATED_PROFILE = Filter.PACKAGE + ".UPDATED_PROFILE";

    private static final int ACTION_MAKE_ME_ADMIN           = 31;
    private static final int ACTION_WRITE_NEW_MEMORY        = 32;
    private static final int ACTION_ORDER_E_SERVICE         = 33;
    private static final int ACTION_MAKE_FAVORITE           = 34;
    private static final int ACTION_REMOVE_FROM_FAVORITES   = 35;
    private static final int ACTION_REQUEST_OWNERSHIP       = 36;
    private static final int ACTION_CREATE_NEW_MEMORY       = 37;
    private static final int ACTION_SELECT_HEADER_IMG       = 38;
    private static final int ACTION_REQUEST_CHANGE_AVATAR   = 39;

    private static final int GALLERY_PERMISSIONS = 3;
    private static final int SHARE_PERMISSIONS = 4;


    private Profile profile;
    private ImageLoader imageLoader;
    private RestControler lc;

    private PagerSlidingTabStrip tabs;
    private ImageView headerImage;
    private CustomViewPager viewPager;
    private View headerStartSection;
    private View headerEndSection;
    private TextView headerStartDate;
    private TextView headerEndDate;
    private FloatingActionMenu floatingMenu;
    private CheckBox addToFavorites;

    private FloatingActionButton makeMeAdminItem;
    private FloatingActionButton writeNewMemory;
    private FloatingActionButton orderEService;
    private SmartFragmentPagerAdapter adapter;

    private Memory newMemory;
    private Memory memory;
    private Account account;
    private Drawable originalImage;
    private String selectedImagePath;
    private FragmentActivity activity;
    private boolean expectingResult = true;
    private boolean returnUpdatedProfile = false;
    private Uri newAvatarImageUri;

    private Bitmap profileHeaderBitmap;
    private View headerContainer;


    public static ProfileFragment newInstance() {
        ProfileFragment fragment = new ProfileFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ProfileFragment() {
        // Required empty public constructor
    }

    public Memory getNewMemory() {
        Memory m = null;
        if(newMemory != null){
            m = new Memory(newMemory);
            newMemory = null;
        }
        return m;
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        String title = null;
        if(profile != null)
            title = ProfileUtils.getProfileName(profile);
        return title;
    }

    //Eventbus is used to sinkronize navigationdrawer with floating menu
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    @Override
    protected void backButtonHit() {
        expectingResult = false;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        menu.clear();
        inflater.inflate(R.menu.profile_menu, menu);

    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.share_menu){
            if(AccessToken.getCurrentAccessToken() == null || !AccessToken.getCurrentAccessToken().getPermissions().contains("publish_actions")){

                return true;
            }else{
                shareDialog(profileHeaderBitmap);
            }
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }

    // This method is used to share Image on facebook timeline by SharePhotoContent model.
    public void shareDialog(Bitmap profileBitmap){
        addToFavorites.setVisibility(View.GONE);
        profileHeaderBitmap = FileUtils.loadBitmapFromView(headerContainer);
        addToFavorites.setVisibility(View.VISIBLE);
        SharePhoto photo = new SharePhoto.Builder()
                .setBitmap(profileHeaderBitmap)
                .setUserGenerated(false)
                .setCaption("Testing")
                .build();
        SharePhotoContent content = new SharePhotoContent.Builder()
                .addPhoto(photo)
                .build();

        ShareApi.share(content, new FacebookCallback<Sharer.Result>() {
            @Override
            public void onSuccess(Sharer.Result result) {
                Toast.makeText(getActivity(),"Facebook share success!!", Toast.LENGTH_LONG).show();
            }

            @Override
            public void onCancel() {

            }

            @Override
            public void onError(FacebookException error) {
                Toast.makeText(getActivity(),"Facebook share error!!", Toast.LENGTH_LONG).show();
                Log.i(TAG,error.toString());

            }
        });

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        activity = getActivity();
//        shareDialog = new ShareDialog(getActivity());
        //prepare objects and check mandatory
        imageLoader = ImageLoader.getInstance();
        lc = new RestControler(getActivity());
        lc.setOnPostResponseListener(this);
        lc.setOnDeleteResponseListener(this);
        adapter = new SmartFragmentPagerAdapter(this, getChildFragmentManager());

        //prepare passed data
        memory = (Memory) getArguments().getParcelable(MEMORY);
        profile = (Profile) getArguments().getParcelable(PROFILE_LIGHT);
        if(profile == null && memory != null){
            profile = memory.getProfile();
            getArguments().putParcelable(PROFILE_LIGHT,(Profile)profile);
        }
        if(profile == null){
            //TODO: nešt ne štima - treba baciti neki exception ili nekak odhendlati
        }
        profile.setIsOwned(profile.getOwnerAccount() != null);
        //setting up Account-Profile relationship
        if(app.isLoggedIn()){
            account = Account.getInstance(null);
            profile.setIsFavorite(ProfileUtils.isProfileInAccountFavorites(account, profile));
            profile.setIsAdmin(ProfileUtils.isAccountProfilesOwner(account, profile));
        }

        // Initialize views and adapter
        View view = inflater.inflate(R.layout.profile_tab_layout_w_header, container, false);
        initLayoutElements(view);
        setActionBar(ProfileUtils.getProfileName(profile), true);

        //Present profile data header
        presentLayoutElements();
        buildFloatingMenu();

        //asdfa
        if (profile.isAdmin()) {
            profile.setIsInEditMode(true);
        }else{
            profile.setIsInEditMode(false);
        }
        makeProfileContentEditable(profile.isInEditMode());
        return view;
    }


    private void makeProfileContentEditable(boolean editable) {
        if(editable){
            headerImage.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EasyImage.openChooserWithGallery(ProfileFragment.this, activity.getResources().getString(R.string.select_images_from_device), 0);
                }
            });
            if(Filter.isDebuggingMode)
                Toast.makeText(getActivity(), "Editable activated", Toast.LENGTH_SHORT).show();
        }else{
            headerImage.setOnClickListener(null);
            if(Filter.isDebuggingMode)
                Toast.makeText(getActivity(), "Editable Deactivated",Toast.LENGTH_SHORT).show();
        }
    }


    private void presentLayoutElements() {
        if(profile.getImage() != null)
            //displayImage(profile.getImage().getFullUrl(), headerImage)
            imageLoader.loadImage(profile.getImage().getFullUrl(), new ImageLoadingListener() {
                @Override
                public void onLoadingStarted(String imageUri, View view) {

                }

                @Override
                public void onLoadingFailed(String imageUri, View view, FailReason failReason) {

                }

                @Override
                public void onLoadingComplete(String imageUri, View view, Bitmap loadedImage) {
                    profileHeaderBitmap = loadedImage;
                    headerImage.setImageBitmap(loadedImage);
                }

                @Override
                public void onLoadingCancelled(String imageUri, View view) {

                }
            });

        //Date/Birth data
        headerStartSection.setVisibility(View.VISIBLE);
        headerStartDate.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.cradle_green),null,null,null);
        headerStartDate.setCompoundDrawablePadding(5);
        if(profile.getBirth() != null){
            headerStartDate.setText(profile.getBirth());
        }else
            headerStartDate.setText(getContext().getResources().getString(R.string.date_unknown));

        headerEndSection.setVisibility(View.VISIBLE);
        if(profile.getDeath() != null){
            headerEndDate.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.icon_cross_green),null, null, null);
            headerEndDate.setCompoundDrawablePadding(5);
            headerEndDate.setText(profile.getDeath());
        }else if(profile.getBurial() != null){
            headerEndDate.setCompoundDrawablesWithIntrinsicBounds(getContext().getResources().getDrawable(R.drawable.icon_headstone_green),null,null,null);
            headerEndDate.setCompoundDrawablePadding(5);
            headerEndDate.setText(profile.getBurial());
        }else
            headerEndDate.setText(getContext().getResources().getString(R.string.date_unknown));

        //Viewpager tabs
        adapter.addTab(getActivity().getResources().getString(R.string.profile_details_title), ProfileDetailsFragment.class, getArguments());
        if(profile.getNumberOfMemories() > 0){
            addMemoriesTab();
        }
        if(profile.getLocation() != null){
            addLocationTab();
        }
        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
        if(memory != null){
            viewPager.setCurrentItem(1);
        }else
            viewPager.setCurrentItem(0);
    }

    private void addLocationTab() {
        adapter.addTab(getActivity().getResources().getString(R.string.profile_location_title), ProfileMapFragment.class, getArguments());
    }

    private void addMemoriesTab() {
        adapter.addTab(getActivity().getResources().getString(R.string.profile_memories_title), ProfileMemoriesFragment.class, getArguments());
    }

    private void initLayoutElements(View view) {
        viewPager = (CustomViewPager) view.findViewById(R.id.viewpager);
        headerImage = (ImageView) view.findViewById(R.id.header_img);
        headerContainer = view.findViewById(R.id.image_text_header_container);
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        headerStartSection = view.findViewById(R.id.profile_details_start_date_section);
        headerEndSection = view.findViewById(R.id.profile_details_end_date_section);
        headerStartDate = (TextView) view.findViewById(R.id.profile_details_start_date);
        headerEndDate = (TextView) view.findViewById(R.id.profile_details_end_date);
        addToFavorites = (CheckBox) view.findViewById(R.id.profile_details_favorite);
        if(!app.isLoggedIn()){
            addToFavorites.setVisibility(View.GONE);
        }else{
            addToFavorites.setChecked(profile.isFavorite());
        }
        addToFavorites.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked){
                    lc.setAsFavorite(profile.getId(), Account.getInstance(null), ACTION_MAKE_FAVORITE);
                }
                else {
                    lc.removeFromFavorites(profile.getId(), Account.getInstance(null), ACTION_REMOVE_FROM_FAVORITES);
                }


            }
        });
        floatingMenu = (FloatingActionMenu) view.findViewById(R.id.menu1);
    }

    @SuppressWarnings("ResourceType")
    private void buildFloatingMenu(){

        if(app.isLoggedIn()){
            if(!profile.isAdmin() && !profile.isOwned()){
                makeMeAdminItem = new FloatingActionButton(getActivity());
                styleFloatingMenuItem(makeMeAdminItem);
                makeMeAdminItem.setId(ACTION_MAKE_ME_ADMIN);
                makeMeAdminItem.setLabelText("Make me admin");
                makeMeAdminItem.setImageResource(R.drawable.ic_edit);
                makeMeAdminItem.setOnClickListener(this);
                floatingMenu.addMenuButton(makeMeAdminItem);
            }

            writeNewMemory = new FloatingActionButton(getActivity());
            styleFloatingMenuItem(writeNewMemory);
            writeNewMemory.setId(ACTION_WRITE_NEW_MEMORY);
            writeNewMemory.setLabelText("Write new memory");
            writeNewMemory.setImageResource(R.drawable.ic_edit);
            writeNewMemory.setOnClickListener(this);
            floatingMenu.addMenuButton(writeNewMemory);

            orderEService = new FloatingActionButton(getActivity());
            styleFloatingMenuItem(orderEService);
            orderEService.setId(ACTION_ORDER_E_SERVICE);
            orderEService.setLabelText("Order e-service");
            orderEService.setImageResource(R.drawable.ic_edit);
            orderEService.setOnClickListener(this);
            floatingMenu.addMenuButton(orderEService);
        }else
            floatingMenu.setVisibility(View.GONE);
    }

    private void styleFloatingMenuItem(FloatingActionButton menuItem){
        menuItem.setButtonSize(FloatingActionButton.SIZE_MINI);
        menuItem.setColorNormalResId(R.color.colorNormal);
        menuItem.setColorPressedResId(R.color.colorPressed);
        menuItem.setShadowColorResource(android.R.color.transparent);
    }

    @SuppressWarnings("ResourceType")
    @Override
    public void onClick(View v) {
        if(v.getId() == ACTION_MAKE_ME_ADMIN){
            if(Filter.isDebuggingMode)Toast.makeText(getActivity(), makeMeAdminItem.getLabelText(), Toast.LENGTH_SHORT).show();
            if(lc != null){
                lc.requestOwnership(profile.getId(), Account.getInstance(null),ACTION_REQUEST_OWNERSHIP);
            }
            floatingMenu.close(true);
        }else if(v.getId() == ACTION_ORDER_E_SERVICE){
            Toast.makeText(getActivity(), orderEService.getLabelText(), Toast.LENGTH_SHORT).show();
            floatingMenu.close(true);
        }else if(v.getId() == ACTION_WRITE_NEW_MEMORY){
            createNewMemory(profile, null);
            floatingMenu.close(true);
        }
    }

    public void createNewMemory(IProfile profile, IMemory memory){
        Intent intent = new Intent(getActivity(), CreateMemoryActivity.class);
        intent.putExtra(CreateMemoryActivity.PROFILE, (Profile) profile);
        if(memory != null)
            intent.putExtra(CreateMemoryActivity.MEMORY, (Memory) memory);
        startActivityForResult(intent, ACTION_CREATE_NEW_MEMORY);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == ACTION_CREATE_NEW_MEMORY && data != null){
            newMemory = data.getParcelableExtra(CREATED_MEMORY);
            int initialNumberOfMemories = profile.getNumberOfMemories();
            profile = data.getParcelableExtra(UPDATED_PROFILE);
            if(profile.getNumberOfMemories() == 1 && profile.getNumberOfMemories() != initialNumberOfMemories){
                returnUpdatedProfile = true;
                addMemoriesTab();
            }
            adapter.notifyDataSetChanged();
        }else if(requestCode == ACTION_SELECT_HEADER_IMG){
            if(data != null){
                Uri selectedImageUri = data.getData();
                selectedImagePath = FileUtils.getPath(getActivity(), selectedImageUri);
                System.out.println("Image Path : " + selectedImagePath);
                if(selectedImageUri != null){
                    originalImage = headerImage.getDrawable();
                    headerImage.setImageURI(selectedImageUri);
                    profile.setHeaderImageModified(true);
                }
            }
        }else if (requestCode == Crop.REQUEST_CROP && resultCode == Activity.RESULT_OK) {
            originalImage = headerImage.getDrawable();
            headerImage.setImageURI(newAvatarImageUri);
            profile.setHeaderImageModified(true);
            updateProfileAvatar();

        }else{
            super.onActivityResult(requestCode, resultCode, data);
            EasyImage.handleActivityResult(requestCode, resultCode, data, activity, new DefaultCallback() {
                @Override
                public void onImagePickerError(Exception e, EasyImage.ImageSource source, int type) {
                    //Some error handling
                }

                @Override
                public void onImagePicked(File imageFile, EasyImage.ImageSource source, int type) {
                    //Handle the image
                    Uri selectedImageUri = Uri.fromFile(imageFile);
                    newAvatarImageUri = Uri.fromFile(new File(activity.getCacheDir(), "cropped"));
                    Crop.of(selectedImageUri, newAvatarImageUri).withAspect(3,2).start(activity.getApplicationContext(), ProfileFragment.this);
                }
            });

            //(th) the rest of the code is used to forward result to child fragments
            List<Fragment> fragments = getChildFragmentManager().getFragments();
            if (fragments != null) {
                for(Fragment f : fragments){
                    if(f != null){
                        f.onActivityResult(requestCode, resultCode, data);
                    }
                }
            }
        }
    }

    @Override
    public void onPostResponse(Response response, int requestId) {
        if(response.body() instanceof ResponseBody){
            ResponseBody message = (ResponseBody)response.body();
        }
        if(response.code() == IParkoviApiService.STATUS_CODE_POST_OK){
            switch (requestId){
                case ACTION_MAKE_FAVORITE:
                    if(Filter.isDebuggingMode)
                        Toast.makeText(getActivity(),"Added to favorites", Toast.LENGTH_LONG).show();
                    account.getFavoriteProfiles().add((Profile)profile);
                    break;
                case ACTION_REQUEST_OWNERSHIP:
                    if(Filter.isDebuggingMode)
                        Toast.makeText(getActivity(),"Ownership requested", Toast.LENGTH_LONG).show();
                    account.getOwnedProfiles().add((Profile) profile);
                    floatingMenu.removeAllMenuButtons();
                    profile.setIsAdmin(true);
                    buildFloatingMenu();
                    break;
            }
        }

    }

    @Override
    public void onDeleteResponse(Response response, int requestId) {
        if(response.body() instanceof ResponseBody){
            ResponseBody message = (ResponseBody)response.body();
        }
        if(response.code() == IParkoviApiService.STATUS_CODE_REMOVED_OK){
            if(Filter.isDebuggingMode)
                Toast.makeText(getActivity(),"Removed from favorites", Toast.LENGTH_LONG).show();
            int favoriteListPosition = ProfileUtils.getProfileListPosition(profile,account.getFavoriteProfiles());
            account.getFavoriteProfiles().remove(favoriteListPosition);
            EventBus.getDefault().post(new ProfileFavoriteEvent(false, profile));
        }
    }




    @Override
    public void onStop() {
        super.onStop();
        if(returnUpdatedProfile){
            EventBus.getDefault().post(new ProfileUpdateEvent(profile));
        }
        EventBus.getDefault().unregister(this);
        if(profile.isInEditMode()){
            //todo ubaciti dialog za potvrdu izlaska iz fragmenta i gubljenja podataka
        }
        if(!expectingResult){
            profile.setIsInEditMode(false);
        }
        selectedImagePath = null;
        originalImage = null;
    }

    public void onEvent(DrawerOpenEvent event){
        floatingMenu.close(true);
    }

    @Override
    public void onPostMPResponse(com.squareup.okhttp.Response response, int requestId) {
        if(response != null && response.code() == HttpURLConnection.HTTP_OK){
            ResponseBody rb = response.body();
            Image image = null;
            Gson gson = new Gson();
            try {
                image = gson.fromJson(response.body().charStream(), Image.class);
            } catch (IOException e) {
                e.printStackTrace();
            }
            if(image != null){
                profile.setImage(image);
            }
            app.clearImageLoaderCache(image.getThumbUrl());
            app.clearImageLoaderCache(image.getFullUrl());
        }
    }


    //*******************PERMISSIONS*****************************
    private void requireExternalStoragePermissions(int permissionRequestId) {
// Should we show an explanation?
        if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE)) {

            // Show an expanation to the user *asynchronously* -- don't block
            // this thread waiting for the user's response! After the user
            // sees the explanation, try again to request the permission.

        } else {
            // No explanation needed, we can request the permission.
            ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, permissionRequestId);

        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case GALLERY_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    updateProfileAvatar();
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            case SHARE_PERMISSIONS: {
                // If request is cancelled, the result arrays are empty.
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    shareDialog(profileHeaderBitmap);
                } else {

                    // permission denied, boo! Disable the
                    // functionality that depends on this permission.
                }
                return;
            }
            default:
                List<Fragment> fragments = getChildFragmentManager().getFragments();
                if (fragments != null) {
                    for (Fragment fragment : fragments) {
                        fragment.onRequestPermissionsResult(requestCode, permissions, grantResults);
                    }
                }
        }
    }

    private void updateProfileAvatar() {
        if(lc != null && newAvatarImageUri != null){
            RequestBody requestBody = new MultipartBuilder().type(MultipartBuilder.FORM)
                    .addFormDataPart("file", "profile-avatar-image.jpg", RequestBody.create(MediaType.parse("image/jpeg"), new File(FileUtils.getPath(getActivity(), newAvatarImageUri))))
                    .build();

            int permissionCheck = ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.READ_EXTERNAL_STORAGE);
            if(permissionCheck != PackageManager.PERMISSION_GRANTED){
                requireExternalStoragePermissions(GALLERY_PERMISSIONS);
                return;
            }

            lc.setOnPostMPResponseListener(this);
            lc.uploadProfileImage(profile.getId(), account, requestBody, ACTION_REQUEST_CHANGE_AVATAR, true);
        }
    }
}
