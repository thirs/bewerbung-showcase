package hr.eonideas.ecemetery.parkovi.settings;

import android.app.Activity;
import android.app.DialogFragment;
import android.content.Intent;
import android.content.res.Configuration;
import android.content.res.Resources;
import android.os.Bundle;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Locale;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.busevents.GenBooleanEvent;
import hr.eonideas.ecemetery.commons.dialogs.LanguageDialog;
import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by Teo on 1.3.2016..
 */
public class SettingsActivty extends BaseActivity implements View.OnClickListener, LanguageDialog.LanguageChangeListener {
    private static final String TAG = Filter.PACKAGE + ".SettingsActivity";
    public static final String LANGUAGE_CHANGED = "LANGUAGE_CHANGED";
    private View languageSetting;
    private TextView selectedLanguage;
    private String selectedLangLocal;
    private String[] languagesList;
    private String[] languageLocalsList;
    private boolean langChanged = false;
    private TextView clearCacheLink;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }

    private void init() {
        setContentView(R.layout.settings);
        initActionBar(getResources().getString(R.string.main_menu_title));

        initializeLayout();

        langChanged = getIntent().getBooleanExtra(LANGUAGE_CHANGED,false);
        languagesList = getResources().getStringArray(R.array.language_items);
        languageLocalsList = getResources().getStringArray(R.array.language_items_locals);
        selectedLangLocal = app.getAppLanguage();
        languageSetting.setOnClickListener(this);
        clearCacheLink.setOnClickListener(this);

        setActiveLanguageValue();
    }

    private void setActiveLanguageValue() {
        for(int i = 0; i<languageLocalsList.length;i++){
            if(languageLocalsList[i].equals(selectedLangLocal)){
                selectedLanguage.setText(languagesList[i]);
            }
        }
    }

    private void initializeLayout() {
        languageSetting = findViewById(R.id.change_language);
        selectedLanguage = (TextView)findViewById(R.id.selected_language);
        clearCacheLink = (TextView)findViewById(R.id.clear_cache);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()){
            case R.id.change_language:
                if(Filter.isDebuggingMode) {
                    Toast.makeText(this,"Language settings pressed",Toast.LENGTH_SHORT).show();
                }
                showLanguageSelectionDialog();
                break;
            case R.id.clear_cache:
                if(Filter.isDebuggingMode) {
                    Toast.makeText(this,"Image loader cache cleared",Toast.LENGTH_SHORT).show();
                }
                app.clearImageLoaderCache();
        }
    }

    private void showLanguageSelectionDialog() {
        DialogFragment dialog = new LanguageDialog(this,selectedLangLocal);
        dialog.show(getFragmentManager(), null);
    }

    @Override
    public void languageChanged(String languageLocal) {
        Log.d(TAG, "Selected language is different as current. Language local is " + languageLocal);
        langChanged = true;
        app.setAppLanguage(languageLocal);
        selectedLangLocal = languageLocal;
        setActiveLanguageValue();
        Intent refresh = new Intent(this, SettingsActivty.class);
        refresh.putExtra(LANGUAGE_CHANGED, langChanged);
        startActivity(refresh);
        finish();
    }

    @Override
    public void languageUnChanged(String languageLocal) {
        Log.d(TAG, "Selected language is same as current. Language local is " + languageLocal);
    }

    @Override
    public void finish() {
        // Prepare data intent
        Intent resultIntent = new Intent();
        resultIntent.putExtra(LANGUAGE_CHANGED, langChanged);
        setResult(Activity.RESULT_OK, resultIntent);
        super.finish();
    }

    @Override
    protected void onStop() {
        super.onStop();
        EventBus.getDefault().post(new GenBooleanEvent(langChanged));
    }

    @Override
    public void onConfigurationChanged(Configuration newConfig) {
        super.onConfigurationChanged(newConfig);
    }
}
