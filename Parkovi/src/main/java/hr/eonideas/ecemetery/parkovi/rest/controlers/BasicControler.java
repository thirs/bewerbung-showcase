package hr.eonideas.ecemetery.parkovi.rest.controlers;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.res.Resources;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;

import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.io.BufferedInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.KeyStoreException;
import java.security.NoSuchAlgorithmException;
import java.security.cert.Certificate;
import java.security.cert.CertificateException;
import java.security.cert.CertificateFactory;
import java.security.cert.X509Certificate;
import java.util.List;
import java.util.concurrent.TimeUnit;

import javax.net.ssl.SSLContext;
import javax.net.ssl.TrustManagerFactory;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.data.AccountLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountRegister;
import hr.eonideas.ecemetery.parkovi.data.MemoryParameter;
import hr.eonideas.ecemetery.parkovi.data.ProfileParameter;
import hr.eonideas.ecemetery.parkovi.interfaces.IAccount;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnDeleteResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnFailedResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostMPResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPutResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;
import hr.eonideas.ecemetery.parkovi.rest.model.AdvertiserCategory;
import hr.eonideas.ecemetery.parkovi.rest.model.Burial;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryHistory;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.Config;
import hr.eonideas.ecemetery.parkovi.rest.model.Gallery;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.model.TourGuide;
import hr.eonideas.ecemetery.parkovi.rest.requestInterceptors.AuthTokenHeaderInterceptor;
import hr.eonideas.ecemetery.parkovi.rest.requestInterceptors.AuthorizationHeaderInterceptor;
import hr.eonideas.ecemetery.parkovi.rest.requestInterceptors.HttpLoggingInterceptor;
import hr.eonideas.ecemetery.parkovi.rest.requestInterceptors.LanguageHeaderInterceptor;
import hr.eonideas.ecemetery.parkovi.rest.requestInterceptors.LoggingInterceptor;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import retrofit.Callback;
import retrofit.GsonConverterFactory;
import retrofit.Response;
import retrofit.Retrofit;

/**
 * Created by thirs on 11.10.2015..
 */
public class BasicControler {
    private static final String LOG_TAG = "BasicControler";

    //successful request listeners
    protected IOnGetResponseListener  onGetResponseListener;
    protected IOnPostResponseListener onPostResponseListener;
    protected IOnPutResponseListener onPutResponseListener;
    protected IOnPostMPResponseListener onPostMPResponseListener;
    protected IOnDeleteResponseListener onDeleteResponseListener;
    protected IOnFailedResponseListener onFailedResponseListener;

    protected Context context;
    protected ProgressDialog mProgressDialog;
    protected String lang;

    public BasicControler(Context context) {
        this(context,Filter.default_locale.getLanguage());
    }

    public BasicControler(Context context, String lang) {
        this.context = context;
        mProgressDialog = new ProgressDialog(context);
        this.lang = lang;
    }

    //creates new RestAdapter and adds default headers attached to every rest request
    protected IParkoviApiService getRestService(){
        return getRestService(null);
    }

    protected IParkoviApiService getRestService(IAccount account){
        Retrofit retrofit = new Retrofit.Builder()
                .baseUrl(Filter.resApi_Url)
                .addConverterFactory(GsonConverterFactory.create())
                .client(getHTTPClient(account))
                .build();
        IParkoviApiService service = retrofit.create(IParkoviApiService.class);
//        IParkoviApiService service = new DummyParkoviApiService();
        return service;
    }

    protected OkHttpClient getHTTPClient(IAccount account) {
        OkHttpClient okHttpClient = new OkHttpClient();
        okHttpClient.setReadTimeout(Filter.READ_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.setConnectTimeout(Filter.CONNECT_TIMEOUT, TimeUnit.SECONDS);
        okHttpClient.interceptors().add(new AuthorizationHeaderInterceptor());
        okHttpClient.interceptors().add(new LanguageHeaderInterceptor(lang));
        if(account != null){
            okHttpClient.interceptors().add(new AuthTokenHeaderInterceptor(account.getAuthToken()));
        }
        if(Filter.isDebuggingMode){
            HttpLoggingInterceptor interc = new HttpLoggingInterceptor();
            interc.setLevel(HttpLoggingInterceptor.Level.BODY);
            okHttpClient.interceptors().add(interc);
        }
        if(Filter.resApi_useHTTPS){
            SSLContext sslContext = null;
            try {
                sslContext = getSSLConfig();
            } catch (CertificateException e) {
                e.printStackTrace();
            } catch (IOException e) {
                e.printStackTrace();
            } catch (KeyStoreException e) {
                e.printStackTrace();
            } catch (NoSuchAlgorithmException e) {
                e.printStackTrace();
            } catch (KeyManagementException e) {
                e.printStackTrace();
            }
            if(sslContext != null){
                okHttpClient.setSslSocketFactory(sslContext.getSocketFactory());
            }

        }
        return okHttpClient;
    }

    private SSLContext getSSLConfig() throws CertificateException, IOException,
            KeyStoreException, NoSuchAlgorithmException, KeyManagementException {

// Load CAs from an InputStream
// (could be from a resource or ByteArrayInputStream or ...)
        CertificateFactory cf = CertificateFactory.getInstance("X.509");
// From https://www.washington.edu/itconnect/security/ca/load-der.crt
        InputStream caInput = context.getResources().openRawResource(R.raw.eonideas);
        Certificate ca;
        try {
            ca = cf.generateCertificate(caInput);
            System.out.println("ca=" + ((X509Certificate) ca).getSubjectDN());
        } finally {
            caInput.close();
        }

// Create a KeyStore containing our trusted CAs
        String keyStoreType = KeyStore.getDefaultType();
        KeyStore keyStore = KeyStore.getInstance(keyStoreType);
        keyStore.load(null, null);
        keyStore.setCertificateEntry("ca", ca);

// Create a TrustManager that trusts the CAs in our KeyStore
        String tmfAlgorithm = TrustManagerFactory.getDefaultAlgorithm();
        TrustManagerFactory tmf = TrustManagerFactory.getInstance(tmfAlgorithm);
        tmf.init(keyStore);

// Create an SSLContext that uses our TrustManager
        SSLContext context = SSLContext.getInstance("TLSv1.2");
        context.init(null, tmf.getTrustManagers(), null);
        return context;
    }


    //LISTENER GET/SET
    public IOnGetResponseListener getOnGetResponseListener(){
        return onGetResponseListener;
    }

    public void setOnGetResponseListener(IOnGetResponseListener onGetResponseListener){
        this.onGetResponseListener = onGetResponseListener;
    }

    public IOnPostResponseListener getOnPostResponseListener(){
        return onPostResponseListener;
    }

    public void setOnPostResponseListener(IOnPostResponseListener onPostResponseListener){
        this.onPostResponseListener = onPostResponseListener;
    }

    public IOnPostMPResponseListener getOnPostMPResponseListener(){
        return onPostMPResponseListener;
    }

    public void setOnPostMPResponseListener(IOnPostMPResponseListener onPostMPResponseListener){
        this.onPostMPResponseListener = onPostMPResponseListener;
    }

    public void setOnPutResponseListener(IOnPutResponseListener onPutResponseListener) {
        this.onPutResponseListener = onPutResponseListener;
    }

    public void setOnDeleteResponseListener(IOnDeleteResponseListener onDeleteResponseListener) {
        this.onDeleteResponseListener = onDeleteResponseListener;
    }

    public IOnFailedResponseListener getOnFailedResponseListener() {
        return onFailedResponseListener;
    }

    public void setOnFailedResponseListener(IOnFailedResponseListener onFailedResponseListener) {
        this.onFailedResponseListener = onFailedResponseListener;
    }

    //ProgressBar
    protected void progressBar(boolean show){

        if(show && mProgressDialog != null && !mProgressDialog.isShowing() ){
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage(context.getResources().getString(R.string.loading_string));
            mProgressDialog.show();
        }else if (show && mProgressDialog == null){
            mProgressDialog = new ProgressDialog(context);
            mProgressDialog.setIndeterminate(true);
            mProgressDialog.setMessage(context.getResources().getString(R.string.loading_string));
            mProgressDialog.show();
        }else if(!show && mProgressDialog != null && mProgressDialog.isShowing()){
            mProgressDialog.dismiss();
        }

    }


    //RESTApi
    public void getConfiguration(){
        getRestService().getConfiguration().enqueue(new Callback<Config>() {
            @Override
            public void onResponse(Response<Config> response) {
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "GETConfiguration response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                // TODO: 13.10.2015. getConfiguration
            }
        });
    }
}
