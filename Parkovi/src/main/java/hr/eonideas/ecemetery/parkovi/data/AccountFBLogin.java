package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

/**
 * Created by Teo on 9.1.2016..
 */
public class AccountFBLogin implements Parcelable {
    String accessToken;

    public AccountFBLogin(String accessToken) {
        this.accessToken = accessToken;
    }

    public String getAccessToken() {
        return accessToken;
    }

    public void setAccessToken(String accessToken) {
        this.accessToken = accessToken;
    }


    protected AccountFBLogin(Parcel in) {
        accessToken = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(accessToken);
    }

    @SuppressWarnings("unused")
    public static final Creator<AccountFBLogin> CREATOR = new Creator<AccountFBLogin>() {
        @Override
        public AccountFBLogin createFromParcel(Parcel in) {
            return new AccountFBLogin(in);
        }

        @Override
        public AccountFBLogin[] newArray(int size) {
            return new AccountFBLogin[size];
        }
    };
}