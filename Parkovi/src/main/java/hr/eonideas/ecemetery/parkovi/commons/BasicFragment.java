package hr.eonideas.ecemetery.parkovi.commons;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.KeyEvent;
import android.view.MenuItem;
import android.view.View;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.interfaces.IApplicationProxy;
import hr.eonideas.ecemetery.parkovi.interfaces.IToolbarManager;

/**
 * Created by thirs on 22.10.2015..
 */
public abstract class BasicFragment extends Fragment {

    protected OnFragmentInteractionListener mListener;
    protected IApplicationProxy app;

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        app = ((BaseActivity) getActivity()).getApp();
        try {
            mListener = (OnFragmentInteractionListener) context;
        } catch (ClassCastException e) {
            throw new ClassCastException(context.toString() + " must implement OnFragmentInteractionListener");
        }
    }

    public BasicFragment() {
        super();
        setHasOptionsMenu(true);
    }

    public IApplicationProxy getApp() {
        return app;
    }

    abstract public String getDefinedTAG();

    abstract public String getScreenTitle();

    @Override
    public void onResume() {
        super.onResume();
        setTitle(getScreenTitle());
        getView().setFocusableInTouchMode(true);
        getView().requestFocus();
        getView().setOnKeyListener(new View.OnKeyListener() {
            @Override
            public boolean onKey(View v, int keyCode, KeyEvent event) {
                int code = keyCode;
                if (event.getAction() == KeyEvent.ACTION_UP && keyCode == KeyEvent.KEYCODE_BACK) {
                    backButtonHit();
                }
                return false;
            }
        });
    }


    public void setTitle(String title){
        setActionBar(title, true);
    }

    //ActionBar setup
    protected void setActionBar(String title,boolean showHomeAsUp){
        AppCompatActivity activity = (AppCompatActivity) getActivity();
        Toolbar toolbar = ((IToolbarManager)activity).getToolbar();
        activity.setSupportActionBar(toolbar);
        ActionBar actionBar = activity.getSupportActionBar();


        if(actionBar != null){
            //Actionbar things
            actionBar.setDisplayHomeAsUpEnabled(true);
            actionBar.setHomeButtonEnabled(true);
            if (title != null){
                actionBar.setTitle(title);
            }
        }

    }

    protected void backButtonHit() {
        //should be overriden if modified behaviour on backpress required
    }

    @Override
    public void onDetach() {
        super.onDetach();
        mListener = null;
    }

    /**
     * This interface must be implemented by activities that contain this
     * fragment to allow an interaction in this fragment to be communicated
     * to the activity and potentially other fragments contained in that
     * activity.
     * <p/>
     * See the Android Training lesson <a href=
     * "http://developer.android.com/training/basics/fragments/communicating.html"
     * >Communicating with Other Fragments</a> for more information.
     */
    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        public void onFragmentInteraction(int menuItemId, Class fragmentClass, Bundle args);
    }
}
