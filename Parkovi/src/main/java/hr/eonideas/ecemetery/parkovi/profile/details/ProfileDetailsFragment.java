package hr.eonideas.ecemetery.parkovi.profile.details;

import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.google.gson.reflect.TypeToken;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.lang.reflect.Type;
import java.net.HttpURLConnection;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.CRUEvent;
import hr.eonideas.ecemetery.commons.busevents.GenBooleanEvent;
import hr.eonideas.ecemetery.commons.busevents.ProfileUpdateEvent;
import hr.eonideas.ecemetery.commons.utils.FileUtils;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.Manifest;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.SimpleGalleryRecyclerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.commons.ImageAlbumActivity;
import hr.eonideas.ecemetery.parkovi.commons.ImageViewPager;
import hr.eonideas.ecemetery.parkovi.data.ProfileParameter;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.profile.ProfileFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostMPResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;
import retrofit.Response;

/**
 * Created by thirs on 6.12.2015..
 */
public class ProfileDetailsFragment extends BasicPagerFragment implements SimpleGalleryRecyclerAdapter.IOnImageClickListener, View.OnClickListener, IOnPostMPResponseListener {
    private static final int UPDATE_BIOGRAPHY_CONTENT   = 33;

    public static final String UPDATED_PROFILE = Filter.PACKAGE + ".updatedprofile";


    private IProfile profile;

    private TextView aboutContent;
    private TextView profileAuthorAbout;
    private TextView profileAuthorGallery;
    private RecyclerView galleryRecyclerView;
    private TextView emptyGallery;
    private View galleryContainer;
    private View fullGalleryLink;
    private View editAboutLink;

    private SimpleGalleryRecyclerAdapter galleryAdapter;
    private ImageView addNewImage;

    private boolean biographyUpdated;
    private boolean galleryUpdated;

    private RestControler lc;
    private List<Uri> imageList4Upload;
    private View aboutSectionTitle;
    private View gallerySectionTitle;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.profile_details, container, false);
        initiLayoutElements(view);
//        LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
//        galleryRecyclerView.setLayoutManager(layoutManager);

        profile = (IProfile) getArguments().getParcelable(ProfileFragment.PROFILE_LIGHT);
        if(profile == null){
            //todo do something in this exceptional situation
        }

        makeProfileContentEditable(profile.isInEditMode());
        setProfileAdmin();
        setAboutContent();
        setGalleryPreview();
        return view;
    }

    //Eventbus is used to sinkronize navigationdrawer with floating menu
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }

    private void initiLayoutElements(View view){
        aboutContent = (TextView)view.findViewById(R.id.about_content);
        aboutSectionTitle = view.findViewById(R.id.about_section_title);
        gallerySectionTitle = view.findViewById(R.id.gallery_section_title);

        galleryRecyclerView = (RecyclerView)view.findViewById(R.id.profile_gallery_recycler);
        profileAuthorAbout = (TextView)view.findViewById(R.id.profile_author_about);
        profileAuthorGallery = (TextView)view.findViewById(R.id.profile_author_gallery);
        galleryContainer = view.findViewById(R.id.profile_gallery_container);
        emptyGallery = (TextView)view.findViewById(R.id.empty_gallery);
        fullGalleryLink = view.findViewById(R.id.full_gallery_link);
        editAboutLink = view.findViewById(R.id.edit_about_link);
//        addNewImage = (ImageView) view.findViewById(R.id.add_new_image);
    }

    private void setProfileAdmin() {
        if(profile.getOwnerAccount() != null){
            profileAuthorAbout.setVisibility(View.VISIBLE);
            profileAuthorAbout.setText(profile.getOwnerAccount().getFullName());
            profileAuthorGallery.setVisibility(View.VISIBLE);
            profileAuthorGallery.setText(profile.getOwnerAccount().getFullName());
        }else{
            profileAuthorAbout.setVisibility(View.INVISIBLE);
            profileAuthorGallery.setVisibility(View.INVISIBLE);
        }
    }

    private void setAboutContent() {
        if(profile != null && profile.getAboutContent() != null){
            aboutContent.setText(profile.getAboutContent());
        }else
            aboutContent.setText(getContext().getString(R.string.profile_details_about_empty));
    }


    private void setGalleryPreview() {
        if(showGalleryPreview()){
            galleryContainer.setVisibility(View.VISIBLE);
            emptyGallery.setVisibility(View.GONE);
            fullGalleryLink.setVisibility(View.VISIBLE);
            galleryRecyclerView.setVisibility(View.VISIBLE);

            gallerySectionTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAlbum();
                }
            });
            fullGalleryLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAlbum();
                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(getActivity(), LinearLayoutManager.HORIZONTAL, false);
            galleryRecyclerView.setLayoutManager(layoutManager);
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            galleryAdapter = new SimpleGalleryRecyclerAdapter(Utils.filterOutImages(profile.getProfileGallery()), R.layout.image_view);
            // Set up the ViewPager with the sections adapter.
            galleryAdapter.setOnImageClickListener(this);
            galleryRecyclerView.setAdapter(galleryAdapter);
            galleryAdapter.notifyDataSetChanged();
        }else{
            if(profile.isInEditMode()){
                gallerySectionTitle.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openAlbum();
                    }
                });
                fullGalleryLink.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        openAlbum();
                    }
                });
                fullGalleryLink.setVisibility(View.VISIBLE);
            }else{
                fullGalleryLink.setVisibility(View.GONE);
            }
            galleryContainer.setVisibility(View.GONE);
            emptyGallery.setVisibility(View.VISIBLE);
        }

    }

    private boolean showGalleryPreview() {
        boolean show = true;
        if(profile == null){
            show = false;
        }else if (!(profile.getProfileGallery() != null && profile.getProfileGallery().size() > 0)){
            show = false;
        }else{
            show = true;
        }
        return show;
    }

    private void makeProfileContentEditable(boolean editable) {
        if(editable){
            editAboutLink.setVisibility(View.VISIBLE);
            editAboutLink.setOnClickListener(this);
            aboutSectionTitle.setOnClickListener(this);
        }else{
            editAboutLink.setVisibility(View.INVISIBLE);
            editAboutLink.setOnClickListener(null);
            aboutSectionTitle.setOnClickListener(null);
        }
    }


    private void openAlbum() {
        Intent intent = new Intent(getActivity(), ImageAlbumActivity.class);
        intent.putExtra(ImageAlbumActivity.ALBUM_GALLERY_ITEMS, new ArrayList(profile.getProfileGallery()));
        intent.putExtra(ImageAlbumActivity.PROFILE, (Profile) profile);
        intent.putExtra(ImageAlbumActivity.ALBUM_TITLE, ProfileUtils.getProfileName(profile));
        intent.putExtra(ImageAlbumActivity.IMAGE_GRID_MODE, ImageAlbumActivity.ImageGridMode.GALLERY);
        getParentFragment().startActivity(intent);
    }

    private void openFullSizeImage(Image imageItem) {
        Intent intent = new Intent(getActivity(), ImageViewPager.class);
        ArrayList<Image> singleItemList = new ArrayList<Image>(1);
        singleItemList.add(imageItem);
        intent.putExtra(ImageViewPager.IMAGE_INDEX, 0);
        intent.putExtra(ImageAlbumActivity.ALBUM_IMAGES, singleItemList);
        startActivity(intent);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == UPDATE_BIOGRAPHY_CONTENT && data != null){
            biographyUpdated = true;
            IProfile updatedProfile = data.getParcelableExtra(UPDATED_PROFILE);
            profile = updatedProfile;
            EventBus.getDefault().post(new ProfileUpdateEvent((Profile) profile));
            setAboutContent();
        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onClick(View v) {
        Intent intent;
        switch (v.getId()){
            case R.id.about_section_title:
            case R.id.edit_about_link:
                intent = new Intent(getActivity(), UpdateBiographyActivity.class);
                intent.putExtra(UpdateBiographyActivity.PROFILE, (Profile) profile);
                //(th) this kind if activityStart is used to forward result to child fragments
                getParentFragment().startActivityForResult(intent, UPDATE_BIOGRAPHY_CONTENT);
                break;
        }
    }

    public void onEvent(ProfileUpdateEvent event){
        profile = event.profile;
        galleryAdapter.updateImagesList(profile.getProfileGallery());
        galleryAdapter.notifyDataSetChanged();
    }

    @Override
    public void onPostMPResponse(com.squareup.okhttp.Response response, int requestId) {
        if(response != null && response.code() == HttpURLConnection.HTTP_OK) {
            ResponseBody rb = response.body();
            ArrayList<GalleryItem> profileGallery = null;
            Gson gson = new Gson();
            Type listType = new TypeToken<ArrayList<GalleryItem>>(){}.getType();
            try {
                profileGallery = gson.fromJson(rb.charStream(), listType);
            } catch (IOException e) {
                e.printStackTrace();
            }
            profile.getProfileGallery().addAll(profileGallery);
            EventBus.getDefault().post(new ProfileUpdateEvent((Profile) profile));

        }
    }

    @Override
    public void onImageClicked(Image image) {
        openFullSizeImage(image);
    }
}
