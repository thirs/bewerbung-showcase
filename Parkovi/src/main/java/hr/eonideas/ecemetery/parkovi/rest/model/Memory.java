package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.interfaces.IMemory;


public class Memory implements Parcelable, IMemory {

    @SerializedName("id")
    private Integer id = null;
    @SerializedName("memoryText")
    private String memoryText = null;
    @SerializedName("author")
    private String author = null;
    @SerializedName("createdByAccountId")
    private Integer createdByAccountId = null;
    @SerializedName("image")
    private Image image = null;
    @SerializedName("profile")
    private Profile profile = null;

    public Memory(Memory memory) {
        this(memory.getAuthor(), memory.getCreatorAccountId(), memory.getId(), memory.getImage(), memory.getMemoryText(), memory.getProfile());
    }

    public Memory(String author, Integer createdByAccountId, Integer id, Image image, String memoryText, Profile profile) {
        this.author = author;
        this.id = id;
        this.createdByAccountId = createdByAccountId;
        this.image = image;
        this.memoryText = memoryText;
        this.profile = profile;
    }

    public Memory(Profile profile) {
        this.profile = profile;
    }

    /**
     * Unique identifier of memory.
     **/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }


    /**
     * Memory's main text.
     **/
    public String getMemoryText() {
        return memoryText;
    }

    public void setMemoryText(String memoryText) {
        this.memoryText = memoryText;
    }


    /**
     * Author of this profile memory.
     **/
    public String getAuthor() {
        return author;
    }

    @Override
    public int getAccountId() {
        return 0;
    }

    public void setAuthor(String author) {
        this.author = author;
    }


    public Integer getCreatorAccountId() {
        return createdByAccountId;
    }

    public void setCreatorAccountId(Integer createdByAccountId) {
        this.createdByAccountId = createdByAccountId;
    }

    /**
     **/
    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }


    /**
     **/
    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }


    @Override
    public int determineMemoryPosition(List<IMemory> memoryList) {
        int position = -1;
        for(int i = 0; i < memoryList.size(); i++){
            if(memoryList.get(i).getId() == getId()){
                position = i;
            }
        }
        return position;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Memory {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  memoryText: ").append(memoryText).append("\n");
        sb.append("  author: ").append(author).append("\n");
        sb.append("  createdByAccountId: ").append(createdByAccountId).append("\n");
        sb.append("  image: ").append(image).append("\n");
        sb.append("  profile: ").append(profile).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected Memory(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        memoryText = in.readString();
        author = in.readString();
        createdByAccountId = in.readByte() == 0x00 ? null : in.readInt();
        image = (Image) in.readValue(Image.class.getClassLoader());
        profile = (Profile) in.readValue(Profile.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(memoryText);
        dest.writeString(author);
        if (createdByAccountId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(createdByAccountId);
        }
        dest.writeValue(image);
        dest.writeValue(profile);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Memory> CREATOR = new Parcelable.Creator<Memory>() {
        @Override
        public Memory createFromParcel(Parcel in) {
            return new Memory(in);
        }

        @Override
        public Memory[] newArray(int size) {
            return new Memory[size];
        }
    };
}