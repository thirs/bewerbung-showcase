package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import hr.eonideas.ecemetery.parkovi.rest.model.Memory;

public class MemoryParameter implements Parcelable {

	private String memoryText = null;
	private String author = null;
	private Integer profileId = null;
	private Integer profileGalleryItemId = null;

	public MemoryParameter(Integer profileId) {
		this.profileId = profileId;
	}

	public String getMemoryText() {
		return memoryText;
	}

	public void setMemoryText(String memoryText) {
		this.memoryText = memoryText;
	}

	public String getAuthor() {
		return author;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public Integer getProfileId() {
		return profileId;
	}

	public void setProfileId(Integer profileId) {
		this.profileId = profileId;
	}

	public Integer getProfileGalleryItemId() {
		return profileGalleryItemId;
	}

	public void setProfileGalleryItemId(Integer profileGalleryItemId) {
		this.profileGalleryItemId = profileGalleryItemId;
	}

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder();
		sb.append("class MemoryRequestModel {\n");
		sb.append("  memoryText: ").append(memoryText).append("\n");
		sb.append("  author: ").append(author).append("\n");
		sb.append("  profileId: ").append(profileId).append("\n");
		sb.append("  profileGalleryItemId: ").append(profileGalleryItemId).append("\n");
		sb.append("}\n");
		return sb.toString();
	}

	protected MemoryParameter(Parcel in) {
		memoryText = in.readString();
		author = in.readString();
		profileId = in.readByte() == 0x00 ? null : in.readInt();
		profileGalleryItemId = in.readByte() == 0x00 ? null : in.readInt();
	}

	@Override
	public int describeContents() {
		return 0;
	}

	@Override
	public void writeToParcel(Parcel dest, int flags) {
		dest.writeString(memoryText);
		dest.writeString(author);
		if (profileId == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(profileId);
		}
		if (profileGalleryItemId == null) {
			dest.writeByte((byte) (0x00));
		} else {
			dest.writeByte((byte) (0x01));
			dest.writeInt(profileGalleryItemId);
		}
	}

	@SuppressWarnings("unused")
	public static final Parcelable.Creator<MemoryParameter> CREATOR = new Parcelable.Creator<MemoryParameter>() {
		@Override
		public MemoryParameter createFromParcel(Parcel in) {
			return new MemoryParameter(in);
		}

		@Override
		public MemoryParameter[] newArray(int size) {
			return new MemoryParameter[size];
		}
	};
}