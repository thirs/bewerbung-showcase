package hr.eonideas.ecemetery.parkovi.myecemetery;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;

/**
 * Created by Teo on 27.5.2016..
 */
public class MyEcemeteryServicesFragment extends BasicPagerFragment {

    public static final String LOCATION_LIST = Filter.PACKAGE + ".LOCATION_LIST";

    public static MyEcemeteryServicesFragment newInstance() {
        MyEcemeteryServicesFragment fragment = new MyEcemeteryServicesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.account_recycler_fragment, null);


        return view;
    }
}
