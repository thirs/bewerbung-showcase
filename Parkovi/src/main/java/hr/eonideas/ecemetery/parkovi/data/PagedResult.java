package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Teo on 16.3.2016..
 */
public class PagedResult<T extends Parcelable> implements Parcelable {

    @SerializedName("offset")
    private Integer offset;

    @SerializedName("total")
    private Integer total;

    @SerializedName("items")
    private List<T> items;

    public Integer getOffset() {
        return offset;
    }

    public void setOffset(Integer offset) {
        this.offset = offset;
    }

    public Integer getTotal() {
        return total;
    }

    public void setTotal(Integer total) {
        this.total = total;
    }

    public List<T> getItems() {
        return items;
    }

    public void setItems(List<T> items) {
        this.items = items;
    }


    protected PagedResult(Parcel in) {
        offset = in.readByte() == 0x00 ? null : in.readInt();
        total = in.readByte() == 0x00 ? null : in.readInt();
        if (in.readByte() == 0x01) {
            Class<?> type = (Class<?>) in.readSerializable();
            items = new ArrayList<T>();
            in.readList(items, type.getClassLoader());
        } else {
            items = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (offset == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(offset);
        }
        if (total == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(total);
        }
        if (items == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(items);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<PagedResult> CREATOR = new Parcelable.Creator<PagedResult>() {
        @Override
        public PagedResult createFromParcel(Parcel in) {
            return new PagedResult(in);
        }

        @Override
        public PagedResult[] newArray(int size) {
            return new PagedResult[size];
        }
    };
}