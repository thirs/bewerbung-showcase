package hr.eonideas.ecemetery.parkovi.tours;

import android.annotation.TargetApi;
import android.bluetooth.BluetoothAdapter;
import android.bluetooth.BluetoothDevice;
import android.bluetooth.BluetoothManager;
import android.content.Context;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.util.Log;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ToggleButton;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.sothree.slidinguppanel.SlidingUpPanelLayout;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.MapBaseActivity;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.PoiListAdapter;
import hr.eonideas.ecemetery.parkovi.rest.model.Poi;
import hr.eonideas.ecemetery.parkovi.rest.model.TourGuide;

public class TourDetailsBigMapActivity extends MapBaseActivity implements OnMapReadyCallback, PoiListAdapter.OnItemClickListener, GoogleMap.OnInfoWindowClickListener {
    private static final String TAG = "TourDetailsBigMapActivity";
    public static final String POI = Filter.PACKAGE + ".POI";


    private int currentapiVersion;
    private TourGuide tour;
    private ImageLoader imageLoader;
    private Poi tourPoi;
    private List<Poi> restOfThePois;

    private ToggleButton myLocationButton;

    private TextView slidingUpTitle;
    private SlidingUpPanelLayout slidingUpPanelLayout;
    private ListView tourPoisRestList;
    private PoiListAdapter adapter;
    private ImageView headerImage;
    private TextView headerTitle;
    private TextView headerSubtitle;
    private TextView slidingUpFullDescription;
    private LinearLayout slidingUpPanelContainer;
    private boolean toBeExpanded = true;

    //Beacons
    private BluetoothManager btManager;
    private BluetoothAdapter btAdapter;
    private Handler scanHandler = new Handler();
    private int scan_interval_ms = 5000;
    private boolean isScanning = false;
    private BluetoothAdapter.LeScanCallback leScanCallback;
    private Runnable scanRunnable = new Runnable()
    {
        @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
        @Override
        public void run() {
            if (isScanning)
            {
                if (btAdapter != null)
                {
                    btAdapter.stopLeScan(leScanCallback);
                }
            }
            else
            {
                if (btAdapter != null)
                {
                    btAdapter.startLeScan(leScanCallback);
                }
            }

            isScanning = !isScanning;
            //ponovno pokretanje za 'scan_interval_ms' vrijeme
            scanHandler.postDelayed(this, scan_interval_ms);
        }
    };

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.tours_details_big_map);
        tourPoisRestList = (ListView) findViewById(R.id.tour_pois_rest);
        View header = getLayoutInflater().inflate(R.layout.tours_details_big_map_slider, null);
        tourPoisRestList.addHeaderView(header);
        slidingUpPanelLayout = (SlidingUpPanelLayout) findViewById(R.id.sliding_layout);
        init(getIntent());
    }

    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private void init(Intent intent) {
        this.imageLoader = ImageLoader.getInstance();
        tour = (TourGuide) intent.getParcelableExtra(TourDetailsActivity.TOUR);
        tourPoi = intent.getParcelableExtra(POI);
        if (tour == null || tour.getTitle() == null || tour.getPOI() == null || tour.getPOI().size() == 0) {
            finish();
        }
        initActionBar(tour.getTitle());

/*
        //Beacons in case of adequate API level
        // init BLE

        currentapiVersion = Build.VERSION.SDK_INT;
        leScanCallback = getScanCallback();
        if (currentapiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2) {
            btManager = (BluetoothManager) getSystemService(Context.BLUETOOTH_SERVICE);
            btAdapter = btManager.getAdapter();
            scanHandler.post(scanRunnable);
        }
*/

        //MAP part
        initializeMapSection((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.map));
        slidingUpPanelContainer = (LinearLayout) findViewById(R.id.sliding_panel);
        myLocationButton = (ToggleButton) findViewById(R.id.my_location);
        myLocationButton.setChecked(false);
        myLocationButton.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked && !isGPSEnabled()) {
                    myLocationButton.setChecked(false);
                    Toast.makeText(TourDetailsBigMapActivity.this, "Turn ON GPS", Toast.LENGTH_SHORT).show();
                } else if (isChecked && currentLocation != null) {
                    myLocationButton.setChecked(requireLocationUpdates());
                    animateCameraToPosition(new LatLng(currentLocation.getLatitude(), currentLocation.getLongitude()));
                } else if (isChecked && currentLocation == null) {
                    myLocationButton.setChecked(requireLocationUpdates());
                } else if (!isChecked) {
                    stopLocationUpdates();
                    showHomeLocation();
                }
            }
        });

        //show/hide poi section
        slidingUpTitle = (TextView) findViewById(R.id.sliding_up_title);
        slidingUpTitle.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (toBeExpanded) {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.EXPANDED);
                } else {
                    slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);
                }
            }
        });
        headerImage = (ImageView) findViewById(R.id.header_img);
        headerTitle = (TextView) findViewById(R.id.header_title);
        headerSubtitle = (TextView) findViewById(R.id.header_subtitle);
        slidingUpFullDescription = (TextView) findViewById(R.id.sliding_up_full_description);
        slidingUpPanelLayout.setPanelSlideListener(new SlidingUpPanelLayout.PanelSlideListener() {
            @Override
            public void onPanelSlide(View panel, float slideOffset) {
            }

            @Override
            public void onPanelCollapsed(View panel) {
                toBeExpanded = true;
                slidingUpTitle.setText(ProfileUtils.getProfileName(tourPoi.getProfile()));
                slidingUpTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.icon_slide_up_big);
            }

            @Override
            public void onPanelExpanded(View panel) {
                toBeExpanded = false;
                slidingUpTitle.setText(tour.getTitle());
                slidingUpTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.icon_slide_down_big);
            }

            @Override
            public void onPanelAnchored(View panel) {
            }

            @Override
            public void onPanelHidden(View panel) {
            }
        });
        if (tourPoi != null) {
            setUpPoiSlider();
        } else
            slidingUpPanelContainer.setVisibility(View.GONE);

    }

    protected void startLocationUpdates(){
        super.startLocationUpdates();
        myLocationButton.setChecked(true);
    }

    private List<Poi> getRestOfPois(List<Poi> tourPois, Poi tourPoi) {
        List<Poi> restOfPois = new ArrayList<Poi>();
        for (Poi poi : tourPois) {
            if (!((poi.getIndex().compareTo(tourPoi.getIndex()) == 0) && (poi.getLat().compareTo(tourPoi.getLat()) == 0) && (poi.getLon().compareTo(tourPoi.getLon()) == 0)))
                restOfPois.add(poi);
        }
        return restOfPois;
    }

    private void setUpPoiSlider() {
        slidingUpPanelContainer.setVisibility(View.VISIBLE);
        slidingUpPanelLayout.setPanelState(SlidingUpPanelLayout.PanelState.COLLAPSED);

        slidingUpTitle.setText(ProfileUtils.getProfileName(tourPoi.getProfile()));
        slidingUpTitle.setCompoundDrawablesWithIntrinsicBounds(0, 0, 0, R.drawable.icon_slide_up_big);
        imageLoader.displayImage(tourPoi.getProfile().getImage().getFullUrl(), headerImage);
        headerTitle.setText(ProfileUtils.getProfileName(tourPoi.getProfile()));
        headerSubtitle.setText(ProfileUtils.getLifePeriod(tourPoi.getProfile(),this));
        headerSubtitle.setVisibility(View.VISIBLE);
        slidingUpFullDescription.setText(tourPoi.getFullDescription());
        slidingUpPanelLayout.setScrollableView(tourPoisRestList);

        restOfThePois = getRestOfPois(tour.getPOI(), tourPoi);
        adapter = new PoiListAdapter(this, restOfThePois, currentLocation);
        adapter.setOnItemClickListener(this);
        tourPoisRestList.setAdapter(adapter);
    }

    @Override
    protected int enableLocationUpdates() {
        if (myLocationButton != null && myLocationButton.isChecked())
            return 10000;
        else
            return 0;
    }

    @Override
    protected void mapLoadingError() {
        finish();
    }

    @Override
    protected void mapLoadingSuccess() {
        animateCameraToPosition(new LatLng(Double.valueOf(Filter.map_default_latitude), Double.valueOf(Filter.map_default_longitude)));
        poiPresenter();
        mMap.setOnInfoWindowClickListener(this);
    }

    private void poiPresenter() {
        if(tourPoi != null){
            showPois(getRestOfPois(tour.getPOI(), tourPoi), tour.getTitle(), R.drawable.map_pin_cross_big);
            List<Poi> activePoi = new ArrayList<Poi>();
            activePoi.add(tourPoi);
            updatePois(activePoi,tour.getTitle(),R.drawable.map_pin_cross_big_active);
        }else{
            showPois(tour.getPOI(), tour.getTitle(),R.drawable.map_pin_cross_big);
        }
    }

    @Override
    public void onConnected(Bundle bundle) {
        super.onConnected(bundle);
        if(adapter != null){
            adapter.updateMyLocation(currentLocation);
            adapter.notifyDataSetChanged();
        }

    }

    @Override
    public void onItemClick(View view, int position) {
        if(restOfThePois == null){
            tourPoi = tour.getPOI().get(0);
        }else{
            tourPoi = restOfThePois.get(position);
        }

        poiPresenter();
        setUpPoiSlider();
    }

    @Override
    public void onInfoWindowClick(Marker marker) {
        Marker m = marker;
        Poi selectedPoi = poiMarkerMap.get(m);
        tourPoi = selectedPoi;
        poiPresenter();
        setUpPoiSlider();
    }


    // ------------------------------------------------------------------------
    // Inner classes
    // ------------------------------------------------------------------------


    static final char[] hexArray = "0123456789ABCDEF".toCharArray();
    private static String bytesToHex(byte[] bytes) {
        char[] hexChars = new char[bytes.length * 2];
        for ( int j = 0; j < bytes.length; j++ ) {
            int v = bytes[j] & 0xFF;
            hexChars[j * 2] = hexArray[v >>> 4];
            hexChars[j * 2 + 1] = hexArray[v & 0x0F];
        }
        return new String(hexChars);
    }


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR2)
    private BluetoothAdapter.LeScanCallback getScanCallback() {
        BluetoothAdapter.LeScanCallback leScanCallback = null;
        if (currentapiVersion >= Build.VERSION_CODES.JELLY_BEAN_MR2){
            leScanCallback = new BluetoothAdapter.LeScanCallback()
            {
                @Override
                public void onLeScan(final BluetoothDevice device, final int rssi, final byte[] scanRecord)
                {
                    int startByte = 2;
                    boolean patternFound = false;
                    while (startByte <= 5)
                    {
                        if (    ((int) scanRecord[startByte + 2] & 0xff) == 0x02 && //Identifies an iBeacon
                                ((int) scanRecord[startByte + 3] & 0xff) == 0x15)
                        { //Identifies correct data length
                            patternFound = true;
                            break;
                        }
                        startByte++;
                    }

                    if (patternFound)
                    {
                        //Convert to hex String
                        byte[] uuidBytes = new byte[16];
                        System.arraycopy(scanRecord, startByte + 4, uuidBytes, 0, 16);
                        String hexString = bytesToHex(uuidBytes);

                        //UUID detection
                        String uuid =  hexString.substring(0,8) + "-" +
                                hexString.substring(8,12) + "-" +
                                hexString.substring(12,16) + "-" +
                                hexString.substring(16,20) + "-" +
                                hexString.substring(20,32);

                        // major
                        final int major = (scanRecord[startByte + 20] & 0xff) * 0x100 + (scanRecord[startByte + 21] & 0xff);

                        // minor
                        final int minor = (scanRecord[startByte + 22] & 0xff) * 0x100 + (scanRecord[startByte + 23] & 0xff);

                        Log.i(TAG,"UUID: " +uuid + "\\nmajor: " +major +"\\nminor" +minor);

                        onItemClick(null,0);
                    }

                }
            };
        }

        return leScanCallback;
    }
}
