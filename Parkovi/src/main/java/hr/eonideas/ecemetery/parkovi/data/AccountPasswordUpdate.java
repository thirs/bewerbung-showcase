package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import hr.eonideas.ecemetery.parkovi.rest.model.Account;

/**
 * Created by Teo on 9.1.2016..
 */
public class AccountPasswordUpdate implements Parcelable {
    String oldPassword;
    String newPassword;

    public AccountPasswordUpdate(String oldPassword, String newPassword) {
        this.oldPassword = oldPassword;
        this.newPassword = newPassword;
    }

    public String getOldPassword() {
        return oldPassword;
    }

    public void setOldPassword(String oldPassword) {
        this.oldPassword = oldPassword;
    }

    public String getNewPassword() {
        return newPassword;
    }

    public void setNewPassword(String newPassword) {
        this.newPassword = newPassword;
    }

    protected AccountPasswordUpdate(Parcel in) {
        oldPassword = in.readString();
        newPassword = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(oldPassword);
        dest.writeString(newPassword);
    }

    @SuppressWarnings("unused")
    public static final Creator<AccountPasswordUpdate> CREATOR = new Creator<AccountPasswordUpdate>() {
        @Override
        public AccountPasswordUpdate createFromParcel(Parcel in) {
            return new AccountPasswordUpdate(in);
        }

        @Override
        public AccountPasswordUpdate[] newArray(int size) {
            return new AccountPasswordUpdate[size];
        }
    };
}