package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import hr.eonideas.ecemetery.parkovi.interfaces.IAdAdapterItem;


public class Advertiser implements Parcelable, IAdAdapterItem {

    @SerializedName("id")
    private Integer id = null;
    @SerializedName("name")
    private String title = null;
    @SerializedName("shortDescription")
    private String description = null;
    @SerializedName("fullDescription")
    private String fullDescription = null;
    @SerializedName("category")
    private AdvertiserCategory category = null;
    @SerializedName("image")
    private Image image = null;
    @SerializedName("location")
    private ProfileLocation location = null;
    @SerializedName("phone")
    private String phone = null;
    @SerializedName("fax")
    private String fax = null;
    @SerializedName("mobile")
    private String mobile = null;
    @SerializedName("email")
    private String email = null;
    @SerializedName("workingTime")
    private String workingTime = null;

    public Advertiser(Integer id, String name, String shortDescription, String fullDescription, AdvertiserCategory category, Image image, ProfileLocation location, String phone, String fax, String mobile, String email, String workingTime) {
        this.id = id;
        this.title = name;
        this.description = description;
        this.fullDescription = fullDescription;
        this.category = category;
        this.image = image;
        this.location = location;
        this.phone = phone;
        this.fax = fax;
        this.mobile = mobile;
        this.email = email;
        this.workingTime = workingTime;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    /**
     * Name of advertiser.
     **/
    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }


    /**
     * Short description of advertiser.
     **/
    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


    /**
     * Full description of advertiser.
     **/
    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }


    /**
     **/
    public AdvertiserCategory getCategory() {
        return category;
    }

    public void setCategory(AdvertiserCategory category) {
        this.category = category;
    }


    /**
     **/
    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }


    /**
     **/
    public ProfileLocation getLocation() {
        return location;
    }

    public void setLocation(ProfileLocation location) {
        this.location = location;
    }


    /**
     * Phone number.
     **/
    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }


    /**
     * Fax number.
     **/
    public String getFax() {
        return fax;
    }

    public void setFax(String fax) {
        this.fax = fax;
    }


    /**
     * Mobile phone number.
     **/
    public String getMobile() {
        return mobile;
    }

    public void setMobile(String mobile) {
        this.mobile = mobile;
    }


    /**
     * Email address.
     **/
    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }


    /**
     * Working time.
     **/
    public String getWorkingTime() {
        return workingTime;
    }

    public void setWorkingTime(String workingTime) {
        this.workingTime = workingTime;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Advertiser {\n");
        sb.append("  id: ").append(id).append("\n");
        sb.append("  name: ").append(title).append("\n");
        sb.append("  shortDescription: ").append(description).append("\n");
        sb.append("  fullDescription: ").append(fullDescription).append("\n");
        sb.append("  category: ").append(category).append("\n");
        sb.append("  image: ").append(image).append("\n");
        sb.append("  location: ").append(location).append("\n");
        sb.append("  phone: ").append(phone).append("\n");
        sb.append("  fax: ").append(fax).append("\n");
        sb.append("  mobile: ").append(mobile).append("\n");
        sb.append("  email: ").append(email).append("\n");
        sb.append("  workingTime: ").append(workingTime).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected Advertiser(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        title = in.readString();
        description = in.readString();
        fullDescription = in.readString();
        category = (AdvertiserCategory) in.readValue(AdvertiserCategory.class.getClassLoader());
        image = (Image) in.readValue(Image.class.getClassLoader());
        location = (ProfileLocation) in.readValue(ProfileLocation.class.getClassLoader());
        phone = in.readString();
        fax = in.readString();
        mobile = in.readString();
        email = in.readString();
        workingTime = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(title);
        dest.writeString(description);
        dest.writeString(fullDescription);
        dest.writeValue(category);
        dest.writeValue(image);
        dest.writeValue(location);
        dest.writeString(phone);
        dest.writeString(fax);
        dest.writeString(mobile);
        dest.writeString(email);
        dest.writeString(workingTime);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Advertiser> CREATOR = new Parcelable.Creator<Advertiser>() {
        @Override
        public Advertiser createFromParcel(Parcel in) {
            return new Advertiser(in);
        }

        @Override
        public Advertiser[] newArray(int size) {
            return new Advertiser[size];
        }
    };
}