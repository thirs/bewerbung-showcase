package hr.eonideas.ecemetery.parkovi.rest.controlers;


import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.util.Log;
import android.view.inputmethod.InputMethodManager;
import com.squareup.okhttp.OkHttpClient;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.dialogs.NotificationDialog;
import hr.eonideas.ecemetery.parkovi.data.AccountFBLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordReset;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordUpdate;
import hr.eonideas.ecemetery.parkovi.data.AccountRegister;
import hr.eonideas.ecemetery.parkovi.data.AccountUpdate;
import hr.eonideas.ecemetery.parkovi.data.MemoryParameter;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.data.ProfileParameter;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;
import hr.eonideas.ecemetery.parkovi.rest.model.AdvertiserCategory;
import hr.eonideas.ecemetery.parkovi.rest.model.Burial;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryHistory;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.Gallery;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfoResponse;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.model.TourGuide;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import retrofit.Callback;
import retrofit.Response;

/**
 * Created by thirs on 11.10.2015..
 */
public class RestControler extends BasicControler {
    private static final String LOG_TAG = "RestControler";
    private static final Integer DEFAULT_OFFSET     = 0;
    private static final Integer DEFAULT_PAGESIZE   = 10;

    public RestControler(Context context) {
        this(context,Filter.default_locale.getLanguage());
    }

    public RestControler(Context context, String lang) {
        super(context);
        this.context = context;
        mProgressDialog = new ProgressDialog(context);
        this.lang = lang;
    }


    public void getAdvertiser(AdvertiserCategory adCategory, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService().getAdvertiser(adCategory.getId()).enqueue(new Callback<PagedResult<Advertiser>>() {
            @Override
            public void onResponse(Response<PagedResult<Advertiser>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "GETAdvertiser response - succeeded");
                    onGetResponseListener.onGetResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getAdvertiser
            }
        });
    }


    public void getAdvertiserCategory(int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService().getAdvertiserCategory().enqueue(new Callback<PagedResult<AdvertiserCategory>>() {
            @Override
            public void onResponse(Response<PagedResult<AdvertiserCategory>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "GETAdvertiserCategory response - succeeded");
                    onGetResponseListener.onGetResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                Throwable kef = t;
                // TODO: 13.10.2015. getAdvertiserCategory
            }
        });
    }

    public void getAdvertiserTop(int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService().getAdvertiserTop().enqueue(new Callback<PagedResult<Advertiser>>() {
            @Override
            public void onResponse(Response<PagedResult<Advertiser>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "GETAdvertiserTop response - succeeded");
                    onGetResponseListener.onGetResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getAdvertiserTop
            }
        });
    }

    public void getCemeteryGallery(){
        progressBar(true);
        getRestService().getCemeteryGallery().enqueue(new Callback<PagedResult<Gallery>>() {
            @Override
            public void onResponse(Response<PagedResult<Gallery>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "getCemeteryGallery response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getCemeteryGallery
                Log.d("KEF", "onFailure() returned: " + t.getMessage());
            }
        });
    }

    public void getCemeteryInfo(){
        progressBar(true);
        getRestService().getCemeteryInfo().enqueue(new Callback<List<GroupedCemeteryInfo>>() {
            @Override
            public void onResponse(Response<List<GroupedCemeteryInfo>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "getCemeteryInfo response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getCemeteryInfo
            }
        });
    }

    public void getCemeteryHistory(){
        progressBar(true);
        getRestService().getCemeteryHistory().enqueue(new Callback<PagedResult<CemeteryHistory>>() {
            @Override
            public void onResponse(Response <PagedResult<CemeteryHistory>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "getCemeteryHistory response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                t.printStackTrace();
                // TODO: 13.10.2015. getCemeteryHistory
            }
        });
    }

    public void getMemory(Integer profileId, Integer offset, Integer pageSize){
        progressBar(true);
        Integer os = offset > -1 ? offset : DEFAULT_OFFSET;
        Integer ps = pageSize > -1 ? pageSize : DEFAULT_PAGESIZE;
        getRestService().getMemory(profileId, os, ps).enqueue(new Callback<PagedResult<Memory>>() {
            @Override
            public void onResponse(Response<PagedResult<Memory>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "getMemory response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getMemory
            }
        });
    }

    public void updateMemory(int memoryId, MemoryParameter memory, Account account){
        progressBar(true);
        getRestService(account).updateMemory(memoryId, memory).enqueue(new Callback<Memory>() {
            @Override
            public void onResponse(Response<Memory> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "updateMemory response - succeeded");
                    onPostResponseListener.onPostResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. updateMemory
            }
        });
    }

    public void setMemory(MemoryParameter memory, Account account){
        progressBar(true);
        getRestService(account).setMemory(memory).enqueue(new Callback<Memory>() {
            @Override
            public void onResponse(Response<Memory> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "setMemory response - succeeded");
                    onPostResponseListener.onPostResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. setMemory
            }
        });
    }

    public void getProfile(String searchString, Integer offset, Integer pageSize){
        progressBar(true);
        Integer os = offset > -1 ? offset : DEFAULT_OFFSET;
        Integer ps = pageSize > -1 ? pageSize : DEFAULT_PAGESIZE;
        getRestService().getProfile(searchString, os, ps).enqueue(new Callback<PagedResult<Profile>>() {
            @Override
            public void onResponse(Response<PagedResult<Profile>> response) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "getProfileLight response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getProfileLightList
            }
        });
    }

    public void getSingleProfile(int id){
        progressBar(true);
        getRestService().getSingleProfile(id).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Response<Profile> response) {
                InputMethodManager imm = (InputMethodManager) context.getSystemService(Activity.INPUT_METHOD_SERVICE);
                imm.toggleSoftInput(InputMethodManager.HIDE_IMPLICIT_ONLY, 0);
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "getProfileLight response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getProfileLightList
            }
        });
    }

    public void setAsFavorite(int profileId, Account account, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService(account).setAsFavorite(profileId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "setAsFavorite response - succeeded");
                    onPostResponseListener.onPostResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. setMemory
            }
        });
    }

    public void removeFromFavorites(int profileId, Account account, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService(account).removeFromFavorites(profileId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                progressBar(false);
                if (onDeleteResponseListener != null) {
                    if (Filter.isDebuggingMode)
                        Log.i(LOG_TAG, "removeFromFavorites response - succeeded");
                    onDeleteResponseListener.onDeleteResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. setMemory
            }
        });
    }

    public void requestOwnership(int profileId, Account account, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService(account).requestOwnership(profileId).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if (Filter.isDebuggingMode)
                        Log.i(LOG_TAG, "requestOwnership response - succeeded");
                    onPostResponseListener.onPostResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. request Ownership failure
            }
        });
    }

    public void uploadProfileImage(final int profileId, final Account account, final RequestBody fileRequestBody, int requestId, boolean isAvatar){
        progressBar(true);
        final int requestIdentifier = requestId;
        final String method = isAvatar ? IParkoviApiService.method_new_profile_avatar : IParkoviApiService.method_new_profile_gallery_image;

        Thread thread = new Thread() {
            @Override
            public void run() {
                OkHttpClient client = getHTTPClient(account);
                Request request = new Request.Builder()
                        .url(Filter.resApi_Url+ "cemetery/api/" + Filter.resApi_Version + "/profile/"+profileId + method)
                        .post(fileRequestBody)
                        .addHeader("content-type", "multipart/form-data")
                        .addHeader("cache-control", "no-cache")
                        .build();
                com.squareup.okhttp.Response response = null;
                try {
                    response = client.newCall(request).execute();
                    progressBar(false);
                } catch (IOException e) {
                    progressBar(false);
                    e.printStackTrace();
                    // TODO: 13.10.2015. request upload avatar failure
                }
                if(onPostMPResponseListener != null){
                    onPostMPResponseListener.onPostMPResponse(response, requestIdentifier);
                }

            };
        };
        thread.start();
    }



    public void removeProfileGalleryImages(int profileId, List<Integer> imageIds, Account account, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService(account).removeProfileGalleryImages(profileId, imageIds).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                progressBar(false);
                if (onDeleteResponseListener != null) {
                    if (Filter.isDebuggingMode)
                        Log.i(LOG_TAG, "removeProfileGalleryImages response - succeeded");
                    onDeleteResponseListener.onDeleteResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. setMemory
            }
        });
    }

    public void updateBiography(int profileId, ProfileParameter biography, Account account){
        progressBar(true);
        getRestService(account).updateBiography(profileId, biography).enqueue(new Callback<Profile>() {
            @Override
            public void onResponse(Response<Profile> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if (Filter.isDebuggingMode) Log.i(LOG_TAG, "updateBiography response - succeeded");
                    onPostResponseListener.onPostResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. updateMemory
            }
        });
    }




    public void getTourGuide(){
        progressBar(true);
        getRestService().getTourGuide().enqueue(new Callback<PagedResult<TourGuide>>() {
            @Override
            public void onResponse(Response<PagedResult<TourGuide>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "getTourGuide response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getTourGuide
            }
        });
    }

    public void getBurialList() {
        progressBar(true);
        getRestService().getBurialList().enqueue(new Callback<PagedResult<Burial>>() {
            @Override
            public void onResponse(Response<PagedResult<Burial>> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if (Filter.isDebuggingMode)
                        Log.i(LOG_TAG, "getProfileLight response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getProfileLight
            }
        });
    }

    public void register(AccountRegister accountRegister){
        progressBar(true);
        getRestService().register(accountRegister).enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Response<Account> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "register response - succeeded");
                    onPostResponseListener.onPostResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getTourGuide
            }
        });
    }

    public void login(AccountLogin accountLogin, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService().login(accountLogin).enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Response<Account> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "register response - succeeded");
                    onPostResponseListener.onPostResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                if(onFailedResponseListener != null){
                    onFailedResponseListener.onFailedResponse(t, requestIdentifier);
                }
            }
        });
    }

    public void fblogin(AccountFBLogin accessToken, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService().fblogin(accessToken).enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Response<Account> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "register response - succeeded");
                    onPostResponseListener.onPostResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                t.printStackTrace();
                // TODO: 13.10.2015. getTourGuide
            }
        });
    }

    public void recoverAccount(Account account, int accountId){
        progressBar(true);
        getRestService(account).recoverAccount(accountId).enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Response<Account> response) {
                progressBar(false);
                if (onGetResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "register response - succeeded");
                    onGetResponseListener.onGetResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getTourGuide
            }
        });
    }

    public void uploadAccountAvatarImage(final Account account, final RequestBody fileRequestBody, int requestId){
        progressBar(true);
        final int requestIdentifier = requestId;
        final String method = IParkoviApiService.method_uploadAccountAvatar;

        Thread thread = new Thread() {
            @Override
            public void run() {
                OkHttpClient client = getHTTPClient(account);
                Request request = new Request.Builder()
                        .url(Filter.resApi_Url+ "cemetery/api/" + Filter.resApi_Version + method)
                        .post(fileRequestBody)
                        .addHeader("content-type", "multipart/form-data")
                        .addHeader("cache-control", "no-cache")
                        .build();
                com.squareup.okhttp.Response response = null;
                try {
                    response = client.newCall(request).execute();
                    progressBar(false);
                } catch (IOException e) {
                    progressBar(false);
                    e.printStackTrace();
                    // TODO: 13.10.2015. request upload avatar failure
                }
                if(onPostMPResponseListener != null){
                    onPostMPResponseListener.onPostMPResponse(response, requestIdentifier);
                }

            };
        };
        thread.start();
    }

    public void updateAccount(Account account){
        progressBar(true);
        getRestService(account).updateAccount(new AccountUpdate(account)).enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Response<Account> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "Account update response - succeeded");
                    onPostResponseListener.onPostResponse(response, -1);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getTourGuide
            }
        });
    }

    public void updateAccountPassword(Account account, AccountPasswordUpdate accountPasswordUpdate, int requestId ){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService(account).updateAccountPassword(accountPasswordUpdate).enqueue(new Callback<Account>() {
            @Override
            public void onResponse(Response<Account> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "Account password update response - succeeded");
                    onPostResponseListener.onPostResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);
                // TODO: 13.10.2015. getTourGuide
            }
        });
    }

    public void resetAccountPassword(AccountPasswordReset accountPasswordReset, int requestId ){
        progressBar(true);
        final int requestIdentifier = requestId;
        getRestService().resetAccountPassword(accountPasswordReset).enqueue(new Callback<ResponseBody>() {
            @Override
            public void onResponse(Response<ResponseBody> response) {
                progressBar(false);
                if (onPostResponseListener != null) {
                    if(Filter.isDebuggingMode)Log.i(LOG_TAG, "Account password reset - succeeded");
                    onPostResponseListener.onPostResponse(response, requestIdentifier);
                }
            }

            @Override
            public void onFailure(Throwable t) {
                progressBar(false);

            }
        });
    }


}
