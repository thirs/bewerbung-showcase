package hr.eonideas.ecemetery.parkovi.commons;


import android.content.Context;
import android.support.v4.app.Fragment;
import android.view.GestureDetector;
import android.view.MotionEvent;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.interfaces.IApplicationProxy;

public class BasicPagerFragment extends Fragment {

    protected IApplicationProxy app;
    protected String language;


    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        app =((BaseActivity)getActivity()).getApp();
        language = app != null ? app.getAppLanguage() : Filter.default_locale.getLanguage();
    }

    @Override
    public void setUserVisibleHint(boolean isVisibleToUser) {
        super.setUserVisibleHint(isVisibleToUser);
        if(isVisibleToUser && (getUserVisibleHintAdditionalCondition())){
            isVisibleToUser();
        }
    }

    //(th)return true if non of additional Conditions are required
    protected boolean getUserVisibleHintAdditionalCondition(){
        return true;
    };

    //(th)this method is called when fragment gets really visible to the user - in this method api calls should be placed
    protected void isVisibleToUser(){};

    public interface IViewPageFragmentSwitchListener {
        public void onFragmentSwitchRequest(int menuItemId);
    }
}
