package hr.eonideas.ecemetery.parkovi.commons;


import android.Manifest;
import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.util.Log;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.LocationListener;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapsInitializer;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.BitmapDescriptorFactory;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.LatLngBounds;
import com.google.android.gms.maps.model.MarkerOptions;

import java.text.DateFormat;
import java.util.Date;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.utils.MapUtils;
import hr.eonideas.ecemetery.parkovi.rest.model.Poi;

public abstract class BasicMapFragment extends BasicLocationFragment implements OnMapReadyCallback {


    private static final String TAG = "BasicMapFragment";
    private static final int FASTEST_LOCATION_REQUEST_INTERVAL = 5000;
    private static final int MY_PERMISSIONS_REQUEST_FINE_LOCATION = 3;
    protected GoogleMap mMap;


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        if (ConnectionResult.API_UNAVAILABLE == MapsInitializer.initialize(getContext())) {
            Log.e("Address Map", "Could not initialize google play");
            mapLoadingError();
            return;
        }
        if (!MapUtils.checkGooglePlayServices(getContext())) {
            Log.e("Address Map", "Could not initialize google play");
            mapLoadingError();
            return;
        }
        setUpMap();
        mapLoadingSuccess();
    }

    @Override
    public void onLocationChanged(Location location) {
        super.onLocationChanged(location);
        //permissions will be chekcked in the parent method
        if (mMap != null && !mMap.isMyLocationEnabled()) {
            if (ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getActivity(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                    //Permissions for location updates already rejected
                    Toast.makeText(getContext(), "Reinstall you app. Additional description inside dedicated dialog should be shown", Toast.LENGTH_LONG).show();
                } else {
                    //Asking for the location updates permissions for the first time
                    ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, MY_PERMISSIONS_REQUEST_FINE_LOCATION);
                }
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            mMap.setMyLocationEnabled(true);
        }
    }

    @Override
    protected void startLocationUpdates() {
        super.startLocationUpdates();


    }

    protected void setUpMap() {
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            if (ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), Manifest.permission.ACCESS_FINE_LOCATION)) {
                //Permissions for location updates already rejected
                Toast.makeText(getActivity(),"Reinstall you app. Additional description inside dedicated dialog should be shown",Toast.LENGTH_LONG).show();
            } else {
                //Asking for the location updates permissions for the first time
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.ACCESS_FINE_LOCATION},MY_PERMISSIONS_REQUEST_FINE_LOCATION);
            }
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        mMap.setMyLocationEnabled(enableMyLocationbutton());
        mMap.getUiSettings().setCompassEnabled(true);
        mMap.getUiSettings().setMyLocationButtonEnabled(enableMyLocationbutton());
        mMap.getUiSettings().setAllGesturesEnabled(true);
        mMap.getUiSettings().setMapToolbarEnabled(false);
        mMap.getUiSettings().setZoomControlsEnabled(true);
        mMap.setMapType(GoogleMap.MAP_TYPE_HYBRID);
    }

    protected boolean enableMyLocationbutton(){
        return false;
    }

    abstract protected void mapLoadingError();

    abstract protected void mapLoadingSuccess();

    protected void showPois(LatLng latLang, String title, String snippet, int pinResourceId, boolean clearMarkers) {
        if(clearMarkers){
            mMap.clear();
        }
        mMap.addMarker(new MarkerOptions()
                .position(latLang)
                .title(title)
                .snippet(snippet)
                .icon(BitmapDescriptorFactory.fromResource(pinResourceId)));
    }


    protected void animateCameraToPosition(LatLng latLng) {
        CameraPosition position = new CameraPosition.Builder().target(latLng)
                .zoom(15.5f)
                .bearing(0)
                .tilt(25)
                .build();
        mMap.moveCamera(CameraUpdateFactory.newCameraPosition(position));
    }

    protected void smartLatLngAnimateCamera(Location currentLocation, List<LatLng> latLngList){
        if(currentLocation == null && latLngList != null && latLngList.size() == 1){
            mMap.setPadding(0,0,0,0);
            animateCameraToPosition(latLngList.get(0));
            return;
        }
        LatLngBounds.Builder builder = new LatLngBounds.Builder();
        if(currentLocation != null){
            builder.include(new LatLng(currentLocation.getLatitude(),currentLocation.getLongitude()));
        }
        for (LatLng latLng : latLngList) {
            builder.include(latLng);
        }
        LatLngBounds bounds = builder.build();

        int padding = 150; // offset from edges of the map in pixels
        CameraUpdate cu = CameraUpdateFactory.newLatLngBounds(bounds, padding);
        mMap.setPadding(0,0,0,0);
        mMap.moveCamera(cu);
    }
}
