package hr.eonideas.ecemetery.parkovi.interfaces;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;

/**
 * Created by tmarkovic on 28.3.2016..
 */
public interface IAccount {

    public enum AccountMode{
        READ,
        ACC_UPDATE,
        PASS_UPDATE
    }

    public enum LoginType{
        FB,
        UP
    }

    public AccountMode inMode();

    public void setMode(AccountMode editMode);

    public LoginType getLoginType();

    public void setLoginType(LoginType loginType);

    public void resetAccount();

    public int getId();

    public void setId(int id);

    public String getFirstName();

    public void setFirstName(String firstName);

    public String getLastName();

    public void setLastName(String lastName);

    public String getEmail();

    public void setEmail(String email);

    public Image getAvatar();

    public void setAvatar(Image avatar);

    public String getAuthToken();

    public void setAuthToken(String authToken);

    public List<Profile> getOwnedProfiles();

    public void setOwnedProfiles(List<Profile> ownedProfiles);

    public List<Profile> getFavoriteProfiles();

    public void setFavoriteProfiles(List<Profile> favoriteProfiles);
}
