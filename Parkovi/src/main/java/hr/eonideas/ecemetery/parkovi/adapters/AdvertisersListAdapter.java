package hr.eonideas.ecemetery.parkovi.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.advertisers.AdCategoryListViewHolder;
import hr.eonideas.ecemetery.parkovi.interfaces.IAdAdapterItem;
import se.emilsjolander.stickylistheaders.StickyListHeadersAdapter;

/**
 * Created by thirs on 7.11.2015..
 */

public class AdvertisersListAdapter extends ArrayAdapter<IAdAdapterItem> implements StickyListHeadersAdapter {
    private final LayoutInflater inflater;
    private Context context;
    private ImageLoader imageLoader;

    private List<IAdAdapterItem> advertisersList;
    private List<IAdAdapterItem> topAdvertisersList;

    private int layoutResourceId;
    private String headerTitle1;
    private String headerTitle2;

    public AdvertisersListAdapter(Activity activity) {
        this(activity,new ArrayList<IAdAdapterItem>(0));
    }

    public AdvertisersListAdapter(Activity activity, List<IAdAdapterItem> advertisersList) {
        super(activity, -1, advertisersList);
        this.context = activity;
        this.inflater = LayoutInflater.from(context);
        this.advertisersList = advertisersList;
        this.imageLoader = ImageLoader.getInstance();
        this.layoutResourceId = R.layout.list_item_ad;
    }

    public void setTopAdvertisersList(List<IAdAdapterItem> topAdvertisersList){
        this.topAdvertisersList = topAdvertisersList;
    }

    public void setHeaderTitles(String headerTitle1,String headerTitle2){
        this.headerTitle1 = headerTitle1;
        this.headerTitle2 = headerTitle2;
    }

    @Override
    public int getCount() {
        int i = advertisersList.size();
        if(topAdvertisersList != null && topAdvertisersList.size() > 0 ){
            i += topAdvertisersList.size();
        }
        return i;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        AdCategoryListViewHolder viewHolder;

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(layoutResourceId, null);
            viewHolder = new AdCategoryListViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (AdCategoryListViewHolder) convertView.getTag();
        }

        //th ->fetch top advertiser or basic element (advertiser or advertiser category)
        IAdAdapterItem adCategory;
        if(topAdvertisersList.size() > position){
            adCategory = topAdvertisersList.get(position);
            viewHolder.rooElement.setBackgroundColor(context.getResources().getColor(R.color.advertisers_topadvertiser_backgrond));
        }else{
            adCategory = advertisersList.get(position - topAdvertisersList.size());
            viewHolder.rooElement.setBackgroundColor(context.getResources().getColor(R.color.advertisers_advertiser_backgrond));
        }

        if(adCategory.getImage() != null)
            imageLoader.displayImage(adCategory.getImage().getThumbUrl(), viewHolder.adCategoryLogo);
        viewHolder.adCategoryTitle.setText(adCategory.getTitle());
        viewHolder.adCategoryDescription.setText(adCategory.getDescription());
        return convertView;
    }

    @Override
    public View getHeaderView(int position, View convertView, ViewGroup parent) {
        HeaderViewHolder holder;
        if (convertView == null) {
            holder = new HeaderViewHolder();
            convertView = inflater.inflate(R.layout.burials_list_header_layout, parent, false);
            holder.date = (TextView) convertView.findViewById(R.id.items_header);
            convertView.setTag(holder);
        } else {
            holder = (HeaderViewHolder) convertView.getTag();
        }
        //set header text as first char in name
        String headerText;
        if(advertisersList.size() > position && (headerTitle1 != null && headerTitle1.length()>0)){
            holder.date.setText(headerTitle1);
        }else if(advertisersList.size() <= position && (headerTitle2 != null && headerTitle2.length()>0)){
            holder.date.setText(headerTitle2);
        }else{
            holder.date.setVisibility(View.GONE);
        }
        return convertView;
    }

    @Override
    public long getHeaderId(int position) {
        if(topAdvertisersList.size() > position){
            //TOP-ADVERTISERS
            return 1;
        }else{
            //ADVERTISERS
            return 2;
        }
    }

    class HeaderViewHolder {
        TextView date;
    }

}