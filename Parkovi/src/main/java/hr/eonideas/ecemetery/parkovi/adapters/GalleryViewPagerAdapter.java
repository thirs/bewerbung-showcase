package hr.eonideas.ecemetery.parkovi.adapters;

import android.net.Uri;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentStatePagerAdapter;

import java.util.List;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.commons.busevents.ActivityTitleEvent;
import hr.eonideas.ecemetery.parkovi.commons.ImageViewPager;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;

/**
 * Created by Teo on 2.1.2016..
 */

public class GalleryViewPagerAdapter extends FragmentStatePagerAdapter {
    private List<Image> imageGalleryList;
    private List<Uri> addedImageUriList;
    private int imageIndex;
    private float pageWidth;
    private ImageViewPager.ViewPagerClickListener mListener;

    public GalleryViewPagerAdapter(FragmentManager fm, List<Image> imageGalleryList) {
        super(fm);
        this.imageGalleryList = imageGalleryList;
        this.pageWidth = 1;
    }

    public GalleryViewPagerAdapter(FragmentManager fm, List<Image> imageGalleryList, float pageWidth, ImageViewPager.ViewPagerClickListener mListener) {
        super(fm);
        this.imageGalleryList = imageGalleryList;
        this.pageWidth = 1/pageWidth;
        this.mListener = mListener;
    }

    public GalleryViewPagerAdapter(FragmentManager fm, List<Image> imageGalleryList, List<Uri> addedImageUriList, float pageWidth, ImageViewPager.ViewPagerClickListener mListener) {
        super(fm);
        this.imageGalleryList = imageGalleryList;
        this.addedImageUriList = addedImageUriList;
        this.pageWidth = 1/pageWidth;
        this.mListener = mListener;
    }

    @Override
    public float getPageWidth(int position) {
        return pageWidth;
    }

    @Override
    public Fragment getItem(int position) {
        // getItem is called to instantiate the fragment for the given page.
        // Return a PlaceholderFragment (defined as a static inner class below).
        ImageViewPager.FullScreenFragment f = ImageViewPager.FullScreenFragment.newInstance(position, imageGalleryList, addedImageUriList);
        if(mListener != null)
            f.setViewPagerListener(mListener);
        return f;
    }

    @Override
    public int getCount() {
        int count = imageGalleryList.size();
        if(addedImageUriList != null){
            count += addedImageUriList.size();
        }
        return count;
    }
}

