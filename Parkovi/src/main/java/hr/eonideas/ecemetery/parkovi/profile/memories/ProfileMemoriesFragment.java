package hr.eonideas.ecemetery.parkovi.profile.memories;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.interfaces.ILazyLoad;
import hr.eonideas.ecemetery.commons.widgets.decorators.DividerItemDecoration;
import hr.eonideas.ecemetery.commons.widgets.decorators.VerticalSpaceItemDecoration;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.MemoriesRecyclerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.IMemory;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.memories.MemoryDetailViewHolder;
import hr.eonideas.ecemetery.parkovi.profile.ProfileFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import retrofit.Response;

/**
 * Created by thirs on 6.12.2015..
 */
public class ProfileMemoriesFragment extends BasicPagerFragment implements IOnGetResponseListener, MemoryDetailViewHolder.IMemoryDetailViewHolderClicks, ILazyLoad {
    private RestControler lc;
    private int memoryPosition;
    private List<IMemory> memoryList;
    private MemoriesRecyclerAdapter memoriesAdapter;
    private RecyclerView memoriesListView;
    private IMemory memory;
    private IProfile profile;
    private Integer offset;
    private Integer totalCount;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        totalCount = -1;
    }

    @Override
    public void onResume() {
        super.onResume();
        Memory newMemory = ((ProfileFragment)getParentFragment()).getNewMemory();
        if(newMemory != null && memoryList != null){
            int newMemoryListPosition = newMemory.determineMemoryPosition(memoryList);
            if(newMemory.determineMemoryPosition(memoryList) > -1){
                memoryList.set(newMemoryListPosition,newMemory);
                app.clearImageLoaderCache(newMemory.getImage().getFullUrl());
                app.clearImageLoaderCache(newMemory.getImage().getThumbUrl());
            }else{
                memoryList.add(newMemory);
                memoriesAdapter.setTotalCount(++totalCount);
            }
            memoriesAdapter.notifyDataSetChanged();
        }
//        updateListView(memoryList, totalCount);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview, container, false);
        memoriesListView = (RecyclerView) view.findViewById(R.id.recyclerView);
        memory = (Memory) getArguments().getParcelable(ProfileFragment.MEMORY);
        profile = (IProfile) getArguments().getParcelable(ProfileFragment.PROFILE_LIGHT);
        if(profile == null && memory != null){
            profile = memory.getProfile();
        }
        offset = 0;
        getProfileMemories();
        return view;
    }

    @Override
    protected boolean getUserVisibleHintAdditionalCondition() {
        return memoryList == null || memoryList.isEmpty();
    }

    protected void getProfileMemories() {
        if (lc == null) {
            lc = new RestControler(getActivity());
        }
        lc.setOnGetResponseListener(this);
        lc.getMemory(profile.getId(), offset,Filter.PAGE_SIZE);

    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if (response != null && response.body() != null) {
            if (((PagedResult)response.body()).getItems() instanceof List && !((List) ((PagedResult)response.body()).getItems()).isEmpty()) {
                this.totalCount = ((PagedResult)response.body()).getTotal();
                updateListView((List<IMemory>) ((PagedResult)response.body()).getItems(),totalCount);
            }
        }
    }

    private void updateListView(List<IMemory> list2show, int totalCount) {
        if (list2show != null && !list2show.isEmpty() && memoriesListView != null) {
            if(memoriesAdapter == null){
                memoriesAdapter = new MemoriesRecyclerAdapter(getActivity(), true, list2show, this, app.getUserAccount().getId());
                memoryList = list2show;
                memoriesAdapter.setTotalCount(totalCount);
                memoriesAdapter.setLazyLoadListener(this);
                LinearLayoutManager lm = new LinearLayoutManager(getActivity());

                if(memory != null) {
                    memoryPosition = memory.determineMemoryPosition(memoryList);
                }
                if(memoryPosition > -1) {
                    lm.scrollToPosition(memoryPosition);
                }

                memoriesListView.addItemDecoration(new DividerItemDecoration(getContext()));
                memoriesListView.addItemDecoration(new VerticalSpaceItemDecoration(10));
                memoriesListView.setLayoutManager(lm);
                memoriesListView.setAdapter(memoriesAdapter);
            }else{
                memoriesAdapter.updateList(list2show);
                memoriesAdapter.notifyDataSetChanged();
            }
        }
    }

    @Override
    public void onMemorySelected(int position) {
        if(Filter.isDebuggingMode)
            Toast.makeText(getActivity(),"selected memory position: "+position + ", with text \'" + memoryList.get(position).getMemoryText()+"\'",Toast.LENGTH_LONG).show();
        ((ProfileFragment)getParentFragment()).createNewMemory(profile, memoryList.get(position));
    }

    @Override
    public void updateLazyList(Integer offset) {
        this.offset = offset;
        getProfileMemories();
    }
}
