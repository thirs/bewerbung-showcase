package hr.eonideas.ecemetery.parkovi.adapters;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.app.FragmentStatePagerAdapter;
import android.support.v4.util.SparseArrayCompat;
import android.util.Log;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.Filter;

/**
 * Created by thirs on 17.10.2015..
 */
public class SmartFragmentPagerAdapter extends FragmentStatePagerAdapter {

    private static final String TAG = "SmartFragmentTabAdapter";

    public static class TabInfo {
        final Class<? extends Fragment> fragmentClass;
        final Bundle fragmentArguments;
        final String title;

        private TabInfo(String title, Class<? extends Fragment> fragmentClass, Bundle fragmentArguments) {
            this.fragmentClass = fragmentClass;
            this.fragmentArguments = fragmentArguments;
            this.title = title;
        }

        public String getTitle() {
            return title;
        }
    }

    private List<TabInfo> tabInfos;
    private FragmentActivity activity;
    private SparseArrayCompat<String> pageTags;

    public SmartFragmentPagerAdapter(Fragment fragment, FragmentManager fragmentManager) {
        super(fragmentManager);
        this.activity = fragment.getActivity();
        tabInfos = new ArrayList<TabInfo>(16);
        pageTags = new SparseArrayCompat<String>(16);
    }

    public SmartFragmentPagerAdapter addTab(String title,  Class<? extends Fragment> fragmentClass, Bundle fragmentArguments) {
        TabInfo tabInfo = new TabInfo(title, fragmentClass, fragmentArguments);
        tabInfos.add(tabInfo);
        return this;
    }

//    public void removeAllTabs(){
//        tabInfos = new ArrayList<TabInfo>(16);
//    }
//
//    public TabInfo getTabAt(int location) {
//        if(tabInfos.size()>location) {
//            return tabInfos.get(location);
//        }
//        return null;
//    }

    @Override
    public Fragment getItem(int index) {
        TabInfo tabInfo = tabInfos.get(index);
        if(tabInfo!=null) {
            if(Filter.isDebuggingMode) {
                Log.d(TAG, "Fragment.instantiate " + tabInfo.fragmentClass.getSimpleName());
            }
            return Fragment.instantiate(activity, tabInfo.fragmentClass.getName(), tabInfo.fragmentArguments);
        }
        return null;
    }

    /**
     * Added to update fragment arguments if changed and get fragment's tag
     */
    @Override
    public Object instantiateItem(ViewGroup container, int position) {
        Fragment fragment = (Fragment) super.instantiateItem(container, position);
        if(Filter.isDebuggingMode) {
            Log.d(TAG, "instantiateItem " + fragment.getClass().getSimpleName());
        }
        refreshArguments(fragment, position);

        pageTags.append(position, fragment.getTag());

        return fragment;
    }

    protected void refreshArguments(Fragment fragment, int position) {
        if(position < tabInfos.size()) {
            Bundle arguments = fragment.getArguments();
            TabInfo tabInfo = tabInfos.get(position);
            if(arguments != null && tabInfo.fragmentArguments != null) {
                arguments.putAll(tabInfo.fragmentArguments);
            }
        }
    }

    @Override
    public int getCount() {
        return tabInfos.size();
    }

    @Override
    public CharSequence getPageTitle(int position) {
        TabInfo tabInfo = tabInfos.get(position);
        if(tabInfo!=null) {
            return tabInfo.title;
        }
        return null;
    }

    public String getPageTag(int position) {
        return pageTags.get(position);
    }
}