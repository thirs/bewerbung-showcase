package hr.eonideas.ecemetery.parkovi.data;

/**
 * Created by thirs on 28.10.2015..
 */
public class AboutInformationType {
    public static String TYPE_ADDRESS = "ADDRESS";
    public static String TYPE_WORKING_TIME = "WORKING_TIME";
    public static String TYPE_PHONE = "PHONE";
    public static String TYPE_MOBILE_PHONE = "MOBILE_PHONE";
    public static String TYPE_EMAIL = "EMAIL";
    public static String TYPE_WEB = "WEB";
    public static String TYPE_MAP = "MAP";
}
