package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.about.cemetery.AboutCemeteryViewHolder;
import hr.eonideas.ecemetery.parkovi.data.AboutCemeteryParentObject;

/**
 * Created by thirs on 7.11.2015..
 */
// Create the basic adapter extending from RecyclerView.Adapter
// Note that we specify the custom ViewHolder which gives us access to our views
public class AboutStraightAdapter extends RecyclerView.Adapter<AboutCemeteryViewHolder> {
    private List<AboutCemeteryParentObject> aboutHistoryList;
    private ImageLoader imageLoader;

    public AboutStraightAdapter(List<AboutCemeteryParentObject> aboutHistoryList) {
        this.aboutHistoryList = aboutHistoryList;
        imageLoader = ImageLoader.getInstance();
    }

    @Override
    public AboutCemeteryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        Context context = parent.getContext();
        LayoutInflater mInflater = LayoutInflater.from(context);
        View view = mInflater.inflate(R.layout.list_item_about_parent, parent, false);
        return new AboutCemeteryViewHolder(view);
    }

    @Override
    public void onBindViewHolder(AboutCemeteryViewHolder holder, int position) {
        AboutCemeteryParentObject parentObject = (AboutCemeteryParentObject) aboutHistoryList.get(position);
        holder.headerContent.setText(parentObject.getFullDescription());
        holder.headerTitle.setText(parentObject.getTitle());
        imageLoader.displayImage(parentObject.getImage().getFullUrl(), holder.headerImage);
    }

    @Override
    public int getItemCount() {
        return aboutHistoryList.size();
    }
}