package hr.eonideas.ecemetery.parkovi.about.gallery;

import android.content.Intent;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.GridView;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.GalleryGridAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.commons.ImageAlbumActivity;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Gallery;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import retrofit.Response;

/**
 * Created by thirs on 17.10.2015..
 */
public class AboutGalleryFragment extends BasicPagerFragment implements IOnGetResponseListener {

    private RestControler lc;
    private GridView gridview;
    private List<Gallery> aboutGalleryList = new ArrayList<Gallery>();
    private GalleryGridAdapter galleryGridAdapter;

    public static AboutGalleryFragment newInstance(int page) {
        Bundle args = new Bundle();
        AboutGalleryFragment fragment = new AboutGalleryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!(language.equals(app.getAppLanguage()))){
            language = app.getAppLanguage();
            lc = new RestControler(getActivity(), language);
            lc.setOnGetResponseListener(this);
            lc.getCemeteryGallery();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_gallery_fragment, container, false);
        gridview = (GridView) view.findViewById(R.id.gridview);
        if(aboutGalleryList != null && !aboutGalleryList.isEmpty()){
            initializeGalleryGrid();
        }
        return view;
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response != null){
            if(((PagedResult) response.body()).getItems() instanceof List && !((List<Gallery>)((PagedResult) response.body()).getItems()).isEmpty()){
                aboutGalleryList = (List<Gallery>)((PagedResult) response.body()).getItems();
                initializeGalleryGrid();
            }
        }
    }


    private void initializeGalleryGrid(){
        if(!aboutGalleryList.isEmpty() && gridview != null){
            galleryGridAdapter = new GalleryGridAdapter(getActivity(),aboutGalleryList);
            gridview.setAdapter(galleryGridAdapter);
            gridview.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    openAlbum(galleryGridAdapter.getItem(position));
                }
            });
        }
    }

    private void openAlbum(Gallery item) {
        Intent intent = new Intent(getActivity(), ImageAlbumActivity.class);
        ArrayList<Image> albumImages = new ArrayList<Image>();
        Iterator iterator = item.getItems().iterator();
        while(iterator.hasNext()){
            GalleryItem galleryItem = (GalleryItem)iterator.next();
            albumImages.add(galleryItem.getImage());
        }
        intent.putExtra(ImageAlbumActivity.ALBUM_IMAGES, albumImages);
        intent.putExtra(ImageAlbumActivity.ALBUM_GALLERY_ITEMS, new ArrayList(item.getItems()));
        intent.putExtra(ImageAlbumActivity.ALBUM_TITLE, item.getTitle());
        intent.putExtra(ImageAlbumActivity.IMAGE_GRID_MODE, ImageAlbumActivity.ImageGridMode.GALLERY);
        startActivity(intent);
    }


    @Override
    protected boolean getUserVisibleHintAdditionalCondition() {
        return aboutGalleryList == null || aboutGalleryList.isEmpty();
    }

    @Override
    protected void isVisibleToUser() {
        if(lc == null){
            lc = new RestControler(getActivity());
        }
        lc.setOnGetResponseListener(this);
        lc.getCemeteryGallery();
    }
}
