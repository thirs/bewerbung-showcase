package hr.eonideas.ecemetery.parkovi.commons;

import android.content.Context;

import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;

public class NavMenuItem implements INavDrawerItem {

    private int id;
    private int label;
    private int icon ;
    private boolean updateActionBarTitle;
    private boolean isEnabled;
    private boolean setIndicator;
    private boolean highlighted;

    private NavMenuItem() {
    }

    public NavMenuItem ( int id, int label, int icon, boolean updateActionBarTitle, boolean isEnabled, boolean setIndicator, boolean highlighted) {
        this.id = id;
        this.label = label;
        this.icon = icon;
        this.updateActionBarTitle = updateActionBarTitle;
        this.isEnabled = isEnabled;
        this.setIndicator = setIndicator;
        this.highlighted = highlighted;
    }

    public NavMenuItem ( int id, int label, int icon, boolean updateActionBarTitle, boolean isEnabled, boolean setIndicator) {
        this(id,label,icon,updateActionBarTitle,isEnabled,setIndicator,false);
    }


    @Override
    public int getId() {
        return id;
    }

    @Override
    public int getLabelResourceId() {
        return label;
    }

    @Override
    public int getIconResourceId() {
        return icon;
    }

    @Override
    public boolean isEnabled() {
        return isEnabled;
    }

    @Override
    public boolean updateActionBarTitle() {
        return updateActionBarTitle;
    }

    @Override
    public int getDrawerItemType() {
        return INavDrawerItem.DRAWER_MENU_ITEM;
    }

    public boolean isSetIndicator() {
        return setIndicator;
    }

    public boolean isHighlighted() {
        return highlighted;
    }

    public void setHighlighted(boolean highlighted) {
        this.highlighted = highlighted;
    }
}