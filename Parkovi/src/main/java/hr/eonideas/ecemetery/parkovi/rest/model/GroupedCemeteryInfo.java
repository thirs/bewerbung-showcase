package hr.eonideas.ecemetery.parkovi.rest.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.interfaces.IGroupedAboutInformationItem;


public class GroupedCemeteryInfo implements IGroupedAboutInformationItem, Parcelable {

    @SerializedName("sectionTitle")
    private String sectionTitle = null;
    @SerializedName("items")
    private List<CemeteryInfo> items = null;

    @Override
    public String getSectionTitle() {
        return sectionTitle;
    }

    public void setSectionTitle(String sectionTitle) {
        this.sectionTitle = sectionTitle;
    }

    @Override
    public List<CemeteryInfo> getItems() {
        return items;
    }

    public void setItems(List<CemeteryInfo> items) {
        this.items = items;
    }

    protected GroupedCemeteryInfo(Parcel in) {
        sectionTitle = in.readString();
        if (in.readByte() == 0x01) {
            items = new ArrayList<CemeteryInfo>();
            in.readList(items, CemeteryInfo.class.getClassLoader());
        } else {
            items = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(sectionTitle);
        if (items == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(items);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GroupedCemeteryInfo> CREATOR = new Parcelable.Creator<GroupedCemeteryInfo>() {
        @Override
        public GroupedCemeteryInfo createFromParcel(Parcel in) {
            return new GroupedCemeteryInfo(in);
        }

        @Override
        public GroupedCemeteryInfo[] newArray(int size) {
            return new GroupedCemeteryInfo[size];
        }
    };
}