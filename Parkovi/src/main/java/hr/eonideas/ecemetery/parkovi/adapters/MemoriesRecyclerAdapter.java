package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.commons.interfaces.ILazyLoad;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IMemory;
import hr.eonideas.ecemetery.parkovi.memories.MemoryDetailViewHolder;
import hr.eonideas.ecemetery.parkovi.memories.MemoryListViewHolder;

/**
 * Created by thirs on 10.12.2015..
 */
public class MemoriesRecyclerAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder> {
    private boolean isDetailItem;
    private Context context;
    private List<IMemory> memories;
    private ImageLoader imageLoader;
    private MemoryDetailViewHolder.IMemoryDetailViewHolderClicks listener;
    private int accountId;

    private static final int LAYOUT_TYPE_LIST = 0;
    private static final int LAYOUT_TYPE_DETAIL = 1;

    private Integer totalCount;
    private ILazyLoad lazyLoadListener;

    public MemoriesRecyclerAdapter(Context context, boolean isDetailItem,  List<IMemory> memories, MemoryDetailViewHolder.IMemoryDetailViewHolderClicks listener, int accountId) {
        this(context,isDetailItem,memories);
        this.listener = listener;
        this.accountId = accountId;
    }
    public MemoriesRecyclerAdapter(Context context, boolean isDetailItem,  List<IMemory> memories) {
        this.context = context;
        this.imageLoader = ImageLoader.getInstance();
        this.isDetailItem = isDetailItem;
        this.memories = memories;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public void setLazyLoadListener(ILazyLoad lazyLoadListener) {
        this.lazyLoadListener = lazyLoadListener;
    }


    private void wakeUpLoading(Integer offset){
        lazyLoadListener.updateLazyList(offset);
    }


    public void updateList(List<IMemory> profiles){
        if(profiles != null){
            this.memories.addAll(profiles);
        }else{
            this.memories = null;
        }

    }

    @Override
    public int getItemViewType(int position) {
        return isDetailItem ? LAYOUT_TYPE_DETAIL : LAYOUT_TYPE_LIST;
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        switch (viewType){
            case LAYOUT_TYPE_LIST:
                return new MemoryListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_memory, parent, false));
            case LAYOUT_TYPE_DETAIL:
                return new MemoryDetailViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.detail_item_memory, parent, false), listener);
            default:
                return new MemoryListViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_memory, parent, false));
        }

    }

    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder genericHolder, int position) {
        if(position == memories.size()-1 && totalCount > memories.size() && lazyLoadListener != null) {
            wakeUpLoading(position+1);
        }

        IMemory memory = memories.get(position);

        if(isDetailItem){
            MemoryDetailViewHolder holder = (MemoryDetailViewHolder)genericHolder;
            if(memory.getAccountId() <= 0 && memory.getAuthor() == null){
                holder.authorSection.setVisibility(View.GONE);
            }else if(memory.getAccountId() > 0){
                holder.authorSection.setVisibility(View.VISIBLE);
//            imageLoader.displayImage(memory.getProfileLight().getImage().getThumbUrl(), holder.authorAvatar);
//            holder.autorName.setText(User.username);
            }else{
                holder.authorSection.setVisibility(View.VISIBLE);
                holder.authorName.setText(memory.getAuthor());
            }
            if(memory.getImage() != null){
                holder.memoryImage.setVisibility(View.VISIBLE);
                imageLoader.displayImage(memory.getImage().getFullUrl(), holder.memoryImage);
            }else{
                holder.memoryImage.setVisibility(View.GONE);
            }
            holder.memoryText.setText(memory.getMemoryText());
            holder.editButton.setVisibility(memory.getCreatorAccountId() == accountId ? View.VISIBLE : View.GONE);
        }else{
            MemoryListViewHolder holder = (MemoryListViewHolder)genericHolder;
            imageLoader.displayImage(memory.getProfile().getImage().getThumbUrl(), holder.memoryAvatar);
            holder.profileName.setText(ProfileUtils.getProfileName(memory.getProfile()));
            holder.profilePeriod.setText(ProfileUtils.getLifePeriod(memory.getProfile(),context));
            if(memory.getAccountId() <= 0 && memory.getAuthor() == null){
                holder.authorSection.setVisibility(View.GONE);
            }else if(memory.getAccountId() > 0){
                holder.authorSection.setVisibility(View.VISIBLE);
//            imageLoader.displayImage(memory.getProfileLight().getImage().getThumbUrl(), holder.authorAvatar);
//            holder.autorName.setText(User.username);
            }else{
                holder.authorSection.setVisibility(View.VISIBLE);
                holder.authorName.setText(memory.getAuthor());
            }
        }
    }

    @Override
    public int getItemCount() {
        return memories.size();
    }
}
