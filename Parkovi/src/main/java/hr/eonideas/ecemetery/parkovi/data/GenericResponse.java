package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 28.1.2016..
 */
public class GenericResponse implements Parcelable {
    @SerializedName("message")
    private String message;

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }


    protected GenericResponse(Parcel in) {
        message = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(message);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<GenericResponse> CREATOR = new Parcelable.Creator<GenericResponse>() {
        @Override
        public GenericResponse createFromParcel(Parcel in) {
            return new GenericResponse(in);
        }

        @Override
        public GenericResponse[] newArray(int size) {
            return new GenericResponse[size];
        }
    };
}