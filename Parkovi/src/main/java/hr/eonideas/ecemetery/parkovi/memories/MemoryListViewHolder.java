package hr.eonideas.ecemetery.parkovi.memories;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by thirs on 9.11.2015..
 */
public class MemoryListViewHolder extends RecyclerView.ViewHolder {
    private int position;
    public LinearLayout rooElement;
    public ImageView memoryAvatar;
    public TextView profileName;
    public TextView profilePeriod;
    public ImageView authorAvatar;
    public TextView authorName;
    public LinearLayout authorSection;


    public MemoryListViewHolder(View itemView) {
        super(itemView);
        rooElement = (LinearLayout) itemView.findViewById(R.id.memory_item);
        memoryAvatar = (ImageView) itemView.findViewById(R.id.memory_item_avatar);
        profileName = (TextView) itemView.findViewById(R.id.memory_item_profile_name);
        profilePeriod = (TextView) itemView.findViewById(R.id.memory_item_profile_period);
        authorAvatar = (ImageView) itemView.findViewById(R.id.memory_item_author_avatar);
        authorName = (TextView) itemView.findViewById(R.id.memory_item_author_name);
        authorSection = (LinearLayout) itemView.findViewById(R.id.memory_item_author_section);
    }
}
