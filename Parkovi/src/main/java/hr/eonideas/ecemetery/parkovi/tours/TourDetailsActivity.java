package hr.eonideas.ecemetery.parkovi.tours;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;
import java.util.Locale;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.MapBaseActivity;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.PoiListAdapter;
import hr.eonideas.ecemetery.parkovi.adapters.SimpleGalleryRecyclerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.ImageAlbumActivity;
import hr.eonideas.ecemetery.parkovi.commons.ImageViewPager;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Poi;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.model.TourGuide;

/**
 * Created by thirs on 7.11.2015..
 */
public class TourDetailsActivity extends MapBaseActivity implements PoiListAdapter.OnItemClickListener, SimpleGalleryRecyclerAdapter.IOnImageClickListener{

    public static final String TOUR = Filter.PACKAGE + ".TOUR";
    private TourGuide tour;

//    private GoogleMap map;
    private ListView listview;
    private PoiListAdapter adapter;
    private View galleryContainer;
    private TextView emptyGallery;
    private View fullGalleryLink;
    private RecyclerView galleryRecyclerView;
    private View gallerySectionTitle;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.listview_w_toolbar);
        init();
    }

    private void init() {
        ImageLoader imageLoader = ImageLoader.getInstance();
        tour = (TourGuide) getIntent().getParcelableExtra(TOUR);
        if(tour == null || tour.getTitle() == null || tour.getPOI() == null || tour.getPOI().size() == 0){
            finish();
        }
        initActionBar(tour.getTitle());

        //Nearest POI's part
        listview = (ListView)findViewById(R.id.listview);
        adapter = new PoiListAdapter(this, tour.getPOI());
        adapter.setOnItemClickListener(this);

        //Header part
        View header = getLayoutInflater().inflate(R.layout.tours_detail_activity, null);
        listview.addHeaderView(header);
        imageLoader.displayImage(tour.getImage().getFullUrl(), ((ImageView) findViewById(R.id.header_img)));
        ((TextView)findViewById(R.id.header_title)).setText(tour.getTitle());
        ((TextView)findViewById(R.id.header_subtitle)).setText(tour.getSubTitle());

        //Tour Image gallery
        galleryContainer = findViewById(R.id.profile_gallery_container);
        emptyGallery = (TextView)findViewById(R.id.empty_gallery);
        fullGalleryLink = findViewById(R.id.full_gallery_link);
        galleryRecyclerView = (RecyclerView)findViewById(R.id.profile_gallery_recycler);
        gallerySectionTitle = findViewById(R.id.gallery_section_title);
        setGalleryPreview();

        //Description part
        ((TextView)findViewById(R.id.tour_poi_number)).setText(String.valueOf(tour.getPOI().size()));
        ((TextView)findViewById(R.id.tour_length)).setText(String.valueOf(tour.getDistance()));
        ((TextView)findViewById(R.id.tour_duration)).setText(String.valueOf(tour.getDuration()));
        ((TextView)findViewById(R.id.tour_full_description)).setText(tour.getFullText());

        //Map part
        initializeMapSection((SupportMapFragment) getSupportFragmentManager().findFragmentById(R.id.tour_small_map));
        listview.setAdapter(adapter);
    }

    private void setGalleryPreview() {
        if(showGalleryPreview()){
            galleryContainer.setVisibility(View.VISIBLE);
            emptyGallery.setVisibility(View.GONE);
            fullGalleryLink.setVisibility(View.VISIBLE);
            galleryRecyclerView.setVisibility(View.VISIBLE);

            gallerySectionTitle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAlbum();
                }
            });
            fullGalleryLink.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    openAlbum();
                }
            });

            LinearLayoutManager layoutManager = new LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL, false);
            galleryRecyclerView.setLayoutManager(layoutManager);
            // Create the adapter that will return a fragment for each of the three
            // primary sections of the activity.
            SimpleGalleryRecyclerAdapter galleryAdapter = new SimpleGalleryRecyclerAdapter(Utils.filterOutImages(tour.getGalleryItems()), R.layout.image_view);
            // Set up the ViewPager with the sections adapter.
            galleryAdapter.setOnImageClickListener(this);
            galleryRecyclerView.setAdapter(galleryAdapter);
            galleryAdapter.notifyDataSetChanged();
        }else{
            fullGalleryLink.setVisibility(View.GONE);
            galleryContainer.setVisibility(View.GONE);
            emptyGallery.setVisibility(View.VISIBLE);
        }

    }

    private boolean showGalleryPreview() {
        boolean show = true;
        if(tour == null){
            show = false;
        }else if (!(tour.getGalleryItems() != null && tour.getGalleryItems().size() > 0)){
            show = false;
        }else{
            show = true;
        }
        return show;
    }

    private void openAlbum() {
        Intent intent = new Intent(this, ImageAlbumActivity.class);
        intent.putExtra(ImageAlbumActivity.ALBUM_GALLERY_ITEMS, new ArrayList(tour.getGalleryItems()));
        intent.putExtra(ImageAlbumActivity.ALBUM_TITLE, tour.getTitle());
        intent.putExtra(ImageAlbumActivity.IMAGE_GRID_MODE, ImageAlbumActivity.ImageGridMode.GALLERY);
        startActivity(intent);
    }

    private void openFullSizeImage(Image imageItem) {
        Intent intent = new Intent(this, ImageViewPager.class);
        ArrayList<Image> singleItemList = new ArrayList<Image>(1);
        singleItemList.add(imageItem);
        intent.putExtra(ImageViewPager.IMAGE_INDEX, 0);
        intent.putExtra(ImageAlbumActivity.ALBUM_IMAGES, singleItemList);
        startActivity(intent);
    }

    @Override
    protected int enableLocationUpdates() {
        return 0;
    }

    @Override
    protected void mapLoadingError() {
        findViewById(R.id.tour_small_map_container).setVisibility(View.INVISIBLE);
    }

    @Override
    protected void mapLoadingSuccess() {
        setUpMap();
        animateCameraToPosition(new LatLng(Double.valueOf(Filter.map_default_latitude), Double.valueOf(Filter.map_default_longitude)));
        showPois(tour.getPOI(), tour.getTitle(),R.drawable.map_pin_cross_big);
        findViewById(R.id.tour_small_map_click_overlay).setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                startBigMapActivity(null);
            }
        });
    }

    @Override
    public void onConnected(Bundle bundle) {
        super.onConnected(bundle);
        adapter.updateMyLocation(currentLocation);
        adapter.notifyDataSetChanged();
    }

    public void startBigMapActivity(Poi poi){
        Intent intent = new Intent(this, TourDetailsBigMapActivity.class);
        intent.fillIn(getIntent(), Intent.FILL_IN_DATA);
        if(poi != null)
            intent.putExtra(TourDetailsBigMapActivity.POI, poi);
        startActivity(intent);
    }


    @Override
    public void onItemClick(View view, int position) {
        startBigMapActivity(tour.getPOI().get(position));
    }

    @Override
    public void onImageClicked(Image image) {
        openFullSizeImage(image);
    }
}
