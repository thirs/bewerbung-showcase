package hr.eonideas.ecemetery.parkovi.rest.model;


import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;


public class CemeteryInfo implements Parcelable, IAboutInformationItem {

    @SerializedName("infoLabel")
    private String infoLabel = null;
    @SerializedName("infoValue")
    private String infoValue = null;
    @SerializedName("infoType")
    private String infoType = null;

    public CemeteryInfo(String infoType) {
        this.infoType = infoType;
    }

    /**
     * Label to display.
     **/
    public String getInfoLabel() {
        return infoLabel;
    }

    public void setInfoLabel(String infoLabel) {
        this.infoLabel = infoLabel;
    }


    /**
     * Value to display.
     **/
    public String getInfoValue() {
        return infoValue;
    }

    public void setInfoValue(String infoValue) {
        this.infoValue = infoValue;
    }

    /**
     * Type to display.
     **/
    public String getInfoType() {
        return infoType;
    }

    public void setInfoType(String infoType) {
        this.infoType = infoType;
    }


    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class CemeteryInfo {\n");

        sb.append("  infoLabel: ").append(infoLabel).append("\n");
        sb.append("  infoValue: ").append(infoValue).append("\n");
        sb.append("  infoType: ").append(infoType).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected CemeteryInfo(Parcel in) {
        infoLabel = in.readString();
        infoValue = in.readString();
        infoType = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(infoLabel);
        dest.writeString(infoValue);
        dest.writeString(infoType);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CemeteryInfo> CREATOR = new Parcelable.Creator<CemeteryInfo>() {
        @Override
        public CemeteryInfo createFromParcel(Parcel in) {
            return new CemeteryInfo(in);
        }

        @Override
        public CemeteryInfo[] newArray(int size) {
            return new CemeteryInfo[size];
        }
    };
}