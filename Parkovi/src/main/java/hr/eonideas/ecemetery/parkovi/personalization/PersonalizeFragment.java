package hr.eonideas.ecemetery.parkovi.personalization;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;

import com.astuetz.PagerSlidingTabStrip;
import com.facebook.FacebookSdk;
import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.commons.busevents.ViewPagerFragmentSwitchEvent;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.SmartFragmentPagerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;
import hr.eonideas.ecemetery.parkovi.personalization.login.PersonalizeLoginFragment;
import hr.eonideas.ecemetery.parkovi.personalization.register.PersonalizeRegisterFragment;

/**
 * Created by thirs on 24.10.2015..
 */
public class PersonalizeFragment extends BasicFragment{
    public static final String TAG = "PersonalizeFragment";
    public static final int FRAGMENT_SWITCH_LOGIN_SUCCESS  = -1;
    public static final int FRAGMENT_SWITCH_INDEX_LOGIN    = 0;
    public static final int FRAGMENT_SWITCH_INDEX_REGISTER = 1;

    private ImageLoader imageLoader;
    private PagerSlidingTabStrip tabs;
    private ViewPager viewPager;


    public static PersonalizeFragment newInstance() {
        PersonalizeFragment fragment = new PersonalizeFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public PersonalizeFragment() {
        // Required empty public constructor
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }

    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);        //prepare objects and check mandatory
        this.imageLoader = ImageLoader.getInstance();
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

        // Initialize views and adapter
        View view = inflater.inflate(R.layout.tab_layout, container, false);
        initLayoutElements(view);


        SmartFragmentPagerAdapter adapter = new SmartFragmentPagerAdapter(this, getChildFragmentManager());


        adapter.addTab(getActivity().getResources().getString(R.string.personalize_login_title), PersonalizeLoginFragment.class, getArguments());
        adapter.addTab(getActivity().getResources().getString(R.string.personalize_register_title), PersonalizeRegisterFragment.class, getArguments());
        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
        if(app.isRegistered())
            viewPager.setCurrentItem(FRAGMENT_SWITCH_INDEX_LOGIN);
        else
            viewPager.setCurrentItem(FRAGMENT_SWITCH_INDEX_REGISTER);
        return view;
    }

    private void initLayoutElements(View view) {
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs);
        tabs.setShouldExpand(true);
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this); // unregister EventBus
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
    }

    public void onEvent(ViewPagerFragmentSwitchEvent event) {
        if(event.getFragmentIndex() > -1){
            viewPager.setCurrentItem(event.getFragmentIndex());
        }else{
            mListener.onFragmentInteraction(INavDrawerItem.MENU_ITEM_HOME, null, null);
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            for(Fragment f : fragments){
                if(f!=null){
                    f.onActivityResult(requestCode, resultCode, data);
                }

            }
        }
    }
}
