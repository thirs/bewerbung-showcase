package hr.eonideas.ecemetery.parkovi.myecemetery;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.ViewPager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.view.WindowManager;
import android.widget.FrameLayout;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.astuetz.PagerSlidingTabStrip;
import com.google.gson.Gson;
import com.nostra13.universalimageloader.core.ImageLoader;
import com.soundcloud.android.crop.Crop;
import com.squareup.okhttp.MediaType;
import com.squareup.okhttp.MultipartBuilder;
import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.Response;
import com.squareup.okhttp.ResponseBody;

import java.io.File;
import java.io.IOException;
import java.net.HttpURLConnection;
import java.util.ArrayList;

import de.greenrobot.event.EventBus;
import de.hdodenhof.circleimageview.CircleImageView;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.AccountEvent;
import hr.eonideas.ecemetery.commons.busevents.ViewPagerFragmentSwitchEvent;
import hr.eonideas.ecemetery.commons.utils.FileUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.NavigationActivity;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.SmartFragmentPagerAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordUpdate;
import hr.eonideas.ecemetery.parkovi.interfaces.IAccount;
import hr.eonideas.ecemetery.parkovi.interfaces.IAccountLocation;
import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.personalization.PersonalizeFragment;
import hr.eonideas.ecemetery.parkovi.personalization.register.PersonalizeRegisterFragment;
import hr.eonideas.ecemetery.parkovi.profile.FindDeceasedFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostMPResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.AccountLocation;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.services.IParkoviApiService;
import pl.aprilapps.easyphotopicker.DefaultCallback;
import pl.aprilapps.easyphotopicker.EasyImage;

/**
 * Created by thirs on 24.10.2015..
 */
public class MyEcemeteryFragment extends BasicFragment implements IOnPostResponseListener {

    public static final String TAG = "MY_E-CEMETERY";
    private static final int ACTION_SELECT_AVATAR_IMG = 31;
    private static final int UPDATE_PASSWORD_REQUEST = 32;

    private ImageLoader imageLoader;
    private RestControler lc;
    private PagerSlidingTabStrip tabs;
    private ViewPager viewPager;
    private SmartFragmentPagerAdapter adapter;

    private IAccount account;
    private FrameLayout accountUpdateForm;
    private LinearLayout accountViewPager;


    public static MyEcemeteryFragment newInstance(){
        MyEcemeteryFragment fragment = new MyEcemeteryFragment();
        fragment.setArguments(new Bundle());
        return fragment;
    }

    public MyEcemeteryFragment() {
        // Required empty public constructor
    }


    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);
        inflater.inflate(R.menu.profile_config_menu, menu);
        boolean editable = !((account == null) || (account.inMode() == IAccount.AccountMode.READ));
        menu.findItem(R.id.lock_menu_item).setVisible(!editable && !(account.getLoginType()== IAccount.LoginType.FB));
        menu.findItem(R.id.account_menu_item).setVisible(!editable && !(account.getLoginType()== IAccount.LoginType.FB));
        menu.findItem(R.id.revert_menu).setVisible(editable && !(account.getLoginType()== IAccount.LoginType.FB));
        menu.findItem(R.id.confirm_menu_item).setVisible(editable && !(account.getLoginType()== IAccount.LoginType.FB));
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.lock_menu_item) {
            account.setMode(IAccount.AccountMode.PASS_UPDATE);
            enableEditMode();
            getActivity().invalidateOptionsMenu();
            return true;
        }else if(item.getItemId() == R.id.account_menu_item) {
            account.setMode(IAccount.AccountMode.ACC_UPDATE);
            enableEditMode();
            getActivity().invalidateOptionsMenu();
            return true;
        }else if (item.getItemId() == R.id.revert_menu){
            account.setMode(IAccount.AccountMode.READ);
            enableEditMode();
            getActivity().invalidateOptionsMenu();
            return true;
        }
        else if (item.getItemId() == R.id.confirm_menu_item){
            boolean changePassword = account.inMode() == IAccount.AccountMode.PASS_UPDATE;
            account.setMode(IAccount.AccountMode.READ);
            if(!changePassword && checkUpdateFormForChanges()){
                if(lc == null){
                    lc = new RestControler(getActivity());
                }
                lc.setOnPostResponseListener(this);
                lc.updateAccount(Account.getInstance(null));
            }else if(changePassword && getPasswordUpdate()!=null){
                if(lc == null){
                    lc = new RestControler(getActivity());
                }
                lc.setOnPostResponseListener(this);
                lc.updateAccountPassword(Account.getInstance(null),getPasswordUpdate(), UPDATE_PASSWORD_REQUEST);
            }else{
                enableEditMode();
                getActivity().invalidateOptionsMenu();
            }
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }
    }

    private AccountPasswordUpdate getPasswordUpdate() {
        AccountPasswordUpdate passwordUpdate = null;
        FragmentManager fm = getChildFragmentManager();
        PersonalizeRegisterFragment registerFragment = (PersonalizeRegisterFragment) fm.findFragmentByTag(PersonalizeRegisterFragment.TAG);
        if (registerFragment != null) {
            passwordUpdate = registerFragment.getPassUpdateFormData();
        }
        return passwordUpdate;

    }

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        getActivity().getWindow().setSoftInputMode(WindowManager.LayoutParams.SOFT_INPUT_ADJUST_PAN);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        account = app.getUserAccount();
        account.setMode(IAccount.AccountMode.READ);
        //prepare objects and check mandatory
        imageLoader = ImageLoader.getInstance();
        lc = new RestControler(getActivity());
        adapter = new SmartFragmentPagerAdapter(this, getChildFragmentManager());

        // Initialize views and adapter
        View view = inflater.inflate(R.layout.my_ecemetery_layout, container, false);
        initLayoutElements(view);

        //Present profile data header
        presentLayoutElements();
        return view;
    }

    private void initLayoutElements(View view) {
        tabs = (PagerSlidingTabStrip) view.findViewById(R.id.tabs);
        viewPager = (ViewPager) view.findViewById(R.id.viewpager);
        accountUpdateForm = (FrameLayout) view.findViewById(R.id.my_ecemetery_update_form);
        accountViewPager = (LinearLayout) view.findViewById(R.id.my_ecemetery_viewpager);


    }

    private void presentLayoutElements() {
        addAccountHeader();
        addFavoritesTab();
        addLocationsTab();
        addServicesTab();
        addUpdateAccountTab();
        enableEditMode();

        viewPager.setAdapter(adapter);
        tabs.setViewPager(viewPager);
        viewPager.setCurrentItem(0);
    }

    private void addAccountHeader() {
        FragmentManager fm = getChildFragmentManager();
        Fragment fragment = (Fragment) fm.findFragmentByTag(MyEcemeteryHeaderFragment.TAG);
        if (fragment != null) {
            FragmentTransaction fragTransaction = fm.beginTransaction();
            fragTransaction.detach(fragment);
            fragTransaction.attach(fragment);
            fragTransaction.commitAllowingStateLoss();
        }else{
            fragment = MyEcemeteryHeaderFragment.newInstance();
            fm.beginTransaction().add(R.id.my_ecemetery_account_header, fragment, MyEcemeteryHeaderFragment.TAG).addToBackStack(MyEcemeteryHeaderFragment.TAG).commit();
        }
    }

    private void enableEditMode() {
        accountViewPager.setVisibility(account.inMode() != IAccount.AccountMode.READ ? View.GONE : View.VISIBLE);
        accountUpdateForm.setVisibility(account.inMode() != IAccount.AccountMode.READ ? View.VISIBLE : View.GONE);
        if(account.inMode() != IAccount.AccountMode.READ){
            addUpdateAccountTab();
        }
    }

    private void addFavoritesTab() {
        Bundle args = new Bundle();
        args.putParcelableArrayList(FindDeceasedFragment.PROFILE_LIST, new ArrayList<Profile>(Account.getInstance(null).getFavoriteProfiles()));
        adapter.addTab(getActivity().getResources().getString(R.string.my_ecemetery_favorites_tab), FindDeceasedFragment.class, args);
    }

    private void addLocationsTab(){
        Bundle args = new Bundle();
        args.putParcelableArrayList(MyEcemeteryLocationsFragment.LOCATION_LIST, new ArrayList<AccountLocation>(Account.getInstance(null).getOwnedLocations()));
        adapter.addTab(getActivity().getResources().getString(R.string.my_ecemetery_locations_tab), MyEcemeteryLocationsFragment.class, args);
    }

    private void addServicesTab() {
        Bundle args = new Bundle();
        adapter.addTab(getActivity().getResources().getString(R.string.my_ecemetery_services_tab), MyEcemeteryServicesFragment.class, args);
    }

    private void addUpdateAccountTab() {
        FragmentManager fm = getChildFragmentManager();
        PersonalizeRegisterFragment fragment = (PersonalizeRegisterFragment) fm.findFragmentByTag(PersonalizeRegisterFragment.TAG);
        if (fragment != null) {
            FragmentTransaction fragTransaction = fm.beginTransaction();
            fragTransaction.detach(fragment);
            fragTransaction.attach(fragment);
            fragTransaction.commitAllowingStateLoss();
        }else{
            fragment = PersonalizeRegisterFragment.newInstance(0, true);
            fm.beginTransaction().add(R.id.my_ecemetery_update_form, fragment, PersonalizeRegisterFragment.TAG).addToBackStack(PersonalizeRegisterFragment.TAG).commit();
        }

    }

    public boolean checkUpdateFormForChanges(){
        boolean formChanged = false;
        FragmentManager fm = getChildFragmentManager();
        PersonalizeRegisterFragment registerFragment = (PersonalizeRegisterFragment) fm.findFragmentByTag(PersonalizeRegisterFragment.TAG);
        if (registerFragment != null) {
            Account a = registerFragment.getUpdateFormData();
            if(!Account.getInstance(null).getFirstName().equals(a.getFirstName())){
                Account.getInstance(null).setFirstName(a.getFirstName());
                formChanged = true;
            }
            if(!Account.getInstance(null).getLastName().equals(a.getLastName())){
                Account.getInstance(null).setLastName(a.getLastName());
                formChanged = true;
            }
        }
        return formChanged;
    }

    @Override
    public void onPostResponse(retrofit.Response response, int requestId) {
        int responseCode = response.code();
        if (responseCode == IParkoviApiService.STATUS_CODE_GENERAL_OK) {
            if(response != null && (response.body() instanceof Account) && requestId == UPDATE_PASSWORD_REQUEST){
                // TODO: 10.6.2016. zamjeniti sa Dialogom da user stigne pročitati i potvrditi
                Toast.makeText(getContext(),"Password changed!!!!",Toast.LENGTH_LONG).show();
            }else if(response != null && (response.body() instanceof Account)){
                Account account = ((Account) response.body());
                Account.getInstance(account);
                addAccountHeader();
                EventBus.getDefault().post(new AccountEvent(account));
            }
        }
        getActivity().invalidateOptionsMenu();
        enableEditMode();
    }
}
