package hr.eonideas.ecemetery.parkovi.data;

/**
 * Created by Teo on 18.4.2016..
 */
public class ProfileAnniversary {
    boolean hasAnniversary;
    boolean hasBirthAnniversary;

    public ProfileAnniversary() {
        hasAnniversary = false;
        hasBirthAnniversary = false;
    }

    public boolean isHasAnniversary() {
        return hasAnniversary;
    }

    public void setHasAnniversary(boolean hasAnniversary) {
        this.hasAnniversary = hasAnniversary;
    }

    public boolean isHasBirthAnniversary() {
        return hasBirthAnniversary;
    }

    public void setHasBirthAnniversary(boolean hasBirthAnniversary) {
        this.hasBirthAnniversary = hasBirthAnniversary;
    }
}
