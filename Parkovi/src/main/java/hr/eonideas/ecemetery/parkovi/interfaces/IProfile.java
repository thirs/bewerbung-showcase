package hr.eonideas.ecemetery.parkovi.interfaces;

import android.os.Parcelable;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.data.ProfileAnniversary;
import hr.eonideas.ecemetery.parkovi.rest.model.AccountLight;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.ProfileLocation;

/**
 * Created by Teo on 29.12.2015..
 */
public interface IProfile extends Comparable<IProfile> {

    /**
     * Unique identifier of profile.
     **/
    public Integer getId();
    public void setId(Integer id);


    /**
     * Profiles first name.
     **/
    public String getFirstName();
    public void setFirstName(String firstName);


    /**
     * Profiles last name.
     **/
    public String getLastName();
    public void setLastName(String lastName);


    /**
     * Profiles birth date.
     **/
    public String getBirth();
    public void setBirth(String birth);


    /**
     * Profiles death date.
     **/
    public String getDeath();
    public void setDeath(String death);


    /**
     * Url to profile's image
     **/
    public Image getImage();
    public void setImage(Image image);


    /**
     * Profiles about content
     **/
    public String getAboutContent();
    public void setAboutContent(String aboutContent);


    /**
     * Profiles owner
     **/
    public AccountLight getOwnerAccount();
    public void setOwnerAccount(AccountLight ownerAccount);


    /**
     * Profiles about content
     **/
    public Integer getNumberOfMemories();
    public void setNumberOfMemories(Integer numberOfMemories);


    /**
     * Profiles gallery
     **/
    public List<GalleryItem> getProfileGallery();
    public void setProfileGallery(List<GalleryItem> profileGalleryItems);



    /**
     * Profiles location
     **/
    public ProfileLocation getLocation();
    public void setLocation(ProfileLocation location);



    /**
     * Profiles anniversary
     **/
    public ProfileAnniversary getProfileAnniversary();
    public void setProfileAnniversary(ProfileAnniversary profileAnniversary);


    /**
     * Profile-account relationship
     **/
    public boolean isFavorite();
    public void setIsFavorite(boolean isFavorite);

    public boolean isAdmin();
    public void setIsAdmin(boolean isAdmin);

    public boolean isOwned();
    public void setIsOwned(boolean isOwned);

    public boolean isInEditMode();
    public void setIsInEditMode(boolean isInEditMode);

    public boolean isHeaderImageModified();
    public void setHeaderImageModified(boolean isHeaderImageModified);

    public String getBurial();
    public void setBurial(String burial);

}
