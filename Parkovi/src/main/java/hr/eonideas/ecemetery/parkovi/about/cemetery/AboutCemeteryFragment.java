package hr.eonideas.ecemetery.parkovi.about.cemetery;

import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.AboutStraightAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.data.AboutCemeteryParentObject;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryHistory;
import retrofit.Response;

/**
 * Created by thirs on 17.10.2015..
 */
public class AboutCemeteryFragment extends BasicPagerFragment implements IOnGetResponseListener {
    private RestControler lc;
    private RecyclerView mRecyclerView;
    private List<AboutCemeteryParentObject> aboutHistoryList = new ArrayList<AboutCemeteryParentObject>();
    AboutStraightAdapter mExpandableAdapter;



    public static AboutCemeteryFragment newInstance(int page) {
        Bundle args = new Bundle();
        AboutCemeteryFragment fragment = new AboutCemeteryFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!(language.equals(app.getAppLanguage()))){
            language = app.getAppLanguage();
            lc = new RestControler(getActivity(), language);
            lc.setOnGetResponseListener(this);
            lc.getCemeteryHistory();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.about_cemetery_fragment, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recycler);
        if(aboutHistoryList != null && !aboutHistoryList.isEmpty()){
            updateRecyclerView();
        }
        return view;
    }

    @Override
    protected boolean getUserVisibleHintAdditionalCondition() {
        return aboutHistoryList == null || aboutHistoryList.isEmpty();
    }

    @Override
    protected void isVisibleToUser() {
        if(lc == null){
            lc = new RestControler(getActivity(), app.getAppLanguage());
        }
        lc.setOnGetResponseListener(this);
        lc.getCemeteryHistory();
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        List<CemeteryHistory> cemeteryHistoryList;
        if(response.body() != null){
            if(((PagedResult) response.body()).getItems() instanceof List && !((List)((PagedResult) response.body()).getItems()).isEmpty()){
                try{
                    cemeteryHistoryList = (List<CemeteryHistory>)((PagedResult) response.body()).getItems();
                    aboutHistoryList = new ArrayList<AboutCemeteryParentObject>();
                    Iterator<CemeteryHistory> iterator = cemeteryHistoryList.iterator();
                    while (iterator.hasNext()){
                        aboutHistoryList.add(new AboutCemeteryParentObject(iterator.next()));
                    }
                    updateRecyclerView();
                }catch (ClassCastException e){
                    e.printStackTrace();
                }
            }
        }
    }

    private void updateRecyclerView() {
        if(!aboutHistoryList.isEmpty() && mRecyclerView != null){
            mExpandableAdapter = new AboutStraightAdapter(aboutHistoryList);
            mRecyclerView.setAdapter(mExpandableAdapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
        }


    }
}
