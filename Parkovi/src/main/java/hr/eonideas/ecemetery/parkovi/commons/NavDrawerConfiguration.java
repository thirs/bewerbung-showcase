package hr.eonideas.ecemetery.parkovi.commons;

import android.widget.BaseAdapter;

import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;


public class NavDrawerConfiguration {

    private int drawerMenuLayout;   //main menu layout with drawer
    private int fragmentContainerId;    //container for all fragments within main menu
    private int drawerLayoutId;   //layout element id of navigation drawer (root element)
    private int drawerMenuListId;   //listview_w_toolbar for all menu items
    private int drawerMenuLayoutId; //layout element id of sliding part
    private INavDrawerItem[] navigationDrawerItems;   //list of all menu item objects
    private BaseAdapter drawerMenuAdapter; //adapter which fills listview_w_toolbar
    private int drawerTitleId; //text which appears on the top of the sliding part
    private int drawerOpenDesc;
    private int drawerClosedDesc;
    private int drawerMenuHeaderId;

    public int getDrawerMenuHeaderId() {
        return drawerMenuHeaderId;
    }

    public void setDrawerMenuHeaderId(int drawerMenuHeaderId) {
        this.drawerMenuHeaderId = drawerMenuHeaderId;
    }


    public int getDrawerLayoutId() {
        return drawerLayoutId;
    }

    public void setDrawerLayoutId(int drawerLayoutId) {
        this.drawerLayoutId = drawerLayoutId;
    }

    public BaseAdapter getDrawerMenuAdapter() {
        return drawerMenuAdapter;
    }

    public void setDrawerMenuAdapter(BaseAdapter drawerMenuAdapter) {
        this.drawerMenuAdapter = drawerMenuAdapter;
    }

    public int getDrawerMenuLayout() {
        return drawerMenuLayout;
    }

    public void setDrawerMenuLayout(int drawerMenuLayout) {
        this.drawerMenuLayout = drawerMenuLayout;
    }

    public int getDrawerMenuLayoutId() {
        return drawerMenuLayoutId;
    }

    public void setDrawerMenuLayoutId(int drawerMenuLayoutId) {
        this.drawerMenuLayoutId = drawerMenuLayoutId;
    }

    public int getDrawerMenuListId() {
        return drawerMenuListId;
    }

    public void setDrawerMenuListId(int drawerMenuListId) {
        this.drawerMenuListId = drawerMenuListId;
    }

    public int getFragmentContainerId() {
        return fragmentContainerId;
    }

    public void setFragmentContainerId(int fragmentContainerId) {
        this.fragmentContainerId = fragmentContainerId;
    }

    public INavDrawerItem[] getNavigationDrawerItems() {
        return navigationDrawerItems;
    }

    public void setNavigationDrawerItems(INavDrawerItem[] navigationDrawerItems) {
        this.navigationDrawerItems = navigationDrawerItems;
    }

    public int getDrawerTitleId() {
        return drawerTitleId;
    }

    public void setDrawerTitleId(int drawerTitleId) {
        this.drawerTitleId = drawerTitleId;
    }

    public int getDrawerOpenDesc() {
        return drawerOpenDesc;
    }

    public void setDrawerOpenDesc(int drawerOpenDesc) {
        this.drawerOpenDesc = drawerOpenDesc;
    }

    public int getDrawerClosedDesc() {
        return drawerClosedDesc;
    }

    public void setDrawerClosedDesc(int rawerClosedDesc) {
        this.drawerClosedDesc = rawerClosedDesc;
    }
}