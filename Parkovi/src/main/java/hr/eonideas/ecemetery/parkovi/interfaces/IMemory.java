package hr.eonideas.ecemetery.parkovi.interfaces;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;

/**
 * Created by thirs on 4.12.2015..
 */
public interface IMemory {
    public Profile getProfile();
    public String getMemoryText();
    public Image getImage();
    public String getAuthor();
    public Integer getCreatorAccountId();
    public int getAccountId();
    public Integer getId();
    public int determineMemoryPosition(List<IMemory> memoryList);



    public void setProfile(Profile profile);
    public void setMemoryText(String memoryText);
    public void setImage(Image image);
    public void setAuthor(String author);
    public void setCreatorAccountId(Integer creatorAccountId);
//    public void setAccountId(Integer accountId);
    public void setId(Integer id);


}
