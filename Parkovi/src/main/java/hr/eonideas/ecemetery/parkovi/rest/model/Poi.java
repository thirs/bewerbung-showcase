package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;


public class Poi implements Parcelable {

    @SerializedName("shortDescription")
    private String shortDescription = null;
    @SerializedName("fullDescription")
    private String fullDescription = null;
    @SerializedName("index")
    private Integer index = null;
    @SerializedName("lat")
    private BigDecimal lat = null;
    @SerializedName("lon")
    private BigDecimal lon = null;
    @SerializedName("profile")
    private Profile profile = null;

    public String getFullDescription() {
        return fullDescription;
    }

    public void setFullDescription(String fullDescription) {
        this.fullDescription = fullDescription;
    }

    public Integer getIndex() {
        return index;
    }

    public void setIndex(Integer index) {
        this.index = index;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public Profile getProfile() {
        return profile;
    }

    public void setProfile(Profile profile) {
        this.profile = profile;
    }

    public String getShortDescription() {
        return shortDescription;
    }

    public void setShortDescription(String shortDescription) {
        this.shortDescription = shortDescription;
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class Poi {\n");

        sb.append("  shortDescription: ").append(shortDescription).append("\n");
        sb.append("  fullDescription: ").append(fullDescription).append("\n");
        sb.append("  index: ").append(index).append("\n");
        sb.append("  lat: ").append(lat).append("\n");
        sb.append("  lon: ").append(lon).append("\n");
        sb.append("  profile: ").append(profile).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected Poi(Parcel in) {
        shortDescription = in.readString();
        fullDescription = in.readString();
        index = in.readByte() == 0x00 ? null : in.readInt();
        lat = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        lon = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        profile = (Profile) in.readValue(Profile.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(shortDescription);
        dest.writeString(fullDescription);
        if (index == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(index);
        }
        dest.writeValue(lat);
        dest.writeValue(lon);
        dest.writeValue(profile);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Poi> CREATOR = new Parcelable.Creator<Poi>() {
        @Override
        public Poi createFromParcel(Parcel in) {
            return new Poi(in);
        }

        @Override
        public Poi[] newArray(int size) {
            return new Poi[size];
        }
    };
}