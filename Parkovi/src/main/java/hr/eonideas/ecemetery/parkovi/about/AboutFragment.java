package hr.eonideas.ecemetery.parkovi.about;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.view.ViewPager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.astuetz.PagerSlidingTabStrip;

import hr.eonideas.ecemetery.commons.widgets.CustomViewPager;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.about.information.AboutInformationFragment;
import hr.eonideas.ecemetery.parkovi.about.cemetery.AboutCemeteryFragment;
import hr.eonideas.ecemetery.parkovi.about.gallery.AboutGalleryFragment;
import hr.eonideas.ecemetery.parkovi.adapters.SmartFragmentPagerAdapter;

/**
 * Created by thirs on 24.10.2015..
 */
public class AboutFragment extends BasicFragment {

    public static final String TAG = "ABOUT_HOST";


    public static AboutFragment newInstance() {
        AboutFragment fragment = new AboutFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public AboutFragment() {
        // Required empty public constructor
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return getResources().getString(R.string.navbar_menu_item_about);
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.tab_layout, container, false);

        // Get the ViewPager and set it's PagerAdapter so that it can display items
        CustomViewPager viewPager = (CustomViewPager) view.findViewById(R.id.viewpager);
        SmartFragmentPagerAdapter adapter = new SmartFragmentPagerAdapter(this,getChildFragmentManager());
        adapter.addTab(getResources().getString(R.string.about_about_tab_title),AboutCemeteryFragment.class,null);
        adapter.addTab(getResources().getString(R.string.about_information_tab_title), AboutInformationFragment.class,null);
        adapter.addTab(getResources().getString(R.string.about_gallery_tab_title), AboutGalleryFragment.class,null);
        viewPager.setPagingEnabled(false);
        viewPager.setAdapter(adapter);

        // Give the TabLayout the ViewPager
        PagerSlidingTabStrip tabLayout = (PagerSlidingTabStrip) view.findViewById(R.id.sliding_tabs);
        tabLayout.setShouldExpand(false);
        tabLayout.setViewPager(viewPager);
        return view;
    }
}
