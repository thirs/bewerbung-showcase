package hr.eonideas.ecemetery.parkovi.rest.services;

import android.app.Activity;
import android.content.Context;
import android.os.Handler;
import android.os.Looper;

import com.squareup.okhttp.RequestBody;
import com.squareup.okhttp.ResponseBody;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.data.AccountFBLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountLogin;
import hr.eonideas.ecemetery.parkovi.data.AccountLoginToken;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordReset;
import hr.eonideas.ecemetery.parkovi.data.AccountPasswordUpdate;
import hr.eonideas.ecemetery.parkovi.data.AccountUpdate;
import hr.eonideas.ecemetery.parkovi.data.GenericResponse;
import hr.eonideas.ecemetery.parkovi.data.MemoryParameter;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.data.ProfileParameter;
import hr.eonideas.ecemetery.parkovi.interfaces.IBurial;
import hr.eonideas.ecemetery.parkovi.data.AccountRegister;
import hr.eonideas.ecemetery.parkovi.rest.model.Account;
import hr.eonideas.ecemetery.parkovi.rest.model.AccountLight;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;
import hr.eonideas.ecemetery.parkovi.rest.model.AdvertiserCategory;
import hr.eonideas.ecemetery.parkovi.rest.model.Burial;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryHistory;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.Config;
import hr.eonideas.ecemetery.parkovi.rest.model.Gallery;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfoResponse;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import hr.eonideas.ecemetery.parkovi.rest.model.TourGuide;
import retrofit.Call;
import retrofit.Callback;
import retrofit.Response;
import retrofit.http.Body;
import retrofit.http.Part;
import retrofit.http.Path;
import retrofit.http.Query;

public class DummyParkoviApiService implements IParkoviApiService {

	//REST METHODS

//	Advertiser
	public static final String method_advertiser    		= "/advertiser";
	public static final String method_advertiserCategory    = "/advertiser/category";
	public static final String method_advertiserTop    		= "/advertiser/top";
//	Cemetery
	public static final String method_cemeteryGallery      	= "/cemetery/gallery";
	public static final String method_cemeteryHistory      	= "/cemetery/history";
	public static final String method_cemeteryInfo   	   	= "/cemetery/info";
//	Configuration
	public static final String method_configuration 	   	= "/configuration";
//	Memory
	public static final String method_memory        	   	= "/memory";
//	Profile
	public static final String method_profile       		= "/profile";
	public static final String method_profileLight       	= "/profile/light";
//	TourGuide
	public static final String method_tourguide     		= "/tour";

	private Activity activity;

		public void setActivity(Context context){
			if(context instanceof Activity)
			this.activity = (Activity)context;
		}


		//	ADVERTISEMENT
		public Call<PagedResult<Advertiser>> getAdvertiser(Integer categoryId){return null;}

		public Call<PagedResult<AdvertiserCategory>> getAdvertiserCategory(){return null;};

		public Call<PagedResult<Advertiser>> getAdvertiserTop(){return null;};


		//	CEMETERY
		public Call<PagedResult<Gallery>> getCemeteryGallery(){return null;}

		public Call<PagedResult<CemeteryHistory>> getCemeteryHistory(){return null;}

		public Call<List<GroupedCemeteryInfo>> getCemeteryInfo(){return null;}


		//	CONFIGURATION
		public Call<Config> getConfiguration(){return null;}


		//	MEMORY
		public Call<PagedResult<Memory>> getMemory(final Integer profileId, Integer offset, Integer pageSize){
			final CallImplementation<List<Memory>> call = new CallImplementation<List<Memory>>();
			final List<Memory> memoriesList = new ArrayList<Memory>();
			Image accountImg = new Image("http://www.ptc.com/images/dummy_avatar.png","http://www.ptc.com/images/dummy_avatar.png");
			AccountLight lightAccount = new AccountLight(1,"Teodor","Hirs","teohirs@gmail.com",accountImg);
			Image ladislavImg = new Image("http://www.enciklopedija.hr/Ilustracije/HE9_0159.jpg","https://library.foi.hr/m3/s/1/n/0000012451.jpg");
			Profile ladislav = new Profile("1950", "2015", "2015", "Ladislav", 1, ladislavImg, "Rakovac",null,lightAccount,null,2, null);
			Image vatroslavImg = new Image("http://www.enciklopedija.hr/Ilustracije/VatroslavJAGIC1.jpg", "https://upload.wikimedia.org/wikipedia/hr/thumb/6/61/Jagic.jpg/220px-Jagic.jpg");
			Profile vatroslav = new Profile("1920", "2011", "2011", "Vatroslav", 2, vatroslavImg, "Jagic",null,lightAccount,null,2, null);
			memoriesList.add(new Memory("Spomenar #1", 1, 1, ladislavImg, "Somenar #1 text", ladislav));
			memoriesList.add(new Memory("Spomenar #2", 2, 1, ladislavImg, "Somenar #2 text", ladislav));
			memoriesList.add(new Memory("Spomenar #3", 3, 1, ladislavImg, "Somenar #3 text", ladislav));
			memoriesList.add(new Memory("Spomenar #4", 4, 1, ladislavImg, "Somenar #4 text", ladislav));
			memoriesList.add(new Memory("Spomenar #1", 5, 1, vatroslavImg, "Somenar #1 text", vatroslav));
			memoriesList.add(new Memory("Spomenar #2", 6, 1, vatroslavImg, "Somenar #2 text", vatroslav));
			memoriesList.add(new Memory("Spomenar #3", 7, 1, vatroslavImg, "Somenar #3 text", vatroslav));
			memoriesList.add(new Memory("Spomenar #4", 8, 1, vatroslavImg, "Somenar #4 text", vatroslav));

			Handler uiHandler = new Handler(Looper.getMainLooper());
			Runnable runnable = new Runnable() {
				public void run() {
					try {
						synchronized(this){
							wait(2000);
						}
					}
					catch(InterruptedException ex){
					}
					List<Memory> filteredMemoryList = new ArrayList<Memory>();
					if(profileId != null){
						for(Memory memory : memoriesList){
							if(memory.getProfile().getId().intValue() == profileId.intValue()){
								filteredMemoryList.add(memory);
							}
						}
					}else{
						filteredMemoryList = memoriesList;
					}

					call.callback.onResponse(Response.success(filteredMemoryList));
				}
			};
			uiHandler.post(runnable);
			return call;
		}

		public Call<Memory> setMemory(@Body MemoryParameter memory){return null;}

	@Override
	public Call<Memory> updateMemory(@Path("id") Integer id, @Body MemoryParameter memory) {
		return null;
	}


	//	PROFILE

		public Call<PagedResult<Profile>> getProfile(final String searchString, Integer ds, Integer da){
			final CallImplementation<List<Profile>> call = new CallImplementation<List<Profile>>();
			final List<Profile> profileList = new ArrayList<Profile>();
			Image accountImg = new Image("http://www.ptc.com/images/dummy_avatar.png","http://www.ptc.com/images/dummy_avatar.png");
			AccountLight lightAccount = new AccountLight(1,"Teodor","Hirs","teohirs@gmail.com",accountImg);
			profileList.add(new Profile("1950", "2015", "2015", "Ladislav", 1, new Image("http://www.enciklopedija.hr/Ilustracije/HE9_0159.jpg","https://library.foi.hr/m3/s/1/n/0000012451.jpg"), "Rakovac", "Ladislav je bio pravi as,\nali AS",lightAccount, getImageList(),2, null));
			profileList.add(new Profile("1920", "2011", "2011", "Vatroslav", 2, new Image("http://www.enciklopedija.hr/Ilustracije/VatroslavJAGIC1.jpg","https://upload.wikimedia.org/wikipedia/hr/thumb/6/61/Jagic.jpg/220px-Jagic.jpg"),"Jagic", null,lightAccount, null,2,null));
			profileList.add(new Profile("August 6, 1922", "December 29, 2015", "December 29, 2015", "Om Prakash", 3, new Image("https://upload.wikimedia.org/wikipedia/commons/d/d9/General_OP_Malhotra.jpg","https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/General_OP_Malhotra.jpg/150px-General_OP_Malhotra.jpg"),"Malhotra", null,lightAccount, null,2,null));
			profileList.add(new Profile("15 July 1939","28 December 2015","28 December 2015","Barnard", 4,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"Chris", null,lightAccount, null,2,null));
			profileList.add(new Profile("16 February 1953","28 December 2015","28 December 2015","John", 5,new Image("https://upload.wikimedia.org/wikipedia/en/a/a5/John_Bradbury_-_Rhythm_Magazine_shoot.jpg","https://upload.wikimedia.org/wikipedia/en/a/a5/John_Bradbury_-_Rhythm_Magazine_shoot.jpg"),"Bradbury", null,lightAccount, null,2,null));
			profileList.add(new Profile("6 June 1964","28 December 2015","28 December 2015","Josh", 6,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"Guru", null,lightAccount, null,2,null));
			profileList.add(new Profile("5 April 1946","24 December 2015","24 December 2015","Anaya", 7,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"Romeo", null,lightAccount, null,2,null));
			profileList.add(new Profile("1 March 1923","19 December 2015","19 December 2015","Jelicich", 8,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"Stephen", null,lightAccount, null,2,null));
			profileList.add(new Profile("17 October 1914","16 December 2015","16 December 2015","Gabric", 9,new Image("https://upload.wikimedia.org/wikipedia/commons/1/12/Gabre_Gabric.jpg","https://upload.wikimedia.org/wikipedia/commons/1/12/Gabre_Gabric.jpg"),"Gabre", null,lightAccount, null,2,null));
			Handler uiHandler = new Handler(Looper.getMainLooper());
			Runnable runnable = new Runnable() {
				public void run() {
					try {
						synchronized(this){
							wait(2000);
						}
					}
					catch(InterruptedException ex){
					}
					List<Profile> filteredProfileList = new ArrayList<Profile>();
					if(searchString != null){
						for(Profile profile : profileList){
							if(profile.getFirstName().toLowerCase().contains(searchString.toLowerCase()) || profile.getLastName().toLowerCase().contains(searchString.toLowerCase())){
								filteredProfileList.add(profile);
							}
						}
					}else{
						filteredProfileList = profileList;
					}

					call.callback.onResponse(Response.success(filteredProfileList));
				}
			};
			uiHandler.post(runnable);

			return call;
		}

	@Override
	public Call<Profile> getSingleProfile(@Path("id") Integer id) {
		return null;
	}

	@Override
	public Call<ResponseBody> setAsFavorite(@Path("id") Integer id) {
		return null;
	}

	@Override
	public Call<ResponseBody> removeFromFavorites(@Path("id") Integer id) {
		return null;
	}

	@Override
	public Call<ResponseBody> requestOwnership(@Path("id") Integer id) {
		return null;
	}

	@Override
	public Call<Profile> updateBiography(@Path("id") Integer id, @Body ProfileParameter biography) {
		return null;
	}

	@Override
	public Call<ResponseBody> removeProfileGalleryImage(@Path("id") Integer profileId, @Path("itemId") Integer imageId) {
		return null;
	}

	@Override
	public Call<ResponseBody> removeProfileGalleryImages(@Path("id") Integer id, @Body List<Integer> galleryImageIds) {
		return null;
	}


	//	TOURGUIDE
		public Call<PagedResult<TourGuide>> getTourGuide(){return null;}


		//  BURIALS
		@Override
		public Call<PagedResult<Burial>> getBurialList() {
			final CallImplementation<List<IBurial>> call = new CallImplementation<List<IBurial>>();
			final List<IBurial> burials = new ArrayList<IBurial>();
			int id = 0;
			for(int i = 0; i < 3; i++ ){
				burials.add(new Burial(id+i,"Ivica Ivić","",67,"Varaždinsko groblje","1970-01-13 14:00:01"));
			}
			for(int j = 0; j < 3; j++ ){
				burials.add(new Burial(id+j,"Miro Mirić","",67,"Varaždinsko groblje","1970-03-21 12:00:01"));
			}
			for(int k = 0; k < 7; k++ ){
				burials.add(new Burial(id+k,"Teo Teić","",67,"Varaždinsko groblje","1970-05-15 12:00:01"));
			}

			Handler uiHandler = new Handler(Looper.getMainLooper());
			Runnable runnable = new Runnable() {
				public void run() {
					try {
						synchronized(this){
							wait(2000);
						}
					}
					catch(InterruptedException ex){
					}
					call.callback.onResponse(Response.success(burials));
				}
			};
			uiHandler.post(runnable);
			return call;
		}

	@Override
	public Call<Account> register(@Body AccountRegister accountRegister) {
		return null;
	}

	@Override
	public Call<Account> login(@Body AccountLogin accountRegister) {
		return null;
	}

	@Override
	public Call<Account> fblogin(@Body AccountFBLogin accessToken) {
		return null;
	}

	@Override
	public Call<Account> recoverAccount(@Query("id") Integer id) {
		return null;
	}

	@Override
	public Call<Account> updateAccount(@Body AccountUpdate accountUpdate) {
		return null;
	}

	@Override
	public Call<Account> updateAccountPassword(@Body AccountPasswordUpdate accountPasswordUpdate) {
		return null;
	}

	@Override
	public Call<ResponseBody> resetAccountPassword(@Body AccountPasswordReset accountPasswordReset) {
		return null;
	}

	private String getDummyLongText(){
		return "Lorem ipsum dolor sit amet, consectetuer adipiscing elit. Aenean commodo ligula eget dolor. Aenean massa. Cum sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Donec quam felis, ultricies nec, pellentesque eu, pretium quis, sem. Nulla consequat massa quis enim. Donec pede justo, fringilla vel, aliquet nec, vulputate eget, arcu. In enim justo, rhoncus ut, imperdiet a, venenatis vitae, justo. Nullam dictum felis eu pede mollis pretium. Integer tincidunt. Cras dapibus. Vivamus elementum semper nisi. Aenean vulputate eleifend tellus. Aenean leo ligula, porttitor eu, consequat vitae, eleifend ac, enim. Aliquam lorem ante, dapibus in, viverra quis, feugiat a, tellus. Phasellus viverra nulla ut metus varius laoreet. Quisque rutrum. Aenean imperdiet. Etiam ultricies nisi vel augue. Curabitur ullamcorper ultricies nisi. Nam eget dui. Etiam rhoncus. Maecenas tempus, tellus eget condimentum rhoncus, sem quam semper libero, sit amet adipiscing sem neque sed ipsum. Nam quam nunc, blandit vel, luctus pulvinar, hendrerit id, lorem. Maecenas nec odio et ante tincidunt tempus. Donec vitae sapien ut libero venenatis faucibus. Nullam quis ante. Etiam sit amet orci eget eros faucibus tincidunt. Duis leo. Sed fringilla mauris sit amet nibh. Donec sodales sagittis magna. Sed consequat, leo eget bibendum sodales, augue velit cursus nunc,";
	}

	private String getDummyShortText(){
		return "Lorem ipsum dolor sit amet, consectetuer adipiscing elit.";
	}

	private List<GalleryItem> getImageList(){
		List<GalleryItem> imageList = new ArrayList<GalleryItem>();
		imageList.add(new GalleryItem(1,new Image("http://www.enciklopedija.hr/Ilustracije/HE9_0159.jpg","https://library.foi.hr/m3/s/1/n/0000012451.jpg"),"title",1));
		imageList.add(new GalleryItem(2,new Image("http://www.enciklopedija.hr/Ilustracije/VatroslavJAGIC1.jpg","https://upload.wikimedia.org/wikipedia/hr/thumb/6/61/Jagic.jpg/220px-Jagic.jpg"),"title",2));
		imageList.add(new GalleryItem(3,new Image("https://upload.wikimedia.org/wikipedia/commons/d/d9/General_OP_Malhotra.jpg","https://upload.wikimedia.org/wikipedia/commons/thumb/d/d9/General_OP_Malhotra.jpg/150px-General_OP_Malhotra.jpg"),"title",3));
		imageList.add(new GalleryItem(4,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"title",4));
		imageList.add(new GalleryItem(5,new Image("https://upload.wikimedia.org/wikipedia/en/a/a5/John_Bradbury_-_Rhythm_Magazine_shoot.jpg","https://upload.wikimedia.org/wikipedia/en/a/a5/John_Bradbury_-_Rhythm_Magazine_shoot.jpg"),"title",5));
		imageList.add(new GalleryItem(6,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"title",6));
		imageList.add(new GalleryItem(7,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"title",7));
		imageList.add(new GalleryItem(8,new Image("http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png","http://www.kulturmedien-riga.de/tl_files/avatars/avatar_dummy.png"),"title",8));
		imageList.add(new GalleryItem(9,new Image("https://upload.wikimedia.org/wikipedia/commons/1/12/Gabre_Gabric.jpg","https://upload.wikimedia.org/wikipedia/commons/1/12/Gabre_Gabric.jpg"),"title",9));
		return imageList;
	}



	public class CallImplementation<T> implements Call{
		public Callback<T> callback;

		public Callback<T> getCallback() {
			return callback;
		}

		@Override
		public Response execute() throws IOException {
			return null;
		}

		@Override
		public void enqueue(Callback callback) {
			this.callback = callback;

		}

		@Override
		public void cancel() {

		}

		@Override
		public Call clone() {
			return null;
		}
	}
}