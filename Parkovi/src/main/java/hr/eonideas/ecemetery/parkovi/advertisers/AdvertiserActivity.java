package hr.eonideas.ecemetery.parkovi.advertisers;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.util.Log;
import android.view.MenuItem;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.parkovi.ClassResolver;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;
import hr.eonideas.ecemetery.parkovi.rest.model.AdvertiserCategory;

/**
 * Created by Teo on 13.3.2016..
 */
public class AdvertiserActivity extends BaseActivity implements BasicFragment.OnFragmentInteractionListener {

    private static final String TAG = Filter.PACKAGE + ".AdvertiserActivity";
    private AdvertiserCategory selectedCategory;
    private Advertiser selectedAdvertiser;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        init();
    }


    private void init() {
        setContentView(R.layout.fragment_container_w_toolbar);
        initActionBar(getResources().getString(R.string.navbar_menu_item_advertisers));
        selectedCategory = getIntent().getParcelableExtra(AdvertisersListFragment.AD_CATEGORY);
        selectedAdvertiser = getIntent().getParcelableExtra(AdvertiserDetailsFragment.ADVERTISER);
        if (selectedCategory == null && selectedAdvertiser == null) {
            finish();
        }
        Bundle args = new Bundle();
        Fragment fragment;
        if(selectedCategory != null){
            args.putParcelable(AdvertisersListFragment.AD_CATEGORY, selectedCategory);
            fragment = getSupportFragmentManager().findFragmentByTag(AdvertisersListFragment.TAG);
            if (fragment == null) {
                fragment = ClassResolver.getInstance(this).getNewFragment(AdvertisersListFragment.class, args);
            }
            addFragment(fragment, AdvertisersListFragment.TAG);
        }else if (selectedAdvertiser != null){
            args.putParcelable(AdvertiserDetailsFragment.ADVERTISER, (Advertiser)selectedAdvertiser);
            args.putBoolean(AdvertiserDetailsFragment.CLOSE_ON_BACK, true);
            fragment = getSupportFragmentManager().findFragmentByTag(AdvertiserDetailsFragment.TAG);
            if (fragment == null) {
                fragment = ClassResolver.getInstance(this).getNewFragment(AdvertiserDetailsFragment.class, args);
            }
            addFragment(fragment, AdvertiserDetailsFragment.TAG);
        }else{
            finish();
        }


    }

    protected void addFragment(Fragment fragment, String TAG) {
        FragmentManager fm = getSupportFragmentManager();
        fm.beginTransaction().add(R.id.fragment_container, fragment, TAG).addToBackStack(TAG).commit();
    }

    @Override
    public void onFragmentInteraction(int menuItemId, Class fragmentClass, Bundle args) {
        if(fragmentClass != null){
            Fragment fragment;
            fragment = ClassResolver.getInstance(this).getNewFragment(fragmentClass, args);
            if (fragment instanceof BasicFragment)
                addFragment(fragment, ((BasicFragment) fragment).getDefinedTAG());
            else
                addFragment(fragment, "UNKNOWN");
        }else{
            if(Filter.isDebuggingMode)
                Log.d(TAG,"Closing AdvertiserActivity");
            finish();
        }


    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case android.R.id.home:
                onBackPressed();
                int backStackSize = getSupportFragmentManager().getBackStackEntryCount();
                if(backStackSize == 0)
                    finish();
                else{
                    Fragment fragment = getSupportFragmentManager().findFragmentByTag(AdvertisersListFragment.TAG);
                    if (fragment != null) {
                        getSupportActionBar().setTitle(selectedCategory.getTitle());
                    }
                }
                return true;
        }

        return super.onOptionsItemSelected(item);
    }
}
