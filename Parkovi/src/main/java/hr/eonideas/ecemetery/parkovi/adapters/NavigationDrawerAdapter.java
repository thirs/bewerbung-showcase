package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.os.Build;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Arrays;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.commons.NavMenuItem;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.INavDrawerItem;

public class NavigationDrawerAdapter extends ArrayAdapter<INavDrawerItem> {

    private static final String TAG = Filter.PACKAGE + ".NavigationDrawerAdapter";
    private LayoutInflater inflater;
    private Context context;
    private INavDrawerItem[] navDrawerItem;
   
    public NavigationDrawerAdapter(Context context, int textViewResourceId, INavDrawerItem[] objects) {
        super(context, textViewResourceId, new ArrayList<INavDrawerItem>(Arrays.asList(objects)));
        this.inflater = LayoutInflater.from(context);
        this.context = context;
        this.navDrawerItem = objects;
    }

    public void updateItems(INavDrawerItem[] objects){
        clear();
        this.navDrawerItem = objects;
        addAll(objects);
        notifyDataSetChanged();
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        View view = null;
        INavDrawerItem menuItem = navDrawerItem[position];
        view = getItemView(convertView, parent, menuItem );
        return view ;
    }
   
    public View getItemView( View convertView, ViewGroup parentView, INavDrawerItem navDrawerItem ) {
       
        NavMenuItem menuItem = (NavMenuItem) navDrawerItem ;
        NavMenuItemHolder navMenuItemHolder = null;
       
        if (convertView == null) {
            convertView = inflater.inflate( R.layout.navdrawer_item, parentView, false);
            TextView labelView = (TextView) convertView.findViewById( R.id.navmenuitem_label );
            ImageView iconView = (ImageView) convertView.findViewById( R.id.navmenuitem_icon );
            View setIndicator = convertView.findViewById(R.id.navmenuitem_set_indicator);
            RelativeLayout containerView = (RelativeLayout) convertView.findViewById( R.id.navmenuitem_container);

            navMenuItemHolder = new NavMenuItemHolder();
            navMenuItemHolder.labelView = labelView ;
            navMenuItemHolder.iconView = iconView ;
            navMenuItemHolder.containerView = containerView;
            navMenuItemHolder.setIndicator = setIndicator;

            convertView.setTag(navMenuItemHolder);
        }

        if ( navMenuItemHolder == null ) {
            navMenuItemHolder = (NavMenuItemHolder) convertView.getTag();
        }
        navMenuItemHolder.labelView.setText(getContext().getText(menuItem.getLabelResourceId()));
        if(menuItem.isHighlighted()){
            navMenuItemHolder.labelView.setTextColor(getContext().getResources().getColor(R.color.font_dark_gray));
        }


        navMenuItemHolder.iconView.setImageResource(menuItem.getIconResourceId());
        if(menuItem.isSetIndicator()){
            navMenuItemHolder.containerView.setBackgroundColor(context.getResources().getColor(R.color.navigationDrawerItemSelected));
        }else{
            navMenuItemHolder.containerView.setBackgroundColor(context.getResources().getColor(R.color.navigationDrawerItem));
        }
       
        return convertView ;
    }


   
    
    @Override
	public int getCount() {
    	int i = super.getCount();
    	if(Filter.isDebuggingMode)Log.i("Adapter","getCount() returns"+i);
		return i;
	}
   
    @Override
    public boolean isEnabled(int position) {
        return getItem(position).isEnabled();
    }
   
   
    private static class NavMenuItemHolder {
        private TextView labelView;
        private ImageView iconView;
        private RelativeLayout containerView;
        private View setIndicator;
    }

}