package hr.eonideas.ecemetery.parkovi.adapters;

import android.app.Activity;
import android.content.Context;
import android.location.Location;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.commons.utils.MapUtils;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.rest.model.Poi;
import hr.eonideas.ecemetery.parkovi.tours.PoiViewHolder;

/**
 * Created by thirs on 7.11.2015..
 */

public class PoiListAdapter extends ArrayAdapter<Poi> {
    Context context;
    List<Poi> pois;
    private OnItemClickListener mItemClickListener;
    private Location myLocation;

    public PoiListAdapter(Activity activity, List<Poi> pois, Location myLocation){
        this(activity,pois);
        this.myLocation = myLocation;
    }

    public PoiListAdapter(Activity activity, List<Poi> pois) {
        super(activity, -1, pois);
        this.context = activity;
        this.pois = pois;
        if(activity instanceof OnItemClickListener)
            mItemClickListener = (OnItemClickListener) activity;
    }

    public void updateMyLocation(Location myLocation){
        this.myLocation = myLocation;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        PoiViewHolder holder;
        ImageLoader imageLoader = ImageLoader.getInstance();

        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(R.layout.list_item_tour_poi, null);
            holder = new PoiViewHolder(convertView, mItemClickListener, position);
            convertView.setTag(holder);
        } else {
            holder = (PoiViewHolder) convertView.getTag();
        }
        Poi poi = pois.get(position);
        imageLoader.displayImage(poi.getProfile().getImage().getThumbUrl(), holder.poiAvatar);
        holder.poiTitle.setText(ProfileUtils.getProfileName(poi.getProfile()));
        holder.poiDescription.setText(poi.getShortDescription());
        if (myLocation != null){
            holder.poiDistanceInfoContainer.setVisibility(View.VISIBLE);
            holder.poiDistance.setText(String.format(context.getResources().getString(R.string.tours_length), MapUtils.distanceBetweenInKm(myLocation.getLatitude(), myLocation.getLongitude(), poi.getLat().doubleValue(), poi.getLon().doubleValue())));
        }else
            holder.poiDistanceInfoContainer.setVisibility(View.INVISIBLE);
        return convertView;
    }

    public interface OnItemClickListener {
        public void onItemClick(View view , int position);
    }

    public void setOnItemClickListener(final OnItemClickListener mItemClickListener) {
        this.mItemClickListener = mItemClickListener;
    }
}