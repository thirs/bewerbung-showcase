package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.location.Location;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.Collections;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.utils.MapUtils;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IAccountLocation;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.rest.model.AccountLocation;

/**
 * Created by Teo on 10.2.2016..
 */
public class LocationsRecyclerAdapter extends RecyclerView.Adapter<LocationsRecyclerAdapter.LocationViewHolder> {

    private static final String TAG = "LocationsRecyclerAdapter";
    private ImageLoader imageLoader;
    private List<AccountLocation> accountLocations;
    private Context context;
    private IOnLocationClickListener mListener;
    private Location currentLocation;

    public LocationsRecyclerAdapter(List<AccountLocation> locations) {
        this.accountLocations = locations;
        this.imageLoader = ImageLoader.getInstance();
    }

    public void setOnImageClickListener(IOnLocationClickListener listener){
        this.mListener = listener;
    }

    public void setCurrentLocation(Location currentLocation){
        this.currentLocation = currentLocation;
    }

    @Override
    public LocationViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        this.context = parent.getContext();
        return new LocationViewHolder(LayoutInflater.from(parent.getContext()).inflate(R.layout.list_item_account_location, parent, false));
    }

    @Override
    public void onBindViewHolder(LocationViewHolder holder, int position) {
        IAccountLocation locationItem = null;
        holder.locationImage.setImageDrawable(null);
        locationItem = accountLocations.get(position);
        if (locationItem != null){
            if(locationItem.getImage() !=null){
                imageLoader.displayImage(locationItem.getImage().getThumbUrl(), holder.locationImage);
            }else{
                holder.locationImage.setImageResource(R.drawable.placeholder_image);
            }
            holder.familyNames.setText(context.getResources().getString(R.string.location_owner_label) + ":");
            holder.locationProfiles.setText(locationItem.getLocationFamilyNames());
            if(currentLocation != null && locationItem.getLat() != null && locationItem.getLon() != null){
                holder.distanceSection.setVisibility(View.VISIBLE);
                holder.distance.setText(String.valueOf(MapUtils.distanceBetweenInKm(locationItem.getLat().doubleValue(), locationItem.getLon().doubleValue(), currentLocation.getLatitude(),currentLocation.getLongitude())));
                holder.distanceUnit.setText(context.getResources().getString(R.string.distance_unit_km));
            }else{
                holder.distanceSection.setVisibility(View.GONE);
            }
        }
        holder.locationListContainer.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                IAccountLocation a = (IAccountLocation) v.getTag();
                if (mListener != null) {
                    mListener.onLocationClicked(a);
                }

            }
        });
        holder.locationListContainer.setTag(locationItem);

    }

    @Override
    public int getItemCount() {
        int count = accountLocations.size();
        return count;
    }


    public static class LocationViewHolder extends RecyclerView.ViewHolder {
        public View locationListContainer;
        public ImageView locationImage;
        public TextView familyNames;
        public TextView locationProfiles;
        public View distanceSection;
        public TextView distance;
        public TextView distanceUnit;

        public LocationViewHolder(View itemView) {
            super(itemView);
            locationListContainer = itemView.findViewById(R.id.account_location_item);
            locationImage = (ImageView) itemView.findViewById(R.id.account_location_item_avatar);
            familyNames = (TextView) itemView.findViewById(R.id.account_location_item_title);
            locationProfiles = (TextView) itemView.findViewById(R.id.account_location_item_profiles);
            distanceSection = itemView.findViewById(R.id.account_location_item_distance_section);
            distance = (TextView) itemView.findViewById(R.id.account_location_item_distance_length);
            distanceUnit = (TextView) itemView.findViewById(R.id.account_location_item_distance_length_unit);
        }
    }

    public interface IOnLocationClickListener{
        public void onLocationClicked(IAccountLocation location);
    }
}
