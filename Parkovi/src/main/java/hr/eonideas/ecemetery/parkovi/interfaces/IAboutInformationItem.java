package hr.eonideas.ecemetery.parkovi.interfaces;

/**
 * Created by thirs on 27.10.2015..
 */
public interface IAboutInformationItem {
    public String getInfoLabel();
    public String getInfoValue();
    public String getInfoType();
}
