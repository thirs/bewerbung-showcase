package hr.eonideas.ecemetery.parkovi.memories;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ListView;

import java.util.List;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.busevents.ProfileUpdateEvent;
import hr.eonideas.ecemetery.commons.interfaces.ILazyLoad;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.MemoriesListAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.IMemory;
import hr.eonideas.ecemetery.parkovi.profile.ProfileFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import retrofit.Response;

/**
 * Created by thirs on 24.10.2015..
 */
public class MemoriesFragment extends BasicFragment implements IOnGetResponseListener, AdapterView.OnItemClickListener, ILazyLoad {

    public static final String TAG = "MEMORIES_FRAGMENT";
    private RestControler lc;
    private ListView mListView;
    private List<IMemory> memoryList;
    private MemoriesListAdapter memoriesListAdapter;
    private Profile modifiedProfile;
    private Integer offset;


    public static MemoriesFragment newInstance() {
        MemoriesFragment fragment = new MemoriesFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }



    public MemoriesFragment() {
        // Required empty public constructor
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return getResources().getString(R.string.navbar_menu_item_memories);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        offset = 0;
        fetchMemories();
    }

    private void fetchMemories() {
        if(lc == null){
            lc = new RestControler(getActivity());
        }
        lc.setOnGetResponseListener(this);
        lc.getMemory(null, offset, Filter.PAGE_SIZE);
    }

    //Eventbus is used to sinkronize navigationdrawer with floating menu
    @Override
    public void onStart() {
        super.onStart();
        EventBus.getDefault().register(this);
    }


    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.listview, container, false);
        mListView = (ListView) view.findViewById(R.id.listview);
        if(memoryList != null && !memoryList.isEmpty()){
            updateListView(memoryList, -1);
        }
        return view;
    }


    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response != null){
            if(((PagedResult)response.body()).getItems() instanceof List && !((List)((PagedResult)response.body()).getItems()).isEmpty()){
                updateListView((List<IMemory>) ((PagedResult) response.body()).getItems(), ((PagedResult) response.body()).getTotal());
            }
        }
    }

//memoryList
    private void updateListView(List<IMemory> list2show, int totalCount) {
        if(!list2show.isEmpty() && list2show != null){
            if(memoriesListAdapter == null){
                memoriesListAdapter = new MemoriesListAdapter(getActivity(), list2show);
                memoryList = list2show;
                memoriesListAdapter.setTotalCount(totalCount);
                memoriesListAdapter.setLazyLoadListener(this);
                mListView.setAdapter(memoriesListAdapter);
                mListView.setOnItemClickListener(this);
            }else{
                memoriesListAdapter.updateList(list2show);
                memoriesListAdapter.notifyDataSetChanged();
            }
        }
    }

    public void onEvent(ProfileUpdateEvent event){
        modifiedProfile = event.profile;
        int memoryPosition = ProfileUtils.getProfileListPositionFromMemoryList(modifiedProfile, memoryList);
        if(memoryPosition > -1){
            memoryList.get(memoryPosition).setProfile((Profile) modifiedProfile);
        }
    }

    @Override
    public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
        IMemory memory = memoryList.get(position);
        if(memory != null){
            Bundle args = new Bundle();
            args.putParcelable(ProfileFragment.MEMORY, (Memory)memory);
            mListener.onFragmentInteraction(-1, ProfileFragment.class, args);
        }
    }

    @Override
    public void onStop() {
        super.onStop();
        EventBus.getDefault().unregister(this);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        //(th) the rest of the code is used to forward result to child fragments
        List<Fragment> fragments = getChildFragmentManager().getFragments();
        if (fragments != null) {
            fragments.get(0).onActivityResult(requestCode, resultCode, data);
        }
    }

    @Override
    public void updateLazyList(Integer offset) {
        this.offset = offset;
        fetchMemories();
    }
}
