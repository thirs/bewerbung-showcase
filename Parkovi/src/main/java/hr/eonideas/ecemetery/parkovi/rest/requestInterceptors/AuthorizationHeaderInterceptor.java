package hr.eonideas.ecemetery.parkovi.rest.requestInterceptors;

import android.util.Base64;

import com.squareup.okhttp.Interceptor;
import com.squareup.okhttp.Request;
import com.squareup.okhttp.Response;

import java.io.IOException;

import hr.eonideas.ecemetery.Filter;

/**
 * Created by thirs on 20.10.2015..
 */
public class AuthorizationHeaderInterceptor implements Interceptor {

    @Override
    public Response intercept(Chain chain) throws IOException {
        Request request = chain.request();
        String base64 = "Basic " + Base64.encodeToString(Filter.resApi_Token.getBytes(), Base64.NO_WRAP);
        request = request.newBuilder()
                .addHeader("Accept", "application/json")
                .addHeader("Authorization", base64)
                .build();
        Response response = chain.proceed(request);
        return response;
    }
}