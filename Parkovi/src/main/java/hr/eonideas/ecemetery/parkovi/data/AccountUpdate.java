package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import hr.eonideas.ecemetery.parkovi.rest.model.Account;

/**
 * Created by Teo on 9.1.2016..
 */
public class AccountUpdate implements Parcelable {
    String firstName;
    String lastName;

    public AccountUpdate(String firstName, String lastName) {
        this.firstName = firstName;
        this.lastName = lastName;
    }

    public AccountUpdate(Account account) {
        this.firstName = account.getFirstName();
        this.lastName = account.getLastName();
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    protected AccountUpdate(Parcel in) {
        firstName = in.readString();
        lastName = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(firstName);
        dest.writeString(lastName);
    }

    @SuppressWarnings("unused")
    public static final Creator<AccountUpdate> CREATOR = new Creator<AccountUpdate>() {
        @Override
        public AccountUpdate createFromParcel(Parcel in) {
            return new AccountUpdate(in);
        }

        @Override
        public AccountUpdate[] newArray(int size) {
            return new AccountUpdate[size];
        }
    };
}