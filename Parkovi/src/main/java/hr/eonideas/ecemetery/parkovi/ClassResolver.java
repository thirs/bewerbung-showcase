package hr.eonideas.ecemetery.parkovi;

import android.content.Context;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentActivity;
import android.util.Log;

import java.util.HashMap;
import java.util.Map;

import hr.eonideas.ecemetery.commons.CustomizationProvider;
import hr.eonideas.ecemetery.Filter;

public class ClassResolver {
	
	private static final String TAG	= "ClassResolver";

	private static ClassResolver instance;

	public static ClassResolver getInstance(Context context) {
		if(instance == null) {
			synchronized (ClassResolver.class) {
				if(instance == null) {
					instance = new ClassResolver();
					Context applicationContext = context.getApplicationContext();
					if(applicationContext instanceof CustomizationProvider) {
						((CustomizationProvider) applicationContext).addClassMapping(instance);
					}
				}
			}
		}
		return instance;
	}



	private Map<Class<?>, Class<?>> classMap = new HashMap<Class<?>, Class<?>>(32);

	private ClassResolver() {}

	public <T> void addMapping(Class<T> srcClass, Class<? extends T> destClass) {
		synchronized (this) {
			classMap.put(srcClass, destClass);
		}
	}

	@SuppressWarnings("unchecked")
	public <T> Class<? extends T> getMapping(Class<T> srcClass) {
		Class<T> retval = null;
		synchronized (this) {
			try {
				retval = (Class<T>) classMap.get(srcClass);
			} catch (ClassCastException e) {
				if(Filter.isDebuggingMode) {
					Log.e(TAG, "error resolving class: " + srcClass.toString(), e);
				}
			}
		}
		if(retval == null) {
			retval = srcClass;
		}
		return retval;
	}
	
	public static <T> T getListener(String tag, FragmentActivity activity, Class<T> type) {
		if(tag != null) {
			Fragment fragment = activity.getSupportFragmentManager().findFragmentByTag(tag);
			if(type.isInstance(fragment)) {
				return type.cast(fragment);
			}
		}
		
		if(type.isInstance(activity)) {
			return type.cast(activity);
		}
		
		return null;
	}
	
	public <T extends Fragment> T getNewFragment(Class<T> fragmentClass, Bundle args) {
		if(fragmentClass != null) {
			try {
				T fragment = getMapping(fragmentClass).newInstance();
				if(args != null) {
					fragment.setArguments(args);
				}
				return fragment;
			} catch (InstantiationException e) {
			} catch (IllegalAccessException e) {
			}
		}
		return null;
	}

}
