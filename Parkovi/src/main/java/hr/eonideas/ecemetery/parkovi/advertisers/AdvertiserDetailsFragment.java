package hr.eonideas.ecemetery.parkovi.advertisers;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.IconicAdvertisersAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.data.AboutInformationType;
import hr.eonideas.ecemetery.parkovi.data.ContactInformation;
import hr.eonideas.ecemetery.parkovi.data.ImageTextHeader;
import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Advertiser;

/**
 * Created by thirs on 26.10.2015..
 */
public class AdvertiserDetailsFragment extends BasicFragment implements View.OnClickListener {
    public static final String TAG = Filter.PACKAGE + ".AdvertiserDetailsFragment";
    public static final String ADVERTISER = "advertiser";
    public static final String CLOSE_ON_BACK = "closeonback";

    private RecyclerView mRecyclerView;
    private IconicAdvertisersAdapter adapter;
    private Advertiser advertiser;
    private List<IAboutInformationItem> advertiserInfoList;
    private boolean closeOnBack;

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return null;
    }

    public static AdvertiserDetailsFragment newInstance() {
        Bundle args = new Bundle();
        AdvertiserDetailsFragment fragment = new AdvertiserDetailsFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview, container, false);
        advertiser = (Advertiser)getArguments().getParcelable(ADVERTISER);
        closeOnBack = getArguments().getBoolean(CLOSE_ON_BACK,false);
        if(advertiser == null){
//            (th) todo do something since this should happen
        }
        setActionBar(advertiser.getTitle(), false);
        initializeViews(view);
        if(advertiser != null && mRecyclerView != null){
            initializeInformationList();
        }
        return view;
    }



    private void initializeInformationList(){
        if(advertiserInfoList == null || advertiserInfoList.isEmpty()){
            //todo ovo treba doći već tako formatirno sa profilom
            fillAdvertiserInfoList();
        }
        adapter = new IconicAdvertisersAdapter(getActivity(),new ImageTextHeader(advertiser.getTitle(),advertiser.getDescription(),advertiser.getImage().getThumbUrl()), advertiser.getFullDescription(),advertiserInfoList, this);
        mRecyclerView.setAdapter(adapter);
        mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
    }

    //(th) ovaj dio bi trebal u tom formatu doći sa servera ili bi trebal postati dio nekog utils-a
    private void fillAdvertiserInfoList() {
        advertiserInfoList = new ArrayList<IAboutInformationItem>();
        advertiserInfoList.add(new ContactInformation("Email",advertiser.getEmail(),AboutInformationType.TYPE_EMAIL));
        advertiserInfoList.add(new ContactInformation("Fax", advertiser.getFax(),AboutInformationType.TYPE_PHONE));
        advertiserInfoList.add(new ContactInformation("Mobile",advertiser.getMobile(),AboutInformationType.TYPE_MOBILE_PHONE));
        advertiserInfoList.add(new ContactInformation("Phone", advertiser.getPhone(), AboutInformationType.TYPE_PHONE));
        advertiserInfoList.add(new ContactInformation("Working time", advertiser.getWorkingTime(), AboutInformationType.TYPE_WORKING_TIME));
    }

    private void initializeViews(View view) {
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);
    }

    @Override
    protected void backButtonHit() {
        if(closeOnBack){
            mListener.onFragmentInteraction(-1, null, null);
        }

    }

    @Override
    public void onClick(View v) {
        int itemPosition = mRecyclerView.getChildAdapterPosition(v)-2;
        IAboutInformationItem item = advertiserInfoList.get(itemPosition);
        if(item.getInfoType().equals(AboutInformationType.TYPE_ADDRESS)){
            Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + item.getInfoValue());
            Intent intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            startActivity(intent);
        }
        else if(item.getInfoType().equals(AboutInformationType.TYPE_EMAIL)){
            Intent intent = new Intent(Intent.ACTION_SEND);
            intent.setType("text/html");
            intent.putExtra(Intent.EXTRA_EMAIL, "emailaddress@emailaddress.com");
            intent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            intent.putExtra(Intent.EXTRA_TEXT, "I'm email body.");

            startActivity(Intent.createChooser(intent, "Send Email"));
        }
      else if(item.getInfoType().equals(AboutInformationType.TYPE_MOBILE_PHONE) || item.getInfoType().equals(AboutInformationType.TYPE_PHONE)){
            Intent intent = new Intent();
            intent.setAction("android.intent.action.DIAL");
            intent.setData(Uri.parse("tel:"+item.getInfoValue()));
            startActivity(intent);
        }
     else if(item.getInfoType().equals(AboutInformationType.TYPE_WEB)){
            String url = item.getInfoValue().startsWith("http://") && !item.getInfoValue().startsWith("https://"    ) ? item.getInfoValue() : "http://" + item.getInfoValue();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    }
}
