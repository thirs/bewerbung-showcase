package hr.eonideas.ecemetery.parkovi.data;

import java.util.ArrayList;

import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;

/**
 * Created by Teo on 15.1.2016..
 */
public class ContactInformation implements IAboutInformationItem {
    private String infoLabel;
    private String infoValue;
    private String infoType;

    public ContactInformation(String infoLabel, String infoValue, String infoType) {
        this.infoLabel = infoLabel;
        this.infoValue = infoValue;
        this.infoType = infoType;
    }

    @Override
    public String getInfoLabel() {
        return infoLabel;
    }

    @Override
    public String getInfoValue() {
        return infoValue;
    }

    @Override
    public String getInfoType() {
        return infoType;
    }


}
