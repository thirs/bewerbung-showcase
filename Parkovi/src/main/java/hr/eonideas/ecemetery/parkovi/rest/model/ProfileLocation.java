package hr.eonideas.ecemetery.parkovi.rest.model;

import java.math.BigDecimal;
import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;


public class ProfileLocation implements Parcelable {
  
  @SerializedName("id")
  private Integer id = null;
  @SerializedName("lat")
  private BigDecimal lat = null;
  @SerializedName("lng")
  private BigDecimal lon = null;

  
  /**
   * Unique identifier of Location in database.
   **/
  public Integer getId() {
    return id;
  }
  public void setId(Integer id) {
    this.id = id;
  }

  
  /**
   * Latitude.
   **/
  public BigDecimal getLat() {
    return lat;
  }
  public void setLat(BigDecimal lat) {
    this.lat = lat;
  }

  
  /**
   * Longitude.
   **/
  public BigDecimal getLon() {
    return lon;
  }
  public void setLon(BigDecimal lon) {
    this.lon = lon;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class Location {\n");
    
    sb.append("  id: ").append(id).append("\n");
    sb.append("  lat: ").append(lat).append("\n");
    sb.append("  lon: ").append(lon).append("\n");
    sb.append("}\n");
    return sb.toString();
  }

    protected ProfileLocation(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        lat = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        lon = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeValue(lat);
        dest.writeValue(lon);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<ProfileLocation> CREATOR = new Parcelable.Creator<ProfileLocation>() {
        @Override
        public ProfileLocation createFromParcel(Parcel in) {
            return new ProfileLocation(in);
        }

        @Override
        public ProfileLocation[] newArray(int size) {
            return new ProfileLocation[size];
        }
    };
}