package hr.eonideas.ecemetery.parkovi.rest.intefaces;

import retrofit.Response;

public interface IOnFailedResponseListener {
	public void onFailedResponse(Throwable throwable, int requestId);
}
