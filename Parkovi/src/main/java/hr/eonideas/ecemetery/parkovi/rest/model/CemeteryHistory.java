package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;
import com.google.gson.annotations.SerializedName;


public class CemeteryHistory implements Parcelable {
  
  @SerializedName("title")
  private String title = null;
  @SerializedName("shortDescription")
  private String shortDescription = null;
  @SerializedName("fullDescription")
  private String fullDescription = null;
  @SerializedName("image")
  private Image image = null;

    public CemeteryHistory(String fullDescription, Image image, String title, String shortDescription) {
        this.fullDescription = fullDescription;
        this.image = image;
        this.shortDescription = shortDescription;
        this.title = title;
    }

    public CemeteryHistory(CemeteryHistory cemeteryHistory) {
        this.fullDescription = cemeteryHistory.getFullDescription();
        this.image = cemeteryHistory.getImage();
        this.shortDescription = cemeteryHistory.getShortDescription();
        this.title = cemeteryHistory.getTitle();
    }

    /**
   * History fact title.
   **/
  public String getTitle() {
    return title;
  }
  public void setTitle(String title) {
    this.title = title;
  }

  
  /**
   * Short description.
   **/
  public String getShortDescription() {
    return shortDescription;
  }
  public void setShortDescription(String shortDescription) {
    this.shortDescription = shortDescription;
  }

  
  /**
   * Full description.
   **/
  public String getFullDescription() {
    return fullDescription;
  }
  public void setFullDescription(String fullDescription) {
    this.fullDescription = fullDescription;
  }

  
  /**
   **/
  public Image getImage() {
    return image;
  }
  public void setImage(Image image) {
    this.image = image;
  }

  

  @Override
  public String toString()  {
    StringBuilder sb = new StringBuilder();
    sb.append("class CemeteryHistory {\n");
    
    sb.append("  title: ").append(title).append("\n");
    sb.append("  shortDescription: ").append(shortDescription).append("\n");
    sb.append("  fullDescription: ").append(fullDescription).append("\n");
    sb.append("  image: ").append(image).append("\n");
    sb.append("}\n");
    return sb.toString();
  }

    protected CemeteryHistory(Parcel in) {
        title = in.readString();
        shortDescription = in.readString();
        fullDescription = in.readString();
        image = (Image) in.readValue(Image.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(title);
        dest.writeString(shortDescription);
        dest.writeString(fullDescription);
        dest.writeValue(image);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<CemeteryHistory> CREATOR = new Parcelable.Creator<CemeteryHistory>() {
        @Override
        public CemeteryHistory createFromParcel(Parcel in) {
            return new CemeteryHistory(in);
        }

        @Override
        public CemeteryHistory[] newArray(int size) {
            return new CemeteryHistory[size];
        }
    };
}