package hr.eonideas.ecemetery.parkovi.interfaces;

public interface INavDrawerItem {


    //    (th) sve menu stavke - int vrijednosti odgovaraju array pozicijama za stringove i icone
    public static final int	MENU_ITEM_HOME		    = 0;
    public static final int	MENU_ITEM_ABOUT		    = 1;
    public static final int	MENU_ITEM_TOURS 	    = 2;
    public static final int	MENU_ITEM_MEMORIES	    = 3;
    public static final int	MENU_ITEM_PROFILES	    = 4;
    public static final int	MENU_ITEM_MYECEMETERY   = 5;
    public static final int	MENU_ITEM_ADVERTISERS   = 6;
    public static final int	MENU_ITEM_BURIALS   	= 7;
    public static final int	MENU_ITEM_SETTINGS  	= 8;
    public static final int	MENU_ITEM_LOGIN      	= 9;
    public static final int	MENU_ITEM_LOGOUT  	    = 10;

    //    (th) menu drawer može imati različite stavke sa različitim  ponašanjima (itemi, sekcije,...)
    public static final int DRAWER_MENU_ITEM        = 31;

    public int getId();
    public int getLabelResourceId();
    public int getIconResourceId();
    public boolean isEnabled();
    public boolean updateActionBarTitle();
    public int getDrawerItemType();
    public boolean isSetIndicator();
    public boolean isHighlighted();
}