package hr.eonideas.ecemetery.parkovi.data;

/**
 * Created by Teo on 17.1.2016..
 */
public class ImageTextHeader {
    private String title;
    private String subtitle;
    private String headerImageUrl;

    public ImageTextHeader(String title, String subtitle, String headerImageUrl) {
        this.title = title;
        this.subtitle = subtitle;
        this.headerImageUrl = headerImageUrl;
    }

    public String getTitle() {
        return title;
    }

    public String getSubtitle() {
        return subtitle;
    }

    public String getHeaderImageUrl() {
        return headerImageUrl;
    }
}
