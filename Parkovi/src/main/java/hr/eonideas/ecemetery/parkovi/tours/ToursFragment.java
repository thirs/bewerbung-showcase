package hr.eonideas.ecemetery.parkovi.tours;

import android.content.Intent;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.commons.BasicFragment;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.TourGuideAdapter;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.ITourGuide;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.TourGuide;
import retrofit.Response;

/**
 * Created by thirs on 31.10.2015..
 */
public class ToursFragment extends BasicFragment implements IOnGetResponseListener {
    public static String TAG ="TOURS";

    private RestControler lc;
    private RecyclerView mRecyclerView;
    private List<ITourGuide> tourGuideList;

    public static ToursFragment newInstance() {
        ToursFragment fragment = new ToursFragment();
        Bundle args = new Bundle();
        fragment.setArguments(args);
        return fragment;
    }

    public ToursFragment() {
    }

    @Override
    public String getDefinedTAG() {
        return TAG;
    }

    @Override
    public String getScreenTitle() {
        return getResources().getString(R.string.navbar_menu_item_tours);
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        if(lc == null)
            lc = new RestControler(getActivity());
        lc.setOnGetResponseListener(this);
        lc.getTourGuide();
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.tours_fragment, container, false);
        mRecyclerView = (RecyclerView) v.findViewById(R.id.recycler);
        //customizacija actionbara
        setActionBar("", true);
        if(tourGuideList != null && !tourGuideList.isEmpty()){
            initializeToursList();
        }

        return v;
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response.body() != null){
            if(((PagedResult)response.body()).getItems() instanceof List && !((List)((PagedResult)response.body()).getItems()).isEmpty()){
                tourGuideList = (List<ITourGuide>) ((PagedResult)response.body()).getItems();
                initializeToursList();
            }
        }
    }

    private void initializeToursList() {
        if(!tourGuideList.isEmpty() && mRecyclerView != null){
            TourGuideAdapter adapter = new TourGuideAdapter(getActivity(),tourGuideList);
            mRecyclerView.setAdapter(adapter);
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            adapter.SetOnItemClickListener(new TourGuideAdapter.OnItemClickListener() {
                @Override
                public void onItemClick(View view, int position) {
                    TourGuide item = (TourGuide) tourGuideList.get(position);
                    Intent intent = new Intent(getActivity(), TourDetailsActivity.class);
                    intent.putExtra(TourDetailsActivity.TOUR, item);
                    startActivity(intent);
                }
            });
        }
    }
}
