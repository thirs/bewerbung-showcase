package hr.eonideas.ecemetery.parkovi.about.information;

import android.content.Intent;
import android.net.Uri;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import com.bignerdranch.expandablerecyclerview.Model.ParentListItem;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.widgets.decorators.DividerItemDecoration;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.IconicAboutAdapter;
import hr.eonideas.ecemetery.parkovi.commons.BasicPagerFragment;
import hr.eonideas.ecemetery.parkovi.data.AboutInformationHeader;
import hr.eonideas.ecemetery.parkovi.data.AboutInformationType;
import hr.eonideas.ecemetery.parkovi.data.PagedResult;
import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;
import hr.eonideas.ecemetery.parkovi.interfaces.IGroupedAboutInformationItem;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnGetResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.CemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfo;
import hr.eonideas.ecemetery.parkovi.rest.model.GroupedCemeteryInfoResponse;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import retrofit.Response;

/**
 * Created by thirs on 26.10.2015..
 */
public class AboutInformationFragment extends BasicPagerFragment implements IOnGetResponseListener, View.OnClickListener {


    private RecyclerView mRecyclerView;
    private IconicAboutAdapter adapter;
    private List<IGroupedAboutInformationItem> cemeteryInfoList;
    private RestControler lc;




    public static AboutInformationFragment newInstance(int page) {
        Bundle args = new Bundle();
        AboutInformationFragment fragment = new AboutInformationFragment();
        fragment.setArguments(args);
        return fragment;
    }

    @Override
    public void onResume() {
        super.onResume();
        if(!(language.equals(app.getAppLanguage()))){
            language = app.getAppLanguage();
            lc = new RestControler(getActivity(), language);
            lc.setOnGetResponseListener(this);
            lc.getCemeteryInfo();
        }
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.recyclerview, container, false);
        mRecyclerView = (RecyclerView) view.findViewById(R.id.recyclerView);

        if(cemeteryInfoList != null && !cemeteryInfoList.isEmpty()){
            initializeInformationList();
        }

        return view;
    }


    @Override
    protected boolean getUserVisibleHintAdditionalCondition() {
        return cemeteryInfoList == null || cemeteryInfoList.isEmpty();
    }

    @Override
    protected void isVisibleToUser() {
        if(lc == null){
            lc = new RestControler(getActivity());
        }
        lc.setOnGetResponseListener(this);
        lc.getCemeteryInfo();
    }

    @Override
    public void onGetResponse(Response response, int requestId) {
        if(response != null && response.body() != null){
            if(response.body() instanceof List && !((List<GroupedCemeteryInfo>)response.body()).isEmpty()){
                cemeteryInfoList = (List<IGroupedAboutInformationItem>)response.body();
                initializeInformationList();
            }
        }
    }

    private void initializeInformationList(){
        if(!cemeteryInfoList.isEmpty() && mRecyclerView != null){
            List<ParentListItem> headers = new ArrayList<ParentListItem>();
            for(IGroupedAboutInformationItem informationItem : cemeteryInfoList){
                headers.add(new AboutInformationHeader(informationItem.getItems(), informationItem.getSectionTitle()));
            }
            //(th) entering additional section - MAP
            List mapSection = new ArrayList<IAboutInformationItem>();
            mapSection.add(new CemeteryInfo(AboutInformationType.TYPE_MAP));
            AboutInformationHeader mapHeader = new AboutInformationHeader(mapSection, getContext().getResources().getString(R.string.about_information_section_header_map));
            headers.add(mapHeader);

            adapter = new IconicAboutAdapter(getActivity(), headers, this, getChildFragmentManager());
            mRecyclerView.setLayoutManager(new LinearLayoutManager(getActivity()));
            mRecyclerView.addItemDecoration(new DividerItemDecoration(getContext()));
            mRecyclerView.setAdapter(adapter);

            adapter.expandParent(0);
        }
    }

    @Override
    public void onClick(View v) {
        int expandedHeader = adapter.getExpandedHeader();
        if(v.getTag() == null && expandedHeader > -1){
            return;
        }
        int itemPosition = (int) v.getTag();
        if(Filter.isDebuggingMode){
            Toast.makeText(getContext(),"Postion is:" + itemPosition + ". Total count is:" + cemeteryInfoList.size(),Toast.LENGTH_SHORT).show();
        }
        IAboutInformationItem item = cemeteryInfoList.get(expandedHeader).getItems().get(itemPosition);
        if(item.getInfoType().equals(AboutInformationType.TYPE_ADDRESS)){
            Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + item.getInfoValue());
            Intent intent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
            startActivity(intent);
        }
        else if(item.getInfoType().equals(AboutInformationType.TYPE_EMAIL)){
            Intent emailIntent = new Intent(Intent.ACTION_SENDTO, Uri.fromParts(
                    "mailto",item.getInfoValue(), null));
            emailIntent.putExtra(Intent.EXTRA_SUBJECT, "Subject");
            emailIntent.putExtra(Intent.EXTRA_TEXT, "Body");
            startActivity(Intent.createChooser(emailIntent, "Send email..."));
        }
      else if(item.getInfoType().equals(AboutInformationType.TYPE_MOBILE_PHONE) || item.getInfoType().equals(AboutInformationType.TYPE_PHONE)){
            Intent intent = new Intent();
            intent.setAction("android.intent.action.DIAL");
            intent.setData(Uri.parse("tel:"+item.getInfoValue()));
            startActivity(intent);
        }
     else if(item.getInfoType().equals(AboutInformationType.TYPE_WEB)){
            String url = item.getInfoValue().startsWith("http://") && !item.getInfoValue().startsWith("https://"    ) ? item.getInfoValue() : "http://" + item.getInfoValue();
            Intent intent = new Intent(Intent.ACTION_VIEW);
            intent.setData(Uri.parse(url));
            startActivity(intent);
        }
    }
}
