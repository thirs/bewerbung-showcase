package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.interfaces.IAccountLocation;

/**
 * Created by Teo on 26.5.2016..
 */
public class AccountLocation implements IAccountLocation, Parcelable {

    @SerializedName("id")
    private Integer id = null;
    @SerializedName("lat")
    private BigDecimal lat = null;
    @SerializedName("lng")
    private BigDecimal lon = null;
    @SerializedName("image")
    private Image image = null;
    @SerializedName("galleryItems")
    private List<GalleryItem> profileGalleryImages = null;
    @SerializedName("accountId")
    private Integer accountId = null;
    @SerializedName("locationFamilyNames")
    private String locationFamilyNames = null;
    @SerializedName("profiles")
    private List<Integer> profiles = null;

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public BigDecimal getLat() {
        return lat;
    }

    public void setLat(BigDecimal lat) {
        this.lat = lat;
    }

    public BigDecimal getLon() {
        return lon;
    }

    public void setLon(BigDecimal lon) {
        this.lon = lon;
    }

    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    public List<GalleryItem> getProfileGalleryImages() {
        return profileGalleryImages;
    }

    public void setProfileGalleryImages(List<GalleryItem> profileGalleryImages) {
        this.profileGalleryImages = profileGalleryImages;
    }

    public Integer getAccountId() {
        return accountId;
    }

    public void setAccountId(Integer accountId) {
        this.accountId = accountId;
    }

    public String getLocationFamilyNames() {
        return locationFamilyNames;
    }

    public void setLocationFamilyNames(String locationFamilyNames) {
        this.locationFamilyNames = locationFamilyNames;
    }

    public List<Integer> getProfiles() {
        return profiles;
    }

    public void setProfiles(List<Integer> profiles) {
        this.profiles = profiles;
    }


    protected AccountLocation(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        lat = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        lon = (BigDecimal) in.readValue(BigDecimal.class.getClassLoader());
        image = (Image) in.readValue(Image.class.getClassLoader());
        if (in.readByte() == 0x01) {
            profileGalleryImages = new ArrayList<GalleryItem>();
            in.readList(profileGalleryImages, GalleryItem.class.getClassLoader());
        } else {
            profileGalleryImages = null;
        }
        accountId = in.readByte() == 0x00 ? null : in.readInt();
        locationFamilyNames = in.readString();
        if (in.readByte() == 0x01) {
            profiles = new ArrayList<Integer>();
            in.readList(profiles, Integer.class.getClassLoader());
        } else {
            profiles = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeValue(lat);
        dest.writeValue(lon);
        dest.writeValue(image);
        if (profileGalleryImages == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(profileGalleryImages);
        }
        if (accountId == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(accountId);
        }
        dest.writeString(locationFamilyNames);
        if (profiles == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(profiles);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AccountLocation> CREATOR = new Parcelable.Creator<AccountLocation>() {
        @Override
        public AccountLocation createFromParcel(Parcel in) {
            return new AccountLocation(in);
        }

        @Override
        public AccountLocation[] newArray(int size) {
            return new AccountLocation[size];
        }
    };
}
