package hr.eonideas.ecemetery.parkovi.profile.memories;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import de.greenrobot.event.EventBus;
import hr.eonideas.ecemetery.Filter;
import hr.eonideas.ecemetery.commons.BaseActivity;
import hr.eonideas.ecemetery.commons.busevents.ProfileUpdateEvent;
import hr.eonideas.ecemetery.commons.utils.FormUtils;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.commons.utils.Utils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.commons.ImageAlbumActivity;
import hr.eonideas.ecemetery.parkovi.data.MemoryParameter;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.profile.ProfileFragment;
import hr.eonideas.ecemetery.parkovi.rest.controlers.RestControler;
import hr.eonideas.ecemetery.parkovi.rest.intefaces.IOnPostResponseListener;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Memory;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;
import retrofit.Response;

/**
 * Created by Teo on 19.1.2016..
 */
public class CreateMemoryActivity extends BaseActivity implements View.OnClickListener, IOnPostResponseListener {

    public static final String PROFILE = Filter.PACKAGE + ".PROFILE";
    public static final String MEMORY = Filter.PACKAGE + ".MEMORY";
    private static final int PICK_MEMORY_IMAGE = 31;

    private IProfile profile;
    private MemoryParameter memory;

    private EditText memoryText;
    private EditText memoryAuthor;
    private TextView memoryImage;

    private RestControler lc;
    private Memory newMemory;
    private Memory editableMemory;

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        super.onCreateOptionsMenu(menu);
        MenuInflater inflater = getMenuInflater();
        inflater.inflate(R.menu.confirm_menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if(item.getItemId() == R.id.confirm_menu_item){
            if(filledMandatoryFields()){
                memory.setMemoryText(memoryText.getText().toString());
                if(!FormUtils.isEditTextEmpty(memoryAuthor)){
                    memory.setAuthor(memoryAuthor.getText().toString());
                }
                uploadMemory();
            }
            return true;
        }else{
            return super.onOptionsItemSelected(item);
        }

    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.create_memory);
        initActionBar("Memory creation");
        init();
    }

    private void init() {
        profile = getIntent().getParcelableExtra(PROFILE);
        editableMemory = getIntent().getParcelableExtra(MEMORY);
        if(profile == null){
            //(th) todo something's wrong --- close activity or throw some kind of warning
        }
        memory = new MemoryParameter(profile.getId());
        initiateViewElements();
        memoryImage.setOnClickListener(this);
        if(editableMemory != null){
            memoryText.setText(editableMemory.getMemoryText());
            memoryAuthor.setText(editableMemory.getAuthor());
        }
    }

    private void initiateViewElements() {
        memoryText = (EditText) findViewById(R.id.memory_text);
        memoryAuthor = (EditText) findViewById(R.id.memory_author);
        memoryImage = (TextView) findViewById(R.id.memory_image);

    }

    private void uploadMemory(){
        //poziv metode za kreiranje memory-a

        if (lc == null) {
            lc = new RestControler(this);
        }
        lc.setOnPostResponseListener(this);
        if(editableMemory != null){
            lc.updateMemory(editableMemory.getId(),memory, app.getUserAccount());
        }else if(memory != null){
            lc.setMemory(memory, app.getUserAccount());
        }

    }

    @Override
    public void onClick(View v) {
        if (v.getId() == R.id.memory_image){
            Intent intent = new Intent(this, ImageAlbumActivity.class);
            intent.putExtra(ImageAlbumActivity.ALBUM_GALLERY_ITEMS, new ArrayList(profile.getProfileGallery()));
            intent.putExtra(ImageAlbumActivity.PROFILE, (Profile)profile);
            intent.putExtra(ImageAlbumActivity.ALBUM_TITLE, ProfileUtils.getProfileName(profile));
            intent.putExtra(ImageAlbumActivity.IMAGE_GRID_MODE, ImageAlbumActivity.ImageGridMode.PICKER);
            startActivityForResult(intent, PICK_MEMORY_IMAGE);
        }

    }

    private boolean filledMandatoryFields() {
        boolean filledMandatoryFields = true;
        if(FormUtils.isEditTextEmpty(memoryText))
            filledMandatoryFields = false;
        return filledMandatoryFields;
    }

    @Override
    public void onPostResponse(Response response, int requestId) {
        if (response != null) {
            if (response.body() != null && response.body() instanceof Memory) {
                newMemory = (Memory) response.body();
                profile.setNumberOfMemories(profile.getNumberOfMemories() + 1);
                EventBus.getDefault().post(new ProfileUpdateEvent((Profile)profile));
                Intent resultIntent = new Intent();
                resultIntent.putExtra(ProfileFragment.CREATED_MEMORY, newMemory);
                resultIntent.putExtra(ProfileFragment.UPDATED_PROFILE, (Profile)profile);
                setResult(Activity.RESULT_OK, resultIntent);
                finish();
            }
        }
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        if(requestCode == PICK_MEMORY_IMAGE && data != null){
            Integer pickedImagePosition = data.getIntExtra(ImageAlbumActivity.PICKED_IMAGE_POSITION,-1);
            if(pickedImagePosition > -1 && profile.getProfileGallery() != null && profile.getProfileGallery().size() > 0){
                memory.setProfileGalleryItemId(profile.getProfileGallery().get(pickedImagePosition).getId());
                memoryImage.setText(profile.getProfileGallery().get(pickedImagePosition).getTitle());
            }

        }else{
            super.onActivityResult(requestCode, resultCode, data);
        }

    }
}
