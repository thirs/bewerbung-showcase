package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 14.6.2016..
 */
public class AccountPasswordReset implements Parcelable {
    @SerializedName("emailAddress")
    String email;

    public AccountPasswordReset(String email) {
        this.email = email;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    protected AccountPasswordReset(Parcel in) {
        email = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(email);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AccountPasswordReset> CREATOR = new Parcelable.Creator<AccountPasswordReset>() {
        @Override
        public AccountPasswordReset createFromParcel(Parcel in) {
            return new AccountPasswordReset(in);
        }

        @Override
        public AccountPasswordReset[] newArray(int size) {
            return new AccountPasswordReset[size];
        }
    };
}
