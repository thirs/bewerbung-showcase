package hr.eonideas.ecemetery.parkovi.adapters;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.data.AboutInformationType;
import hr.eonideas.ecemetery.parkovi.data.ContactInformation;
import hr.eonideas.ecemetery.parkovi.data.ImageTextHeader;
import hr.eonideas.ecemetery.parkovi.interfaces.IAboutInformationItem;

/**
 * Created by Teo on 17.1.2016..
 */
public class IconicAdvertisersAdapter extends RecyclerView.Adapter<RecyclerView.ViewHolder>  {
    private static final int TYPE_HEADER = 0;
    private static final int TYPE_ABOUT = 1;
    private static final int TYPE_ITEM = 2;

    private Context context;
    private ImageTextHeader header;
    private List<IAboutInformationItem> listItems;
    private ImageLoader imageLoader;
    private String aboutText;
    private View.OnClickListener listener;

    public IconicAdvertisersAdapter(Context context, ImageTextHeader header, String aboutText, List<IAboutInformationItem> listItems, View.OnClickListener listener)
    {
        this.context = context;
        this.header = header;
        this.listItems = listItems;
        this.aboutText = aboutText;
        this.imageLoader = ImageLoader.getInstance();
        this.listener = listener;
        setHasStableIds(false);
    }

    @Override
    public RecyclerView.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        if(viewType == TYPE_HEADER)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.image_text_header, parent, false);
            return  new VHHeader(v);
        }
        else if(viewType == TYPE_ABOUT)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.advertisers_details_about_item, parent, false);
            return new VHAbout(v);
        }
        else if(viewType == TYPE_ITEM)
        {
            View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.contact_information_item, parent, false);
            v.setOnClickListener(listener);
            return new VHItem(v);
        }
        throw new RuntimeException("there is no type that matches the type " + viewType + " + make sure your using types correctly");
    }

    private IAboutInformationItem getItem(int position)
    {
        return listItems.get(position);
    }


    @Override
    public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
        if(holder instanceof VHHeader)
        {
            VHHeader VHheader = (VHHeader)holder;
            VHheader.ad_header_title.setText(header.getTitle());
            if(TextUtils.isEmpty(header.getSubtitle())){
                VHheader.ad_header_subtitle.setVisibility(View.VISIBLE);
                VHheader.ad_header_subtitle.setText(header.getSubtitle());
            }
            imageLoader.displayImage(header.getHeaderImageUrl(),VHheader.ad_header_image);
        }
        else if(holder instanceof VHAbout)
        {
            VHAbout VHabout = (VHAbout)holder;
            VHabout.ad_about_text.setText(aboutText);
        }
        else if(holder instanceof VHItem)
        {
            IAboutInformationItem currentItem = getItem(position-2);
            VHItem VHitem = (VHItem) holder;
            VHitem.about_information_label.setText(currentItem.getInfoLabel());
            VHitem.about_information_value.setText(currentItem.getInfoValue());
            if(currentItem.getInfoType().equals(AboutInformationType.TYPE_ADDRESS)){
                VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_home));
            }else if(currentItem.getInfoType().equals(AboutInformationType.TYPE_EMAIL)){
                VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_mail));
            }else if(currentItem.getInfoType().equals(AboutInformationType.TYPE_MOBILE_PHONE)){
                VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_mobile));
            }else if(currentItem.getInfoType().equals(AboutInformationType.TYPE_PHONE)){
                VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_phone));
            }else if(currentItem.getInfoType().equals(AboutInformationType.TYPE_WEB)){
                VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_www));
            }else if(currentItem.getInfoType().equals(AboutInformationType.TYPE_WORKING_TIME)){
                VHitem.about_information_icon.setImageDrawable(context.getResources().getDrawable(R.drawable.icon_contact_workinghours));
            }
        }
    }

    //    need to override this method
    @Override
    public int getItemViewType(int position) {
        if(isPositionHeader(position)){
            return TYPE_HEADER;
        }
        else if(isPositionAbout(position)){
            return TYPE_ABOUT;
        }
        else{
            return TYPE_ITEM;
        }
    }

    private boolean isPositionHeader(int position)
    {
        return position == 0;
    }

    private boolean isPositionAbout(int position)
    {
        return position == 1;
    }

    //increasing getItemcount for 2.  will be the row of header.
    @Override
    public int getItemCount() {
        return listItems.size()+2;
    }

    class VHHeader extends RecyclerView.ViewHolder {
        protected TextView ad_header_title;
        protected TextView ad_header_subtitle;
        protected ImageView ad_header_image;

        public VHHeader(View itemView) {
            super(itemView);
            ad_header_title = (TextView) itemView.findViewById(R.id.header_title);
            ad_header_subtitle = (TextView) itemView.findViewById(R.id.header_subtitle);
            ad_header_image = (ImageView) itemView.findViewById(R.id.header_img);
        }
    }

    class VHAbout extends RecyclerView.ViewHolder {
        protected TextView ad_about_text;

        public VHAbout(View itemView) {
            super(itemView);
            ad_about_text = (TextView) itemView.findViewById(R.id.ad_about);
        }
    }

     class VHItem extends RecyclerView.ViewHolder {
        protected TextView about_information_label;
        protected TextView about_information_value;
        protected ImageView about_information_icon;

        public VHItem(View itemView) {
            super(itemView);
            about_information_label = (TextView) itemView.findViewById(R.id.about_information_label);
            about_information_value = (TextView) itemView.findViewById(R.id.about_information_value);
            about_information_icon = (ImageView) itemView.findViewById(R.id.about_information_icon);
        }
    }
}
