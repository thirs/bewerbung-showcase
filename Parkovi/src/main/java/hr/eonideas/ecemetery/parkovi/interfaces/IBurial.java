package hr.eonideas.ecemetery.parkovi.interfaces;

/**
 * Created by thirs on 31.10.2015..
 */
public interface IBurial {

    public String getTime();
    public String getName();
    public Integer getAge();
    public String getLocation();
}
