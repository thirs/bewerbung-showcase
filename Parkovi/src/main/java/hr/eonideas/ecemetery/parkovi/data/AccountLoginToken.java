package hr.eonideas.ecemetery.parkovi.data;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

/**
 * Created by Teo on 11.1.2016..
 */
public class AccountLoginToken implements Parcelable {
    @SerializedName("auth_token")
    String authToken;

    public AccountLoginToken(String authToken) {
        this.authToken = authToken;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    protected AccountLoginToken(Parcel in) {
        authToken = in.readString();
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeString(authToken);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<AccountLoginToken> CREATOR = new Parcelable.Creator<AccountLoginToken>() {
        @Override
        public AccountLoginToken createFromParcel(Parcel in) {
            return new AccountLoginToken(in);
        }

        @Override
        public AccountLoginToken[] newArray(int size) {
            return new AccountLoginToken[size];
        }
    };
}
