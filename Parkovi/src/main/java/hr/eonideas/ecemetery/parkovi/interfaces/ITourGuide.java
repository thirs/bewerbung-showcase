package hr.eonideas.ecemetery.parkovi.interfaces;

import java.math.BigDecimal;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;
import hr.eonideas.ecemetery.parkovi.rest.model.Poi;

/**
 * Created by thirs on 31.10.2015..
 */
public interface ITourGuide {

        public String getShortText();
        public String getFullText();
        public Integer getDuration();
        public BigDecimal getDistance();
        public String getTitle();
        public String getSubTitle();
        public Image getImage();
        public List<Poi> getPOI();
        public List<GalleryItem> getGalleryItems();
}
