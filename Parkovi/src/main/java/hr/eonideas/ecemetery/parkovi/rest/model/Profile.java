package hr.eonideas.ecemetery.parkovi.rest.model;


import com.google.gson.annotations.SerializedName;

import android.os.Parcel;
import android.os.Parcelable;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;

import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.data.ProfileAnniversary;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;

public class Profile implements IProfile, Parcelable {

    private static final long TIME_UNAVAILABLE = 730;
    @SerializedName("id")
    private Integer id = null;
    @SerializedName("firstName")
    private String firstName = null;
    @SerializedName("lastName")
    private String lastName = null;
    @SerializedName("birth")
    private String birth = null;
    @SerializedName("death")
    private String death = null;
    @SerializedName("burial")
    private String burial = null;
    @SerializedName("image")
    private Image image = null;
    @SerializedName("biography")
    private String aboutText = null;
    @SerializedName("ownerAccount")
    private AccountLight ownerAccount = null;
    @SerializedName("galleryItems")
    private List<GalleryItem> profileGalleryImages = null;
    @SerializedName("numberOfMemories")
    private Integer numberOfMemories = null;
    @SerializedName("location")
    private ProfileLocation location = null;


    private boolean isFavorite;
    private boolean isAdmin;
    private boolean isOwned;
    private boolean isInEditMode;
    private boolean isHeaderImageModified;
    private ProfileAnniversary profileAnniversary;

    public Profile(String birth, String death, String burial, String firstName, Integer id, Image image, String lastName, String aboutText,AccountLight ownerAccount, List<GalleryItem> profileGalleryImages, Integer numberOfMemories, ProfileLocation location) {
        this.birth = birth;
        this.death = death;
        this.burial = burial;
        this.firstName = firstName;
        this.id = id;
        this.image = image;
        this.lastName = lastName;
        this.aboutText = aboutText;
        this.ownerAccount = ownerAccount;
        this.profileGalleryImages = profileGalleryImages;
        this.numberOfMemories = numberOfMemories;
        this.location = location;
    }

    /**
     * Unique identifier of profile.
     **/
    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getNumberOfMemories() {
        return numberOfMemories;
    }

    public void setNumberOfMemories(Integer numberOfMemories) {
        this.numberOfMemories = numberOfMemories;
    }

    /**
     * Profile's first name.
     **/
    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }


    /**
     * Profile's last name.
     **/
    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }


    /**
     * Profile's birth date.
     **/
    public String getBirth() {
        return birth;
    }

    public void setBirth(String birth) {
        this.birth = birth;
    }


    /**
     * Profile's death date.
     **/
    public String getDeath() {
        return death;
    }

    public void setDeath(String death) {
        this.death = death;
    }

    /**
     * Profile's burial date.
     **/
    public String getBurial() {
        return burial;
    }

    public void setBurial(String burial) {
        this.burial = burial;
    }

    /**
     * Url to profile's thumb image
     **/
    public Image getImage() {
        return image;
    }

    public void setImage(Image image) {
        this.image = image;
    }

    @Override
    public String getAboutContent() {
        return this.aboutText;
    }

    @Override
    public void setAboutContent(String aboutContent) {
        this.aboutText = aboutContent;
    }

    public AccountLight getOwnerAccount() {
        return ownerAccount;
    }

    public void setOwnerAccount(AccountLight ownerAccount) {
        this.ownerAccount = ownerAccount;
    }

    @Override
    public List<GalleryItem> getProfileGallery() {
        return this.profileGalleryImages;
    }

    @Override
    public void setProfileGallery(List<GalleryItem> profileGalleryItems) {
        this.profileGalleryImages = profileGalleryItems;
    }

    @Override
    public boolean isFavorite() {
        return isFavorite;
    }

    @Override
    public void setIsFavorite(boolean isFavorite) {
        this.isFavorite = isFavorite;
    }

    @Override
    public boolean isAdmin() {
        return isAdmin;
    }

    @Override
    public void setIsAdmin(boolean isAdmin) {
        this.isAdmin = isAdmin;
    }

    @Override
    public boolean isOwned() {
        return isOwned;
    }

    @Override
    public void setIsOwned(boolean isOwned) {
        this.isOwned = isOwned;

    }

    @Override
    public boolean isInEditMode() {
        return isInEditMode;
    }

    @Override
    public void setIsInEditMode(boolean isInEditMode) {
        this.isInEditMode = isInEditMode;

    }

    @Override
    public boolean isHeaderImageModified() {
        return isHeaderImageModified;
    }

    @Override
    public void setHeaderImageModified(boolean isHeaderImageModified) {
        this.isHeaderImageModified =isHeaderImageModified;
    }

    public ProfileLocation getLocation() {
        return location;
    }

    public void setLocation(ProfileLocation location) {
        this.location = location;
    }

    @Override
    public ProfileAnniversary getProfileAnniversary() {
        if(this.profileAnniversary == null){
            this.profileAnniversary = new ProfileAnniversary();
        }
        return this.profileAnniversary;
    }

    @Override
    public void setProfileAnniversary(ProfileAnniversary profileAnniversary) {
        this.profileAnniversary = new ProfileAnniversary();
    }

    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append("class ProfileLight {\n");

        sb.append("  id: ").append(id).append("\n");
        sb.append("  firstName: ").append(firstName).append("\n");
        sb.append("  lastName: ").append(lastName).append("\n");
        sb.append("  birth: ").append(birth).append("\n");
        sb.append("  death: ").append(death).append("\n");
        sb.append("  burial: ").append(burial).append("\n");
        sb.append("  image: ").append(image).append("\n");
        sb.append("  aboutText: ").append(aboutText).append("\n");
        sb.append("  profileGalleryImages: ").append(profileGalleryImages).append("\n");
        sb.append("  numberOfMemories: ").append(numberOfMemories).append("\n");
        sb.append("  location: ").append(location).append("\n");
        sb.append("}\n");
        return sb.toString();
    }

    protected Profile(Parcel in) {
        id = in.readByte() == 0x00 ? null : in.readInt();
        firstName = in.readString();
        lastName = in.readString();
        birth = in.readString();
        death = in.readString();
        burial = in.readString();
        image = (Image) in.readValue(Image.class.getClassLoader());
        aboutText = in.readString();
        if (in.readByte() == 0x01) {
            profileGalleryImages = new ArrayList<GalleryItem>();
            in.readList(profileGalleryImages, GalleryItem.class.getClassLoader());
        } else {
            profileGalleryImages = null;
        }
        numberOfMemories = in.readByte() == 0x00 ? null : in.readInt();
        location = (ProfileLocation) in.readValue(ProfileLocation.class.getClassLoader());
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        if (id == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(id);
        }
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(birth);
        dest.writeString(death);
        dest.writeString(burial);
        dest.writeValue(image);
        dest.writeString(aboutText);
        if (profileGalleryImages == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(profileGalleryImages);
        }
        if (numberOfMemories == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeInt(numberOfMemories);
        }
        dest.writeValue(location);
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Profile> CREATOR = new Parcelable.Creator<Profile>() {
        @Override
        public Profile createFromParcel(Parcel in) {
            return new Profile(in);
        }

        @Override
        public Profile[] newArray(int size) {
            return new Profile[size];
        }
    };

    @Override
    public int compareTo(IProfile compareProfile) {
        // https://docs.oracle.com/javase/tutorial/collections/interfaces/order.html
        // The compareTo method compares the receiving object with the specified object
        // and returns a negative integer, 0, or a positive integer depending on
        // whether the receiving object is less than, equal to, or greater than the specified object.
        // If the specified object cannot be compared to the receiving object, the method throws a ClassCastException.

        final int BEFORE = -1;
        final int EQUAL = 0;
        final int AFTER = 1;

        String profileName = ProfileUtils.getProfileName(this).toUpperCase();
        String comparableName = ProfileUtils.getProfileName(compareProfile).toUpperCase();

        DateFormat formatter = new SimpleDateFormat("yyyy-MM-dd");
        int currentYear = Calendar.getInstance().get(Calendar.YEAR);
        int nextYear = currentYear + 1;
        Date currentDate = new Date();
        Date profileBirth = null;
        Date profileDeath = null;
        Date profileNextBirth = null;
        Date profileNextDeath = null;
        Date comparableBirth = null;
        Date comparableDeath = null;
        Date comparableNextBirth = null;
        Date comparableNextDeath = null;
        try {
            profileBirth = this.getBirth() != null ? formatter.parse(currentYear + this.getBirth().substring(4)) : null;
            profileDeath = this.getDeath() != null ? formatter.parse(currentYear + this.getDeath().substring(4)) : null;
            profileNextBirth = this.getBirth() != null ? formatter.parse(nextYear + this.getBirth().substring(4)) : null;
            profileNextDeath = this.getDeath() != null ? formatter.parse(nextYear + this.getDeath().substring(4)) : null;
            comparableBirth = compareProfile.getBirth() != null ? formatter.parse(currentYear + compareProfile.getBirth().substring(4)) : null;
            comparableDeath = compareProfile.getDeath() != null ? formatter.parse(currentYear + compareProfile.getDeath().substring(4)) : null;
            comparableNextBirth = compareProfile.getBirth() != null ? formatter.parse(nextYear + compareProfile.getBirth().substring(4)) : null;
            comparableNextDeath = compareProfile.getDeath() != null ? formatter.parse(nextYear + compareProfile.getDeath().substring(4)) : null;
        } catch (ParseException e) {
            e.printStackTrace();
        }


        long profileDaysUntilBirthAnniversary = profileBirth != null ? TimeUnit.DAYS.convert(profileBirth.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : TIME_UNAVAILABLE;
        profileDaysUntilBirthAnniversary = profileDaysUntilBirthAnniversary < 0 ? TimeUnit.DAYS.convert(profileNextBirth.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : profileDaysUntilBirthAnniversary;
        long profileDaysUntilDeathAnniversary = profileDeath != null ? TimeUnit.DAYS.convert(profileDeath.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : TIME_UNAVAILABLE;
        profileDaysUntilDeathAnniversary = profileDaysUntilDeathAnniversary < 0 ? TimeUnit.DAYS.convert(profileNextDeath.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : profileDaysUntilDeathAnniversary;

        long profileDaysUntilAnniversary;
        if(profileDaysUntilBirthAnniversary < profileDaysUntilDeathAnniversary){
            profileDaysUntilAnniversary = profileDaysUntilBirthAnniversary;
            if(profileDaysUntilAnniversary != TIME_UNAVAILABLE){
                getProfileAnniversary().setHasAnniversary(true);
                getProfileAnniversary().setHasBirthAnniversary(true);
            }
        }else{
            profileDaysUntilAnniversary = profileDaysUntilDeathAnniversary;
            if(profileDaysUntilAnniversary != TIME_UNAVAILABLE){
                getProfileAnniversary().setHasAnniversary(true);
                getProfileAnniversary().setHasBirthAnniversary(false);
            }
        }



        long comparableDaysUntilBirthAnniversary = comparableBirth != null ? TimeUnit.DAYS.convert(comparableBirth.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : TIME_UNAVAILABLE;
        comparableDaysUntilBirthAnniversary = comparableDaysUntilBirthAnniversary < 0 ? TimeUnit.DAYS.convert(comparableNextBirth.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : comparableDaysUntilBirthAnniversary;
        long comparableDaysUntilDeathAnniversary = comparableDeath != null ? TimeUnit.DAYS.convert(comparableDeath.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : TIME_UNAVAILABLE;
        comparableDaysUntilDeathAnniversary = comparableDaysUntilDeathAnniversary < 0 ? TimeUnit.DAYS.convert(comparableNextDeath.getTime() - currentDate.getTime(), TimeUnit.MILLISECONDS) : comparableDaysUntilDeathAnniversary;

        long comparableDaysUntilAnniversary;
        if(comparableDaysUntilBirthAnniversary < comparableDaysUntilDeathAnniversary){
            comparableDaysUntilAnniversary = comparableDaysUntilBirthAnniversary;
            if(comparableDaysUntilAnniversary != TIME_UNAVAILABLE){
                compareProfile.getProfileAnniversary().setHasAnniversary(true);
                compareProfile.getProfileAnniversary().setHasBirthAnniversary(true);
            }
        }else{
            comparableDaysUntilAnniversary = comparableDaysUntilDeathAnniversary;
            if(comparableDaysUntilAnniversary != TIME_UNAVAILABLE){
                compareProfile.getProfileAnniversary().setHasAnniversary(true);
                compareProfile.getProfileAnniversary().setHasBirthAnniversary(false);
            }
        }



        if(comparableDaysUntilAnniversary == TIME_UNAVAILABLE && profileDaysUntilAnniversary != TIME_UNAVAILABLE){
            return BEFORE;
        }else if(comparableDaysUntilAnniversary != TIME_UNAVAILABLE && profileDaysUntilAnniversary == TIME_UNAVAILABLE){
            return AFTER;
        }else if(comparableDaysUntilAnniversary == TIME_UNAVAILABLE && profileDaysUntilAnniversary == TIME_UNAVAILABLE){
            return profileName.compareTo(comparableName);
        }else if(profileDaysUntilAnniversary > comparableDaysUntilAnniversary){
            return AFTER; //receiving object before specified
        }else if(profileDaysUntilAnniversary < comparableDaysUntilAnniversary){
            return BEFORE;
        }else{
            return profileName.compareTo(comparableName);
        }
    }
}