package hr.eonideas.ecemetery.parkovi.rest.model;

import android.os.Parcel;
import android.os.Parcelable;

import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import hr.eonideas.ecemetery.parkovi.interfaces.IAccount;
import hr.eonideas.ecemetery.parkovi.interfaces.IAccountLocation;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;

/**
 * Created by Teo on 2.1.2016..
 */
public class Account implements Parcelable, IAccount {
    private static Account instance;

    private int id = -1;
    private String firstName;
    private String lastName;
    private String email;
    @SerializedName("image")
    private Image avatar;
    private String authToken = null;
    private List<Profile> ownedProfiles = new ArrayList<Profile>();
    private List<AccountLocation> ownedLocations = new ArrayList<AccountLocation>();
    private List<Profile> favoriteProfiles = new ArrayList<Profile>();
    private AccountMode accountMode;
    private LoginType loginType;

    public static Account getInstance(Account account){
        if (instance == null){
            instance = account == null ? new Account() : account;
        }else if(account != null){
            instance = account;
        }
        return instance;
    }

    public static Account getInstance(String firstName, String lastName){
        Account a = new Account();
        a.setFirstName(firstName);
        a.setLastName(lastName);
        return a;
    }

    public void resetAccount(){
        instance = new Account();
    }

    private Account(){}

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public Image getAvatar() {
        return avatar;
    }

    public void setAvatar(Image avatar) {
        this.avatar = avatar;
    }

    public String getAuthToken() {
        return authToken;
    }

    public void setAuthToken(String authToken) {
        this.authToken = authToken;
    }

    public List<Profile> getOwnedProfiles() {
        return ownedProfiles;
    }

    public void setOwnedProfiles(List<Profile> ownedProfiles) {
        this.ownedProfiles = ownedProfiles;
    }

    public List<AccountLocation> getOwnedLocations() {
        return ownedLocations;
    }

    public void setOwnedLocations(List<AccountLocation> ownedLocations) {
        this.ownedLocations = ownedLocations;
    }

    public List<Profile> getFavoriteProfiles() {
        return favoriteProfiles;
    }

    public void setFavoriteProfiles(List<Profile> favoriteProfiles) {
        this.favoriteProfiles = favoriteProfiles;
    }



    @Override
    public AccountMode inMode() {
        return this.accountMode;
    }

    @Override
    public void setMode(AccountMode accountMode) {
        this.accountMode = accountMode;
    }

    @Override
    public LoginType getLoginType() {
        return loginType;
    }

    @Override
    public void setLoginType(LoginType loginType) {
        this.loginType = loginType;
    }

    protected Account(Parcel in) {
        id = in.readInt();
        firstName = in.readString();
        lastName = in.readString();
        email = in.readString();
        avatar = (Image) in.readValue(Image.class.getClassLoader());
        if (in.readByte() == 0x01) {
            ownedProfiles = new ArrayList<Profile>();
            in.readList(ownedProfiles, Profile.class.getClassLoader());
        } else {
            ownedProfiles = null;
        }
        if (in.readByte() == 0x01) {
            ownedLocations = new ArrayList<AccountLocation>();
            in.readList(ownedLocations, AccountLocation.class.getClassLoader());
        } else {
            ownedLocations = null;
        }
        if (in.readByte() == 0x01) {
            favoriteProfiles = new ArrayList<Profile>();
            in.readList(favoriteProfiles, Profile.class.getClassLoader());
        } else {
            favoriteProfiles = null;
        }
    }

    @Override
    public int describeContents() {
        return 0;
    }

    @Override
    public void writeToParcel(Parcel dest, int flags) {
        dest.writeInt(id);
        dest.writeString(firstName);
        dest.writeString(lastName);
        dest.writeString(email);
        dest.writeValue(avatar);
        if (ownedProfiles == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ownedProfiles);
        }
        if (ownedLocations == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(ownedLocations);
        }
        if (favoriteProfiles == null) {
            dest.writeByte((byte) (0x00));
        } else {
            dest.writeByte((byte) (0x01));
            dest.writeList(favoriteProfiles);
        }
    }

    @SuppressWarnings("unused")
    public static final Parcelable.Creator<Account> CREATOR = new Parcelable.Creator<Account>() {
        @Override
        public Account createFromParcel(Parcel in) {
            return new Account(in);
        }

        @Override
        public Account[] newArray(int size) {
            return new Account[size];
        }
    };
}