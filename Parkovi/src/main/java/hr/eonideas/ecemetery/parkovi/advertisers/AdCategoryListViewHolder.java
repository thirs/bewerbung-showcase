package hr.eonideas.ecemetery.parkovi.advertisers;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hr.eonideas.ecemetery.parkovi.R;

/**
 * Created by thirs on 9.11.2015..
 */
public class AdCategoryListViewHolder {
    private int position;
    public LinearLayout rooElement;
    public ImageView adCategoryLogo;
    public TextView adCategoryTitle;
    public TextView adCategoryDescription;


    public AdCategoryListViewHolder(View itemView) {
        rooElement = (LinearLayout) itemView.findViewById(R.id.ad_category_item);
        adCategoryLogo = (ImageView) itemView.findViewById(R.id.ad_category_item_logo);
        adCategoryTitle = (TextView) itemView.findViewById(R.id.ad_category_item_title);
        adCategoryDescription = (TextView) itemView.findViewById(R.id.ad_category_item_description);
    }
}
