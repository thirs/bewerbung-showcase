package hr.eonideas.ecemetery.parkovi.rest.intefaces;

import retrofit.Response;

public interface IOnGetResponseListener {
	public void onGetResponse(Response response, int requestId);
}
