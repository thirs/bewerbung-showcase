package hr.eonideas.ecemetery.parkovi.adapters;

import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;

import com.nostra13.universalimageloader.core.ImageLoader;

import java.util.List;

import hr.eonideas.ecemetery.commons.interfaces.ILazyLoad;
import hr.eonideas.ecemetery.commons.utils.ProfileUtils;
import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.interfaces.IProfile;
import hr.eonideas.ecemetery.parkovi.profile.ProfileListViewHolder;
import hr.eonideas.ecemetery.parkovi.rest.model.Profile;

/**
 * Created by thirs on 7.11.2015..
 */

public class ProfileListAdapter extends ArrayAdapter<Profile> {
    private Context context;
    private List<Profile> profiles;
    private ImageLoader imageLoader;
    private int layoutResourceId;

    private ILazyLoad lazyLoadListener;
    Integer totalCount;

    public ProfileListAdapter(Activity activity, List<Profile> profiles) {
        super(activity, -1, profiles);
        this.context = activity;
        this.profiles = profiles;
        this.imageLoader = ImageLoader.getInstance();
        this.layoutResourceId = R.layout.list_item_profile;
    }

    public void setTotalCount(Integer totalCount) {
        this.totalCount = totalCount;
    }

    public void setLazyLoadListener(ILazyLoad lazyLoadListener) {
        this.lazyLoadListener = lazyLoadListener;
    }

    private void wakeUpLoading(Integer offset){
        lazyLoadListener.updateLazyList(offset);
    }


    public void updateList(List<Profile> profiles){
        if(profiles != null){
            this.profiles.addAll(profiles);
        }else{
            this.profiles = null;
        }

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        if(position == profiles.size()-1 && totalCount > profiles.size() && lazyLoadListener != null) {
            wakeUpLoading(position+1);
        }

        ProfileListViewHolder viewHolder;
        if (convertView == null) {
            convertView = LayoutInflater.from(context).inflate(layoutResourceId, null);
            viewHolder = new ProfileListViewHolder(convertView);
            convertView.setTag(viewHolder);
        } else {
            viewHolder = (ProfileListViewHolder) convertView.getTag();
        }
        IProfile profile = profiles.get(position);
        if(profile.getImage() != null) {
            viewHolder.profileAvatar.setVisibility(View.VISIBLE);
            imageLoader.displayImage(profile.getImage().getFullUrl(), viewHolder.profileAvatar);
        }
        else{
            viewHolder.profileAvatar.setImageResource(R.drawable.placeholder_image);
        }

        viewHolder.profileName.setText(ProfileUtils.getProfileName(profile));
        viewHolder.profilePeriod.setText(ProfileUtils.getLifePeriod(profile,context));

        viewHolder.authorSection.setVisibility(View.INVISIBLE);
        return convertView;
    }

}