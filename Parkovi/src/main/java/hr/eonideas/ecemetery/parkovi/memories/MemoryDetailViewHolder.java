package hr.eonideas.ecemetery.parkovi.memories;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;

/**
 * Created by thirs on 9.11.2015..
 */
public class MemoryDetailViewHolder extends RecyclerView.ViewHolder implements View.OnClickListener {
    public LinearLayout rooElement;
    public LinearLayout authorSection;
    public ImageView authorAvatar;
    public TextView authorName;
    public ImageView memoryImage;
    public TextView memoryText;
    public ImageView editButton;
    private MemoryDetailViewHolder.IMemoryDetailViewHolderClicks listener;


    public MemoryDetailViewHolder(View itemView, MemoryDetailViewHolder.IMemoryDetailViewHolderClicks listener) {
        super(itemView);
        rooElement = (LinearLayout) itemView.findViewById(R.id.memory_item);
        authorSection = (LinearLayout) itemView.findViewById(R.id.memory_item_author_section);
        authorAvatar = (ImageView) itemView.findViewById(R.id.memory_item_author_avatar);
        authorName = (TextView) itemView.findViewById(R.id.memory_item_author_name);
        memoryImage = (ImageView) itemView.findViewById(R.id.memory_item_image);
        memoryText = (TextView) itemView.findViewById(R.id.memory_item_text);
        editButton = (ImageView) itemView.findViewById(R.id.memory_item_edit);
        editButton.setOnClickListener(this);
        this.listener = listener;
    }

    @Override
    public void onClick(View v) {
        if(listener != null)
            listener.onMemorySelected(getAdapterPosition());
    }

    public static interface IMemoryDetailViewHolderClicks {
        public void onMemorySelected(int position);
    }
}
