package hr.eonideas.ecemetery.parkovi.rest.intefaces;

import retrofit.Response;

public interface IOnDeleteResponseListener {
	public void onDeleteResponse(Response response, int requestId);
}
