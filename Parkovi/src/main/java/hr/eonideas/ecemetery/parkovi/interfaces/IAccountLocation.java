package hr.eonideas.ecemetery.parkovi.interfaces;

import android.os.Parcelable;

import java.math.BigDecimal;
import java.util.List;
import hr.eonideas.ecemetery.parkovi.rest.model.GalleryItem;
import hr.eonideas.ecemetery.parkovi.rest.model.Image;

/**
 * Created by Teo on 27.5.2016..
 */
public interface IAccountLocation {
        public Integer getId();

        public void setId(Integer id);

        public BigDecimal getLat();

        public void setLat(BigDecimal lat);

        public BigDecimal getLon();

        public void setLon(BigDecimal lon);

        public Image getImage();

        public void setImage(Image image);

        public List<GalleryItem> getProfileGalleryImages();

        public void setProfileGalleryImages(List<GalleryItem> profileGalleryImages);

        public Integer getAccountId();

        public void setAccountId(Integer accountId);

        public String getLocationFamilyNames();

        public void setLocationFamilyNames(String locationFamilyNames);

        public List<Integer> getProfiles();

        public void setProfiles(List<Integer> profiles);
}
