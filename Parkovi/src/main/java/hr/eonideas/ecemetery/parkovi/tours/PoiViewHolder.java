package hr.eonideas.ecemetery.parkovi.tours;

import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import hr.eonideas.ecemetery.parkovi.R;
import hr.eonideas.ecemetery.parkovi.adapters.PoiListAdapter;

/**
 * Created by thirs on 9.11.2015..
 */
public class PoiViewHolder implements View.OnClickListener {
    private int position;
    public LinearLayout rooElement;
    public LinearLayout poiDistanceInfoContainer;
    public ImageView poiAvatar;
    public TextView poiTitle;
    public TextView poiDescription;
    public TextView poiDistance;
    private PoiListAdapter.OnItemClickListener mItemClickListener;

    public PoiViewHolder(View itemView, PoiListAdapter.OnItemClickListener mItemClickListener, int position) {
        this.mItemClickListener = mItemClickListener;
        rooElement = (LinearLayout) itemView.findViewById(R.id.tour_detail_poi_item);
        poiAvatar = (ImageView) itemView.findViewById(R.id.tour_detail_poi_item_avatar);
        poiTitle = (TextView) itemView.findViewById(R.id.tour_detail_poi_item_title);
        poiDescription = (TextView) itemView.findViewById(R.id.tour_detail_poi_item_short_description);
        poiDistance = (TextView) itemView.findViewById(R.id.tour_detail_poi_item_tour_length);
        poiDistanceInfoContainer = (LinearLayout) itemView.findViewById(R.id.tour_detail_poi_item_distance_section);
        this.position = position;
        itemView.setOnClickListener(this);
    }

    @Override
    public void onClick(View v) {
        if(mItemClickListener != null){
            mItemClickListener.onItemClick(v, position);
        }
    }
    
}
