package hr.eonideas.ecemetery.remoting;

/**
 * Created by thirs on 19.10.2015..
 */

public class Status {
    public static final int S_NOT_SIMULATED = -2;
    public static final int S_UNKNOWN = -1;
    public static final int S_OK = 0;
    public static final int S_NO_SUCH_TOKEN = 5;
    public static final int S_LOCKED_2 = 7;
    public static final int S_SXS_ERROR = 8;
    public static final int S_WRONG_PIN = 9;
    public static final int S_MAXPIN = 32;
    public static final int S_INCORRECT_PIN = 33;
    public static final int S_LOCKED = 39;
    public static final int S_SHOW_INFO_MSG_AND_RETURN = 111;
    public static final int S_SHOW_WARN_MSG_AND_RETURN = 112;
    public static final int S_SHOW_WARN_MSG_AND_CONTINUE = 113;
    public static final int S_FILE_NOT_FOUND = 500;
    public static final int S_C_CANCELED = 999;
    public static final int S_C_COMMUNICATION_ERROR = 1000;
    public static final int S_C_CERTIFICATE_ERROR = 1002;
    public static final int S_C_CONTENT_ERROR = 1003;
    public static final int S_C_BASE_PROTOCOL_ERROR = 1004;
    public static final int S_C_APP_PROTOCOL_ERROR = 1005;
    public static final int S_C_SEQUENCE_ERROR = 1006;
    public static final int S_C_CONFIG_ERROR = 1007;
    public static final int S_C_INVALID_ACTIVATION_CODE = 1008;
    public static final int S_C_SECURITY_DISABLED_TMP = 2000;
    public static final int S_C_SECURITY_ERR_OLD_SRV_KEY = 2001;
    public static final int S_C_SECURITY_EMPTY_REQUEST = 2002;
    public static final int S_C_MEMORY_ERROR = 1010;
    public static final int S_C_MEMORY_ERROR_WRITE = 1011;
    public static final int S_C_MEMORY_ERROR_READ = 1012;
    public static final int S_C_NO_SUCH_ALGORITHM = 1013;
    public static final int S_C_INVALID_KEY_SPEC = 1014;
    public static final int S_INTERNAL_SERVER_ERROR_30 = 30;
    public static final int S_INTERNAL_SERVER_ERROR_99 = 99;
    public static final int S_INTERNAL_SERVER_ERROR_153 = 153;

    public Status() {
    }

    public static final boolean isReturnOk(int status) {
        return status == 0 || status == 113;
    }

    public static final boolean isInvalidAC(int status) {
        return status == 33 || status == 500;
    }

    public static final boolean isTokenLocked(int status) {
        return status == 32 || status == 5 || status == 39 || status == 7;
    }
}
